#ifndef EIRE_LINEARITY_H
#define EIRE_LINEARITY_H

#include <iostream> 
#include <math.h>
#include <TFitResult.h>
#include <TFitResultPtr.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include <TH1.h>
#include <TCanvas.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TLine.h>
#include <cstdlib>
#include <stdio.h>
#include <iomanip>
using namespace std;

namespace eire{
  class Linearity{
  public:
    Linearity(string name);
    ~Linearity();
    void AddPointF(int index, double f_gen, double f_obs, double f_obs_err, double p_av, double p_averr, double p_rms, double p_rmserr); 
    void AddPointNtt(int index, double f_gen, double f_obs, double f_obs_err, double p_av, double p_averr, double p_rms, double p_rmserr); 
    void AddPointNbkg(int index, double f_gen, double f_obs, double f_obs_err, double p_av, double p_averr, double p_rms, double p_rmserr); 
    void Plot();
    
  private:
    double x[5];
    double Ntt_in;
    double Nbkg_in;
    double y_f[5];
    double y_ferr[5];
    double y_Ntt[5];
    double y_Ntterr[5];
    double y_Nbkg[5];
    double y_Nbkgerr[5];
    double P_fav[5];
    double P_faverr[5];
    double P_frms[5];
    double P_frmserr[5];
    double P_Nttav[5];
    double P_Nttaverr[5];
    double P_Nttrms[5];
    double P_Nttrmserr[5];
    double P_Nbkgav[5];
    double P_Nbkgaverr[5];
    double P_Nbkgrms[5];
    double P_Nbkgrmserr[5];
    string Name;
  };
}

#endif
