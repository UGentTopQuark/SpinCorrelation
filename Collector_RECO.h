#ifndef EIRE_COLLECTOR_RECO_H
#define EIRE_COLLECTOR_RECO_H

#include <iostream> 
#include <math.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include <TTree.h>
#include <TLeaf.h>
#include <TFile.h>
#include <cstdlib>
#include <stdio.h>
#include <iomanip>
#include <TH1F.h>
#include <TCanvas.h>
using namespace std;

namespace eire{
  class Collector_RECO{
  public:
    Collector_RECO(bool HF=false, float Chi=99999, float M_low=0, float M_high=99999, float Like_cor_cut_up = 999999, float Like_cor_cut_down = 0,float Like_uncor_cut_up = 999999, float Like_uncor_cut_down = 0, int PU = 0);
    ~Collector_RECO();
    std::vector<std::vector<float> > CollectLikelihoods(std::string name); 
    std::vector<std::vector<float> > CollectEventWeights();
    void SetUpWeights(std::vector<std::string> s);
    
  private:
    bool use_HitFit;
    float chicut;
    float m_low;
    float m_high;
    float Like_cut_up;
    float Like_cut_down;
    float Like_cutuncor_up;
    float Like_cutuncor_down;
    std::vector<std::vector<float> > weights_vector;
    map<int, int> w_setup;
    int PU_var;
  };
}

#endif
