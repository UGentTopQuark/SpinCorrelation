#include "Systematics.h"

//constructor
eire::Systematics::Systematics()
{
  Cor_sysup = new std::vector<TH1F>();
  Uncor_sysup = new std::vector<TH1F>();
  Cor_sysdown = new std::vector<TH1F>();
  Uncor_sysdown = new std::vector<TH1F>();

  Dataset_size = 38269; //19361; //19286; //pos //38269; //15344;//4 jets //22925; //5 jets //combined 38269;

  outfile = new TFile("SystematicVariation_stat_OneSample_combined_nominal.root","RECREATE");
  tree = new TTree("SystematicVariation","SystematicVariation");

  gsl = new ROOT::Math::GSLRandomEngine();
  gsl->Initialize();
  gsl->SetSeed(0);

  rng = new TRandom1();
  
}

//destructor
eire::Systematics::~Systematics()
{
  delete Cor_sysup;
  delete Uncor_sysup;
  delete Cor_sysdown;
  delete Uncor_sysdown;
  delete nominal_cor;
  delete nominal_uncor;
  
  delete gsl;
  gsl = NULL;
  delete rng;
  rng = NULL;

  if(outfile){
    outfile->cd();
    tree->Write();
  }

  if(tree){
    delete tree;
    tree = NULL;
  }
}

void eire::Systematics::Book_branches()
{
  tree->Branch("xc_stat",&xc_stat,"xc_stat/D");
  tree->Branch("xc_JER",&xc_JER,"xc_JER/D");
  tree->Branch("xc_JES",&xc_JES,"xc_JES/D");
  tree->Branch("xc_trigger",&xc_trigger,"xc_trigger/D");
  tree->Branch("xc_leptonid",&xc_leptonid,"xc_leptonid/D");
  tree->Branch("xc_btag",&xc_btag,"xc_btag/D");
  tree->Branch("xc_mistag",&xc_mistag,"xc_mistag/D");
  tree->Branch("xc_PU",&xc_PU,"xc_PU/D");
  tree->Branch("xc_TopPt",&xc_TopPt,"xc_TopPt/D");
  tree->Branch("xc_mass",&xc_mass,"xc_mass/D");
  tree->Branch("xc_Q2",&xc_Q2,"xc_Q2/D");
  tree->Branch("xu_stat",&xu_stat,"xu_stat/D");
  tree->Branch("xu_JER",&xu_JER,"xu_JER/D");
  tree->Branch("xu_JES",&xu_JES,"xu_JES/D");
  tree->Branch("xu_trigger",&xu_trigger,"xu_trigger/D");
  tree->Branch("xu_leptonid",&xu_leptonid,"xu_leptonid/D");
  tree->Branch("xu_btag",&xu_btag,"xu_btag/D");
  tree->Branch("xu_mistag",&xu_mistag,"xu_mistag/D");
  tree->Branch("xu_PU",&xu_PU,"xu_PU/D");
  tree->Branch("xu_TopPt",&xu_TopPt,"xu_TopPt/D");
  tree->Branch("xu_mass",&xu_mass,"xu_mass/D");
  tree->Branch("xu_Q2",&xu_Q2,"xu_Q2/D");
  tree->Branch("Cor_Sample_likelihood",&Cor_Sample_likelihood,"Cor_Sample_likelihood/D");
  tree->Branch("Uncor_Sample_likelihood",&Uncor_Sample_likelihood,"Uncor_Sample_likelihood/D");
}

void eire::Systematics::RunSystematics()
{
  Book_branches();
  SetTemplates();

  TH1F *Cor_fluctuated;
  TH1F *Uncor_fluctuated;

  float Cor_sample_likelihood = 0;
  float Uncor_sample_likelihood = 0;

  TH1F *Sample_cor = new TH1F("Sample_Cor","sample likelihood;-2ln#lambda_{Sample};entries",250,8000,11000);//3500,6500);//8000,11000);
  TH1F *Sample_uncor = new TH1F("Sample_Uncor","sample likelihood;-2ln#lambda_{Sample};entries",250,8000,11000);//3500,6500);//8000,11000);
  
  //do systematic fluctuations
  for(int k = 0; k < 10000; k++){
    std::cout<<"fluc k = "<<k<<std::endl;
    stringstream naam_cor_syst;
    naam_cor_syst<<"Cor_"<<k;
    stringstream naam_uncor_syst;
    naam_uncor_syst<<"Uncor_"<<k;
    Cor_fluctuated = ReDrawTemplate(nominal_cor,Cor_sysup,Cor_sysdown,naam_cor_syst.str());
    Uncor_fluctuated = ReDrawTemplate(nominal_uncor,Uncor_sysup,Uncor_sysdown,naam_uncor_syst.str());
    //for systematic variation at this fluctuation
    for(int i = 0; i < 1; i++){
      for(int x = 0; x < Dataset_size; x++){
	Cor_sample_likelihood += Cor_fluctuated->GetRandom();
	Uncor_sample_likelihood += Uncor_fluctuated->GetRandom();
      }
      //fill in the tree variables
      Cor_Sample_likelihood = Cor_sample_likelihood;
      Uncor_Sample_likelihood = Uncor_sample_likelihood;
      Sample_cor->Fill(Cor_sample_likelihood);
      Sample_uncor->Fill(Uncor_sample_likelihood);
      tree->Fill();
      Cor_sample_likelihood = 0;
      Uncor_sample_likelihood = 0;
    }
  }

  TCanvas *c_fluc = new TCanvas();
  c_fluc->cd();
  Sample_cor->SetLineColor(kBlue);
  Sample_uncor->SetLineColor(kRed);
  Sample_cor->Draw();
  Sample_uncor->Draw("sames");
  c_fluc->Modified();
  c_fluc->Update();
  c_fluc->SaveAs("SystematicFluctuation_Simultaneously_stat_OneSample_combined_nominal.C");

  delete c_fluc;
  delete Sample_cor;
  delete Sample_uncor;
  delete Cor_fluctuated;
  delete Uncor_fluctuated;

}

void eire::Systematics::SetTemplates()
{
  TFile *file_nominal = new TFile("/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/NominalTemplates_Muon_combined.root","OPEN");
  std::cout<<"getting nominal template"<<std::endl;
  nominal_cor = (TH1F*) (file_nominal->GetDirectory("Templates")->Get("Mayor_cor_temp"))->Clone();
  nominal_uncor = (TH1F*) (file_nominal->GetDirectory("Templates")->Get("Mayor_uncor_temp"))->Clone();
  
  //for statistics -> push back the nominal template
  Cor_sysup->push_back(*nominal_cor);
  Cor_sysdown->push_back(*nominal_cor);
  Uncor_sysup->push_back(*nominal_uncor);
  Uncor_sysdown->push_back(*nominal_uncor);

  std::cout<<"getting JER template"<<std::endl;
  string file_JER = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_JER_VarW.root";
  GetHistos(file_JER,"8TeV_JERUp_MCatNLO","8TeV_JERDown_MCatNLO","JER");
  std::cout<<"getting JES template"<<std::endl;
  string file_JES = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_pos_Syst_JES_noTF_VarW.root";
  GetHistos(file_JES,"8TeV_noTF_JESUp_MCatNLO","8TeV_noTF_JESDown_MCatNLO","JES");
  std::cout<<"getting trigger template"<<std::endl;
  string file_trigger = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_trigger_VarW.root";
  GetHistos(file_trigger,"8TeV_12k_MCatNLO","8TeV_12k_MCatNLO","trigger");
  std::cout<<"getting lepton id template"<<std::endl;
  string file_leptonid = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_leptonid_VarW.root";
  GetHistos(file_leptonid,"8TeV_12k_MCatNLO","8TeV_12k_MCatNLO","leptonid");
  std::cout<<"getting btag template"<<std::endl;
  string file_btag = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_btag_VarW.root";
  GetHistos(file_btag,"8TeV_12k_MCatNLO","8TeV_12k_MCatNLO","btag");
  std::cout<<"getting mistag template"<<std::endl;
  string file_mistag = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_mistag_VarW.root";
  GetHistos(file_mistag,"8TeV_12k_MCatNLO","8TeV_12k_MCatNLO","mistag");
  std::cout<<"getting PU template"<<std::endl;
  string file_PU = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_PU_VarW.root";
  GetHistos(file_PU,"8TeV_12k_MCatNLO","8TeV_12k_MCatNLO","PU");
  std::cout<<"getting Top pt template"<<std::endl;
  string file_TopPt = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_TopPt_VarW.root";
  GetHistos(file_TopPt,"8TeV_12k_MCatNLO","8TeV_12k_MCatNLO","TopPt");
  std::cout<<"getting mass template"<<std::endl;
  string file_mass = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_mass_constr_VarW.root";
  GetHistos(file_mass,"8TeV_mass_175_5_MCatNLO","8TeV_mass_169_5_MCatNLO","mass");
  std::cout<<"getting Q2 template"<<std::endl;
  string file_Q2 = "/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/SystTemplates_Mu_combined_Syst_Q2_VarW.root";
  GetHistos(file_Q2,"8TeV_Q2Up_MCatNLO","8TeV_Q2Down_MCatNLO","Q2");
}

void eire::Systematics::GetHistos(string filename, string systup, string systdown, string syst)
{
  TFile *file = new TFile((filename).c_str(),"OPEN");
  TH1F *hist_cor_systup = (TH1F*) (file->GetDirectory("Templates_systup")->Get(("temp_Correlated_"+systup+"_Cor_mu_pos").c_str()))->Clone(("Temp_Correlated_up_"+syst).c_str());
  TH1F *hist_uncor_systup = (TH1F*) (file->GetDirectory("Templates_systup")->Get(("temp_UnCorrelated_"+systup+"_Uncor_mu_pos").c_str()))->Clone(("Temp_UnCorrelated_up_"+syst).c_str());
  TH1F *hist_cor_systdown = (TH1F*) (file->GetDirectory("Templates_systdown")->Get(("temp_Correlated_"+systdown+"_Cor_mu_pos").c_str()))->Clone(("Temp_Correlated_down_"+syst).c_str());
  TH1F *hist_uncor_systdown = (TH1F*) (file->GetDirectory("Templates_systdown")->Get(("temp_UnCorrelated_"+systdown+"_Uncor_mu_pos").c_str()))->Clone(("Temp_UnCorrelated_down_"+syst).c_str());

  Cor_sysup->push_back(*hist_cor_systup);
  Cor_sysdown->push_back(*hist_cor_systdown);
  Uncor_sysup->push_back(*hist_uncor_systup);
  Uncor_sysdown->push_back(*hist_uncor_systdown);
}

TH1F* eire::Systematics::ReDrawTemplate(TH1F *nominal, std::vector<TH1F> *Syst_up, std::vector<TH1F> *Syst_down, string name)
{
  TH1F *hist_new = (TH1F*) nominal->Clone(name.c_str());
  //  TF1 *gauss = new TF1("gauss","gaus(0)");
  //gauss->SetParameter(0,1);
  //gauss->SetParameter(1,0);
  //gauss->SetParameter(2,1);
  float x_gen[11] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  float x_used[11] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
  double r_1 = 0.;
  
  //run over each systematic
  for(unsigned int i = 0; i < Syst_up->size(); i++){
    //systematic fluctation, i == 0 will be overwritten since is statistical fluctuation
    //r_1 = rng->Gaus(0,1);  
    //      std::cout<<"i = "<<i<<" x = "<<x[i]<<" r_1 = "<<r_1<<std::endl;
    //x[i] = r_1/fabs(r_1)*gsl->LogNormal(0,1); 
    x_gen[i] = rng->Gaus(0,1);
    //    std::cout<<"x_gen = "<<x_gen[i]<<std::endl;
  }
  
  if(Syst_up->size() != 0){
    if(name.find("Cor") != std::string::npos){
      //fill random numbers in struct
      xc_stat = 0.0;//x_gen[0];
      xc_JER = 0.0;//x_gen[1];
      xc_JES = 0.0;//x_gen[2];
      xc_trigger = 0.0;//x_gen[3];
      xc_leptonid = 0.0;//x_gen[4];
      xc_btag = 0.0;//x_gen[5];
      xc_mistag = 0.0;//x_gen[6];
      xc_PU = 0.0;//x_gen[7];
      xc_TopPt = 0.0;//x_gen[8];
      xc_mass = 0.0;//x_gen[9];
      xc_Q2 = 0.0;//x_gen[10];
      x_used[0] = xc_stat;
      x_used[1] = xc_JER;
      x_used[2] = xc_JES;
      x_used[3] = xc_trigger;
      x_used[4] = xc_leptonid;
      x_used[5] = xc_btag;
      x_used[6] = xc_mistag;
      x_used[7] = xc_PU;
      x_used[8] = xc_TopPt;
      x_used[9] = xc_mass;
      x_used[10] = xc_Q2;
    }else{
      xu_stat = 0.0;//x_gen[0];
      xu_JER = 0.0;//x_gen[1];
      xu_JES = 0.0;//x_gen[2];
      xu_trigger = 0.0;//x_gen[3];
      xu_leptonid = 0.0;//x_gen[4];
      xu_btag = 0.0;//x_gen[5];
      xu_mistag = 0.0;//x_gen[6];
      xu_PU = 0.0;//x_gen[7];
      xu_TopPt = 0.0;//x_gen[8];
      xu_mass = 0.0;//x_gen[9];
      xu_Q2 = 0.0;//x_gen[10];
      x_used[0] = xu_stat;
      x_used[1] = xu_JER;
      x_used[2] = xu_JES;
      x_used[3] = xu_trigger;
      x_used[4] = xu_leptonid;
      x_used[5] = xu_btag;
      x_used[6] = xu_mistag;
      x_used[7] = xu_PU;
      x_used[8] = xu_TopPt;
      x_used[9] = xu_mass;
      x_used[10] = xu_Q2;
    }
  }
  //run over each bin
  double dev = 0.;
  for(int z = 0; z <= nominal->GetNbinsX(); z++){
    for(unsigned int i = 0; i < Syst_up->size(); i++){
      //take syst up variation
      if(x_used[i] > 0){
	//for mass divide by 3, not used for the moment
	if(i == 9){
	  dev += fabs(x_used[i])*((*Syst_up)[i].GetBinContent(z+1) - nominal->GetBinContent(z+1))/3.0;
	}else if(i == 0){
	  //we want to keep the sign for statistics + stat error bars -> draw x for each bin
	  dev += rng->Gaus(0,1)*nominal->GetBinError(z+1);
	}else{
	  //for all other systematics
	  dev += fabs(x_used[i])*((*Syst_up)[i].GetBinContent(z+1) - nominal->GetBinContent(z+1));
	}
      }
      //take syst down variation
      else{
	//for mass divide by 3, not used for the moment
	if(i == 9){
	  dev += fabs(x_used[i])*((*Syst_down)[i].GetBinContent(z+1) - nominal->GetBinContent(z+1))/3.0;
	}else if(i == 0){
	  //we want to keep the sign for statistics + stat error bars -> draw x for each bin
	  dev += rng->Gaus(0,1)*nominal->GetBinError(z+1);
	}else{
	  //for all other systematics
	  dev += fabs(x_used[i])*((*Syst_down)[i].GetBinContent(z+1) - nominal->GetBinContent(z+1));
	}
	
      }
    }
    double entries = nominal->GetBinContent(z+1) + dev;
    if(entries < 0){entries = 0;}
    hist_new->SetBinContent(z+1,entries);
    dev = 0;
  }

  //  delete gauss;
  return hist_new;
}


