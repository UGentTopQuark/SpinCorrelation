//
//Steering Code for the spin correlations measurement
//author: Kelly Beernaert
//
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "ConfigReader/ConfigReader.h"
#include "Collector.h"
#include "Collector_RECO.h"
#include "PEBuilder.h"
#include "Linearity.h"
#include "TFile.h"
#include "HypTesting.h"

int main(int argc, char **argv)
{
  if(argc < 2){
    std::cerr << "usage: ./SpinCor <config file>" << std::endl;
    exit(1);
  }
  
  // read config file name as parameter from command line
  std::string filename(argv[1]);
  
  eire::ConfigReader *config_reader = new eire::ConfigReader();
  config_reader->read_config_from_file(filename);

  //read config file and get all parameters necessary
  //get the samples that will make your correlated and uncorrelated template, if more than one sample makes a template, then provide the relative weights
  //this assumes that the same number of correlated and uncorrelated samples will be given
  std::vector< std::string > Template_cor = config_reader->get_vec_var("Template_cor","global",true);
  std::vector< std::string > Template_uncor = config_reader->get_vec_var("Template_uncor","global",true);
  std::vector< std::string > Sig_weights = config_reader->get_vec_var("Sig_weights","global",false);
  float cor_errors = atof(config_reader->get_var("cor_errors","global",false).c_str());
  float uncor_errors = atof(config_reader->get_var("uncor_errors","global",false).c_str());
  //only fill if you want a background template or background mixture
  //if more than one background sample is added, give the relative contribution
  std::vector< std::string > Template_background = config_reader->get_vec_var("Template_background","global",false);
  std::vector< std::string > Background_weights = config_reader->get_vec_var("Background_weights","global",false);
  float background_errors = atof(config_reader->get_var("Background_errors","global",false).c_str());
  //only fill if you want your "Data" to be independent of your MC_template or for real data
  std::vector< std::string > Dataset = config_reader->get_vec_var("Dataset","global",false);
  std::vector< std::string > Data_weights = config_reader->get_vec_var("Data_weights","global",false);
  float data_errors = atof(config_reader->get_var("Data_errors","global",false).c_str());
  //Which event weights (lepton ID, trigger, ...) do you want applied to the Templates?
  std::vector< std::string > event_weights = config_reader->get_vec_var("event_weights", "global",false);

  //Name for the output files
  std::string Name = config_reader->get_var("output_name","global",true);

  //verbose will turn on output for RooFit and store the fit.pdfs
  bool verbose = false;
  verbose = config_reader->get_bool_var("verbose","global",false);

  //when not running on Data, which fraction of background do you want to mix in?
  float Wfrac = atof(config_reader->get_var("Wfrac","global",false).c_str());

  //only fill for MC
  float mixing = atof(config_reader->get_var("mixing","global",false).c_str());

  //if -1 take full data in one pseudo-experiment
  int nEvPE = atoi(config_reader->get_var("Ev_PE","global",true).c_str());

  //number of bins and range used in the InputTemplate Distribution
  int nbins = atoi(config_reader->get_var("nbins","global",true).c_str());
  float lowRange = atof(config_reader->get_var("lowRange","global",true).c_str());
  float highRange = atof(config_reader->get_var("highRange","global",true).c_str());
  //do you want to make cuts on the HitFit output tree? If so, then switch on HitFit
  bool with_HitFit = false;
  with_HitFit = config_reader->get_bool_var("with_HitFit","global",false);
  float Chicut = atof(config_reader->get_var("Chicut","global",false).c_str());
  if(Chicut == 0){Chicut = 99999;}
  float MassLow = atof(config_reader->get_var("MassLow","global",false).c_str());
  if(MassLow == 0){MassLow = -10;}
  float MassHigh = atof(config_reader->get_var("MassHigh","global",false).c_str());
  if(MassHigh == 0){MassHigh = 99999;}

  //add extra cuts to the absolute likelihood distributions
  float Like_cut_up = atof(config_reader->get_var("Like_cor_cut_up","global",false).c_str());
    if(Like_cut_up == 0){Like_cut_up = 999999;}
  float Like_cut_down = atof(config_reader->get_var("Like_cor_cut_down","global",false).c_str());
  float Like_cutuncor_up = atof(config_reader->get_var("Like_uncor_cut_up","global",false).c_str());
  if(Like_cutuncor_up == 0){Like_cutuncor_up = 999999;}
  float Like_cutuncor_down = atof(config_reader->get_var("Like_uncor_cut_down","global",false).c_str());

  //if you want to run the complete pseudo-experiments at once! Only makes sense in MC
  bool RunFullExp = config_reader->get_bool_var("RunFullExp","global",false);

  //naming
  stringstream name;
  name<<Name<<"_b"<<nbins<<lowRange<<"-"<<highRange<<"_"<<nEvPE<<"evPE";
  
  //all parameters have been read in, proceed to processing



  //get the likelihoods of the chosen dataset of the first signal templates
  std::vector<std::vector<float> > L_cor;
  std::vector<std::vector<float> > L_uncor;
  std::vector<std::vector<float> > Cor_weights;
  std::vector<std::vector<float> > Uncor_weights;
  eire::Collector_RECO *Cor_collector_RECO = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
  eire::Collector_RECO *Uncor_collector_RECO = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
  
  L_cor = Cor_collector_RECO->CollectLikelihoods((Template_cor)[0]);
  Cor_weights = Cor_collector_RECO->CollectEventWeights();
  L_uncor = Uncor_collector_RECO->CollectLikelihoods((Template_uncor)[0]);
  Uncor_weights = Uncor_collector_RECO->CollectEventWeights();
  
  eire::HypTesting *hypothesis_tester = new eire::HypTesting(Name, L_cor,L_uncor);
  //  hypothesis_tester->DrawSeparation();

  //check if using the background (meaning having a background template), when providing a background sample, but use_back = false, then some background is mixed in with the pseudo-data, but only signal templates are used
  bool UseBack = false;
  UseBack = config_reader->get_bool_var("use_back","global",true);
  std::vector<std::vector<float> > L_Background;
  std::vector<std::vector<float> > Background_event_weights;
  eire::Collector_RECO *Background_collector;
  if(Template_background.size() > 0){
    Background_collector = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    L_Background = Background_collector->CollectLikelihoods((Template_background)[0]);
    Background_event_weights = Background_collector->CollectEventWeights();
  }

  //split into the Pseudo-Experiments with proper mixing
  eire::PEBuilder *builder;
  builder = new eire::PEBuilder(L_cor, L_uncor, L_Background, Wfrac, UseBack, name.str(), nbins,nEvPE,lowRange,highRange,verbose);
  builder->SetUpWeights(event_weights);
  builder->SetEventWeights(Cor_weights,Uncor_weights,Background_event_weights,cor_errors,uncor_errors,background_errors);
  //  builder->SetProperties(nbins,nEvPE,lowRange,highRange,verbose);

  //add extra samples if these were specified
  for(unsigned int j = 1; j < Template_cor.size(); j++){
    std::vector<std::vector<float> > L_extra_cor;
    eire::Collector_RECO *Collector_extra_Cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    L_extra_cor = Collector_extra_Cor->CollectLikelihoods((Template_cor)[j]);
    builder->AddCorTemplate(L_extra_cor,atof((Sig_weights)[j].c_str()), Collector_extra_Cor->CollectEventWeights(),cor_errors);
    L_extra_cor.clear();
    delete Collector_extra_Cor;
  }

  for(unsigned int j = 1; j < Template_uncor.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor;
    eire::Collector_RECO *Collector_extra_UnCor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    L_extra_uncor = Collector_extra_UnCor->CollectLikelihoods((Template_uncor)[j]);
    builder->AddUncorTemplate(L_extra_uncor,atof((Sig_weights)[j].c_str()), Collector_extra_UnCor->CollectEventWeights(),uncor_errors);
    L_extra_uncor.clear();
    delete Collector_extra_UnCor;
  }

  for(unsigned int j = 1; j < Template_background.size(); j++){
    std::vector<std::vector<float> > L_extra_back;
    eire::Collector_RECO *Collector_extra_back = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    L_extra_back = Collector_extra_back->CollectLikelihoods((Template_background)[j]);
    if(j == 1){
      builder->AddBackgroundTemplate(L_extra_back,atof((Background_weights)[j].c_str()), Collector_extra_back->CollectEventWeights(),background_errors);
    }else{builder->AddBackgroundTemplateUnCor(L_extra_back,atof((Background_weights)[j].c_str()), Collector_extra_back->CollectEventWeights(),background_errors);}
    L_extra_back.clear();
    delete Collector_extra_back;
  }

  if(Dataset.size() != 0){
    for(unsigned int i = 0; i < Dataset.size(); i++){
      std::vector<std::vector<float> > L_extra_Data;
      eire::Collector_RECO *Collector_extra_Data = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
      L_extra_Data = Collector_extra_Data->CollectLikelihoods((Dataset)[i]);
      std::cout<<"Data has been collected"<<std::endl;
      builder->AddDataSet(L_extra_Data,atof((Data_weights)[i].c_str()),data_errors);
      L_extra_Data.clear();
      delete Collector_extra_Data;
    }
  }

  //if running various mixing fractions, store the output to make the linearity plots
  eire::Linearity *LinPlotter;
  LinPlotter = new eire::Linearity(name.str());

  std::vector<TH1F> templates;
  std::vector<double> Results; 

  builder->DrawTemplates();
  if(!RunFullExp){
    Results = builder->Mix(mixing);
  }
  else{
    Results = builder->Mix(0);
    LinPlotter->AddPoint(0, 0.,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    Results = builder->Mix(0.25);
    LinPlotter->AddPoint(1, 0.25,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    Results = builder->Mix(0.50);
    LinPlotter->AddPoint(2, 0.50,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    Results = builder->Mix(0.75);
    LinPlotter->AddPoint(3, 0.75,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    Results = builder->Mix(1.00);
    LinPlotter->AddPoint(4, 1.,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    LinPlotter->Plot();
  }

  Results.clear();
  templates.clear();
  L_cor.clear();
  L_uncor.clear();

  if(config_reader){
    delete config_reader;
    config_reader = NULL;
  }
  //  if(Cor_collector){
  //delete Cor_collector;
  //Cor_collector = NULL;
  //}
  //if(Uncor_collector){
  //delete Uncor_collector;
  //Uncor_collector = NULL;
  //}
  //  if(W_collector){
  //delete W_collector;
  //W_collector = NULL;
  //}
  if(builder){
    delete builder;
    builder = NULL;
  }
  if(LinPlotter){
    delete LinPlotter;
    LinPlotter = NULL;
  }
  if(hypothesis_tester){
    delete hypothesis_tester;
    hypothesis_tester = NULL;
  }
  return 0;
}
