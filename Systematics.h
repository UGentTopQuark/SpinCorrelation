#ifndef EIRE_SYSTEMATICS_H
#define EIRE_SYSTEMATICS_H

#include <THStack.h>
#include <Math/GSLRndmEngines.h>
#include <iostream> 
#include <TStyle.h>
#include <TLatex.h>
#include <TArrow.h>
#include <TColor.h>
#include <TLegendEntry.h>
#include <math.h>
#include <TFile.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include <TRandom1.h>
#include <TTree.h>
#include <TLeaf.h>
#include <TFile.h>
#include <cstdlib>
#include <stdio.h>
#include <iomanip>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h> 
#include <TCanvas.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <RooFit.h>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooProdPdf.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooGenericPdf.h"
#include "RooPolynomial.h"
#include "RooChi2Var.h"
#include "RooMinuit.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooHistPdf.h"
#include "RooAddPdf.h"
#include "RooAbsPdf.h"
#include "TTree.h"

using namespace std;

namespace eire{
  class Systematics{
  public:
    Systematics();
    ~Systematics();
    void RunSystematics();

  private:
    void Book_branches();
    void GetHistos(string filename, string systup, string systdown, string syst);
    void SetTemplates();
    TH1F* ReDrawTemplate(TH1F *nominal, std::vector<TH1F> *Syst_up, std::vector<TH1F> *Syst_down, string name);
    std::vector<TH1F> *Cor_sysup;
    std::vector<TH1F> *Uncor_sysup;
    std::vector<TH1F> *Cor_sysdown;
    std::vector<TH1F> *Uncor_sysdown;
    TH1F *nominal_cor;
    TH1F *nominal_uncor;

    float Dataset_size;

    TTree *tree;
    TFile *outfile;

    double xc_stat;
    double xc_trigger; 
    double xc_leptonid;
    double xc_JER;
    double xc_JES;
    double xc_btag;
    double xc_mistag;
    double xc_PU;
    double xc_TopPt;
    double xc_mass;
    double xc_Q2;
    double xu_stat;
    double xu_trigger; 
    double xu_leptonid;
    double xu_JER;
    double xu_JES;
    double xu_btag;
    double xu_mistag;
    double xu_PU;
    double xu_TopPt;
    double xu_mass;
    double xu_Q2; 
    double Cor_Sample_likelihood;
    double Uncor_Sample_likelihood;

    ROOT::Math::GSLRandomEngine *gsl;
    TRandom1 *rng;

  };
}

#endif
