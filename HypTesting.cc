#include "HypTesting.h"

//constructor
eire::HypTesting::HypTesting(string name, string channel_name, float backgroundfrac)
{
  mean_cor = 0.;
  sigma_cor = 0.;
  mean_uncor = 0.;
  sigma_uncor = 0.;
  Name = name;
  Data_sample = 0.;
  Data_sample_error = 0.;
  Data_sample_cor = 0.;
  Data_sample_cor_error = 0.;
  Data_sample_uncor = 0.;
  Data_sample_uncor_error = 0.;
  Dataset_size = 0;
  Dataset_size_1 = 0;
  Dataset_size_2 = 0;
  channel = channel_name;
  Bfrac = backgroundfrac;

  Temp_Cor_Datasize = new std::vector<TH1F>();
  Temp_Uncor_Datasize = new std::vector<TH1F>();

  AbsLik = new std::vector<TH1F>();
  AbsLikUncor = new std::vector<TH1F>();
  Template = new std::vector<TH1F>();

  AbsLik_syst_up = new std::vector<TH1F>();
  AbsLikUncor_syst_up = new std::vector<TH1F>();
  Template_syst_up = new std::vector<TH1F>();

  AbsLik_syst_down = new std::vector<TH1F>();
  AbsLikUncor_syst_down = new std::vector<TH1F>();
  Template_syst_down = new std::vector<TH1F>();

  StatVariedMean_cor = new std::vector<TH1F>();
  StatVariedSigma_cor = new std::vector<TH1F>();
  StatVariedMean_uncor = new std::vector<TH1F>();
  StatVariedSigma_uncor = new std::vector<TH1F>();

  AbsLikelihoodStack = new THStack("AbsLikelihood","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_uncorsample = new THStack("AbsLikelihood_uncorsample","-lnL(H=C); -lnL; normalised entries");
  AbsLikBackHist = new TH1F("AbsLikBackHist","-lnL_{H=C};-lnL_{H=C};entries", 25, 40, 100);
  TemplateStack = new THStack("Template","-2ln#lambda; -2ln#lambda; normalised entries");
  TemplateStack_uncor = new THStack("Template_uncor","-2ln#lambda; -2ln#lambda; normalised entries");
  AbsLikelihoodStack_uncorhyp = new THStack("AbsLikelihood_uncorhyp","-lnL(H=U); -lnL; normalised entries");
  AbsLikelihoodStack_uncorhyp_uncorsample = new THStack("AbsLikelihood_uncorhyp_uncorsample","-lnL(H=U); -lnL; normalised entries");
  
  AbsLikelihoodHist = new TH1F("AbsLikelihoodHist","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  AbsLikelihoodHist_uncorhyp = new TH1F("AbsLikelihoodHist_uncorhyp","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  AbsLikelihoodHist_uncorhyp_uncorsample = new TH1F("AbsLikelihoodHist_uncorhyp_uncorsample","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  AbsLikelihoodHist_uncorsample = new TH1F("AbsLikelihoodHist_uncorsample","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);

  TemplateHist = new TH1F("TemplateHist","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateHist_uncor = new TH1F("TemplateHist_uncor","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);

  TemplateHist_syst_up = new TH1F("TemplateHist_syst_up","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateHist_uncor_syst_up = new TH1F("TemplateHist_uncor_syst_up","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateStack_syst_up = new THStack("Template_syst_up","-2ln#lambda; -2ln#lambda; normalised entries");
  TemplateStack_uncor_syst_up = new THStack("Template_uncor_syst_up","-2ln#lambda; -2ln#lambda; normalised entries");

  TemplateHist_syst_down = new TH1F("TemplateHist_syst_down","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateHist_uncor_syst_down = new TH1F("TemplateHist_uncor_syst_down","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateStack_syst_down = new THStack("Template_syst_down","-2ln#lambda; -2ln#lambda; normalised entries");
  TemplateStack_uncor_syst_down = new THStack("Template_uncor_syst_down","-2ln#lambda; -2ln#lambda; normalised entries");

  AbsLikelihoodStack_syst_up = new THStack("AbsLikelihood_syst_up","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_syst_down = new THStack("AbsLikelihood_syst_down","-lnL(H=C); -lnL; normalised entries");

  AbsLikelihoodStack_uncorsample_syst_up = new THStack("AbsLikelihood_uncorsample_syst_up","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_uncorsample_syst_down = new THStack("AbsLikelihood_uncorsample_syst_down","-lnL(H=C); -lnL; normalised entries");

  AbsLikelihoodStack_uncorhyp_uncorsample_syst_up = new THStack("AbsLikelihood_uncorhyp_uncorsample_syst_up","-lnL(H=U); -lnL; normalised entries");
  AbsLikelihoodStack_uncorhyp_uncorsample_syst_down = new THStack("AbsLikelihood_uncorhyp_uncorsample_syst_down","-lnL(H=U); -lnL; normalised entries");

  AbsLikelihoodStack_uncorhyp_syst_up = new THStack("AbsLikelihood_uncorhyp_syst_up","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_uncorhyp_syst_down = new THStack("AbsLikelihood_uncorhyp_syst_down","-lnL(H=C); -lnL; normalised entries");
}

eire::HypTesting::HypTesting(string name, string channel_name, float backgroundfrac_systup, float backgroundfrac_systdown)
{
  mean_cor = 0.;
  sigma_cor = 0.;
  mean_uncor = 0.;
  sigma_uncor = 0.;
  Name = name;
  Data_sample = 0.;
  Data_sample_error = 0.;
  Data_sample_cor = 0.;
  Data_sample_cor_error = 0.;
  Data_sample_uncor = 0.;
  Data_sample_uncor_error = 0.;
  Dataset_size = 0;
  Dataset_size_1 = 0;
  Dataset_size_2 = 0;
  channel = channel_name;

  Temp_Cor_Datasize = new std::vector<TH1F>();
  Temp_Uncor_Datasize = new std::vector<TH1F>();

  AbsLik = new std::vector<TH1F>();
  AbsLikUncor = new std::vector<TH1F>();
  Template = new std::vector<TH1F>();

  AbsLik_syst_up = new std::vector<TH1F>();
  AbsLikUncor_syst_up = new std::vector<TH1F>();
  Template_syst_up = new std::vector<TH1F>();

  AbsLik_syst_down = new std::vector<TH1F>();
  AbsLikUncor_syst_down = new std::vector<TH1F>();
  Template_syst_down = new std::vector<TH1F>();

  StatVariedMean_cor = new std::vector<TH1F>();
  StatVariedSigma_cor = new std::vector<TH1F>();
  StatVariedMean_uncor = new std::vector<TH1F>();
  StatVariedSigma_uncor = new std::vector<TH1F>();

  AbsLikelihoodStack = new THStack("AbsLikelihood","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_uncorsample = new THStack("AbsLikelihood_uncorsample","-lnL(H=C); -lnL; normalised entries");
  AbsLikBackHist = new TH1F("AbsLikBackHist","-lnL_{H=C};-lnL_{H=C};entries", 25, 40, 100);
  TemplateStack = new THStack("Template","-2ln#lambda; -2ln#lambda; normalised entries");
  TemplateStack_uncor = new THStack("Template_uncor","-2ln#lambda; -2ln#lambda; normalised entries");
  AbsLikelihoodStack_uncorhyp = new THStack("AbsLikelihood_uncorhyp","-lnL(H=U); -lnL; normalised entries");
  AbsLikelihoodStack_uncorhyp_uncorsample = new THStack("AbsLikelihood_uncorhyp_uncorsample","-lnL(H=U); -lnL; normalised entries");
  
  AbsLikelihoodHist = new TH1F("AbsLikelihoodHist","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  AbsLikelihoodHist_uncorhyp = new TH1F("AbsLikelihoodHist_uncorhyp","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  AbsLikelihoodHist_uncorhyp_uncorsample = new TH1F("AbsLikelihoodHist_uncorhyp_uncorsample","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  AbsLikelihoodHist_uncorsample = new TH1F("AbsLikelihoodHist_uncorsample","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);

  TemplateHist = new TH1F("TemplateHist","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateHist_uncor = new TH1F("TemplateHist_uncor","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);

  TemplateHist_syst_up = new TH1F("TemplateHist_syst_up","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateHist_uncor_syst_up = new TH1F("TemplateHist_uncor_syst_up","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateStack_syst_up = new THStack("Template_syst_up","-2ln#lambda; -2ln#lambda; normalised entries");
  TemplateStack_uncor_syst_up = new THStack("Template_uncor_syst_up","-2ln#lambda; -2ln#lambda; normalised entries");

  TemplateHist_syst_down = new TH1F("TemplateHist_syst_down","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateHist_uncor_syst_down = new TH1F("TemplateHist_uncor_syst_down","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", 100, -7, 7);
  TemplateStack_syst_down = new THStack("Template_syst_down","-2ln#lambda; -2ln#lambda; normalised entries");
  TemplateStack_uncor_syst_down = new THStack("Template_uncor_syst_down","-2ln#lambda; -2ln#lambda; normalised entries");

  AbsLikelihoodStack_syst_up = new THStack("AbsLikelihood_syst_up","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_syst_down = new THStack("AbsLikelihood_syst_down","-lnL(H=C); -lnL; normalised entries");

  AbsLikelihoodStack_uncorsample_syst_up = new THStack("AbsLikelihood_uncorsample_syst_up","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_uncorsample_syst_down = new THStack("AbsLikelihood_uncorsample_syst_down","-lnL(H=C); -lnL; normalised entries");

  AbsLikelihoodStack_uncorhyp_uncorsample_syst_up = new THStack("AbsLikelihood_uncorhyp_uncorsample_syst_up","-lnL(H=U); -lnL; normalised entries");
  AbsLikelihoodStack_uncorhyp_uncorsample_syst_down = new THStack("AbsLikelihood_uncorhyp_uncorsample_syst_down","-lnL(H=U); -lnL; normalised entries");

  AbsLikelihoodStack_uncorhyp_syst_up = new THStack("AbsLikelihood_uncorhyp_syst_up","-lnL(H=C); -lnL; normalised entries");
  AbsLikelihoodStack_uncorhyp_syst_down = new THStack("AbsLikelihood_uncorhyp_syst_down","-lnL(H=C); -lnL; normalised entries");

  Bfrac = backgroundfrac_systup;
  Bfrac_systup = backgroundfrac_systup;
  Bfrac_systdown = backgroundfrac_systdown;
}

//destructor
eire::HypTesting::~HypTesting()
{
  delete Temp_Cor_Datasize;
  delete Temp_Uncor_Datasize;

  delete AbsLik;
  delete AbsLikUncor;
  delete Template;

  delete AbsLik_syst_up;
  delete AbsLikUncor_syst_up;
  delete Template_syst_up;

  delete AbsLik_syst_down;
  delete AbsLikUncor_syst_down;
  delete Template_syst_down;

  delete StatVariedMean_cor;
  delete StatVariedSigma_cor;
  delete StatVariedMean_uncor;
  delete StatVariedSigma_uncor;

  delete AbsLikelihoodStack;
  delete AbsLikelihoodStack_syst_up;
  delete AbsLikelihoodStack_syst_down;
  delete AbsLikelihoodStack_uncorsample;
  delete AbsLikelihoodStack_uncorsample_syst_up;
  delete AbsLikelihoodStack_uncorsample_syst_down;
  delete AbsLikBackHist;
  delete TemplateStack;
  delete TemplateStack_uncor;
  delete AbsLikelihoodStack_uncorhyp;
  delete AbsLikelihoodStack_uncorhyp_syst_up;
  delete AbsLikelihoodStack_uncorhyp_syst_down;
  delete AbsLikelihoodStack_uncorhyp_uncorsample;
  delete AbsLikelihoodStack_uncorhyp_uncorsample_syst_up;
  delete AbsLikelihoodStack_uncorhyp_uncorsample_syst_down;
  delete AbsLikelihoodHist;
  delete AbsLikelihoodHist_uncorsample;
  delete AbsLikelihoodHist_uncorhyp;
  delete AbsLikelihoodHist_uncorhyp_uncorsample;
  delete TemplateHist;
  delete TemplateHist_uncor;

  delete TemplateHist_syst_up;
  delete TemplateHist_uncor_syst_up;
  delete TemplateHist_syst_down;
  delete TemplateHist_uncor_syst_down;

  delete TemplateStack_syst_up;
  delete TemplateStack_uncor_syst_up;
  delete TemplateStack_syst_down;
  delete TemplateStack_uncor_syst_down;
}

//set up, fill the total background fraction, import the weights for the backgrounds
void eire::HypTesting::SetUp(std::vector<string> BackgroundWeights, std::vector<string> SignalWeights)
{
  for(unsigned int i = 0; i < BackgroundWeights.size(); i++){
    Bweights.push_back(atof((BackgroundWeights)[i].c_str()));
  }

  for(unsigned int i = 0; i < SignalWeights.size(); i++){
    SigWeights.push_back(atof((SignalWeights)[i].c_str()));
  }
}

//set up, fill the total background fraction, import the weights for the backgrounds
void eire::HypTesting::SetUp(std::vector<string> BackgroundWeights_systup, std::vector<string> BackgroundWeights_systdown, std::vector<string> SignalWeights)
{
  for(unsigned int i = 0; i < BackgroundWeights_systup.size(); i++){
    Bweights_systup.push_back(atof((BackgroundWeights_systup)[i].c_str()));
    Bweights.push_back(atof((BackgroundWeights_systup)[i].c_str()));
  }

  for(unsigned int i = 0; i < BackgroundWeights_systdown.size(); i++){
    Bweights_systdown.push_back(atof((BackgroundWeights_systdown)[i].c_str()));
  }

  for(unsigned int i = 0; i < SignalWeights.size(); i++){
    SigWeights.push_back(atof((SignalWeights)[i].c_str()));
  }
}

//take the Data likelihoods and calculate the Data sample likelihood, with the error
void eire::HypTesting::SetData(std::vector<std::vector<float> > Data_1, std::vector<std::vector<float> > Data_2)
{
  TH1F *uncer_cor = new TH1F("uncer_cor","-lndeltaL",50,40,100);
  TH1F *uncer_uncor = new TH1F("uncer_uncor","-lndeltaL",50,40,100);
  TH1F *Rel_cor = new TH1F("Rel_cor","#deltaL/L;#deltaL/L;entries",50,0.,2.0);
  TH1F *Rel_uncor = new TH1F("Rel_uncor","#deltaL/L;#deltaL/L;entries",50,0.,2.0);
  TH2F *LvsU_cor = new TH2F("LvsU_cor","-lnL vs -ln(#deltaL)",50,40,100,50,40,100);
  TH2F *LvsU_uncor = new TH2F("LvsU_uncor","-lnL vs -ln(#deltaL)",50,40,100,50,40,100);
  TH1F *DRel_cor = new TH1F("DRel_cor","ln(#deltaL)/ln(L)",50,0.9,1.2);
  TH1F *DRel_uncor = new TH1F("DRel_uncor","ln(#deltaL)/ln(L)",50,0.9,1.2);
  Rel_cor->SetMarkerColor(kRed);
  DRel_cor->SetMarkerColor(kRed);
  Rel_uncor->SetMarkerColor(kBlack);
  DRel_uncor->SetMarkerColor(kBlack);
  Rel_cor->SetLineColor(kRed);
  DRel_cor->SetLineColor(kRed);
  Rel_uncor->SetLineColor(kBlack);
  DRel_uncor->SetLineColor(kBlack);


  Dataset_size = (Data_1)[0].size();
  Dataset_size_1 = (Data_1)[0].size();
  //  std::cout<<"Dataset size = "<<(Data_1)[0].size()<<std::endl;

  //get the likelihood vectors
  std::vector<float> cor_1 = (Data_1)[0];
  std::vector<float> uncor_1 = (Data_1)[1];
  std::vector<float> cor_err_1 = (Data_1)[2];
  std::vector<float> uncor_err_1 = (Data_1)[3];

  std::vector<float> cor_2;
  std::vector<float> uncor_2;
  std::vector<float> cor_err_2;
  std::vector<float> uncor_err_2;
  if(Data_2.size() != 0){
    cor_2 = (Data_2)[0];
    uncor_2 = (Data_2)[1];
    cor_err_2 = (Data_2)[2];
    uncor_err_2 = (Data_2)[3];
    Dataset_size += (Data_2)[0].size();
    Dataset_size_2 = (Data_2)[0].size();
  }

  //  std::cout<<"Dataset size 2 = "<<(Data_2)[0].size()<<std::endl;
  //std::cout<<"Dataset total = "<<Dataset_size<<std::endl;
  //  int counter = 0;
  //run over each event, add the -2lnLambda to the Sample likelihood
  for(unsigned int j = 0; j < (Data_1)[0].size(); j++){
    Data_sample += 2*log((cor_1)[j]) - 2*log((uncor_1)[j]);
    Data_sample_error += 4*(((cor_err_1)[j]/(cor_1)[j])*((cor_err_1)[j]/(cor_1)[j]) + ((uncor_err_1)[j]/(uncor_1)[j])*((uncor_err_1)[j]/(uncor_1)[j]));
    Data_sample_cor += -log((cor_1)[j]);
    Data_sample_cor_error += (cor_err_1)[j]/(cor_1)[j]*(cor_err_1)[j]/(cor_1)[j];
    Data_sample_uncor += -log((uncor_1)[j]);
    Data_sample_uncor_error += (uncor_err_1)[j]/(uncor_1)[j]*(uncor_err_1)[j]/(uncor_1)[j];
    uncer_cor->Fill(-log((cor_err_1)[j]));
    uncer_uncor->Fill(-log((uncor_err_1)[j]));
    Rel_cor->Fill((cor_err_1)[j]/(cor_1)[j]);
    Rel_uncor->Fill((uncor_err_1)[j]/(uncor_1)[j]);
    DRel_cor->Fill(log((cor_err_1)[j])/log((cor_1)[j]));
    DRel_uncor->Fill(log((uncor_err_1)[j])/log((uncor_1)[j]));
    LvsU_cor->Fill(-log((cor_1)[j]),-log((cor_err_1)[j]));
    LvsU_uncor->Fill(-log((uncor_1)[j]),-log((uncor_err_1)[j]));
  }

  //run over each event, add the -2lnLambda to the Sample likelihood
  if(Data_2.size() != 0){
    for(unsigned int j = 0; j < (Data_2)[0].size(); j++){
      Data_sample += 2*log((cor_2)[j]) - 2*log((uncor_2)[j]);
      Data_sample_error += 4*(((cor_err_2)[j]/(cor_2)[j])*((cor_err_2)[j]/(cor_2)[j]) + ((uncor_err_2)[j]/(uncor_2)[j])*((uncor_err_2)[j]/(uncor_2)[j]));
      Data_sample_cor += -log((cor_2)[j]);
      Data_sample_cor_error += (cor_err_2)[j]/(cor_2)[j]*(cor_err_2)[j]/(cor_2)[j];
      Data_sample_uncor += -log((uncor_2)[j]);
      Data_sample_uncor_error += (uncor_err_2)[j]/(uncor_2)[j]*(uncor_err_2)[j]/(uncor_2)[j];
      uncer_cor->Fill(-log((cor_err_2)[j]));
      uncer_uncor->Fill(-log((uncor_err_2)[j]));
      Rel_cor->Fill((cor_err_2)[j]/(cor_2)[j]);
      Rel_uncor->Fill((uncor_err_2)[j]/(uncor_2)[j]);
      DRel_cor->Fill(log((cor_err_2)[j])/log((cor_2)[j]));
      DRel_uncor->Fill(log((uncor_err_2)[j])/log((uncor_2)[j]));
      LvsU_cor->Fill(-log((cor_2)[j]),-log((cor_err_2)[j]));
      LvsU_uncor->Fill(-log((uncor_2)[j]),-log((uncor_err_2)[j]));
    }
  }

  uncer_cor->SetLineColor(kRed);
  uncer_uncor->SetLineColor(kBlack);
  /*
  TCanvas *c = new TCanvas();
  gStyle->SetOptStat(2222);
  c->cd();
  uncer_cor->Draw("e");
  uncer_uncor->Draw("esame");
  c->Modified();
  c->Update();
  c->SaveAs("DataLikelihoodUncertainties.C");

  TLegend *legrel = new TLegend(0.6773985,0.607473,0.9676871,0.848228,NULL,"brNDC");
  legrel->SetFillColor(0);
  legrel->SetFillStyle(0);
  legrel->SetBorderSize(0);
  legrel->AddEntry(Rel_cor,"Data correlated hypothesis");
  legrel->AddEntry(Rel_cor,"Data uncorrelated hypothesis");

  TCanvas *c2 = new TCanvas();
  c2->cd();
  Rel_cor->Draw("e");
  Rel_uncor->Draw("esame");
  legrel->Draw("same");
  c2->SetLogy();
  c2->Modified();
  c2->Update();
  c2->SaveAs("RelativeUncertainties.C");

  TCanvas *c8 = new TCanvas();
  c8->cd();
  DRel_cor->Draw("e");
  DRel_uncor->Draw("esame");
  c8->Modified();
  c8->Update();
  c8->SaveAs("DRelativeUncertainties.C");

  TCanvas *c3 = new TCanvas();
  c3->cd();
  LvsU_cor->Draw("colz");
  c3->Modified();
  c3->Update();
  c3->SaveAs("LikVsUnc_cor.C");

  TCanvas *c4 = new TCanvas();
  c4->cd();
  LvsU_uncor->Draw("colz");
  c4->Modified();
  c4->Update();
  c4->SaveAs("LikVsUnc_uncor.C");
  */

  Data_sample_error = sqrt(Data_sample_error);
  Data_sample_cor_error = sqrt(Data_sample_cor_error);
  Data_sample_uncor_error = sqrt(Data_sample_uncor_error);

  std::cout<<"end of add data"<<std::endl;

}

void eire::HypTesting::PreparePlots(std::vector<TH1F> *histos){

  std::cout<<"prepare plots"<<std::endl;

  (*histos)[13].SetFillColor(kWhite+8);
  (*histos)[12].SetFillColor(kWhite+8);
  (*histos)[11].SetFillColor(kWhite+8);
  (*histos)[10].SetFillColor(kWhite+8);
  (*histos)[9].SetFillColor(kWhite+8);
  (*histos)[8].SetFillColor(kWhite+8);
  (*histos)[7].SetFillColor(kCyan-6);
  (*histos)[6].SetFillColor(kBlue-2);
  (*histos)[5].SetFillColor(kBlue-2);
  (*histos)[4].SetFillColor(kYellow-7);
  (*histos)[3].SetFillColor(kYellow-7);
  (*histos)[2].SetFillColor(kRed-3);
  (*histos)[1].SetFillColor(kRed-3);
  (*histos)[0].SetFillColor(kBlack);

  (*histos)[13].SetLineColor(kWhite+8);
  (*histos)[12].SetLineColor(kWhite+8);
  (*histos)[11].SetLineColor(kWhite+8);
  (*histos)[10].SetLineColor(kWhite+8);
  (*histos)[9].SetLineColor(kWhite+8);
  (*histos)[8].SetLineColor(kWhite+8);
  (*histos)[7].SetLineColor(kCyan-6);
  (*histos)[6].SetLineColor(kBlue-2);
  (*histos)[5].SetLineColor(kBlue-2);
  (*histos)[4].SetLineColor(kYellow-7);
  (*histos)[3].SetLineColor(kYellow-7);
  (*histos)[2].SetLineColor(kRed-3);
  (*histos)[1].SetLineColor(kRed-3);
  (*histos)[0].SetLineColor(kBlack);
  
  (*histos)[13].SetMarkerColor(kWhite+8);
  (*histos)[12].SetMarkerColor(kWhite+8);
  (*histos)[11].SetMarkerColor(kWhite+8);
  (*histos)[10].SetMarkerColor(kWhite+8);
  (*histos)[9].SetMarkerColor(kWhite+8);
  (*histos)[8].SetMarkerColor(kWhite+8);
  (*histos)[7].SetMarkerColor(kCyan-6);
  (*histos)[6].SetMarkerColor(kBlue-2);
  (*histos)[5].SetMarkerColor(kBlue-2);
  (*histos)[4].SetMarkerColor(kYellow-7);
  (*histos)[3].SetMarkerColor(kYellow-7);
  (*histos)[2].SetMarkerColor(kRed-3);
  (*histos)[1].SetMarkerColor(kRed-3);
  (*histos)[0].SetMarkerColor(kBlack);

  (*histos)[0].SetMarkerStyle(20);
  (*histos)[0].SetMarkerSize(0.75);

  if(histos->size() > 14){
    (*histos)[26].SetFillColor(kWhite+8);
    (*histos)[25].SetFillColor(kWhite+8);
    (*histos)[24].SetFillColor(kWhite+8);
    (*histos)[23].SetFillColor(kWhite+8);
    (*histos)[22].SetFillColor(kWhite+8);
    (*histos)[21].SetFillColor(kWhite+8);
    (*histos)[20].SetFillColor(kCyan-6);
    (*histos)[19].SetFillColor(kBlue-2);
    (*histos)[18].SetFillColor(kBlue-2);
    (*histos)[17].SetFillColor(kYellow-7);
    (*histos)[16].SetFillColor(kYellow-7);
    (*histos)[15].SetFillColor(kRed-3);
    (*histos)[14].SetFillColor(kRed-3);
    
    (*histos)[26].SetLineColor(kWhite+8);
    (*histos)[25].SetLineColor(kWhite+8);
    (*histos)[24].SetLineColor(kWhite+8);
    (*histos)[23].SetLineColor(kWhite+8);
    (*histos)[22].SetLineColor(kWhite+8);
    (*histos)[21].SetLineColor(kWhite+8);
    (*histos)[20].SetLineColor(kCyan-6);
    (*histos)[19].SetLineColor(kBlue-2);
    (*histos)[18].SetLineColor(kBlue-2);
    (*histos)[17].SetLineColor(kYellow-7);
    (*histos)[16].SetLineColor(kYellow-7);
    (*histos)[15].SetLineColor(kRed-3);
    (*histos)[14].SetLineColor(kRed-3);
    
    (*histos)[26].SetMarkerColor(kWhite+8);
    (*histos)[25].SetMarkerColor(kWhite+8);
    (*histos)[24].SetMarkerColor(kWhite+8);
    (*histos)[23].SetMarkerColor(kWhite+8);
    (*histos)[22].SetMarkerColor(kWhite+8);
    (*histos)[21].SetMarkerColor(kWhite+8);
    (*histos)[20].SetMarkerColor(kCyan-6);
    (*histos)[19].SetMarkerColor(kBlue-2);
    (*histos)[18].SetMarkerColor(kBlue-2);
    (*histos)[17].SetMarkerColor(kYellow-7);
    (*histos)[16].SetMarkerColor(kYellow-7);
    (*histos)[15].SetMarkerColor(kRed-3);
    (*histos)[14].SetMarkerColor(kRed-3);
  }
}


//get the templates and assign them to the right sample
void eire::HypTesting::SetTemplates(std::vector<std::vector<TH1F> > vec, std::vector<std::vector<TH1F> > vec_data, int Syst)
{
  std::cout<<"set templates, syst = "<<Syst<<std::endl;
  TH1F *temp_template = (TH1F*) (vec_data)[0][0].Clone("temp_Data");
  std::cout<<"template Data integral = "<<temp_template->Integral()<<std::endl;
  temp_template->Scale(Dataset_size_1);
  std::cout<<"template Data integral = "<<temp_template->Integral()<<std::endl;
  if(vec_data.size() > 1){
    std::cout<<"debug in second"<<std::endl;
    (vec_data)[1][0].Scale(Dataset_size_2);
    temp_template->Add(&(vec_data)[1][0]);
  }
  std::cout<<"template Data integral = "<<temp_template->Integral()<<std::endl;
  temp_template->Scale(1./temp_template->Integral());
  std::cout<<"template Data integral = "<<temp_template->Integral()<<std::endl;
  TH1F *temp_AbsLik = (TH1F*) (vec_data)[0][1].Clone("temp_abs_Data");
  std::cout<<"AbsLik Data integral = "<<temp_AbsLik->Integral()<<std::endl;
  temp_AbsLik->Scale(Dataset_size_1);
  std::cout<<"AbsLik Data integral = "<<temp_AbsLik->Integral()<<std::endl;
  if(vec_data.size() > 1){
    (vec_data)[1][1].Scale(Dataset_size_2);
    temp_AbsLik->Add(&(vec_data)[1][1]);
  }
  std::cout<<"AbsLik Data integral = "<<temp_AbsLik->Integral()<<std::endl;
  temp_AbsLik->Scale(1./temp_AbsLik->Integral());
  std::cout<<"AbsLik Data integral = "<<temp_AbsLik->Integral()<<std::endl;
  TH1F *temp_AbsLikUncor = (TH1F*) (vec_data)[0][2].Clone("temp_absuncor_Data");
  std::cout<<"AbsLikUncor Data integral = "<<temp_AbsLikUncor->Integral()<<std::endl;
  temp_AbsLikUncor->Scale(Dataset_size_1);
  std::cout<<"AbsLikUncor Data integral = "<<temp_AbsLikUncor->Integral()<<std::endl;
  if(vec_data.size() > 1){
    (vec_data)[1][2].Scale(Dataset_size_2);
    temp_AbsLikUncor->Add(&(vec_data)[1][2]);
  }
  std::cout<<"AbsLikUncor Data integral = "<<temp_AbsLikUncor->Integral()<<std::endl;
  temp_AbsLikUncor->Scale(1./temp_AbsLikUncor->Integral());
  std::cout<<"AbsLikUncor Data integral = "<<temp_AbsLikUncor->Integral()<<std::endl;
  std::cout<<"debug after abs lik uncor"<<std::endl;

  if(Syst == 0){
    Template->push_back(*temp_template);
    AbsLik->push_back(*temp_AbsLik);
    AbsLikUncor->push_back(*temp_AbsLikUncor);
  }else if(Syst == 1){
    std::cout<<"bla1"<<std::endl;
    Template_syst_up->push_back(*temp_template);
    std::cout<<"bla2"<<std::endl;
    AbsLik_syst_up->push_back(*temp_AbsLik);
    std::cout<<"bla3"<<std::endl;
    AbsLikUncor_syst_up->push_back(*temp_AbsLikUncor);
    std::cout<<"bla4"<<std::endl;
  }else if(Syst == -1){
    Template_syst_down->push_back(*temp_template);
    AbsLik_syst_down->push_back(*temp_AbsLik);
    AbsLikUncor_syst_down->push_back(*temp_AbsLikUncor);
  }

  if(Syst == 0){
    for(unsigned int i = 0; i < vec.size(); i++){
      Template->push_back((vec)[i][0]);
      AbsLik->push_back((vec)[i][1]);
      AbsLikUncor->push_back((vec)[i][2]);
    }
  }else if(Syst == 1){
    for(unsigned int i = 0; i < vec.size(); i++){
      Template_syst_up->push_back((vec)[i][0]);
      AbsLik_syst_up->push_back((vec)[i][1]);
      AbsLikUncor_syst_up->push_back((vec)[i][2]);
    }
  }else if(Syst == -1){
    for(unsigned int i = 0; i < vec.size(); i++){
      Template_syst_down->push_back((vec)[i][0]);
      AbsLik_syst_down->push_back((vec)[i][1]);
      AbsLikUncor_syst_down->push_back((vec)[i][2]);
    }
  }

  /*
    Data_template_minus = (vec_data)[0][0];
    Data_AbsLik_minus = (vec_data)[0][1];
    
    if(vec_data.size() > 1){
    Data_template_minus.Add(&(vec_data)[1][0]);
    Data_AbsLik_minus.Add(&(vec_data)[1][1]);
    }
  */

  cor_template_minus = (vec)[0][0];
  uncor_template_minus = (vec)[1][0];
  B_cor_template_minus = (vec)[2][0];
  B_uncor_template_minus = (vec)[3][0];
  W3Jets_template_minus = (vec)[4][0];
  W4Jets_template_minus = (vec)[5][0];
  DY_template_minus = (vec)[6][0];
  TsChan_template_minus = (vec)[7][0];
  TtChan_template_minus = (vec)[8][0];
  TtWChan_template_minus = (vec)[9][0];
  TbarsChan_template_minus = (vec)[10][0];
  TbartChan_template_minus = (vec)[11][0];
  TbartWChan_template_minus = (vec)[12][0];

  //if there are more samples specified, it is because we combine e+ and e- channel
  if(vec.size() > 13){
    cor_template_plus = (vec)[13][0];
    std::cout<<"template mean TTbar cor neg= "<<(vec)[13][0].GetMean()<<std::endl;
    uncor_template_plus = (vec)[14][0];
    std::cout<<"template mean TTbar uncor neg= "<<(vec)[14][0].GetMean()<<std::endl;
    B_cor_template_plus = (vec)[15][0];
    std::cout<<"template mean TTbar other cor neg= "<<(vec)[15][0].GetMean()<<std::endl;
    B_uncor_template_plus = (vec)[16][0];
    std::cout<<"template mean TTbar other uncor neg= "<<(vec)[16][0].GetMean()<<std::endl;
    W3Jets_template_plus = (vec)[17][0];
    W4Jets_template_plus = (vec)[18][0];
    DY_template_plus = (vec)[19][0];
    TsChan_template_plus = (vec)[20][0];
    TtChan_template_plus = (vec)[21][0];
    TtWChan_template_plus = (vec)[22][0];
    TbarsChan_template_plus = (vec)[23][0];
    TbartChan_template_plus = (vec)[24][0];
    TbartWChan_template_plus = (vec)[25][0];
  }

  cor_AbsLik_minus = (vec)[0][1];
  uncor_AbsLik_minus = (vec)[1][1];
  B_cor_AbsLik_minus = (vec)[2][1];
  B_uncor_AbsLik_minus = (vec)[3][1];
  W3Jets_AbsLik_minus = (vec)[4][1];
  W4Jets_AbsLik_minus = (vec)[5][1];
  DY_AbsLik_minus = (vec)[6][1];
  TsChan_AbsLik_minus = (vec)[7][1];
  TtChan_AbsLik_minus = (vec)[8][1];
  TtWChan_AbsLik_minus = (vec)[9][1];
  TbarsChan_AbsLik_minus = (vec)[10][1];
  TbartChan_AbsLik_minus = (vec)[11][1];
  TbartWChan_AbsLik_minus = (vec)[12][1];

  //if there are more samples specified, it is because we combine e+ and e- channel
  if(vec.size() > 13){
    cor_AbsLik_plus = (vec)[13][1];
    uncor_AbsLik_plus = (vec)[14][1];
    B_cor_AbsLik_plus = (vec)[15][1];
    B_uncor_AbsLik_plus = (vec)[16][1];
    W3Jets_AbsLik_plus = (vec)[17][1];
    W4Jets_AbsLik_plus = (vec)[18][1];
    DY_AbsLik_plus = (vec)[19][1];
    TsChan_AbsLik_plus = (vec)[20][1];
    TtChan_AbsLik_plus = (vec)[21][1];
    TtWChan_AbsLik_plus = (vec)[22][1];
    TbarsChan_AbsLik_plus = (vec)[23][1];
    TbartChan_AbsLik_plus = (vec)[24][1];
    TbartWChan_AbsLik_plus = (vec)[25][1];
  }

}

TH1F* eire::HypTesting::PrepareStack(THStack *stack, TH1F *hist, std::vector<TH1F> *vec_hists, bool cor, float frac, std::vector<float> weights)
{
  float loc_bfrac = 0.;
  std::vector<float> loc_bweights;
  loc_bfrac = frac;
  loc_bweights = weights;

  if(cor){
    hist = (TH1F*)(*vec_hists)[1].Clone();
    //hist->Add(&(*vec_hists)[1]);
  }else{
    hist = (TH1F*)(*vec_hists)[2].Clone();
    //hist->Add(&(*vec_hists)[2]);
  }
  hist->Scale(1./hist->Integral());
  hist->Scale((1.-loc_bfrac)*(SigWeights)[0]);

  if((*vec_hists)[1].Integral() > 0.99 ){
    (*vec_hists)[1].Scale((1.-loc_bfrac)*(SigWeights)[0]);
    (*vec_hists)[2].Scale((1.-loc_bfrac)*(SigWeights)[0]); //TTbar uncor                                                                                       
    (*vec_hists)[3].Scale(loc_bfrac*(loc_bweights)[0]); //TTbar other cor                                                                                          
    (*vec_hists)[4].Scale(loc_bfrac*(loc_bweights)[0]); //TTbar other uncor                                                                                        
    (*vec_hists)[5].Scale(loc_bfrac*(loc_bweights)[1]); //W3Jets                                                                                                   
    (*vec_hists)[6].Scale(loc_bfrac*(loc_bweights)[2]); //W4Jets                                                                                                   
    (*vec_hists)[7].Scale(loc_bfrac*(loc_bweights)[3]); //DY                                                                                                        
    (*vec_hists)[8].Scale(loc_bfrac*(loc_bweights)[4]); //TsChan                                                                                                   
    (*vec_hists)[9].Scale(loc_bfrac*(loc_bweights)[5]); //TtChan                                                                                                   
    (*vec_hists)[10].Scale(loc_bfrac*(loc_bweights)[6]); //TtWChan                                                                                                 
    (*vec_hists)[11].Scale(loc_bfrac*(loc_bweights)[7]); //TbarsChan                                                                                               
    (*vec_hists)[12].Scale(loc_bfrac*(loc_bweights)[8]); //TbartChan                                                                                              
    (*vec_hists)[13].Scale(loc_bfrac*(loc_bweights)[9]); //TbartWChan                                                                                             

    if(vec_hists->size() > 14){
      (*vec_hists)[14].Scale((1.-loc_bfrac)*(SigWeights)[1]); //TTbar cor neg                                                                                  
      (*vec_hists)[15].Scale((1.-loc_bfrac)*(SigWeights)[1]); //TTbar uncor neg                                                                                
      (*vec_hists)[16].Scale(loc_bfrac*(loc_bweights)[10]); //TTbar other cor neg                                                                                  
      (*vec_hists)[17].Scale(loc_bfrac*(loc_bweights)[10]); //TTbar other uncor neg                                                                                
      (*vec_hists)[18].Scale(loc_bfrac*(loc_bweights)[11]); //W3Jets neg                                                                                           
      (*vec_hists)[19].Scale(loc_bfrac*(loc_bweights)[12]); //W4Jets neg                                                                                           
      (*vec_hists)[20].Scale(loc_bfrac*(loc_bweights)[13]); //DY neg                                                                                               
      (*vec_hists)[21].Scale(loc_bfrac*(loc_bweights)[14]); //TsChan                                                                                               
      (*vec_hists)[22].Scale(loc_bfrac*(loc_bweights)[15]); //TtChan                                                                                               
      (*vec_hists)[23].Scale(loc_bfrac*(loc_bweights)[16]); //TtWChan                                                                                              
      (*vec_hists)[24].Scale(loc_bfrac*(loc_bweights)[17]); //TbarsChan                                                                                            
      (*vec_hists)[25].Scale(loc_bfrac*(loc_bweights)[18]); //TbartChan                                                                                            
      (*vec_hists)[26].Scale(loc_bfrac*(loc_bweights)[19]); //TbartWChan                                                                                           
    }
    
  }
  if(cor){
    hist->Add(&(*vec_hists)[3]); //TTbar other cor                                                                                                   
  }else{
    hist->Add(&(*vec_hists)[4]); //TTbar other uncor                                                                                                   
  }
  hist->Add(&(*vec_hists)[5]); //W3Jets                                                                                                            
  hist->Add(&(*vec_hists)[6]); //W4Jets                                                                                                            
  hist->Add(&(*vec_hists)[7]); //DY                                                                                                                
  hist->Add(&(*vec_hists)[8]); //TsChan                                                                                                            
  hist->Add(&(*vec_hists)[9]); //TtChan                                                                                                            
  hist->Add(&(*vec_hists)[10]); //TtWChan                                                                                                          
  hist->Add(&(*vec_hists)[11]); //TbarsChan                                                                                                        
  hist->Add(&(*vec_hists)[12]); //TbartChan                                                                                                        
  hist->Add(&(*vec_hists)[13]); //TbartWChan                                                                                                       

  if(vec_hists->size() > 14){
    if(cor){
      hist->Add(&(*vec_hists)[14]); //TTbar cor neg                                                                                                  
      hist->Add(&(*vec_hists)[16]); //TTbar other cor neg                                                                                            
    }else{
      hist->Add(&(*vec_hists)[15]); //TTbar uncor neg                                                                                                  
      hist->Add(&(*vec_hists)[17]); //TTbar other uncor neg                                                                                            
    }
    hist->Add(&(*vec_hists)[18]); //W3Jets neg                                                                                                     
    hist->Add(&(*vec_hists)[19]); //W4Jets neg                                                                                                     
    hist->Add(&(*vec_hists)[20]); //DY neg                                                                                                         
    hist->Add(&(*vec_hists)[21]); //TsChan neg                                                                                                     
    hist->Add(&(*vec_hists)[22]); //TtChan neg                                                                                                     
    hist->Add(&(*vec_hists)[23]); //TtWChan neg                                                                                                    
    hist->Add(&(*vec_hists)[24]); //TbarsChan neg                                                                                                  
    hist->Add(&(*vec_hists)[25]); //TbartChan neg                                                                                                  
    hist->Add(&(*vec_hists)[26]); //TbartWChan neg                                                                                                 
  }

  double Integ = hist->Integral();
  std::cout<<"integral of total hist = "<<Integ<<std::endl;
  hist->Scale(1./Integ);

  for(int i = 1; i < vec_hists->size(); i++){
    (*vec_hists)[i].Scale(1./Integ);
  }

  std::cout<<"after extra scaling"<<std::endl;
  double stackInt = 0.;

  stack->Add(&(*vec_hists)[9]); //TtChan                                                                                                           
  stackInt += (*vec_hists)[9].Integral();
  stack->Add(&(*vec_hists)[10]); //TtWChan                                                                                                         
  stackInt += (*vec_hists)[10].Integral();
  stack->Add(&(*vec_hists)[11]); //TbarsChan                                                                                                       
  stackInt += (*vec_hists)[11].Integral();
  stack->Add(&(*vec_hists)[12]); //TbartChan                                                                                                       
  stackInt += (*vec_hists)[12].Integral();
  stack->Add(&(*vec_hists)[13]); //TbartWChan                                                                                                      
  stackInt += (*vec_hists)[13].Integral();
  stack->Add(&(*vec_hists)[8]); //TsChan                                                                                                           
  stackInt += (*vec_hists)[8].Integral();
  if(vec_hists->size() > 14){
    stack->Add(&(*vec_hists)[21]); //TsChan neg                                                                                                    
    stackInt += (*vec_hists)[21].Integral();
    stack->Add(&(*vec_hists)[22]); //TtChan neg                                                                                                    
    stackInt += (*vec_hists)[22].Integral();
    stack->Add(&(*vec_hists)[23]); //TtWChan neg                                                                                                   
    stackInt += (*vec_hists)[23].Integral();
    stack->Add(&(*vec_hists)[24]); //TbarsChan neg                                                                                                 
    stackInt += (*vec_hists)[24].Integral();
    stack->Add(&(*vec_hists)[25]); //TbartChan neg                                                                                                 
    stackInt += (*vec_hists)[25].Integral();
    stack->Add(&(*vec_hists)[26]); //TbartWChan neg                                                                                                
    stackInt += (*vec_hists)[26].Integral();
  }
  stack->Add(&(*vec_hists)[7]); //DY                                                                                                               
  stackInt += (*vec_hists)[7].Integral();
  if(vec_hists->size() > 14){
    stack->Add(&(*vec_hists)[20]); //DY neg                                                                                                        
    stackInt += (*vec_hists)[20].Integral();
  }
  stack->Add(&(*vec_hists)[6]); //W4Jets                                                                                                           
  stackInt += (*vec_hists)[6].Integral();
  stack->Add(&(*vec_hists)[5]); //W3Jets                                                                                                           
  stackInt += (*vec_hists)[5].Integral();
  if(vec_hists->size() > 14){
    stack->Add(&(*vec_hists)[18]); //W3Jets neg                                                                                                    
    stackInt += (*vec_hists)[18].Integral();
    stack->Add(&(*vec_hists)[19]); //W4Jets neg                                                                                                    
    stackInt += (*vec_hists)[19].Integral();
  }
  if(cor){
    stack->Add(&(*vec_hists)[3]); //TTbar other cor                                                                                                  
    stackInt += (*vec_hists)[3].Integral();
    if(vec_hists->size() > 14){
      stack->Add(&(*vec_hists)[16]); //TTbar other cor neg                                                                                           
      stackInt += (*vec_hists)[16].Integral();
    }
    stack->Add(&(*vec_hists)[1]); //TTbar cor                                                                                                        
    stackInt += (*vec_hists)[1].Integral();
    if(vec_hists->size() > 14){
      stack->Add(&(*vec_hists)[14]); //TTbar cor neg                                                                                                 
      stackInt += (*vec_hists)[14].Integral();
    }
  }else{
    stack->Add(&(*vec_hists)[4]); //TTbar other cor                                                                                                  
    stackInt += (*vec_hists)[4].Integral();
    if(vec_hists->size() > 14){
      stack->Add(&(*vec_hists)[17]); //TTbar other cor neg                                                                                           
      stackInt += (*vec_hists)[17].Integral();
    }
    stack->Add(&(*vec_hists)[2]); //TTbar cor                                                                                                        
    stackInt += (*vec_hists)[2].Integral();
    if(vec_hists->size() > 14){
      stack->Add(&(*vec_hists)[15]); //TTbar cor neg                                                                                                 
      stackInt += (*vec_hists)[15].Integral();
    }
  }

  std::cout<<"stack integral = "<<stackInt<<std::endl;

  return hist;
}

void eire::HypTesting::DrawStack(bool UsingSyst)
{
  std::cout<<"in DrawStack"<<std::endl;

  if(!UsingSyst){
    PreparePlots(AbsLik);
    PreparePlots(Template);
    PreparePlots(AbsLikUncor);
  }

  /*
  if(UsingSyst){
    PreparePlots(AbsLik_syst_up);
    PreparePlots(Template_syst_up);
    PreparePlots(AbsLikUncor_syst_up);

    PreparePlots(AbsLik_syst_down);
    PreparePlots(Template_syst_down);
    PreparePlots(AbsLikUncor_syst_down);
  }
  */

  if(UsingSyst){
    for(unsigned int i = 0; i < AbsLik_syst_up->size(); i++){
      (*AbsLik_syst_up)[i].Rebin(2);
      (*AbsLikUncor_syst_up)[i].Rebin(2);
      (*AbsLik_syst_down)[i].Rebin(2);
      (*AbsLikUncor_syst_down)[i].Rebin(2);
    }
  }else{
    for(unsigned int i = 0; i < AbsLik->size(); i++){
      (*AbsLik)[i].Rebin(2);
      (*AbsLikUncor)[i].Rebin(2);
    }
  }


  if(!UsingSyst){
    std::cout<<"beforePrepareStack AbsLikelihood"<<std::endl;
    AbsLikelihoodHist = PrepareStack(AbsLikelihoodStack,AbsLikelihoodHist,AbsLik,true,Bfrac,Bweights);
    std::cout<<"beforePrepareStack Template"<<std::endl;
    TemplateHist = PrepareStack(TemplateStack,TemplateHist,Template,true,Bfrac,Bweights);
    std::cout<<"beforePrepareStack Template uncor"<<std::endl;
    TemplateHist_uncor = PrepareStack(TemplateStack_uncor,TemplateHist_uncor,Template,false,Bfrac,Bweights);
    std::cout<<"beforePrepareStack AbsLikelihood uncor"<<std::endl;
    AbsLikelihoodHist_uncorhyp = PrepareStack(AbsLikelihoodStack_uncorhyp, AbsLikelihoodHist_uncorhyp, AbsLikUncor,true,Bfrac,Bweights);
    std::cout<<"AfterPrepareStack"<<std::endl;
    AbsLikelihoodHist_uncorsample = PrepareStack(AbsLikelihoodStack_uncorsample,AbsLikelihoodHist_uncorsample,AbsLik,false,Bfrac,Bweights);
    AbsLikelihoodHist_uncorhyp_uncorsample = PrepareStack(AbsLikelihoodStack_uncorhyp_uncorsample,AbsLikelihoodHist_uncorhyp_uncorsample,AbsLikUncor,false,Bfrac,Bweights);
  }else{
    std::cout<<"before stack syst up"<<std::endl;
    AbsLikelihoodHist_syst_up = PrepareStack(AbsLikelihoodStack_syst_up,AbsLikelihoodHist_syst_up,AbsLik_syst_up,true,Bfrac_systup, Bweights_systup);
    AbsLikelihoodHist_uncorsample_syst_up = PrepareStack(AbsLikelihoodStack_uncorsample_syst_up,AbsLikelihoodHist_uncorsample_syst_up,AbsLik_syst_up,false,Bfrac_systup,Bweights_systup);
    AbsLikelihoodHist_uncorhyp_syst_up = PrepareStack(AbsLikelihoodStack_uncorhyp_syst_up,AbsLikelihoodHist_uncorhyp_syst_up,AbsLikUncor_syst_up,true,Bfrac_systup,Bweights_systup);
    AbsLikelihoodHist_uncorhyp_uncorsample_syst_up = PrepareStack(AbsLikelihoodStack_uncorhyp_uncorsample_syst_up,AbsLikelihoodHist_uncorhyp_uncorsample_syst_up,AbsLikUncor_syst_up,false,Bfrac_systup, Bweights_systup);
    TemplateHist_syst_up = PrepareStack(TemplateStack_syst_up,TemplateHist_syst_up,Template_syst_up,true,Bfrac_systup,Bweights_systup);
    TemplateHist_uncor_syst_up = PrepareStack(TemplateStack_uncor_syst_up,TemplateHist_uncor_syst_up,Template_syst_up,false,Bfrac_systup,Bweights_systup);

    std::cout<<"before stack syst down"<<std::endl;
    AbsLikelihoodHist_syst_down = PrepareStack(AbsLikelihoodStack_syst_down,AbsLikelihoodHist_syst_down,AbsLik_syst_down,true,Bfrac_systdown,Bweights_systdown);
    AbsLikelihoodHist_uncorsample_syst_down = PrepareStack(AbsLikelihoodStack_uncorsample_syst_down,AbsLikelihoodHist_uncorsample_syst_down,AbsLik_syst_down,false,Bfrac_systdown,Bweights_systdown);
    AbsLikelihoodHist_uncorhyp_syst_down = PrepareStack(AbsLikelihoodStack_uncorhyp_syst_down,AbsLikelihoodHist_uncorhyp_syst_down,AbsLikUncor_syst_down,true,Bfrac_systdown,Bweights_systdown);
    AbsLikelihoodHist_uncorhyp_uncorsample_syst_down = PrepareStack(AbsLikelihoodStack_uncorhyp_uncorsample_syst_down,AbsLikelihoodHist_uncorhyp_uncorsample_syst_down,AbsLikUncor_syst_down,false,Bfrac_systdown,Bweights_systdown);
    TemplateHist_syst_down = PrepareStack(TemplateStack_syst_down,TemplateHist_syst_down,Template_syst_down,true,Bfrac_systdown,Bweights_systdown);
    TemplateHist_uncor_syst_down = PrepareStack(TemplateStack_uncor_syst_down,TemplateHist_uncor_syst_down,Template_syst_down,false,Bfrac_systdown,Bweights_systdown);

    TFile *Templatefile = new TFile(("SystTemplates_"+Name+".root").c_str(),"RECREATE");
    Templatefile->cd();
    Templatefile->mkdir("Templates_systup");
    Templatefile->mkdir("Templates_systdown");
    Templatefile->cd("Templates_systup");
    //    TH1F *Mayor_cor_temp = (TH1F*) TemplateHist_syst_up->Clone("Mayor_cor_temp");
    //Mayor_cor_temp->Write();
    //TH1F *Mayor_uncor_temp = (TH1F*) TemplateHist_uncor_syst_up->Clone("Mayor_uncor_temp");
    //Mayor_uncor_temp->Write();
    TemplateHist_syst_up->Write();
    TemplateHist_uncor_syst_up->Write();
    AbsLikelihoodHist_syst_up->Write();
    AbsLikelihoodHist_uncorsample_syst_up->Write();
    AbsLikelihoodHist_uncorhyp_syst_up->Write();
    AbsLikelihoodHist_uncorhyp_uncorsample_syst_up->Write();
    Templatefile->cd();
    Templatefile->cd("Templates_systdown");
    TemplateHist_syst_down->Write();
    TemplateHist_uncor_syst_down->Write();
    AbsLikelihoodHist_syst_down->Write();
    AbsLikelihoodHist_uncorsample_syst_down->Write();
    AbsLikelihoodHist_uncorhyp_syst_down->Write();
    AbsLikelihoodHist_uncorhyp_uncorsample_syst_down->Write();
    Templatefile->Write();
    Templatefile->Close();
    delete Templatefile;
    Templatefile = NULL;
  }


  if(!UsingSyst){
  TLegend *leg = new TLegend(0.6773985,0.607473,0.9676871,0.848228,NULL,"brNDC");
  leg->SetFillColor(0);
  leg->SetFillStyle(0);
  leg->SetBorderSize(0);
  leg->AddEntry(&(*Template)[1],"t #bar{t} (signal,e)","f");
  leg->AddEntry(&(*Template)[3],"t #bar{t} (other,e)","f");
  leg->AddEntry(&(*Template)[8],"t, #bar{t}, t-channel ","f");
  leg->AddEntry(&(*Template)[10],"t, #bar{t}, s-channel ","f");
  leg->AddEntry(&(*Template)[12],"t, #bar{t}, tW-channel ","f");
  leg->AddEntry(&(*Template)[7],"DYJetsToLL","f");
  leg->AddEntry(&(*Template)[5],"W + Jets","f");//only fill one of the Wjets
  leg->AddEntry(&(*Template)[0],"Data","p");

  TH1F *SF_Abs = (TH1F*) (*AbsLik)[0].Clone("SF_Abs");
  std::cout<<"integral AbsLik data = "<<SF_Abs->Integral()<<std::endl;
  std::cout<<"integral AbsLik MC = "<<AbsLikelihoodHist->Integral()<<std::endl;
  SF_Abs->Divide(AbsLikelihoodHist);
  SF_Abs->GetYaxis()->SetTitle("Data/MC");

  TH1F *SF_Template = (TH1F*) (*Template)[0].Clone("SF_Template");
  std::cout<<"integral Template data = "<<SF_Template->Integral()<<std::endl;
  std::cout<<"integral Template MC = "<<TemplateHist->Integral()<<std::endl;
  SF_Template->Divide(TemplateHist);
  SF_Template->GetYaxis()->SetTitle("Data/MC");

  TH1F *SF_Abs_uncorsample = (TH1F*) (*AbsLik)[0].Clone("SF_Abs_uncorsample");
  std::cout<<"integral abs uncor data = "<<SF_Abs_uncorsample->Integral()<<std::endl;
  std::cout<<"integral abs uncor MC = "<<AbsLikelihoodHist_uncorsample->Integral()<<std::endl;
  SF_Abs_uncorsample->Divide(AbsLikelihoodHist_uncorsample);
  SF_Abs_uncorsample->GetYaxis()->SetTitle("Data/MC");

  TH1F *SF_Abs_uncorhyp = (TH1F*) (*AbsLikUncor)[0].Clone("SF_Abs_uncorhyp");
  std::cout<<"integral abs uncor data = "<<SF_Abs_uncorhyp->Integral()<<std::endl;
  std::cout<<"integral abs uncor MC = "<<AbsLikelihoodHist_uncorhyp->Integral()<<std::endl;
  SF_Abs_uncorhyp->Divide(AbsLikelihoodHist_uncorhyp);
  SF_Abs_uncorhyp->GetYaxis()->SetTitle("Data/MC");

  TH1F *SF_Abs_uncorhyp_uncorsample = (TH1F*) (*AbsLikUncor)[0].Clone("SF_Abs_uncorhyp_uncorsample");
  std::cout<<"integral abs uncor data = "<<SF_Abs_uncorhyp_uncorsample->Integral()<<std::endl;
  std::cout<<"integral abs uncor MC = "<<AbsLikelihoodHist_uncorhyp_uncorsample->Integral()<<std::endl;
  SF_Abs_uncorhyp_uncorsample->Divide(AbsLikelihoodHist_uncorhyp_uncorsample);
  SF_Abs_uncorhyp_uncorsample->GetYaxis()->SetTitle("Data/MC");

  TH1F *SF_Template_uncor = (TH1F*) (*Template)[0].Clone("SF_Template_uncor");
  std::cout<<"integral Template uncor data = "<<SF_Template_uncor->Integral()<<std::endl;
  std::cout<<"integral Template uncor MC = "<<TemplateHist_uncor->Integral()<<std::endl;
  SF_Template_uncor->Divide(TemplateHist_uncor);
  SF_Template_uncor->GetYaxis()->SetTitle("Data/MC");

  char temp[100];

  double ks = 0.;
  TCanvas *c_Abs = new TCanvas();
  c_Abs->cd();
  TPad *c1_Abs = new TPad("c1_Abs", "newpad",0.01,0.01,0.99,0.32);
  c1_Abs->Draw();
  c1_Abs->cd();
  gStyle->SetOptStat(000000);
  c1_Abs->SetTopMargin(0.1496599);
  c1_Abs->SetBottomMargin(0.1836735);
  c1_Abs->SetFillColor(kWhite);
  SF_Abs->GetYaxis()->SetRangeUser(0.5,1.5);
  SF_Abs->Draw();
  TLine *l_Abs = new TLine(40,1,100,1); 
  l_Abs->Draw("same");
  c1_Abs->Modified();
  c1_Abs->Update();
  c_Abs->cd();
  TPad *c2_Abs = new TPad("c2_Abs", "newpad",0.01,0.33,0.99,0.99);
  c2_Abs->SetFillColor(kWhite);
  c2_Abs->Draw();
  c2_Abs->cd();
  AbsLikelihoodStack->Draw("HIST");
  (*AbsLik)[0].Draw("same");
  ks = AbsLikelihoodHist->KolmogorovTest(&(*AbsLik)[0]);
  TPaveText *KS = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS->SetTextColor(kBlack);
  KS->SetFillColor(kWhite);
  KS->SetBorderSize(0);
  KS->SetTextAlign(12);
  KS->SetTextSize(0.03);
  sprintf(temp, "KS = %.5f", ks);
  KS->AddText(temp);
  KS->Draw();
  leg->Draw();
  c2_Abs->Modified();
  c2_Abs->Update();
  c_Abs->SaveAs(("AbsLikelihoodStack_"+Name+".C").c_str());
  c_Abs->SaveAs(("AbsLikelihoodStack_"+Name+".png").c_str());
  c_Abs->SaveAs(("AbsLikelihoodStack_"+Name+".pdf").c_str());

  TCanvas *c_Abs_uncorsample = new TCanvas();
  c_Abs_uncorsample->cd();
  TPad *c1_Abs_uncorsample = new TPad("c1_Abs_uncorsample", "newpad",0.01,0.01,0.99,0.32);
  c1_Abs_uncorsample->Draw();
  c1_Abs_uncorsample->cd();
  gStyle->SetOptStat(000000);
  c1_Abs_uncorsample->SetTopMargin(0.1496599);
  c1_Abs_uncorsample->SetBottomMargin(0.1836735);
  c1_Abs_uncorsample->SetFillColor(kWhite);
  SF_Abs_uncorsample->GetYaxis()->SetRangeUser(0.5,1.5);
  SF_Abs_uncorsample->Draw();
  //TLine *l_Abs = new TLine(40,1,100,1); 
  l_Abs->Draw("same");
  c1_Abs_uncorsample->Modified();
  c1_Abs_uncorsample->Update();
  c_Abs_uncorsample->cd();
  TPad *c2_Abs_uncorsample = new TPad("c2_Abs_uncorsample", "newpad",0.01,0.33,0.99,0.99);
  c2_Abs_uncorsample->SetFillColor(kWhite);
  c2_Abs_uncorsample->Draw();
  c2_Abs_uncorsample->cd();
  AbsLikelihoodStack_uncorsample->Draw("HIST");
  (*AbsLik)[0].Draw("same");
  ks = AbsLikelihoodHist_uncorsample->KolmogorovTest(&(*AbsLik)[0]);
  TPaveText *KS_uncorsample = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS_uncorsample->SetTextColor(kBlack);
  KS_uncorsample->SetFillColor(kWhite);
  KS_uncorsample->SetBorderSize(0);
  KS_uncorsample->SetTextAlign(12);
  KS_uncorsample->SetTextSize(0.03);
  sprintf(temp, "KS = %.5f", ks);
  KS_uncorsample->AddText(temp);
  KS_uncorsample->Draw();
  leg->Draw();
  c2_Abs_uncorsample->Modified();
  c2_Abs_uncorsample->Update();
  c_Abs_uncorsample->SaveAs(("AbsLikelihoodStackUncorSample_"+Name+".C").c_str());
  c_Abs_uncorsample->SaveAs(("AbsLikelihoodStackUncorSample_"+Name+".png").c_str());
  c_Abs_uncorsample->SaveAs(("AbsLikelihoodStackUncorSample_"+Name+".pdf").c_str());

  TCanvas *c_Abs_uncorhyp_uncorsample = new TCanvas();
  c_Abs_uncorhyp_uncorsample->cd();
  TPad *c1_Abs_uncorhyp_uncorsample = new TPad("c1_Abs_uncorhyp_uncorsample", "newpad",0.01,0.01,0.99,0.32);
  c1_Abs_uncorhyp_uncorsample->Draw();
  c1_Abs_uncorhyp_uncorsample->cd();
  gStyle->SetOptStat(000000);
  c1_Abs_uncorhyp_uncorsample->SetTopMargin(0.1496599);
  c1_Abs_uncorhyp_uncorsample->SetBottomMargin(0.1836735);
  c1_Abs_uncorhyp_uncorsample->SetFillColor(kWhite);
  SF_Abs_uncorhyp_uncorsample->GetYaxis()->SetRangeUser(0.5,1.5);
  SF_Abs_uncorhyp_uncorsample->Draw();
  //TLine *l_Abs = new TLine(40,1,100,1); 
  l_Abs->Draw("same");
  c1_Abs_uncorhyp_uncorsample->Modified();
  c1_Abs_uncorhyp_uncorsample->Update();
  c_Abs_uncorhyp_uncorsample->cd();
  TPad *c2_Abs_uncorhyp_uncorsample = new TPad("c2_Abs_uncorhyp_uncorsample", "newpad",0.01,0.33,0.99,0.99);
  c2_Abs_uncorhyp_uncorsample->SetFillColor(kWhite);
  c2_Abs_uncorhyp_uncorsample->Draw();
  c2_Abs_uncorhyp_uncorsample->cd();
  AbsLikelihoodStack_uncorhyp_uncorsample->Draw("HIST");
  (*AbsLikUncor)[0].Draw("same");
  ks = AbsLikelihoodHist_uncorhyp_uncorsample->KolmogorovTest(&(*AbsLikUncor)[0]);
  TPaveText *KS_uncorhyp_uncorsample = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS_uncorhyp_uncorsample->SetTextColor(kBlack);
  KS_uncorhyp_uncorsample->SetFillColor(kWhite);
  KS_uncorhyp_uncorsample->SetBorderSize(0);
  KS_uncorhyp_uncorsample->SetTextAlign(12);
  KS_uncorhyp_uncorsample->SetTextSize(0.03);
  sprintf(temp, "KS = %.5f", ks);
  KS_uncorhyp_uncorsample->AddText(temp);
  KS_uncorhyp_uncorsample->Draw();
  leg->Draw();
  c2_Abs_uncorhyp_uncorsample->Modified();
  c2_Abs_uncorhyp_uncorsample->Update();
  c_Abs_uncorhyp_uncorsample->SaveAs(("AbsLikelihoodStackUncorhyp_Uncorsample_"+Name+".C").c_str());
  c_Abs_uncorhyp_uncorsample->SaveAs(("AbsLikelihoodStackUncorhyp_Uncorsample_"+Name+".png").c_str());
  c_Abs_uncorhyp_uncorsample->SaveAs(("AbsLikelihoodStackUncorhyp_Uncorsample_"+Name+".pdf").c_str());

  TCanvas *c_Abs_uncorhyp = new TCanvas();
  c_Abs_uncorhyp->cd();
  TPad *c1_Abs_uncorhyp = new TPad("c1_Abs_uncorhyp", "newpad",0.01,0.01,0.99,0.32);
  c1_Abs_uncorhyp->Draw();
  c1_Abs_uncorhyp->cd();
  gStyle->SetOptStat(000000);
  c1_Abs_uncorhyp->SetTopMargin(0.1496599);
  c1_Abs_uncorhyp->SetBottomMargin(0.1836735);
  c1_Abs_uncorhyp->SetFillColor(kWhite);
  SF_Abs_uncorhyp->GetYaxis()->SetRangeUser(0.5,1.5);
  SF_Abs_uncorhyp->Draw();
  //TLine *l_Abs = new TLine(40,1,100,1); 
  l_Abs->Draw("same");
  c1_Abs_uncorhyp->Modified();
  c1_Abs_uncorhyp->Update();
  c_Abs_uncorhyp->cd();
  TPad *c2_Abs_uncorhyp = new TPad("c2_Abs_uncorhyp", "newpad",0.01,0.33,0.99,0.99);
  c2_Abs_uncorhyp->SetFillColor(kWhite);
  c2_Abs_uncorhyp->Draw();
  c2_Abs_uncorhyp->cd();
  AbsLikelihoodStack_uncorhyp->Draw("HIST");
  (*AbsLikUncor)[0].Draw("same");
  ks = AbsLikelihoodHist_uncorhyp->KolmogorovTest(&(*AbsLikUncor)[0]);
  TPaveText *KS_uncorhyp = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS_uncorhyp->SetTextColor(kBlack);
  KS_uncorhyp->SetFillColor(kWhite);
  KS_uncorhyp->SetBorderSize(0);
  KS_uncorhyp->SetTextAlign(12);
  KS_uncorhyp->SetTextSize(0.03);
  sprintf(temp, "KS = %.5f", ks);
  KS_uncorhyp->AddText(temp);
  KS_uncorhyp->Draw();
  leg->Draw();
  c2_Abs_uncorhyp->Modified();
  c2_Abs_uncorhyp->Update();
  c_Abs_uncorhyp->SaveAs(("AbsLikelihoodStackUncorhyp_"+Name+".C").c_str());
  c_Abs_uncorhyp->SaveAs(("AbsLikelihoodStackUncorhyp_"+Name+".png").c_str());
  c_Abs_uncorhyp->SaveAs(("AbsLikelihoodStackUncorhyp_"+Name+".pdf").c_str());

  TCanvas *c_Template = new TCanvas();
  c_Template->cd();
  TPad *c1_Template = new TPad("c1_Template", "newpad",0.01,0.01,0.99,0.32);
  c1_Template->Draw();
  c1_Template->cd();
  gStyle->SetOptStat(000000);
  c1_Template->SetTopMargin(0.1496599);
  c1_Template->SetBottomMargin(0.1836735);
  c1_Template->SetFillColor(kWhite);
  SF_Template->GetYaxis()->SetRangeUser(0.5,1.5);
  SF_Template->Draw();
  TLine *l_Template = new TLine(-7,1,7,1); 
  l_Template->Draw("same");
  c1_Template->Modified();
  c1_Template->Update();
  c_Template->cd();
  TPad *c2_Template = new TPad("c2_Template", "newpad",0.01,0.33,0.99,0.99);
  c2_Template->SetFillColor(kWhite);
  c2_Template->Draw();
  c2_Template->cd();
  TemplateStack->Draw("HIST");
  (*Template)[0].Draw("same");
  ks = TemplateHist->KolmogorovTest(&(*Template)[0]);
  TPaveText *KS2 = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS2->SetTextColor(kBlack);
  KS2->SetFillColor(kWhite);
  KS2->SetBorderSize(0);
  KS2->SetTextAlign(12);
  KS2->SetTextSize(0.03);
  sprintf(temp, "KS = %.5f", ks);
  KS2->AddText(temp);
  KS2->Draw();
  leg->Draw("same");
  c2_Template->Modified();
  c2_Template->Update();
  c_Template->SaveAs(("TemplateStack_"+Name+".C").c_str());
  c_Template->SaveAs(("TemplateStack_"+Name+".png").c_str());
  c_Template->SaveAs(("TemplateStack_"+Name+".pdf").c_str());

  TCanvas *c_Template_uncor = new TCanvas();
  c_Template_uncor->cd();
  TPad *c1_Template_uncor = new TPad("c1_Template_uncor", "newpad",0.01,0.01,0.99,0.32);
  c1_Template_uncor->Draw();
  c1_Template_uncor->cd();
  gStyle->SetOptStat(000000);
  c1_Template_uncor->SetTopMargin(0.1496599);
  c1_Template_uncor->SetBottomMargin(0.1836735);
  c1_Template_uncor->SetFillColor(kWhite);
  SF_Template_uncor->GetYaxis()->SetRangeUser(0.5,1.5);
  SF_Template_uncor->Draw();
  TLine *l_Template_uncor = new TLine(-7,1,7,1); 
  l_Template_uncor->Draw("same");
  c1_Template_uncor->Modified();
  c1_Template_uncor->Update();
  c_Template_uncor->cd();
  TPad *c2_Template_uncor = new TPad("c2_Template_uncor", "newpad",0.01,0.33,0.99,0.99);
  c2_Template_uncor->SetFillColor(kWhite);
  c2_Template_uncor->Draw();
  c2_Template_uncor->cd();
  TemplateStack_uncor->Draw("HIST");
  (*Template)[0].Draw("same");
  ks = TemplateHist_uncor->KolmogorovTest(&(*Template)[0]);
  TPaveText *KS2_uncor = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
  KS2_uncor->SetTextColor(kBlack);
  KS2_uncor->SetFillColor(kWhite);
  KS2_uncor->SetBorderSize(0);
  KS2_uncor->SetTextAlign(12);
  KS2_uncor->SetTextSize(0.03);
  sprintf(temp, "KS = %.5f", ks);
  KS2_uncor->AddText(temp);
  KS2_uncor->Draw();
  leg->Draw("same");
  c2_Template_uncor->Modified();
  c2_Template_uncor->Update();
  c_Template_uncor->SaveAs(("TemplateStackUncor_"+Name+".C").c_str());
  c_Template_uncor->SaveAs(("TemplateStackUncor_"+Name+".png").c_str());
  c_Template_uncor->SaveAs(("TemplateStackUncor_"+Name+".pdf").c_str());

  /*
  //to store nominal templates in root file
  TFile *Templatefile = new TFile("NominalTemplates_Muon_combined.root","RECREATE");
  Templatefile->cd();
  Templatefile->mkdir("Templates");
  Templatefile->cd("Templates");
  TH1F *Mayor_cor_temp = (TH1F*) TemplateHist->Clone("Mayor_cor_temp");
  Mayor_cor_temp->Write();
  TH1F *Mayor_uncor_temp = (TH1F*) TemplateHist_uncor->Clone("Mayor_uncor_temp");
  Mayor_uncor_temp->Write();
  TH1F *Mayor_AbsLik_temp = (TH1F*)  AbsLikelihoodHist->Clone("Mayor_AbsLik_cor_temp");
  Mayor_AbsLik_temp->Write();
  TH1F *Mayor_AbsLik_uncorsample_temp = (TH1F*)  AbsLikelihoodHist_uncorsample->Clone("Mayor_AbsLik_uncorsample_temp");
  Mayor_AbsLik_uncorsample_temp->Write();
  TH1F *Mayor_AbsLik_uncorhyp_uncorsample_temp = (TH1F*)  AbsLikelihoodHist_uncorhyp_uncorsample->Clone("Mayor_AbsLik_uncorhyp_uncorsample_temp");
  Mayor_AbsLik_uncorhyp_uncorsample_temp->Write();
  TH1F *Mayor_AbsLik_uncorhyp_temp = (TH1F*)  AbsLikelihoodHist_uncorhyp->Clone("Mayor_AbsLik_uncorhyp_temp");
  Mayor_AbsLik_uncorhyp_temp->Write();
  Templatefile->Write();
  Templatefile->Close();
  delete Templatefile;
  Templatefile = NULL;
  */
  }
}


//Make a systematic fluctuation of the template
TH1F * eire::HypTesting::ReDrawTemplate(TH1F hist_nom, TH1F hist_syst_up, TH1F hist_syst_down, string name)
{
  TH1F *hist_new = (TH1F*) hist_nom.Clone(name.c_str());
  //double mean;
  double rms_up;
  double rms_down;
  TF1 *gauss = new TF1("gauss","gaus(0)"); 
  gauss->SetParameter(0,1);
  gauss->SetParameter(1,0);
  gauss->SetParameter(2,1);
  //the random variation counts for each bin to take the correlation between bins into account
  double x = 0.;

  x = gauss->GetRandom();

  for(int i = 1; i <= hist_nom.GetNbinsX(); i++){
    rms_up = hist_syst_up.GetBinContent(i+1) - hist_nom.GetBinContent(i+1);
    rms_down = hist_syst_down.GetBinContent(i+1) - hist_nom.GetBinContent(i+1);
    double dev = 0.;
    if(x > 0){dev = rms_up;}
    else{dev = rms_down;}
    double entries = hist_nom.GetBinContent(i+1) + fabs(x)*dev;
    if(entries < 0){entries = 0;}
    hist_new->SetBinContent(i+1,entries);
  }

  delete gauss;

  return hist_new;
}

//Make a systematic fluctuation of the template
TH1F * eire::HypTesting::ReDrawTemplateMass(TH1F hist_nom, TH1F hist_syst_up, TH1F hist_syst_down, string name)
{
  TH1F *hist_new = (TH1F*) hist_nom.Clone(name.c_str());
  //double mean;
  double rms_up;
  double rms_down;
  TF1 *gauss = new TF1("gauss","gaus(0)"); 
  gauss->SetParameter(0,1);
  gauss->SetParameter(1,0);
  gauss->SetParameter(2,1);
  //the random variation counts for each bin to take the correlation between bins into account
  double x = 0.;

  x = gauss->GetRandom();

  for(int i = 1; i <= hist_nom.GetNbinsX(); i++){
    rms_up = hist_syst_up.GetBinContent(i+1) - hist_nom.GetBinContent(i+1);
    rms_down = hist_syst_down.GetBinContent(i+1) - hist_nom.GetBinContent(i+1);
    double dev = 0.;
    if(x > 0){dev = rms_up;}
    else{dev = rms_down;}
    double entries = hist_nom.GetBinContent(i+1) + fabs(x)*dev*1./2.5;
    if(entries < 0){entries = 0;}
    hist_new->SetBinContent(i+1,entries);
  }

  delete gauss;

  return hist_new;
}

//Make a systematic fluctuation of the template
TH1F * eire::HypTesting::ReDrawTemplateMW(TH1F hist_nom, TH1F hist_syst_up, TH1F hist_syst_down, string name)
{
  TH1F *hist_new = (TH1F*) hist_nom.Clone(name.c_str());
  //double mean;
  double rms_up;
  double rms_down;
  TF1 *gauss = new TF1("gauss","gaus(0)",-1,1); 
  gauss->SetParameter(0,1);
  gauss->SetParameter(1,0);
  gauss->SetParameter(2,1);
  //the random variation counts for each bin to take the correlation between bins into account
  double x = 0.;

  for(int i = 1; i <= hist_nom.GetNbinsX(); i++){
    x = gauss->GetRandom();
    rms_up = hist_syst_up.GetBinContent(i+1) - hist_nom.GetBinContent(i+1);
    rms_down = hist_syst_down.GetBinContent(i+1) - hist_nom.GetBinContent(i+1);
    double dev = 0.;
    if(x > 0){dev = rms_up;}
    else{dev = rms_down;}
    double entries = hist_nom.GetBinContent(i+1) + fabs(x)*dev;
    if(entries < 0){entries = 0;}
    hist_new->SetBinContent(i+1,entries);
  }

  delete gauss;

  return hist_new;
}

//Make a statistical fluctuation of the template
TH1F * eire::HypTesting::ReDrawTemplate(TH1F hist, string name)
{
  TH1F *hist_new = (TH1F*) hist.Clone(name.c_str());
  double mean;
  double rms;
  for(int i = 0; i < hist.GetNbinsX(); i++){
    if(hist.GetBinContent(i+1) == 0){continue;}
    mean = hist.GetBinContent(i+1);    
    rms = hist.GetBinError(i+1);
    TF1 *gauss = new TF1("gauss","gaus(0)",mean-rms,mean+rms); 
    gauss->SetParameter(0,1);
    gauss->SetParameter(1,mean);
    gauss->SetParameter(2,rms);
    double entries = gauss->GetRandom();
    if(entries < 0){entries = 0;}
    hist_new->SetBinContent(i+1,entries);
    delete gauss;
  }

  hist_new->Scale(1./hist_new->Integral());
  return hist_new;
}


//Draw the pseudo-experiments to verify the scaling and make the fit of the gaussian mean and rms
void eire::HypTesting::DrawSeparation(bool UsingSyst)
{

  std::cout<<"background fraction is = "<<Bfrac<<std::endl;

  float N_bin[15];
  float binning[15];
  float low_x[15];
  float high_x[15];

  //optimised binning for the Sample Likelihood plots for each tested Data-set size
  float N_bin_e_minus[15] = {50,100,200,350,500,750,1000,1500,3000,5000,7500,10000,15000,20000,30000};
  float binning_e_minus[15] = {20,30,40,50,50,50,50,50,100,100,100,100,125,125,125};
  float low_x_e_minus[15] = {-30,-20,-10,0,0,25,80,150,250,580,800,1150,1800,2400,2500};
  float high_x_e_minus[15] = {30,60,110,100,150,175,325,450,650,950,1500,1800,2700,3500,3600};
  
  float N_bin_e_plus[15] = {50,100,200,350,500,750,1000,1500,3000,5000,7500,10000,15000,20000,30000};
  float binning_e_plus[15] = {20,30,40,50,50,50,50,50,100,100,100,100,125,125,125};
  float low_x_e_plus[15] = {-30,-20,-20,-20,-10,-15,-15,0,50,125,200,400,600,900,1000};
  float high_x_e_plus[15] = {30,40,50,80,175,175,225,400,600,900,1100,1600,1600,2100,2200};  

  float N_bin_mu_minus[15] = {50,100,200,350,500,750,1000,1500,3000,5000,7500,10000,15000,20000,18983};
  float binning_mu_minus[15] = {20,30,30,40,40,50,50,50,100,100,100,100,100,100,100};
  //mu neg all jets (chi2 < 5)
  float low_x_mu_minus[15] = {-10,-10,0,50,75,100,150,250,600,1000,1500,2100,3200,4500,4000};
  float high_x_mu_minus[15] = {30,50,90,150,175,250,350,500,1000,1500,2200,3000,4600,5500,5500};
  
  float N_bin_mu_plus[15] = {50,100,200,350,500,750,1000,1500,3000,5000,7500,10000,15000,20000,19746};
  float binning_mu_plus[15] = {20,30,45,55,55,55,55,55,75,80,80,100,100,100,100};
  //  float low_x_mu_plus[15] = {-30,-20,-20,-30,-20,-15,-15,0,50,125,250,300,600,900};
  //float high_x_mu_plus[15] = {30,40,50,80,100,150,170,225,400,600,850,1100,1600,2000};  
  //mu pos all jets (chi2 < 4)
  float low_x_mu_plus[15] = {-10,-0,15,50,70,100,160,260,550,1000,1500,2000,3100,4200,4000};
  float high_x_mu_plus[15] = {30,50,80,130,180,250,310,470,850,1500,2100,2700,4000,5300,5500};  
  //mu pos all jets (chi2 < 5)
  //float low_x_mu_plus[15] = {-10,-0,15,50,70,100,160,260,550,1000,1500,2000,3100,4200};
  //float high_x_mu_plus[15] = {30,50,80,130,180,250,310,470,850,1500,2100,2700,4000,5300};  
  //mu pos all jets (chi2 < 10)
  //float low_x_mu_plus[15] = {-10,-0,10,40,40,100,150,240,525,900,1500,1900,2900,4000};
  //float high_x_mu_plus[15] = {30,50,80,120,170,250,290,430,800,1350,2000,2600,3800,5000};  
  //mu pos all jets (chi2 < 10) without veto
  //float low_x_mu_plus[15] = {-10,-0,10,40,40,100,150,240,525,900,1500,1900,2900,4000};
  //float high_x_mu_plus[15] = {30,50,80,120,170,250,290,430,800,1350,2000,2600,3800,5000};  
  //mu pos all jets (chi2 < 15)
  //float low_x_mu_plus[15] = {-10,-0,0,20,40,100,140,220,475,750,1300,1750,2700,3600};
  //float high_x_mu_plus[15] = {30,50,70,110,160,250,290,400,775,1300,1900,2400,3600,4600};  
  //mu pos all jets (chi2 < 20)
  //float low_x_mu_plus[14] = {-10,-0,0,20,40,70,110,200,425,750,1200,1600,2400,3400};
  //float high_x_mu_plus[15] = {30,50,70,110,160,210,260,370,700,1150,1700,2200,3300,4300};  
  //mu pos all jets veto
  //float low_x_mu_plus[15] = {-30,-20,-20,-30,-20,-15,-15,0,50,125,250,350,650,950};
  //float high_x_mu_plus[15] = {30,40,50,80,100,140,170,225,400,650,850,1100,1500,2000};  
  //mu pos 5 jets
  //float low_x_mu_plus[15] = {-30,-20,-10,0,10,20,40,100,250,400,700,1000,1400,2000};
  //float high_x_mu_plus[15] = {30,40,60,90,120,160,190,275,550,800,1400,1600,2200,3000};  
  //mu pos 5 jets extended
  //float low_x_mu_plus[15] = {-30,-20,-10,0,10,20,40,100,250,450,700,1000,1600,2100};
  //float high_x_mu_plus[15] = {30,40,60,90,120,160,210,290,550,850,1300,1600,2400,3100};  
  //mu pos 4 jets
  //float low_x_mu_plus[15] = {-30,-30,-30,-50,-60,-75,-75,-75,-90,-110,-175,-200,-200,-200};
  //float high_x_mu_plus[15] = {30,30,40,50,70,80,100,120,190,250,450,450,650,800};  

  float N_bin_mu_combined[15] = {50,100,200,350,500,750,1000,1500,3000,5000,7500,10000,15000,20000,38269};
  float binning_mu_combined[15] = {20,30,30,30,30,30,30,30,50,50,50,50,60,60,70};
  //mu all jets (chi2 < 5)
  float low_x_mu_combined[15] = {-10,-10,0,50,75,100,150,250,600,1000,1500,2100,3200,4200,8000};
  float high_x_mu_combined[15] = {30,50,90,150,175,250,350,500,1000,1500,2200,3000,4600,5500,12000};

  //set the corresponding binning if in e+ or e- channel
  if(channel == "e_pos"){  
    //e_plus
    for(int i = 0; i < 15; i++){
      N_bin[i] = N_bin_e_plus[i];
      binning[i] = binning_e_plus[i];
      low_x[i] = low_x_e_plus[i];
      high_x[i] = high_x_e_plus[i];
    }
  }else if(channel == "e_neg"){
    for(int i = 0; i < 15; i++){
      N_bin[i] = N_bin_e_minus[i];
      binning[i] = binning_e_minus[i];
      low_x[i] = low_x_e_minus[i];
      high_x[i] = high_x_e_minus[i];
    }
  }else if(channel == "mu_pos"){
    for(int i = 0; i < 15; i++){
      N_bin[i] = N_bin_mu_plus[i];
      binning[i] = binning_mu_plus[i];
      low_x[i] = low_x_mu_plus[i];
      high_x[i] = high_x_mu_plus[i];
    }
  }else if(channel == "mu_neg"){
    for(int i = 0; i < 15; i++){
      N_bin[i] = N_bin_mu_minus[i];
      binning[i] = binning_mu_minus[i];
      low_x[i] = low_x_mu_minus[i];
      high_x[i] = high_x_mu_minus[i];
    }
  }else if(channel == "mu_combined"){
    for(int i = 0; i < 15; i++){
      N_bin[i] = N_bin_mu_combined[i];
      binning[i] = binning_mu_combined[i];
      low_x[i] = low_x_mu_combined[i];
      high_x[i] = high_x_mu_combined[i];
    }
  }

  //for each sample size, store the mean and RMS of the distribution, and the errors on them
  float Mean_cor[15];
  float RMS_cor[15];
  float Mean_uncor[15];
  float RMS_uncor[15];
  float MeanErr_cor[15];
  float RMSErr_cor[15];
  float MeanErr_uncor[15];
  float RMSErr_uncor[15];
  float separation[15];
  float separation_err[15];

  //print out the weights for the background
  for(unsigned int i = 0; i < Bweights.size(); i++){
    std::cout<<"weight "<<i<<" = "<<(Bweights)[i]<<std::endl;
  }

  char meancorname[2000], meanuncorname[2000];
  char sigmacorname[2000], sigmauncorname[2000];

  double cor_mean[15] = {12,26,50,89,128,192,257,385,768,1279,1921,2562,3843,5122,5122};
  double uncor_mean[15] = {11,24,46,81,118,177,234,352,704,1173,1761,2342,3522,4688,4688};
  double cor_sigma[15] = {2.7,4.5,6.0,7.8,9.7,12.2,13.7,16.5,23.0,28.0,39.2,39.9,50.4,57.7,57.7};
  double uncor_sigma[15] = {3.0,4.2,5.7,7.6,9.8,11.7,12.9,16.5,23.3,30.7,36.2,42.5,52.0,57.5,57.5};

  for (int j = 0; j < 15; j++){
    sprintf(meancorname,"correlatedMean_%iEv",(int) N_bin[j]);
    sprintf(meanuncorname,"uncorrelatedMean_%iEv",(int) N_bin[j]);
    sprintf(sigmacorname,"correlatedSigma_%iEv",(int) N_bin[j]);
    sprintf(sigmauncorname,"uncorrelatedSigma_%iEv",(int) N_bin[j]);

    //store the sample likelihoods for the pseudo-experiments
    TH1F* MeanCorHist = new TH1F(meancorname,"Gaussian Mean; Gaussian Mean; entries", 300, cor_mean[j]-50, cor_mean[j]+50);
    TH1F* MeanUnCorHist = new TH1F(meanuncorname,"Gaussian Mean; Gaussian Mean; entries", 300, uncor_mean[j]-50, uncor_mean[j]+50);

    TH1F* SigmaCorHist = new TH1F(sigmacorname,"Gaussian Sigma; Gaussian Sigma; entries", 200, cor_sigma[j]-7, cor_sigma[j]+7);
    TH1F* SigmaUnCorHist = new TH1F(sigmauncorname,"Gaussian Sigma; Gaussian Sigma; entries", 200, uncor_sigma[j]-7, uncor_sigma[j]+7);

    StatVariedMean_cor->push_back(*MeanCorHist);
    StatVariedMean_uncor->push_back(*MeanUnCorHist);
    StatVariedSigma_cor->push_back(*SigmaCorHist);
    StatVariedSigma_uncor->push_back(*SigmaUnCorHist);

    delete MeanCorHist;
    delete MeanUnCorHist;
    delete SigmaCorHist;
    delete SigmaUnCorHist;
  }

  TFile *Nom_file = new TFile("/afs/cern.ch/work/k/ksbeerna/UGentTopQuark/SpinCorrelation/NominalTemplates_Muon_combined.root","OPEN");
  TH1F *hist_nom_cor = (TH1F*) (Nom_file->GetDirectory("Templates")->Get("Mayor_cor_temp"))->Clone();
  TH1F *hist_nom_uncor = (TH1F*) (Nom_file->GetDirectory("Templates")->Get("Mayor_uncor_temp"))->Clone();
			    
  TFile *TemplateFlucfile = new TFile(("SystTemplatesFluctuations_"+Name+".root").c_str(),"RECREATE");
  TemplateFlucfile->cd();
  TemplateFlucfile->mkdir("SampleGaussians");
  TemplateFlucfile->cd("SampleGaussians");
  //TemplateFlucfile->mkdir("Templates_fluc_uncor");

  TCanvas *c_fluc = new TCanvas();
  c_fluc->cd();
  hist_nom_cor->SetLineColor(kBlack);
  hist_nom_cor->SetLineWidth(2);
  hist_nom_cor->SetFillColor(kWhite);
  hist_nom_cor->SetMarkerColor(kBlack);
  hist_nom_cor->Draw("h");
  hist_nom_uncor->SetLineColor(kBlack);
  hist_nom_uncor->SetLineWidth(2);
  hist_nom_uncor->SetFillColor(kWhite);
  hist_nom_uncor->SetLineStyle(2);
  hist_nom_uncor->SetMarkerColor(kBlack);
  hist_nom_uncor->Draw("hsame");

  char sampleCorname[2000], sampleUnCorname[2000];

  TH1F *Datasize_Cor_fullup = new TH1F("SampleLikCor_Datasize_fullup","sample likelihood",70,8000,12000);
  TH1F *Datasize_Uncor_fullup = new TH1F("SampleLikUnCor_Datasize_fullup","sample likelihood",70,8000,12000);
  TH1F *Datasize_Cor_fulldown = new TH1F("SampleLikCor_Datasize_fulldown","sample likelihood",70,8000,12000);
  TH1F *Datasize_Uncor_fulldown = new TH1F("SampleLikUnCor_Datasize_fulldown","sample likelihood",70,8000,12000);

  for(int k = 0; k < 1000; k++){
    double sample_cor_fullup = 0.;
    double sample_cor_fulldown = 0.;
    double sample_uncor_fullup = 0.;
    double sample_uncor_fulldown = 0.;
    for(int z = 0; z < Dataset_size; z++){
      sample_cor_fullup += TemplateHist_syst_up->GetRandom();
      sample_uncor_fullup += TemplateHist_uncor_syst_up->GetRandom();
      sample_cor_fulldown +=  TemplateHist_syst_down->GetRandom();
      sample_uncor_fulldown += TemplateHist_syst_down->GetRandom();
    }
    Datasize_Cor_fullup->Fill(sample_cor_fullup);
    Datasize_Uncor_fullup->Fill(sample_uncor_fullup);
    Datasize_Cor_fulldown->Fill(sample_cor_fulldown);
    Datasize_Uncor_fulldown->Fill(sample_uncor_fulldown);
  }

  //  Datasize_Cor_fullup->Write();
  //Datasize_Uncor_fullup->Write();
  //Datasize_Cor_fulldown->Write();
  //Datasize_Uncor_fulldown->Write();

  //statistical fluctuations/stystematic variation
  //  for(int k = 0; k < 750; k++){
    int k = 0;
    std::cout<<"syst variation "<<k<<std::endl;

    sprintf(sampleCorname,"SampleLikCor_Datasize_%i",k);
    sprintf(sampleUnCorname,"SampleLikUnCor_Datasize_%i",k);

    TH1F *Datasize_Cor = new TH1F(sampleCorname,"sample likelihood",70,8000,12000);
    TH1F *Datasize_Uncor = new TH1F(sampleUnCorname,"sample likelihood",70,8000,12000);

    /*
      int k = 0;
      
      stringstream cor_name;
      cor_name<<"cor_"<<k;
      stringstream uncor_name;
      uncor_name<<"uncor_"<<k;
      stringstream Bcor_name;
      Bcor_name<<"Bcor_"<<k;
      stringstream Buncor_name;
      Buncor_name<<"Buncor_"<<k;
      stringstream W3_name;
      W3_name<<"W3_"<<k;
      stringstream W4_name;
      W4_name<<"W4_"<<k;
      stringstream DY_name;
      DY_name<<"DY_"<<k;
      stringstream Ts_name;
      Ts_name<<"Ts_"<<k;
      stringstream Tbars_name;
      Tbars_name<<"Tbars_"<<k;
      stringstream Tt_name;
      Tt_name<<"Tt_"<<k;
      stringstream Tbart_name;
      Tbart_name<<"Tbart_"<<k;
      stringstream TtW_name;
      TtW_name<<"TtW_"<<k;
      stringstream TbartW_name;
      TbartW_name<<"TbartW_"<<k;
      
      
      TH1F *cor_temp_minus = &cor_template_minus;
      TH1F *uncor_temp_minus = &uncor_template_minus;
      TH1F *B_cor_temp_minus = &B_cor_template_minus;
      TH1F *B_uncor_temp_minus = &B_uncor_template_minus;
      TH1F *W3Jets_temp_minus = &W3Jets_template_minus;
      TH1F *W4Jets_temp_minus = &W4Jets_template_minus;
      TH1F *DY_temp_minus = &DY_template_minus;
      TH1F *TsChan_temp_minus = &TsChan_template_minus;
      TH1F *TbarsChan_temp_minus = &TbarsChan_template_minus;
      TH1F *TtChan_temp_minus = &TtChan_template_minus;
      TH1F *TbartChan_temp_minus = &TbartChan_template_minus;
      TH1F *TtWChan_temp_minus = &TtWChan_template_minus;
      TH1F *TbartWChan_temp_minus = &TbartWChan_template_minus;
      TH1F *cor_temp_plus = &cor_template_plus;
      TH1F *uncor_temp_plus = &uncor_template_plus;
      TH1F *B_cor_temp_plus = &B_cor_template_plus;
      TH1F *B_uncor_temp_plus = &B_uncor_template_plus;
      TH1F *W3Jets_temp_plus = &W3Jets_template_plus;
      TH1F *W4Jets_temp_plus = &W4Jets_template_plus;
      TH1F *DY_temp_plus = &DY_template_plus;
      TH1F *TsChan_temp_plus = &TsChan_template_plus;
      TH1F *TbarsChan_temp_plus = &TbarsChan_template_plus;
      TH1F *TtChan_temp_plus = &TtChan_template_plus;
      TH1F *TbartChan_temp_plus = &TbartChan_template_plus;
      TH1F *TtWChan_temp_plus = &TtWChan_template_plus;
      TH1F *TbartWChan_temp_plus = &TbartWChan_template_plus;
    */
    
    //cor_temp_minus = ReDrawTemplate(cor_template_minus,cor_name.str());
    //uncor_temp_minus = ReDrawTemplate(uncor_template_minus,uncor_name.str());
    //B_cor_temp_minus = ReDrawTemplate(B_cor_template_minus,Bcor_name.str());
    //B_uncor_temp_minus = ReDrawTemplate(B_uncor_template_minus,Buncor_name.str());
    //W3Jets_temp_minus = ReDrawTemplate(W3Jets_template_minus,W3_name.str());
    //W4Jets_temp_minus = ReDrawTemplate(W4Jets_template_minus,W4_name.str());
    //DY_temp_minus = ReDrawTemplate(DY_template_minus,DY_name.str());
    //      TsChan_temp_minus = ReDrawTemplate(TsChan_template_minus,Ts_name.str());
    //      TbarsChan_temp_minus = ReDrawTemplate(TbarsChan_template_minus,Tbars_name.str());
    //      TtChan_temp_minus = ReDrawTemplate(TtChan_template_minus,Tt_name.str());
    //      TbartChan_temp_minus = ReDrawTemplate(TbartChan_template_minus,Tbart_name.str());
    //TtWChan_temp_minus = ReDrawTemplate(TtWChan_template_minus,TtW_name.str());
    //      TbartWChan_temp_minus = ReDrawTemplate(TbartWChan_template_minus,TbartW_name.str());
    
    //      cor_temp_plus = ReDrawTemplate(cor_template_plus,cor_name.str());
    //      uncor_temp_plus = ReDrawTemplate(uncor_template_plus,uncor_name.str());
    //      B_cor_temp_plus = ReDrawTemplate(B_cor_template_plus,Bcor_name.str());
    //      B_uncor_temp_plus = ReDrawTemplate(B_uncor_template_plus,Buncor_name.str());
    //      W3Jets_temp_plus = ReDrawTemplate(W3Jets_template_plus,W3_name.str());
    //      W4Jets_temp_plus = ReDrawTemplate(W4Jets_template_plus,W4_name.str());
    //      DY_temp_plus = ReDrawTemplate(DY_template_plus,DY_name.str());
    //      TsChan_temp_plus = ReDrawTemplate(TsChan_template_plus,Ts_name.str());
    //      TbarsChan_temp_plus = ReDrawTemplate(TbarsChan_template_plus,Tbars_name.str());
    //      TtChan_temp_plus = ReDrawTemplate(TtChan_template_plus,Tt_name.str());
    //      TbartChan_temp_plus = ReDrawTemplate(TbartChan_template_plus,Tbart_name.str());
    //      TtWChan_temp_plus = ReDrawTemplate(TtWChan_template_plus,TtW_name.str());
    //      TbartWChan_temp_plus = ReDrawTemplate(TbartWChan_template_plus,TbartW_name.str());

    TH1F* Cor_template;
    TH1F* Uncor_template;

    stringstream naam_cor_syst;
    naam_cor_syst<<"Cor_"<<k;
    stringstream naam_uncor_syst;
    naam_uncor_syst<<"Uncor_"<<k;
    
    //    if(!UsingSyst){
      Cor_template = TemplateHist;
      Uncor_template = TemplateHist_uncor;
      //}else{
      //      TemplateFlucfile->cd("Template_fluc_cor");
      //Cor_template = ReDrawTemplateMass(*hist_nom_cor, *TemplateHist_syst_up, *TemplateHist_syst_down, naam_cor_syst.str());
      //Cor_template = ReDrawTemplate(*hist_nom_cor, *TemplateHist_syst_up, *TemplateHist_syst_down, naam_cor_syst.str());
      //Cor_template = ReDrawTemplate(*hist_nom_cor, naam_cor_syst.str());
      Cor_template->SetLineColor(kRed);
      Cor_template->SetLineWidth(1);
      Cor_template->SetMarkerColor(kRed);
      Cor_template->Draw("psame");
      //Cor_template->Write();
      //TemplateFlucfile->cd();
      //TemplateFlucfile->cd("Template_fluc_uncor");
      //Uncor_template = ReDrawTemplateMass(*hist_nom_uncor, *TemplateHist_uncor_syst_up, *TemplateHist_uncor_syst_down, naam_uncor_syst.str());
      //Uncor_template = ReDrawTemplate(*hist_nom_uncor, *TemplateHist_uncor_syst_up, *TemplateHist_uncor_syst_down, naam_uncor_syst.str());
      //Uncor_template = ReDrawTemplate(*hist_nom_uncor, naam_uncor_syst.str());
      //Uncor_template->Write();
      Uncor_template->SetLineColor(kRed);
      Uncor_template->SetLineWidth(1);
      Uncor_template->SetLineStyle(2);
      Uncor_template->SetMarkerColor(kRed);
      Uncor_template->Draw("psame");
      
      double sample_cor = 0.;
      double sample_uncor = 0.;
      /*
      for(int i = 0; i < 1000; i++){
	for(int z = 0; z < Dataset_size; z++){
	  sample_cor += Cor_template->GetRandom();
	  sample_uncor += Uncor_template->GetRandom();
	}
	Datasize_Cor->Fill(sample_cor);
	Datasize_Uncor->Fill(sample_uncor);
	sample_cor = 0.;
	sample_uncor = 0.;
      }
      Datasize_Cor->Write();
      Datasize_Uncor->Write();
      */
      //  }
    

    //draw pseudo-experiments for the 15 different sample sizes
    for(int j = 0; j < 15; j++){
      int N = N_bin[j];
      
      //store the sample likelihoods for the pseudo-experiments
      TH1F* CorSampleLik = new TH1F("Correlated","Sample -2ln#lambda; -2ln#lambda; entries", binning[j], low_x[j], high_x[j]);
      TH1F* UnCorSampleLik = new TH1F("Uncorrelated","Sample -2ln#lambda; -2ln#lambda; entries", binning[j], low_x[j], high_x[j]);
      
      //print out the absolute number of events for each background contribution for this sample size
      //results are not rounded off
      
      std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
      std::cout<<"background fraction = "<<Bfrac<<std::endl;
      std::cout<<"Sample size = "<<N<<std::endl;
      std::cout<<"signal  = "<<(1.-Bfrac)*N*(SigWeights)[0]<<std::endl;
      std::cout<<"TT other = "<<Bfrac*N*(Bweights)[0]<<std::endl;
      std::cout<<"W3Jets = "<<Bfrac*N*(Bweights)[1]<<std::endl;
      std::cout<<"W4Jets = "<<Bfrac*N*(Bweights)[2]<<std::endl;
      std::cout<<"DY = "<<Bfrac*N*(Bweights)[3]<<std::endl;
      std::cout<<"TsChan = "<<Bfrac*N*(Bweights)[4]<<std::endl;
      std::cout<<"TtChan = "<<Bfrac*N*(Bweights)[5]<<std::endl;
      std::cout<<"TtWChan = "<<Bfrac*N*(Bweights)[6]<<std::endl;
      std::cout<<"TbarsChan = "<<Bfrac*N*(Bweights)[7]<<std::endl;
      std::cout<<"TbartChan = "<<Bfrac*N*(Bweights)[8]<<std::endl;
      std::cout<<"TbartWChan = "<<Bfrac*N*(Bweights)[9]<<std::endl;
      //if we combine e+ and e- channel, show the same for the e+ samples
      if(Bweights.size() > 10){
      std::cout<<"signal - = "<<(1.-Bfrac)*N*(SigWeights)[1]<<std::endl;
      std::cout<<"TT other - = "<<Bfrac*N*(Bweights)[10]<<std::endl;
      std::cout<<"W3Jets - = "<<Bfrac*N*(Bweights)[11]<<std::endl;
      std::cout<<"W4Jets - = "<<Bfrac*N*(Bweights)[12]<<std::endl;
      std::cout<<"DY - = "<<Bfrac*N*(Bweights)[13]<<std::endl;
      std::cout<<"TsChan - = "<<Bfrac*N*(Bweights)[14]<<std::endl;
      std::cout<<"TtChan - = "<<Bfrac*N*(Bweights)[15]<<std::endl;
      std::cout<<"TtWChan - = "<<Bfrac*N*(Bweights)[16]<<std::endl;
      std::cout<<"TbarsChan - = "<<Bfrac*N*(Bweights)[17]<<std::endl;
      std::cout<<"TbartChan - = "<<Bfrac*N*(Bweights)[18]<<std::endl;
      std::cout<<"TbartWChan - = "<<Bfrac*N*(Bweights)[19]<<std::endl;
      }
      
      
      //do 400 pseudo-experiments per sample size
      for(int i = 0; i < 1000; i++){
	double sample_cor = 0.;
	double sample_uncor = 0.;
	
	//draw events from template
	for( int z = 0; z < N; z++){
	  sample_cor += Cor_template->GetRandom();
	  sample_uncor += Uncor_template->GetRandom();
	}
	
	//total sample likelihood has been calculated, fill in the histogram
	CorSampleLik->Fill(sample_cor);
	UnCorSampleLik->Fill(sample_uncor);
	
      }
      
      if(j == 14){
	Temp_Cor_Datasize->push_back(*CorSampleLik);
      Temp_Uncor_Datasize->push_back(*UnCorSampleLik);
      }
      //pseudo-experiments have been drawn, fit a gaussian to the sample likelihood distributions
      TF1 *g_cor = new TF1("g_cor","gaus",0,100);
      g_cor->SetLineColor(kRed);
      TF1 *g_uncor = new TF1("g_uncor","gaus",0,100);
      g_uncor->SetLineColor(kBlack);
      CorSampleLik->Fit(g_cor);
      UnCorSampleLik->Fit(g_uncor);
      
      //get mean and RMS of histograms for this sample size
      Mean_cor[j] = CorSampleLik->GetMean();
      Mean_uncor[j] = UnCorSampleLik->GetMean();
      RMS_cor[j] = CorSampleLik->GetRMS();
      RMS_uncor[j] = UnCorSampleLik->GetRMS();
      MeanErr_cor[j] = CorSampleLik->GetMeanError();
      MeanErr_uncor[j] = UnCorSampleLik->GetMeanError();
      RMSErr_cor[j] = CorSampleLik->GetRMSError();
      RMSErr_uncor[j] = UnCorSampleLik->GetRMSError();
      
      //for stat variation systematic
      (*StatVariedMean_cor)[j].Fill(CorSampleLik->GetMean());
      (*StatVariedMean_uncor)[j].Fill(UnCorSampleLik->GetMean());
      (*StatVariedSigma_cor)[j].Fill(CorSampleLik->GetRMS());
      (*StatVariedSigma_uncor)[j].Fill(UnCorSampleLik->GetRMS());
      
      //calculate the separation for this sample size
      separation[j] = (CorSampleLik->GetMean() - UnCorSampleLik->GetMean())/sqrt(CorSampleLik->GetRMS()*CorSampleLik->GetRMS() + UnCorSampleLik->GetRMS()*UnCorSampleLik->GetRMS()); 
      separation_err[j] = separation[j]*sqrt((pow(CorSampleLik->GetMeanError(),2)+pow(UnCorSampleLik->GetMeanError(),2))/pow(CorSampleLik->GetMean() - UnCorSampleLik->GetMean(),2)+pow((CorSampleLik->GetRMS()*CorSampleLik->GetRMSError()+UnCorSampleLik->GetRMS()*UnCorSampleLik->GetRMSError())/(pow(CorSampleLik->GetRMS(),2)+pow(UnCorSampleLik->GetRMS(),2)),2));
      
      
      //if you want to use the fitted gaussian mean and gaussian sigma for the separation power
      //Mean_cor[j] = g_cor->GetParameter(1);
      //Mean_uncor[j] = g_uncor->GetParameter(1);
      //RMS_cor[j] = g_cor->GetParameter(2);
      //RMS_uncor[j] = g_uncor->GetParameter(2);
      //MeanErr_cor[j] = g_cor->GetParError(1);
      //MeanErr_uncor[j] = g_uncor->GetParError(1);
      //RMSErr_cor[j] = g_cor->GetParError(2);
      //RMSErr_uncor[j] = g_uncor->GetParError(2);
      //separation[j] = (Mean_cor[j] - Mean_uncor[j])/sqrt(RMS_cor[j]*RMS_cor[j] + RMS_uncor[j]*RMS_uncor[j]); 
      //separation_err[j] = separation[j]*sqrt((pow(MeanErr_cor[j],2)+pow(MeanErr_uncor[j],2))/pow(Mean_cor[j] - Mean_uncor[j],2)+pow((RMS_cor[j]*RMSErr_cor[j]+RMS_uncor[j]*RMSErr_uncor[j])/(pow(RMS_cor[j],2)+pow(RMS_uncor[j],2)),2));
      
      
      
    //draw the sample likelihood distribution for this sample size
      TCanvas *c1 = new TCanvas();
      c1->cd();
      gStyle->SetOptStat(2222);
      CorSampleLik->SetLineColor(kRed);
      UnCorSampleLik->SetLineColor(kBlack);
      TPaveStats *ptstats = new TPaveStats(0.7658046,0.6588983,0.9798851,0.9364407,"brNDC");
      ptstats->SetName("stats");
      ptstats->SetBorderSize(1);
      ptstats->SetFillColor(0);
      ptstats->SetTextAlign(12);
      ptstats->SetTextFont(42);
      ptstats->SetOptStat(1111);
      ptstats->SetOptFit(111);
      ptstats->Draw();
      CorSampleLik->GetListOfFunctions()->Add(ptstats);
      ptstats->SetParent(CorSampleLik);
      ptstats = new TPaveStats(0.7658046,0.3834746,0.9841954,0.6398305,"brNDC");
      ptstats->SetName("stats");
      ptstats->SetBorderSize(1);
      ptstats->SetFillColor(0);
      ptstats->SetTextAlign(12);
      ptstats->SetTextFont(42);
      ptstats->SetOptStat(1111);
      ptstats->SetOptFit(111);
      ptstats->Draw();
      UnCorSampleLik->GetListOfFunctions()->Add(ptstats);
      ptstats->SetParent(UnCorSampleLik);
      CorSampleLik->Draw("e");
      UnCorSampleLik->Draw("esames");
      g_uncor->Draw("sames");
      TLegend *leg = new TLegend(0.06752874,0.7266949,0.3577586,0.9173729,NULL,"brNDC");
      leg->SetFillColor(0);
      leg->AddEntry(CorSampleLik,"Correlated Events");
      leg->AddEntry(UnCorSampleLik,"Uncorrelated Events");
      leg->Draw("same");
      c1->Modified();
      c1->Update();
      stringstream fname;
      fname<<"SampleLikelihood_"<<Name<<"_"<<N<<"evPE.C";
      c1->SaveAs(fname.str().c_str());
      delete c1;
      delete g_cor;
      delete g_uncor;
    }
    
    //}
  TemplateFlucfile->Write();
  TemplateFlucfile->Close();

  hist_nom_cor->SetLineColor(kBlack);
  hist_nom_cor->SetLineWidth(2);
  hist_nom_cor->SetFillColor(kWhite);
  hist_nom_cor->SetMarkerColor(kBlack);
  hist_nom_cor->Draw("hsame");
  TemplateHist_syst_up->SetLineColor(kGreen);
  TemplateHist_syst_up->SetLineWidth(2);
  TemplateHist_syst_up->SetMarkerColor(kGreen);
  TemplateHist_syst_up->Draw("hsame");
  TemplateHist_syst_down->SetLineColor(kBlue);
  TemplateHist_syst_down->SetLineWidth(2);
  TemplateHist_syst_down->SetMarkerColor(kBlue);
  TemplateHist_syst_down->Draw("hsame");
  hist_nom_uncor->SetLineColor(kBlack);
  hist_nom_uncor->SetLineWidth(2);
  hist_nom_uncor->SetFillColor(kWhite);
  hist_nom_uncor->SetLineStyle(2);
  hist_nom_uncor->SetMarkerColor(kBlack);
  hist_nom_uncor->Draw("hsame");
  TemplateHist_uncor_syst_up->SetLineColor(kGreen);
  TemplateHist_uncor_syst_up->SetLineWidth(2);
  TemplateHist_uncor_syst_up->SetMarkerColor(kGreen);
  TemplateHist_uncor_syst_up->SetLineStyle(2);
  TemplateHist_uncor_syst_up->Draw("hsame");
  TemplateHist_uncor_syst_down->SetLineColor(kBlue);
  TemplateHist_uncor_syst_down->SetLineWidth(2);
  TemplateHist_uncor_syst_down->SetMarkerColor(kBlue);
  TemplateHist_uncor_syst_down->SetLineStyle(2);
  TemplateHist_uncor_syst_down->Draw("hsame");
  
  c_fluc->Modified();
  c_fluc->Update();
  c_fluc->SaveAs("Fluc_JER_cor.C");
  // TemplateFlucfile->Write();
  //TemplateFlucfile->Close();
  //delete TemplateFlucfile;
  //TemplateFlucfile = NULL;


  /*
  for(int z = 0; z < 15; z++){
    //draw the sample likelihood distribution for this sample size
    TCanvas *cSyst_mean = new TCanvas();
    cSyst_mean->Divide(2,1);
    cSyst_mean->cd(1);
    TF1 *g_mean_cor = new TF1("g_mean_cor","gaus",cor_mean[z]-50,cor_mean[z]+50);
    (*StatVariedMean_cor)[z].Fit(g_mean_cor);
    TF1 *g_mean_uncor = new TF1("g_mean_uncor","gaus",uncor_mean[z]-50,uncor_mean[z]+50);
    (*StatVariedMean_uncor)[z].Fit(g_mean_uncor);
    (*StatVariedMean_cor)[z].SetLineColor(kRed);
    (*StatVariedMean_uncor)[z].SetLineColor(kBlack);
    TPaveStats *ptstatsM = new TPaveStats(0.7658046,0.6588983,0.9798851,0.9364407,"brNDC");
    ptstatsM->SetName("stats");
    ptstatsM->SetBorderSize(1);
    ptstatsM->SetFillColor(0);
    ptstatsM->SetTextAlign(12);
    ptstatsM->SetTextFont(42);
    ptstatsM->SetOptStat(1111);
    ptstatsM->SetOptFit(111);
    //ptstatsM->Draw();
    (*StatVariedMean_cor)[z].GetListOfFunctions()->Add(ptstatsM);
    ptstatsM->SetParent(&(*StatVariedMean_cor)[z]);
    ptstatsM = new TPaveStats(0.7658046,0.3834746,0.9841954,0.6398305,"brNDC");
    ptstatsM->SetName("stats");
    ptstatsM->SetBorderSize(1);
    ptstatsM->SetFillColor(0);
    ptstatsM->SetTextAlign(12);
    ptstatsM->SetTextFont(42);
    ptstatsM->SetOptStat(1111);
    ptstatsM->SetOptFit(111);
    //ptstatsM->Draw();
    (*StatVariedMean_uncor)[z].GetListOfFunctions()->Add(ptstatsM);
    ptstatsM->SetParent(&(*StatVariedMean_uncor)[z]);
    (*StatVariedMean_cor)[z].Draw("e");
    g_mean_cor->Draw("same");
    //TLegend *legM = new TLegend(0.06752874,0.7266949,0.3577586,0.9173729,NULL,"brNDC");
    //legM->SetFillColor(0);
    //legM->AddEntry(&(*StatVariedMean_cor)[z],"Correlated");
    //legM->AddEntry(&(*StatVariedMean_uncor)[z],"Uncorrelated");
    //    legM->Draw("same");
    cSyst_mean->Modified();
    cSyst_mean->Update();
    cSyst_mean->cd(2);
    (*StatVariedMean_uncor)[z].Draw("e");
    g_mean_uncor->Draw("same");
    cSyst_mean->Modified();
    cSyst_mean->Update();
    stringstream fname_mean;
    int N = N_bin[z];
    fname_mean<<"SystVariedMean_"<<Name<<"_"<<N<<"evPE.C";
    cSyst_mean->SaveAs(fname_mean.str().c_str());
    TCanvas *cSyst_sigma = new TCanvas();
    cSyst_sigma->Divide(2,1);
    cSyst_sigma->cd(1);
    TF1 *g_sigma_cor = new TF1("g_sigma_cor","gaus",cor_sigma[z]-7,cor_sigma[z]+7);
    (*StatVariedSigma_cor)[z].Fit(g_sigma_cor);
    TF1 *g_sigma_uncor = new TF1("g_sigma_uncor","gaus",uncor_sigma[z]-7,uncor_sigma[z]+7);
    (*StatVariedSigma_uncor)[z].Fit(g_sigma_uncor);
    (*StatVariedSigma_cor)[z].SetLineColor(kRed);
    (*StatVariedSigma_uncor)[z].SetLineColor(kBlack);
    TPaveStats *ptstatsS = new TPaveStats(0.7658046,0.6588983,0.9798851,0.9364407,"brNDC");
    ptstatsS->SetName("stats");
    ptstatsS->SetBorderSize(1);
    ptstatsS->SetFillColor(0);
    ptstatsS->SetTextAlign(12);
    ptstatsS->SetTextFont(42);
    ptstatsS->SetOptStat(1111);
    ptstatsS->SetOptFit(111);
    //ptstatsS->Draw();
    (*StatVariedSigma_cor)[z].GetListOfFunctions()->Add(ptstatsS);
    ptstatsS->SetParent(&(*StatVariedSigma_cor)[z]);
    ptstatsS = new TPaveStats(0.7658046,0.3834746,0.9841954,0.6398305,"brNDC");
    ptstatsS->SetName("stats");
    ptstatsS->SetBorderSize(1);
    ptstatsS->SetFillColor(0);
    ptstatsS->SetTextAlign(12);
    ptstatsS->SetTextFont(42);
    ptstatsS->SetOptStat(1111);
    ptstatsS->SetOptFit(111);
    //ptstatsS->Draw();
    (*StatVariedSigma_uncor)[z].GetListOfFunctions()->Add(ptstatsS);
    ptstatsS->SetParent(&(*StatVariedSigma_uncor)[z]);
    (*StatVariedSigma_cor)[z].Draw("e");
    g_sigma_cor->Draw("same");
    //TLegend *legS = new TLegend(0.06752874,0.7266949,0.3577586,0.9173729,NULL,"brNDC");
    //legS->SetFillColor(0);
    //legS->AddEntry(&(*StatVariedSigma_cor)[z],"Correlated");
    //legS->AddEntry(&(*StatVariedSigma_uncor)[z],"Uncorrelated");
    //    legS->Draw("same");
    cSyst_sigma->Modified();
    cSyst_sigma->Update();
    cSyst_sigma->cd(2);
    (*StatVariedSigma_uncor)[z].Draw("e");
    g_sigma_uncor->Draw("same");
    cSyst_sigma->Modified();
    cSyst_sigma->Update();
    stringstream fname_sigma;
    fname_sigma<<"SystVariedSigma_"<<Name<<"_"<<N<<"evPE.C";
    cSyst_sigma->SaveAs(fname_sigma.str().c_str());
    delete cSyst_mean;
    delete cSyst_sigma;
    //    delete legM;
    delete ptstatsM;
    //delete legS;
    delete ptstatsS;
    //    delete g_cor_syst;
    //delete g_uncor_syst;
    //delete s_cor_syst;
    //delete s_uncor_syst;
  }
  */
  //  std::cout<<"before temp delete"<<std::endl;
  

  /*
  delete cor_temp_minus;
  delete uncor_temp_minus;
  delete B_cor_temp_minus;
  delete B_uncor_temp_minus;
  delete W3Jets_temp_minus;
  delete W4Jets_temp_minus;
  delete DY_temp_minus;
  delete TsChan_temp_minus;
  delete TbarsChan_temp_minus;
  delete TtChan_temp_minus;
  delete TbartChan_temp_minus;
  delete TtWChan_temp_minus;
  delete TbartWChan_temp_minus;
  std::cout<<"after first del"<<std::endl;
  cor_temp_minus = NULL;
  uncor_temp_minus = NULL;
  B_cor_temp_minus = NULL;
  B_uncor_temp_minus = NULL;
  W3Jets_temp_minus = NULL;
  W4Jets_temp_minus = NULL;
  DY_temp_minus = NULL;
  TsChan_temp_minus = NULL;
  TbarsChan_temp_minus = NULL;
  TtChan_temp_minus = NULL;
  TbartChan_temp_minus = NULL;
  TtWChan_temp_minus = NULL;
  TbartWChan_temp_minus = NULL;
  std::cout<<"after first null"<<std::endl;
  delete cor_temp_plus;
  delete uncor_temp_plus;
  delete B_cor_temp_plus;
  delete B_uncor_temp_plus;
  delete W3Jets_temp_plus;
  delete W4Jets_temp_plus;
  delete DY_temp_plus;
  delete TsChan_temp_plus;
  delete TbarsChan_temp_plus;
  delete TtChan_temp_plus;
  delete TbartChan_temp_plus;
  delete TtWChan_temp_plus;
  delete TbartWChan_temp_plus;
  std::cout<<"after second del"<<std::endl;
  cor_temp_plus = NULL;
  uncor_temp_plus = NULL;
  B_cor_temp_plus = NULL;
  B_uncor_temp_plus = NULL;
  W3Jets_temp_plus = NULL;
  W4Jets_temp_plus = NULL;
  DY_temp_plus = NULL;
  TsChan_temp_plus = NULL;
  TbarsChan_temp_plus = NULL;
  TtChan_temp_plus = NULL;
  TbartChan_temp_plus = NULL;
  TtWChan_temp_plus = NULL;
  TbartWChan_temp_plus = NULL;
  */
  std::cout<<"after temp delete"<<std::endl;

    //all sample sizes have been considered
    //draw the scaling plots
    TCanvas *c2 = new TCanvas();
    c2->cd();
    TGraphErrors *graph = new TGraphErrors(14,N_bin,separation,0,separation_err); 
    //TF1 *ff1 = new TF1("ff1","[0]+[1]*pow(x,[2])");
    TF1 *ff1 = new TF1("ff1","[0]*pow(x,[1])");
    ff1->SetParLimits(1,0,1.5);
    ff1->SetLineColor(kRed);
    //full range
    ff1->SetRange(10,80000);
    //range based on stat
    //ff1->SetRange(1000,80000);
    graph->SetMarkerStyle(8);
    graph->SetMarkerColor(kBlack);
    graph->SetTitle("Separation Power");
    graph->GetXaxis()->SetTitle("sample size (N)");
    graph->GetYaxis()->SetTitle("separation power (#sigma)");
    graph->GetXaxis()->SetRangeUser(10,80000);
    graph->GetYaxis()->SetRangeUser(0.1,10);
    graph->Draw("ap");
    graph->Fit("ff1","B");
    double exp_sig = ff1->GetParameter(0)*pow(Dataset_size,ff1->GetParameter(1));
    double exp_sig_05 = ff1->GetParameter(0)*pow(Dataset_size,0.5);
    //    std::cout<<"dataset size = "<<Dataset_size<<" par 1 = "<<ff1->GetParameter(0)<<" par 2 ="<<ff1->GetParameter(1)<<std::endl;
    std::cout<<"expected significance fit = "<<exp_sig<<" or "<<exp_sig_05<<std::endl;
    ff1->Draw("same");
    c2->SetLogy();
    c2->SetLogx();
    c2->Modified();
    c2->Update();
    c2->SaveAs(("SeparationScaling_"+Name+".C").c_str());
    c2->SaveAs(("SeparationScaling_"+Name+".png").c_str());
    c2->SaveAs(("SeparationScaling_"+Name+".pdf").c_str());
    
    TCanvas *c3 = new TCanvas();
    c3->Divide(2,1);
  c3->cd(1);
  TGraphErrors *graphMC = new TGraphErrors(14,N_bin,Mean_cor,0,MeanErr_cor); 
  TGraphErrors *graphMU = new TGraphErrors(14,N_bin,Mean_uncor,0,MeanErr_uncor); 
  TF1 *ff1MC = new TF1("ff1MC","[0]*x");
  ff1MC->SetLineColor(kRed);
  // ff1MC->SetRange(10,80000);
  ff1MC->SetRange(1000,80000);
  TF1 *ff1MU = new TF1("ff1MU","[0]*x");
  ff1MU->SetLineColor(kBlack);
  //ff1MU->SetRange(10,80000);
  ff1MU->SetRange(1000,80000);
  graphMC->SetMarkerStyle(8);
  graphMC->SetMarkerColor(kRed);
  graphMC->SetLineColor(kRed);
  graphMC->SetTitle("Mean Correlated");
  graphMC->GetXaxis()->SetTitle("sample size (N)");
  graphMC->GetYaxis()->SetTitle("Mean");
  graphMC->GetXaxis()->SetRangeUser(10,80000);
  graphMC->GetYaxis()->SetRangeUser(0,4000);
  graphMC->Draw("ap");
  graphMC->Fit("ff1MC");//,"R");
  //the fit parameter is the gaussian mean for the correlated sample
  mean_cor = ff1MC->GetParameter(0);
  ff1MC->Draw("same");
  c3->cd(2);
  graphMU->SetMarkerStyle(8);
  graphMU->SetMarkerColor(kBlack);
  graphMU->SetTitle("Mean Uncorrelated");
  graphMU->GetXaxis()->SetTitle("sample size (N)");
  graphMU->GetYaxis()->SetTitle("Mean");
  graphMU->GetXaxis()->SetRangeUser(10,80000);
  graphMU->GetYaxis()->SetRangeUser(0,4000);
  graphMU->Draw("ap");
  graphMU->Fit("ff1MU");//,"R");
  //the fit parameter is the gaussian mean for the uncorrelated sample
  mean_uncor = ff1MU->GetParameter(0);
  ff1MU->Draw("same");
  c3->SetLogx();
  c3->Modified();
  c3->Update();
  c3->SaveAs(("GaussianMean_"+Name+".C").c_str());
  c3->SaveAs(("GaussianMean_"+Name+".png").c_str());
  c3->SaveAs(("GaussianMean_"+Name+".pdf").c_str());

  TCanvas *c4 = new TCanvas();
  c4->Divide(2,1);
  c4->cd(1);
  TGraphErrors *graphRC = new TGraphErrors(14,N_bin,RMS_cor,0,RMSErr_cor); 
  TGraphErrors *graphRU = new TGraphErrors(14,N_bin,RMS_uncor,0,RMSErr_uncor); 
  TF1 *ff1RC = new TF1("ff1RC","[0]*pow(x,0.5)");
  ff1RC->SetLineColor(kRed);
  //  ff1RC->SetRange(10,80000);
  ff1RC->SetRange(1000,80000);
  TF1 *ff1RU = new TF1("ff1RU","[0]*pow(x,0.5)");
  ff1RU->SetLineColor(kBlack);
  ff1RU->SetRange(1000,80000);
  //  ff1RU->SetRange(10,80000);
  graphRC->SetMarkerStyle(8);
  graphRC->SetMarkerColor(kRed);
  graphRC->SetLineColor(kRed);
  graphRC->SetTitle("RMS Correlated");
  graphRC->GetXaxis()->SetTitle("sample size (N)");
  graphRC->GetYaxis()->SetTitle("RMS");
  graphRC->GetXaxis()->SetRangeUser(10,80000);
  graphRC->GetYaxis()->SetRangeUser(0,140);
  graphRC->Draw("ap");
  ff1RC->SetParameter(0,100);
  ff1RC->SetParameter(1,0.5);
  graphRC->Fit("ff1RC");//,"R");
  //the fit parameter is the gaussian sigma for the correlated sample
  sigma_cor = ff1RC->GetParameter(0);
  ff1RC->Draw("same");
  c4->cd(2);
  graphRU->SetMarkerStyle(8);
  graphRU->SetMarkerColor(kBlack);
  graphRU->SetTitle("RMS Uncorrelated");
  graphRU->GetXaxis()->SetTitle("sample size (N)");
  graphRU->GetYaxis()->SetTitle("RMS");
  graphRU->GetXaxis()->SetRangeUser(10,80000);
  graphRU->GetYaxis()->SetRangeUser(0,140);
  graphRU->Draw("ap");
  ff1RU->SetParameter(0,100);
  ff1RU->SetParameter(1,0.5);
  graphRU->Fit("ff1RU");//,"R");
  //the fit parameter is the gaussian sigma for the uncorrelated sample
  sigma_uncor = ff1RU->GetParameter(0);
  ff1RU->Draw("same");
  //c4->SetLogx();
  c4->SetLogx();
  c4->Modified();
  c4->Update();
  c4->SaveAs(("GaussianSigma_"+Name+".C").c_str());
  c4->SaveAs(("GaussianSigma_"+Name+".png").c_str());
  c4->SaveAs(("GaussianSigma_"+Name+".pdf").c_str());

}

//the scaling behaviour of the means and RMSs has been fitted
//compare the data sample likelihood with the results in MC at the Dataset size
void eire::HypTesting::DoMeasurement()
{
  std::cout<<"%%%%%%%%%%Results%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"Dataset size = "<<Dataset_size<<std::endl;
  std::cout<<"Data sample likelihood = "<<Data_sample<<" +/- "<<Data_sample_error<<std::endl;
  std::cout<<"Data sample likelihood (-lnL_cor) = "<<Data_sample_cor<<" +/- "<<Data_sample_cor_error<<std::endl;
  std::cout<<"Data sample likelihood (-lnL_uncor) = "<<Data_sample_uncor<<" +/- "<<Data_sample_uncor_error<<std::endl;
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5%%%"<<std::endl;


  TH1F *cor_datasize = (TH1F*) ((*Temp_Cor_Datasize)[0]).Clone();
  TH1F *uncor_datasize = (TH1F*) ((*Temp_Uncor_Datasize)[0]).Clone();
  for(int i = 1; i < Temp_Cor_Datasize->size(); i++){
    cor_datasize->Add(&(*Temp_Cor_Datasize)[i]);
    uncor_datasize->Add(&(*Temp_Uncor_Datasize)[i]);
  }

  cor_datasize->Scale(1./cor_datasize->Integral());
  uncor_datasize->Scale(1./uncor_datasize->Integral());

  TCanvas *c_test = new TCanvas();
  c_test->cd();
  cor_datasize->Draw();
  uncor_datasize->Draw("same");
  c_test->Modified();
  c_test->Update();
  c_test->SaveAs("Test_Systematics.C");

  //fill the systematically varied fit parameters (template statistics)
  double mean_cor_sysup = mean_cor; //0.256867;
  double mean_cor_sysdown = mean_cor; //0.255407;
  double mean_uncor_sysup = mean_uncor; //0.235464;
  double mean_uncor_sysdown = mean_uncor; //0.233879;
  double sigma_cor_sysup = 0.460078;
  double sigma_cor_sysdown = 0.395063;
  double sigma_uncor_sysup = 0.456985;
  double sigma_uncor_sysdown = 0.394983;

  //using the pre-determined gaussian means and sigmas, draw a gaussian distribution in the full range
  TF1 *Gauss_Cor = new TF1("Gauss_Cor","gaus",mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  Gauss_Cor->FixParameter(0,1);
  Gauss_Cor->FixParameter(1,mean_cor*Dataset_size);
  Gauss_Cor->FixParameter(2,sigma_cor*pow(Dataset_size,0.5));
  TH1 *Gauss_Cor_hist = Gauss_Cor->GetHistogram();
  TF1 *Gauss_Uncor = new TF1("Gauss_Uncor","gaus",mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  Gauss_Uncor->FixParameter(0,1);
  Gauss_Uncor->FixParameter(1,mean_uncor*Dataset_size);
  Gauss_Uncor->FixParameter(2,sigma_uncor*pow(Dataset_size,0.5));
  Gauss_Uncor->SetLineColor(kBlack);
  TH1 *Gauss_Uncor_hist = Gauss_Uncor->GetHistogram();

  TF1 *Gauss_Cor_sysup = new TF1("Gauss_Cor_sysup","gaus",mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  Gauss_Cor_sysup->FixParameter(0,1);
  Gauss_Cor_sysup->FixParameter(1,mean_cor_sysup*Dataset_size);
  Gauss_Cor_sysup->FixParameter(2,sigma_cor_sysup*pow(Dataset_size,0.5));
  TF1 *Gauss_Cor_sysdown = new TF1("Gauss_Cor_sysdown","gaus",mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  Gauss_Cor_sysdown->FixParameter(0,1);
  Gauss_Cor_sysdown->FixParameter(1,mean_cor_sysdown*Dataset_size);
  Gauss_Cor_sysdown->FixParameter(2,sigma_cor_sysdown*pow(Dataset_size,0.5));
  TF1 *Gauss_Uncor_sysup = new TF1("Gauss_Uncor_sysup","gaus",mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  Gauss_Uncor_sysup->FixParameter(0,1);
  Gauss_Uncor_sysup->FixParameter(1,mean_uncor_sysup*Dataset_size);
  Gauss_Uncor_sysup->FixParameter(2,sigma_uncor_sysup*pow(Dataset_size,0.5));
  TF1 *Gauss_Uncor_sysdown = new TF1("Gauss_Uncor_sysdown","gaus",mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  Gauss_Uncor_sysdown->FixParameter(0,1);
  Gauss_Uncor_sysdown->FixParameter(1,mean_uncor_sysdown*Dataset_size);
  Gauss_Uncor_sysdown->FixParameter(2,sigma_uncor_sysdown*pow(Dataset_size,0.5));

  //print the fit parameters
  std::cout<<"%%%%%%%%%%Fit parameters%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"par mean cor = "<<mean_cor<<" sys up = "<<mean_cor_sysup<<" sys down = "<<mean_cor_sysdown<<std::endl;
  std::cout<<"par mean uncor = "<<mean_uncor<<" sys up = "<<mean_uncor_sysup<<" sys down = "<<mean_uncor_sysdown<<std::endl;
  std::cout<<"par sigma cor = "<<sigma_cor<<" sys up = "<<sigma_cor_sysup<<" sys down = "<<sigma_cor_sysdown<<std::endl;
  std::cout<<"par sigma uncor = "<<sigma_uncor<<" sys up = "<<sigma_uncor_sysup<<" sys down = "<<sigma_uncor_sysdown<<std::endl;

  //print the results
  std::cout<<"%%%%%%%%%%Results%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
  std::cout<<"Dataset size = "<<Dataset_size<<std::endl;
  std::cout<<"Data sample likelihood = "<<Data_sample<<" +/- "<<Data_sample_error<<std::endl;
  std::cout<<"Correlated Gaussian mean = "<<mean_cor*Dataset_size<<" sigma = "<<sigma_cor*pow(Dataset_size,0.5)<<std::endl;
  //std::cout<<"Correlated Gaussian mean stat up = "<<mean_cor*(Dataset_size+sqrt(Dataset_size))<<" sigma = "<<sigma_cor*pow((Dataset_size+sqrt(Dataset_size)),0.5)<<std::endl;
  //std::cout<<"Correlated Gaussian mean stat down = "<<mean_cor*(Dataset_size-sqrt(Dataset_size))<<" sigma = "<<sigma_cor*pow((Dataset_size-sqrt(Dataset_size)),0.5)<<std::endl;
  std::cout<<"Uncorrelated Gaussian mean = "<<mean_uncor*Dataset_size<<" sigma = "<<sigma_uncor*pow(Dataset_size,0.5)<<std::endl;
  //std::cout<<"Uncorrelated Gaussian mean stat up = "<<mean_uncor*(Dataset_size+sqrt(Dataset_size))<<" sigma = "<<sigma_uncor*pow((Dataset_size+sqrt(Dataset_size)),0.5)<<std::endl;
  //std::cout<<"Uncorrelated Gaussian mean stat down = "<<mean_uncor*(Dataset_size-sqrt(Dataset_size))<<" sigma = "<<sigma_uncor*pow((Dataset_size-sqrt(Dataset_size)),0.5)<<std::endl;
  double SM_Comp=fabs(mean_cor*Dataset_size-Data_sample)/sigma_cor/pow(Dataset_size,0.5);
  double U_Comp=fabs(mean_uncor*Dataset_size-Data_sample)/sigma_uncor/pow(Dataset_size,0.5);
  double SM_Comp_up=fabs(mean_cor*(Dataset_size+sqrt(Dataset_size))-Data_sample)/sigma_cor/pow(Dataset_size+sqrt(Dataset_size),0.5);
  double U_Comp_up=fabs(mean_uncor*(Dataset_size+sqrt(Dataset_size))-Data_sample)/sigma_uncor/pow(Dataset_size+sqrt(Dataset_size),0.5);
  double SM_Comp_down=fabs(mean_cor*(Dataset_size-sqrt(Dataset_size))-Data_sample)/sigma_cor/pow(Dataset_size-sqrt(Dataset_size),0.5);
  double U_Comp_down=fabs(mean_uncor*(Dataset_size-sqrt(Dataset_size))-Data_sample)/sigma_uncor/pow(Dataset_size-sqrt(Dataset_size),0.5);
  double SM_Comp_MCup=fabs(mean_cor*Dataset_size-Data_sample+Data_sample_error)/sigma_cor/pow(Dataset_size,0.5);
  double U_Comp_MCup=fabs(mean_uncor*Dataset_size-Data_sample+Data_sample_error)/sigma_uncor/pow(Dataset_size,0.5);
  double SM_Comp_sysup=fabs(mean_cor_sysup*Dataset_size-Data_sample)/sigma_cor_sysup/pow(Dataset_size,0.5);
  double U_Comp_sysup=fabs(mean_uncor_sysup*Dataset_size-Data_sample)/sigma_uncor_sysup/pow(Dataset_size,0.5);
  double SM_Comp_sysdown=fabs(mean_cor_sysdown*Dataset_size-Data_sample)/sigma_cor_sysdown/pow(Dataset_size,0.5);
  double U_Comp_sysdown=fabs(mean_uncor_sysdown*Dataset_size-Data_sample)/sigma_uncor_sysdown/pow(Dataset_size,0.5); 
  std::cout<<"SM_Comp = "<<SM_Comp<<" +/- "<<fabs(SM_Comp_MCup - SM_Comp)<<" (MW) "<<(SM_Comp_sysup - SM_Comp)<<" (MC Stat. up ) "<<(SM_Comp - SM_Comp_sysdown)<<" (MC Stat. down)"<<std::endl;
  std::cout<<"SM_Comp sys up = "<<SM_Comp_sysup<<" sys down = "<<SM_Comp_sysdown<<std::endl;
  std::cout<<"U_Comp = "<<U_Comp<<" +/- "<<fabs(U_Comp_MCup - U_Comp)<<" (MW.) "<<(U_Comp_sysup - U_Comp)<<" (MC Stat. up ) "<<(U_Comp - U_Comp_sysdown)<<" (MC Stat. down)"<<std::endl;
  std::cout<<"U_Comp sys up = "<<U_Comp_sysup<<" sys down = "<<U_Comp_sysdown<<std::endl;
  std::cout<<"expected significance = "<<(mean_cor*Dataset_size - mean_uncor*Dataset_size)/sqrt(sigma_cor*sigma_cor*Dataset_size + sigma_uncor*sigma_uncor*Dataset_size)<<std::endl;
  std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;

  //get a histogram with the correct range for the canvas
  TH1F *hist = new TH1F("hist","; -2#timesln[L(u)/L(c)]",100,mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5), mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5));
  hist->GetYaxis()->SetRangeUser(0,1.4);

  //make the arrow at the data location
  TArrow *arrow = new TArrow(Data_sample,0.4159958,Data_sample,0.001977414,0.05,">");
  arrow->SetFillColor(1);
  arrow->SetFillStyle(1001);
  //make the Data statistical band
  TH1F *Data_stat_hist = new TH1F("Data_stat_hist","",1,Data_sample-Data_sample_error,Data_sample+Data_sample_error);
  Data_stat_hist->Fill(Data_sample);
  Int_t ci;
  ci = TColor::GetColor("#009900");
  arrow->SetLineColor(ci);
  arrow->SetLineWidth(3);
  Data_stat_hist->SetFillColor(ci);
  Data_stat_hist->SetLineWidth(0);
  Data_stat_hist->SetLineColor(kWhite);
  Data_stat_hist->SetFillStyle(3004);


  Gauss_Cor_hist->SetLineColor(kWhite);
  Gauss_Cor_hist->SetLineWidth(0);
  Gauss_Cor_hist->SetFillColor(4);
  Gauss_Cor_hist->SetFillStyle(3007);
  Gauss_Uncor_hist->SetLineColor(kWhite);
  Gauss_Uncor_hist->SetLineWidth(0);
  Gauss_Uncor_hist->SetFillColor(2);
  Gauss_Uncor_hist->SetFillStyle(3006);

  Gauss_Cor_sysup->SetLineColor(4);
  Gauss_Cor_sysup->SetLineStyle(9);
  Gauss_Cor_sysdown->SetLineColor(4);
  Gauss_Cor_sysdown->SetLineStyle(3);
  Gauss_Uncor_sysup->SetLineColor(2);
  Gauss_Uncor_sysup->SetLineStyle(9);
  Gauss_Uncor_sysdown->SetLineColor(2);
  Gauss_Uncor_sysdown->SetLineStyle(3);

  TCanvas *c = new TCanvas();
  gStyle->SetOptStat(000);
  c->cd();
  hist->Draw();
  Gauss_Cor_hist->Draw("same");
  Gauss_Uncor_hist->Draw("same");
  Data_stat_hist->Draw("same");
  Gauss_Cor_sysup->Draw("same");
  Gauss_Cor_sysdown->Draw("same");
  Gauss_Uncor_sysup->Draw("same");
  Gauss_Uncor_sysdown->Draw("same");
  arrow->Draw();
  TLatex * tex;
  tex = new TLatex(mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5),1.429621,"CMS Preliminary");
  tex->SetTextSize(0.03521127);
  tex->SetLineWidth(2);
  tex->Draw("same");
  TLatex * tex2;
  tex2  = new TLatex(mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5)-0.32*(mean_cor*Dataset_size + 3*sigma_cor*pow(Dataset_size,0.5) - (mean_uncor*Dataset_size - 3*sigma_uncor*pow(Dataset_size,0.5))),1.429621,"#sqrt{s}= 8 TeV, L=19.7 fb^{-1}");
  tex2->SetTextSize(0.03521127);
  tex2->SetLineWidth(2);
  tex2->Draw("same");
  TLegend *leg = new TLegend(0.1640625,0.6926752,0.5375,0.8184713,NULL,"brNDC");
  leg->SetTextFont(62);
  leg->SetLineColor(0);
  leg->SetLineStyle(1);
  leg->SetLineWidth(1);
  leg->SetFillColor(0);
  leg->SetFillStyle(1001);
  TLegendEntry *entry;
  if(channel == "e_pos"){
    entry=leg->AddEntry("NULL","Positron Channel","h");
  }else if(channel == "e_neg"){
    entry=leg->AddEntry("NULL","Electron Channel","h");
  }else if(channel == "mu_pos"){
    entry=leg->AddEntry("NULL","Positive Muon Channel","h");
  }else if(channel == "mu_neg"){
    entry=leg->AddEntry("NULL","Negative Muon Channel","h");
  }else if(channel == "mu_combined"){
    entry=leg->AddEntry("NULL","Muon Channel","h");
  }
  entry->SetLineColor(1);
  entry->SetLineStyle(1);
  entry->SetLineWidth(1);
  entry->SetMarkerColor(1);
  entry->SetMarkerStyle(21);
  entry->SetMarkerSize(1);
  entry->SetTextFont(62);
  entry=leg->AddEntry("TArrow","CMS Data","l");
  entry->SetFillStyle(3006);
  ci = TColor::GetColor("#009900");
  entry->SetLineColor(ci);
  entry->SetLineStyle(1);
  entry->SetLineWidth(3);
  entry->SetMarkerColor(ci);
  entry->SetMarkerStyle(1);
  entry->SetMarkerSize(1);
  entry=leg->AddEntry("NULL","SM, Correlated t#bar{t}","f");
  entry->SetFillColor(4);
  entry->SetFillStyle(3007);
  entry->SetLineColor(4);
  entry->SetLineStyle(1);
  entry->SetLineWidth(2);
  entry=leg->AddEntry("NULL","Uncorrelated t#bar{t}","f");
  entry->SetFillColor(2);
  entry->SetFillStyle(3006);
  entry->SetLineColor(2);
  entry->SetLineWidth(2);
  leg->Draw();
  c->Modified();
  c->Update();
  c->SaveAs(("HypothesisMeasurement_"+Name+".C").c_str());
  c->SaveAs(("HypothesisMeasurement_"+Name+".png").c_str());
  c->SaveAs(("HypothesisMeasurement_"+Name+".pdf").c_str());
}
