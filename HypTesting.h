#ifndef EIRE_HYPTESTING_H
#define EIRE_HYPTESTING_H

#include <THStack.h>
#include <iostream> 
#include <TStyle.h>
#include <TLatex.h>
#include <TArrow.h>
#include <TColor.h>
#include <TLegendEntry.h>
#include <math.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include <TTree.h>
#include <TLeaf.h>
#include <TFile.h>
#include <cstdlib>
#include <stdio.h>
#include <iomanip>
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h> 
#include <TCanvas.h>
#include <TPaveStats.h>
#include <TLegend.h>
#include <TGraphErrors.h>
#include <RooFit.h>
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooProdPdf.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooGenericPdf.h"
#include "RooPolynomial.h"
#include "RooChi2Var.h"
#include "RooMinuit.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooHistPdf.h"
#include "RooAddPdf.h"
#include "RooAbsPdf.h"


using namespace std;

namespace eire{
  class HypTesting{
  public:
    HypTesting(string name, string channel_name, float backgroundfrac);
    HypTesting(string name, string channel_name, float backgroundfrac_systup, float backgroundfrac_systdown);
    ~HypTesting();
    void SetUp(std::vector<string> BackgroundWeights, std::vector<string> SignalWeights);
    void SetUp(std::vector<string> BackgroundWeights_systup, std::vector<string> BackgroundWeights_systdown, std::vector<string> SignalWeights);
    void SetTemplates(std::vector<std::vector<TH1F> > vec, std::vector<std::vector<TH1F> > vec_data, int Syst = 0);
    TH1F* ReDrawTemplate(TH1F hist_nom, TH1F hist_syst_up, TH1F hist_syst_down, string name);
    TH1F* ReDrawTemplateMass(TH1F hist_nom, TH1F hist_syst_up, TH1F hist_syst_down, string name);
    TH1F* ReDrawTemplateMW(TH1F hist_nom, TH1F hist_syst_up, TH1F hist_syst_down, string name);
    TH1F* ReDrawTemplate(TH1F hist, string name);
    TH1F* PrepareStack(THStack *stack, TH1F *hist, std::vector<TH1F> *vec_hists, bool cor, float frac, std::vector<float> weights);
    void DrawStack(bool UsingSyst = false);
    void PreparePlots(std::vector<TH1F> *histos);
    void DrawSeparation(bool UsingSyst = false);
    void SetData(std::vector<std::vector<float> > Data_1, std::vector<std::vector<float> > Data_2);
    void DoMeasurement();

  private:
    TH1F Data_template_minus;
    TH1F cor_template_minus;
    TH1F uncor_template_minus;
    TH1F B_cor_template_minus;
    TH1F B_uncor_template_minus;
    TH1F W3Jets_template_minus;
    TH1F W4Jets_template_minus;
    TH1F DY_template_minus;
    TH1F TsChan_template_minus;
    TH1F TtChan_template_minus;
    TH1F TbarsChan_template_minus;
    TH1F TbartChan_template_minus;
    TH1F TtWChan_template_minus;
    TH1F TbartWChan_template_minus;

    TH1F Data_template_plus;
    TH1F cor_template_plus;
    TH1F uncor_template_plus;
    TH1F B_cor_template_plus;
    TH1F B_uncor_template_plus;
    TH1F W3Jets_template_plus;
    TH1F W4Jets_template_plus;
    TH1F DY_template_plus;
    TH1F TsChan_template_plus;
    TH1F TtChan_template_plus;
    TH1F TbarsChan_template_plus;
    TH1F TbartChan_template_plus;
    TH1F TtWChan_template_plus;
    TH1F TbartWChan_template_plus;

    TH1F Data_AbsLik_minus;
    TH1F cor_AbsLik_minus;
    TH1F uncor_AbsLik_minus;
    TH1F B_cor_AbsLik_minus;
    TH1F B_uncor_AbsLik_minus;
    TH1F W3Jets_AbsLik_minus;
    TH1F W4Jets_AbsLik_minus;
    TH1F DY_AbsLik_minus;
    TH1F TsChan_AbsLik_minus;
    TH1F TtChan_AbsLik_minus;
    TH1F TbarsChan_AbsLik_minus;
    TH1F TbartChan_AbsLik_minus;
    TH1F TtWChan_AbsLik_minus;
    TH1F TbartWChan_AbsLik_minus;
    TH1F Data_AbsLik_plus;
    TH1F cor_AbsLik_plus;
    TH1F uncor_AbsLik_plus;
    TH1F B_cor_AbsLik_plus;
    TH1F B_uncor_AbsLik_plus;
    TH1F W3Jets_AbsLik_plus;
    TH1F W4Jets_AbsLik_plus;
    TH1F DY_AbsLik_plus;
    TH1F TsChan_AbsLik_plus;
    TH1F TtChan_AbsLik_plus;
    TH1F TbarsChan_AbsLik_plus;
    TH1F TbartChan_AbsLik_plus;
    TH1F TtWChan_AbsLik_plus;
    TH1F TbartWChan_AbsLik_plus;

    TH1F Data_AbsLikUncor_minus;
    TH1F cor_AbsLikUncor_minus;
    TH1F uncor_AbsLikUncor_minus;
    TH1F B_cor_AbsLikUncor_minus;
    TH1F B_uncor_AbsLikUncor_minus;
    TH1F W3Jets_AbsLikUncor_minus;
    TH1F W4Jets_AbsLikUncor_minus;
    TH1F DY_AbsLikUncor_minus;
    TH1F TsChan_AbsLikUncor_minus;
    TH1F TtChan_AbsLikUncor_minus;
    TH1F TbarsChan_AbsLikUncor_minus;
    TH1F TbartChan_AbsLikUncor_minus;
    TH1F TtWChan_AbsLikUncor_minus;
    TH1F TbartWChan_AbsLikUncor_minus;
    TH1F Data_AbsLikUncor_plus;
    TH1F cor_AbsLikUncor_plus;
    TH1F uncor_AbsLikUncor_plus;
    TH1F B_cor_AbsLikUncor_plus;
    TH1F B_uncor_AbsLikUncor_plus;
    TH1F W3Jets_AbsLikUncor_plus;
    TH1F W4Jets_AbsLikUncor_plus;
    TH1F DY_AbsLikUncor_plus;
    TH1F TsChan_AbsLikUncor_plus;
    TH1F TtChan_AbsLikUncor_plus;
    TH1F TbarsChan_AbsLikUncor_plus;
    TH1F TbartChan_AbsLikUncor_plus;
    TH1F TtWChan_AbsLikUncor_plus;
    TH1F TbartWChan_AbsLikUncor_plus;

    std::vector<TH1F> *AbsLik;
    std::vector<TH1F> *Template;
    std::vector<TH1F> *AbsLikUncor;

    std::vector<TH1F> *AbsLik_syst_up;
    std::vector<TH1F> *Template_syst_up;
    std::vector<TH1F> *AbsLikUncor_syst_up;

    std::vector<TH1F> *AbsLik_syst_down;
    std::vector<TH1F> *Template_syst_down;
    std::vector<TH1F> *AbsLikUncor_syst_down;

    float mean_cor;
    float sigma_cor;
    float mean_uncor;
    float sigma_uncor;
    string Name;

    float Data_sample;
    float Data_sample_error;
    float Data_sample_cor;
    float Data_sample_cor_error;
    float Data_sample_uncor;
    float Data_sample_uncor_error;
    float Dataset_size;
    float Dataset_size_1;
    float Dataset_size_2;

    float Bfrac;
    float Bfrac_systup;
    float Bfrac_systdown;
    std::vector<float> Bweights;
    std::vector<float> Bweights_systup;
    std::vector<float> Bweights_systdown;
    std::vector<float> SigWeights;
    string channel;

    THStack *AbsLikelihoodStack;
    TH1F *AbsLikelihoodHist;

    THStack *AbsLikelihoodStack_syst_up;
    TH1F *AbsLikelihoodHist_syst_up;

    THStack *AbsLikelihoodStack_syst_down;
    TH1F *AbsLikelihoodHist_syst_down;

    THStack *AbsLikelihoodStack_uncorsample;
    TH1F *AbsLikelihoodHist_uncorsample;

    THStack *AbsLikelihoodStack_uncorsample_syst_up;
    TH1F *AbsLikelihoodHist_uncorsample_syst_up;

    THStack *AbsLikelihoodStack_uncorsample_syst_down;
    TH1F *AbsLikelihoodHist_uncorsample_syst_down;

    TH1F *AbsLikBackHist;

    THStack *TemplateStack;
    TH1F *TemplateHist;

    THStack *TemplateStack_uncor;
    TH1F *TemplateHist_uncor;

    THStack *TemplateStack_syst_up;
    TH1F *TemplateHist_syst_up;
    THStack *TemplateStack_syst_down;
    TH1F *TemplateHist_syst_down;

    THStack *TemplateStack_uncor_syst_up;
    TH1F *TemplateHist_uncor_syst_up;
    THStack *TemplateStack_uncor_syst_down;
    TH1F *TemplateHist_uncor_syst_down;

    THStack *AbsLikelihoodStack_uncorhyp;
    THStack *AbsLikelihoodStack_uncorhyp_uncorsample;
    TH1F *AbsLikelihoodHist_uncorhyp;
    TH1F *AbsLikelihoodHist_uncorhyp_uncorsample;

    THStack *AbsLikelihoodStack_uncorhyp_syst_up;
    THStack *AbsLikelihoodStack_uncorhyp_uncorsample_syst_up;
    TH1F *AbsLikelihoodHist_uncorhyp_syst_up;
    TH1F *AbsLikelihoodHist_uncorhyp_uncorsample_syst_up;

    THStack *AbsLikelihoodStack_uncorhyp_syst_down;
    THStack *AbsLikelihoodStack_uncorhyp_uncorsample_syst_down;
    TH1F *AbsLikelihoodHist_uncorhyp_syst_down;
    TH1F *AbsLikelihoodHist_uncorhyp_uncorsample_syst_down;

    TH1F *AbsLikBackUncorrelatedHist;

    std::vector<TH1F> *StatVariedMean_cor;
    std::vector<TH1F> *StatVariedSigma_cor;
    std::vector<TH1F> *StatVariedMean_uncor;
    std::vector<TH1F> *StatVariedSigma_uncor;

    std::vector<TH1F> *Temp_Cor_Datasize;
    std::vector<TH1F> *Temp_Uncor_Datasize;
  };
}

#endif
