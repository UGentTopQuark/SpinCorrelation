#include "PEBuilder.h"

//Pseudo-Experiment Builder
eire::PEBuilder::PEBuilder(std::vector< std::vector<float> > Cor, std::vector< std::vector<float> > Uncor, std::vector< std::vector<float> > WJets, float Back_frac, bool use_back, string name, int Bins, int EvPerPE, float lowRange, float highRange, bool v)
{
  nBins = Bins;
  EvPE = EvPerPE;
  low_range = lowRange;
  high_range = highRange;
  verbose = v;
  Template_SigCor = new TH1F("Correlated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  Template_SigUnCor = new TH1F("UnCorrelated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  LikelihoodCutCor = new TH1F("LikelihoodCutCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCor = new TH1F("LikelihoodCutUnCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorCor = new TH1F("LikelihoodCutUnCorCor","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCutUnCorUnCor = new TH1F("LikelihoodCutUnCorUnCor","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_corsample = new TH2F("LikelihoodCorVsUnCor_corsample","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);
  LikelihoodCorVsUnCor_uncorsample = new TH2F("LikelihoodCorVsUnCor_uncorsample","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);
  Template_Data = new TH1F("Data","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  LikelihoodCutData = new TH1F("LikelihoodCutData","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorData = new TH1F("LikelihoodCutUnCorData","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_Data = new TH2F("LikelihoodCorVsUnCor_Data","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  Template_Background = new TH1F("Background","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  LikelihoodCutBackground = new TH1F("LikelihoodCutBackground","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorBackground = new TH1F("LikelihoodCutUnCorBackground","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_Background = new TH2F("LikelihoodCorVsUnCor_Background","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  Wfrac = Back_frac;
  Use_back = use_back;
  Use_data = false;
  Name = name;

  L_Cor = Cor;
  L_Uncor = Uncor;
  L_Background = WJets;
}

eire::PEBuilder::~PEBuilder()
{
}

void eire::PEBuilder::SetUpWeights(std::vector<std::string> s)
{
  if(s.size() < 13){std::cout<<"%%%%%%%%%ERROR%%%%%%%%: event weights are not configured properly!"<<std::endl;}

  for(unsigned int i = 0; i < s.size(); i++)
    {
      w_setup[i] = atoi(((s)[i]).c_str());
    }
}

void eire::PEBuilder::SetEventWeights(std::vector< std::vector<float> > w_cor, std::vector< std::vector<float> > w_uncor, std::vector< std::vector<float> > w_back, float cor_errors, float uncor_errors, float background_errors)
{
  AddCorTemplate(L_Cor,1, w_cor, cor_errors);
  AddUncorTemplate(L_Uncor,1, w_uncor, uncor_errors);
  if(Use_back){
    AddBackgroundTemplate(L_Background,1,w_back, background_errors);
  }
}

void eire::PEBuilder::AddDataSet(std::vector< std::vector<float> > Likelihoods, float weight, float data_errors)
{
  std::cout<<"Add Data"<<std::endl;
  Use_data = true;
  TH1F* temp_Template_Data = new TH1F("temp_Data","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  TH1F* temp_LikelihoodCutData = new TH1F("temp_LikelihoodCutData","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorData = new TH1F("temp_LikelihoodCutUnCorData","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_Data = new TH2F("temp_LikelihoodCorVsUnCor_Data","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];
  for(unsigned int i = 0; i < Cor.size(); i++){
    temp_Template_Data->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + data_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])));
    temp_LikelihoodCutData->Fill(-1*log((Cor)[i]) + data_errors*(-1)*(Cor_unc)[i]/(Cor)[i]);
    temp_LikelihoodCutUnCorData->Fill(-1*log((Uncor)[i]) + data_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i]);
    temp_LikelihoodCorVsUnCor_Data->Fill(-1*log((Cor)[i])+ data_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i])+ data_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i]);
  }

  temp_Template_Data->Sumw2();
  temp_LikelihoodCutData->Sumw2();
  temp_LikelihoodCutUnCorData->Sumw2();
  temp_LikelihoodCorVsUnCor_Data->Sumw2();

  Template_Data->Add(temp_Template_Data,weight);
  LikelihoodCutData->Add(temp_LikelihoodCutData,weight);
  LikelihoodCutUnCorData->Add(temp_LikelihoodCutUnCorData,weight);
  LikelihoodCorVsUnCor_Data->Add(temp_LikelihoodCorVsUnCor_Data,weight);

  L_Data.push_back(Cor);
  L_Data.push_back(Uncor);
  Data_weights.push_back(weight*temp_Template_Data->Integral("width"));

  delete temp_Template_Data;
  delete temp_LikelihoodCutData;
  delete temp_LikelihoodCutUnCorData;
  delete temp_LikelihoodCorVsUnCor_Data;

}

void eire::PEBuilder::AddCorTemplate(std::vector< std::vector<float> > Likelihoods, float weight, std::vector< std::vector<float> > event_weights, float cor_errors)
{
  std::cout<<"in addcortemplate"<<std::endl;
  TH1F* temp_Template_SigCor = new TH1F("temp_Correlated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  TH1F* temp_LikelihoodCutCor = new TH1F("temp_LikelihoodCutCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorCor = new TH1F("temp_LikelihoodCutUnCorCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_corsample = new TH2F("temp_LikelihoodCorVsUnCor_corsample","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  std::vector<float> Cor = (Likelihoods)[0];
  std::cout<<"events = "<<Cor.size()<<std::endl;
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];
  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j]){
	w *= (weights)[j];
      }
    }
    temp_Template_SigCor->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + cor_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_LikelihoodCutCor->Fill(-1*log((Cor)[i]) + cor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorCor->Fill(-1*log((Uncor)[i]) + cor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_corsample->Fill(-1*log((Cor)[i]) + cor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + cor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
  }

  temp_Template_SigCor->Sumw2();
  temp_LikelihoodCutCor->Sumw2();
  temp_LikelihoodCutUnCorCor->Sumw2();
  temp_LikelihoodCorVsUnCor_corsample->Sumw2();

  std::cout<<"weight = "<<weight<<std::endl;
  Template_SigCor->Add(temp_Template_SigCor,weight);
  LikelihoodCutCor->Add(temp_LikelihoodCutCor,weight);
  LikelihoodCutUnCorCor->Add(temp_LikelihoodCutUnCorCor,weight);
  LikelihoodCorVsUnCor_corsample->Add(temp_LikelihoodCorVsUnCor_corsample,weight);

  L_Cor.push_back(Cor);
  L_Cor.push_back(Uncor);
  Cor_weights.push_back(weight*temp_Template_SigCor->Integral("width"));

  delete temp_Template_SigCor;
  delete temp_LikelihoodCutCor;
  delete temp_LikelihoodCutUnCorCor;
  delete temp_LikelihoodCorVsUnCor_corsample;
}

void eire::PEBuilder::AddUncorTemplate(std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float uncor_errors)
{
  TH1F* temp_Template_SigUnCor = new TH1F("temp_UnCorrelated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  TH1F* temp_LikelihoodCutUnCor = new TH1F("temp_LikelihoodCutUnCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorUnCor = new TH1F("temp_LikelihoodCutUnCorUnCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_uncorsample = new TH2F("temp_LikelihoodCorVsUnCor_uncorsample","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];
  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j]){
        w *= (weights)[j];
      }
    }
    temp_Template_SigUnCor->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + uncor_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_LikelihoodCutUnCor->Fill(-1*log((Cor)[i]) + uncor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorUnCor->Fill(-1*log((Uncor)[i]) + uncor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_uncorsample->Fill(-1*log((Cor)[i]) + uncor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + uncor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
  }

  temp_Template_SigUnCor->Sumw2();
  temp_LikelihoodCutUnCor->Sumw2();
  temp_LikelihoodCutUnCorUnCor->Sumw2();
  temp_LikelihoodCorVsUnCor_uncorsample->Sumw2();

  Template_SigUnCor->Add(temp_Template_SigUnCor,weight);
  LikelihoodCutUnCor->Add(temp_LikelihoodCutUnCor,weight);
  LikelihoodCutUnCorUnCor->Add(temp_LikelihoodCutUnCorUnCor,weight);
  LikelihoodCorVsUnCor_corsample->Add(temp_LikelihoodCorVsUnCor_uncorsample,weight);

  L_Uncor.push_back(Cor);
  L_Uncor.push_back(Uncor);
  Uncor_weights.push_back(weight*temp_Template_SigUnCor->Integral("width"));

  delete temp_Template_SigUnCor;
  delete temp_LikelihoodCutUnCor;
  delete temp_LikelihoodCutUnCorUnCor;
  delete temp_LikelihoodCorVsUnCor_uncorsample;
}

void eire::PEBuilder::AddBackgroundTemplate(std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float background_errors)
{
  TH1F* temp_Template_Background = new TH1F("temp_Background","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", nBins, low_range, high_range);
  TH1F* temp_LikelihoodCutBackground = new TH1F("temp_LikelihoodCutBackground","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorBackground = new TH1F("temp_LikelihoodCutUnCorBackground","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_Background = new TH2F("temp_LikelihoodCorVsUnCor_Background","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];

  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j]){
        w *= (weights)[j];
      }
    }
    temp_Template_Background->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + background_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_LikelihoodCutBackground->Fill(-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorBackground->Fill(-1*log((Uncor)[i]) + background_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_Background->Fill(-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + background_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
  }

  temp_Template_Background->Sumw2();
  temp_LikelihoodCutBackground->Sumw2();
  temp_LikelihoodCutUnCorBackground->Sumw2();
  temp_LikelihoodCorVsUnCor_Background->Sumw2();

  Template_Background->Add(temp_Template_Background,weight);
  LikelihoodCutBackground->Add(temp_LikelihoodCutBackground,weight);
  LikelihoodCutUnCorBackground->Add(temp_LikelihoodCutUnCorBackground,weight);
  LikelihoodCorVsUnCor_Background->Add(temp_LikelihoodCorVsUnCor_Background,weight);

  L_Background.push_back(Cor);
  L_Background.push_back(Uncor);
  Background_weights.push_back(weight*temp_Template_Background->Integral("width"));

  delete temp_Template_Background;
  delete temp_LikelihoodCutBackground;
  delete temp_LikelihoodCutUnCorBackground;
  delete temp_LikelihoodCorVsUnCor_Background;

}

void eire::PEBuilder::DrawTemplates()
{
  std::cout<<"Draw templates"<<std::endl;
  Template_SigCor->Scale(1./Template_SigCor->Integral("width"));
  Template_SigUnCor->Scale(1./Template_SigUnCor->Integral("width"));
  LikelihoodCutCor->Scale(1./LikelihoodCutCor->Integral("width"));
  LikelihoodCutUnCor->Scale(1./LikelihoodCutUnCor->Integral("width"));
  LikelihoodCutUnCorCor->Scale(1./LikelihoodCutUnCorCor->Integral("width"));
  LikelihoodCutUnCorUnCor->Scale(1./LikelihoodCutUnCorUnCor->Integral("width"));
  TH1F* Draw_Template_Data = (TH1F*) Template_Data->Clone();

  if(Use_data){
    Draw_Template_Data->Scale(1./Draw_Template_Data->Integral("width"));
    LikelihoodCutData->Scale(1./LikelihoodCutData->Integral("width"));
    LikelihoodCutUnCorData->Scale(1./LikelihoodCutUnCorData->Integral("width"));
  }
  
  if(Use_back){
    Template_Background->Scale(1./Template_Background->Integral("width"));
    LikelihoodCutBackground->Scale(1./LikelihoodCutBackground->Integral("width"));
    LikelihoodCutUnCorBackground->Scale(1./LikelihoodCutUnCorBackground->Integral("width"));
  }

  Template_SigCor->SetLineColor(kRed);
  Template_SigUnCor->SetLineColor(kBlack);
  Draw_Template_Data->SetLineColor(kBlack);
  Template_Background->SetLineColor(kBlue);
  LikelihoodCutCor->SetLineColor(kRed);
  LikelihoodCutUnCor->SetLineColor(kBlack);
  LikelihoodCutData->SetLineColor(kBlack);
  LikelihoodCutBackground->SetLineColor(kBlue);
  LikelihoodCutUnCorCor->SetLineColor(kRed);
  LikelihoodCutUnCorUnCor->SetLineColor(kBlack);
  LikelihoodCutUnCorData->SetLineColor(kBlack);
  LikelihoodCutUnCorBackground->SetLineColor(kBlue);

  Template_SigUnCor->SetLineWidth(2);
  Template_SigCor->SetLineWidth(2);
  Draw_Template_Data->SetLineWidth(2);
  Template_Background->SetLineWidth(2);
  LikelihoodCutCor->SetLineWidth(2);
  LikelihoodCutUnCor->SetLineWidth(2);
  LikelihoodCutData->SetLineWidth(2);
  LikelihoodCutBackground->SetLineWidth(2);
  LikelihoodCutUnCorCor->SetLineWidth(2);
  LikelihoodCutUnCorUnCor->SetLineWidth(2);
  LikelihoodCutUnCorData->SetLineWidth(2);
  LikelihoodCutUnCorBackground->SetLineWidth(2);
 
  Template_SigUnCor->SetLineStyle(2);
  LikelihoodCutUnCor->SetLineStyle(2);
  LikelihoodCutUnCorUnCor->SetLineStyle(2);
  Template_Background->SetLineStyle(3);
  LikelihoodCutBackground->SetLineStyle(3);
  LikelihoodCutUnCorBackground->SetLineStyle(3);

  
  TH1F* SF = new TH1F("SF","", nBins, low_range, high_range);
  SF = (TH1F*) Template_SigCor->Clone();
  SF->Divide(Template_SigUnCor);
  SF->SetTitle("");
  TCanvas *c1 = new TCanvas("c1", "c1",0,0,600,500);
  gStyle->SetOptStat(2222);
  c1->Range(0,0,1,1);
  TPad *c1_1 = new TPad("c1_1", "newpad",0.01,0.01,0.99,0.32);
  c1_1->Draw();
  c1_1->cd();
  c1_1->SetTopMargin(0.01);
  c1_1->SetBottomMargin(0.3);
  c1_1->SetRightMargin(0.1);
  c1_1->SetFillStyle(0);
  SF->SetMinimum(0.800);
  SF->SetMaximum(1.200);
  SF->GetYaxis()->SetNdivisions(5);
  SF->GetXaxis()->SetTitle("-2ln#lambda");
  SF->GetYaxis()->SetTitle("cor/uncor");
  SF->GetXaxis()->SetTitleSize(0.08);
  SF->GetXaxis()->SetLabelSize(0.08);
  SF->GetYaxis()->SetLabelSize(0.08);
  SF->GetYaxis()->SetTitleSize(0.08);
  SF->GetYaxis()->SetTitleOffset(0.34);
  SF->Draw();
  TLine* l = new TLine(low_range,1,high_range,1);
  l->Draw("same");
  c1->cd();
  TPad *c1_2 = new TPad("c1_2", "newpad",0.01,0.33,0.99,0.99);
  c1_2->Draw();
  c1_2->cd();
  c1_2->SetTopMargin(0.1);
  c1_2->SetBottomMargin(0.01);
  c1_2->SetRightMargin(0.1);
  c1_2->SetFillStyle(0);
  TLegend *leg = new TLegend(0.6,0.7,0.89,0.89);
  leg->AddEntry(Template_SigCor,"correlated");
  leg->AddEntry(Template_SigUnCor,"uncorrelated");
  leg->SetFillColor(kWhite);
  Template_SigCor->GetYaxis()->SetLabelSize(0.05);
  Template_SigCor->GetYaxis()->SetTitleSize(0.05);
  Template_SigCor->GetYaxis()->SetTitleOffset(1.);
  Template_SigCor->Draw("EHIST");
  Template_SigUnCor->GetYaxis()->SetLabelSize(0.05);
  Template_SigUnCor->GetYaxis()->SetTitleSize(0.05);
  Template_SigUnCor->GetYaxis()->SetTitleOffset(1.);
  Template_SigUnCor->Draw("EHISTsames");
  c1->Modified();
  c1->Update();
  std::cout<<"use_back = "<<Use_back<<std::endl;
  std::cout<<"use_data = "<<Use_data<<std::endl;
  if(Use_back){
    leg->AddEntry(Template_Background,"Background");
    Template_Background->Draw("EHISTsames");
  }
  if(Use_data){
    leg->AddEntry(Draw_Template_Data,"Data");
    Draw_Template_Data->Draw("pEHISTsames");
  }
  leg->Draw();
  c1->Modified();
  c1->Update();
  c1->SaveAs(("InputTemplates"+Name+".C").c_str());
  c1->SaveAs(("InputTemplates"+Name+".png").c_str());
  c1->SaveAs(("InputTemplates"+Name+".pdf").c_str());
  delete c1;

  TCanvas *c2 = new TCanvas();
  gStyle->SetOptStat(2222);
  c2->cd();
  TLegend *leg2 = new TLegend(0.6,0.7,0.89,0.89);
  leg2->AddEntry(LikelihoodCutCor,"correlated");
  leg2->AddEntry(LikelihoodCutUnCor,"uncorrelated");
  leg2->SetFillColor(kWhite);
  LikelihoodCutCor->Draw("EHIST");
  LikelihoodCutUnCor->Draw("EHISTsames");
  if(Use_back){
    LikelihoodCutBackground->Draw("EHISTsames");
    leg2->AddEntry(LikelihoodCutBackground,"Background");
  }
  if(Use_data){
    LikelihoodCutData->Draw("psames");
    leg2->AddEntry(LikelihoodCutData,"Data");
  }
  leg2->Draw();
  c2->Modified();
  c2->Update();
  c2->SaveAs(("LikelihoodCut"+Name+".C").c_str());
  c2->SaveAs(("LikelihoodCut"+Name+".png").c_str());
  c2->SaveAs(("LikelihoodCut"+Name+".pdf").c_str());
  delete c2;

  TCanvas *c25 = new TCanvas();
  gStyle->SetOptStat(2222);
  c25->cd();
  TLegend *leg25 = new TLegend(0.6,0.7,0.89,0.89);
  leg25->AddEntry(LikelihoodCorVsUnCor_corsample,"correlated");
  leg25->SetFillColor(kWhite);
  LikelihoodCorVsUnCor_corsample->Draw("colz");
  leg25->Draw();
  c25->Modified();
  c25->Update();
  c25->SaveAs(("LikelihoodCorVsUnCor_corsample"+Name+".C").c_str());
  c25->SaveAs(("LikelihoodCorVsUnCor_corsample"+Name+".png").c_str());
  c25->SaveAs(("LikelihoodCorVsUnCor_corsample"+Name+".pdf").c_str());
  delete c25;

  TCanvas *c26 = new TCanvas();
  gStyle->SetOptStat(2222);
  c26->cd();
  TLegend *leg26 = new TLegend(0.6,0.7,0.89,0.89);
  leg26->AddEntry(LikelihoodCorVsUnCor_uncorsample,"uncorrelated");
  leg26->SetFillColor(kWhite);
  LikelihoodCorVsUnCor_uncorsample->Draw("colz");
  leg26->Draw();
  c26->Modified();
  c26->Update();
  c26->SaveAs(("LikelihoodCorVsUnCor_uncorsample"+Name+".C").c_str());
  c26->SaveAs(("LikelihoodCorVsUnCor_uncorsample"+Name+".png").c_str());
  c26->SaveAs(("LikelihoodCorVsUnCor_uncorsample"+Name+".pdf").c_str());
  delete c26;

  if(Use_back){
    TCanvas *c27 = new TCanvas();
    gStyle->SetOptStat(2222);
    c27->cd();
    TLegend *leg27 = new TLegend(0.6,0.7,0.89,0.89);
    leg27->AddEntry(LikelihoodCorVsUnCor_Background,"Background");
    leg27->SetFillColor(kWhite);
    LikelihoodCorVsUnCor_Background->Draw("colz");
    leg27->Draw();
    c27->Modified();
    c27->Update();
    c27->SaveAs(("LikelihoodCorVsUnCor_background"+Name+".C").c_str());
    c27->SaveAs(("LikelihoodCorVsUnCor_background"+Name+".png").c_str());
    c27->SaveAs(("LikelihoodCorVsUnCor_background"+Name+".pdf").c_str());
    delete c27;
  }

  TCanvas *c3 = new TCanvas();
  gStyle->SetOptStat(2222);
  c3->cd();
  TLegend *leg3 = new TLegend(0.6,0.7,0.89,0.89);
  leg3->AddEntry(LikelihoodCutUnCorCor,"correlated");
  leg3->AddEntry(LikelihoodCutUnCorUnCor,"uncorrelated");
  leg3->SetFillColor(kWhite);
  LikelihoodCutUnCorCor->Draw("EHIST");
  LikelihoodCutUnCorUnCor->Draw("EHISTsames");
  if(Use_back){
    LikelihoodCutUnCorBackground->Draw("EHISTsames");
    leg3->AddEntry(LikelihoodCutUnCorBackground,"Background");
  }
  if(Use_data){
    LikelihoodCutUnCorData->Draw("psames");
    leg3->AddEntry(LikelihoodCutUnCorData,"Data");
  }
  leg3->Draw();
  c3->Modified();
  c3->Update();
  c3->SaveAs(("LikelihoodCutUnCor"+Name+".C").c_str());
  c3->SaveAs(("LikelihoodCutUnCor"+Name+".png").c_str());
  c3->SaveAs(("LikelihoodCutUnCor"+Name+".pdf").c_str());
  delete c3;
  std::cout<<"Templates drawn"<<std::endl;
}

//void eire::PEBuilder::SetProperties(int Bins, int EvPerPE, float lowRange, float highRange, bool v)
//{
//}

std::vector<double> eire::PEBuilder::Mix(float mixing)
{
  mix_frac = mixing;
  
  char Pn[2000], fn[2000], fen[2000], Wn[2000], Wen[2000], Wp[2000], Nn[2000], Nne[2000], Cov_f_Nobs[2000], Cov_f_W[2000];
  char PNe[2000], Cov_W_N[2000];
  sprintf(Pn,"Pull_mix_%i",(int) (mix_frac*10));
  sprintf(fn,"h_fobs_mix_%i", (int) (mix_frac*10));
  sprintf(fen,"h_ferr_mix_%i", (int) (mix_frac*10));
  sprintf(Wn,"h_Wobs_mix_%i",(int) (mix_frac*10));
  sprintf(Wen,"h_Werr_mix_%i",(int) (mix_frac*10));
  sprintf(Wp,"h_WPull_mix_%i",(int) (mix_frac*10));
  sprintf(Nn,"h_Nobs_mix_%i",(int) (mix_frac*10));
  sprintf(Nne,"h_Nobs_err_mix_%i",(int) (mix_frac*10));
  sprintf(PNe,"h_Pull_Ne_mix_%i",(int) (mix_frac*10));
  sprintf(Cov_f_Nobs,"h_Cov_f_Nobs_mix_%i",(int) (mix_frac*10));
  sprintf(Cov_f_W,"h_Cov_f_W_mix_%i",(int) (mix_frac*10));
  sprintf(Cov_W_N,"h_Cov_W_N_mix_%i",(int) (mix_frac*10));
  TH1F* Pull = new TH1F(Pn,"(f_obs - f_input)/sigma_obs; pull; number of PE", 15, -6, 6);
  TH1F* h_fobs = new TH1F(fn,"Fitted fraction of correlated events f; f_{obs}; number of PE", 60, -3., 4.);
  TH1F* h_ferr = new TH1F(fen,"Fit error; #delta f; number of PE", 150, 0.0, 1.0);
  TH1F* h_Wobs = new TH1F(Wn,"Fitted number of background events N_{bkg}; N_{bkg}; number of PE", 70, 50.0, 200.0);//0.2
  TH1F* h_Werr = new TH1F(Wen,"Fitted error; #delta N_{bkg}; number of PE", 100, 0, 100);
  TH1F* h_WPull = new TH1F(Wp,"(N_{bkg} - N^{input}_{bkg})/sigma_obs); pull; number of PE", 10, -4., 4.);
  TH1F* h_NPull = new TH1F(PNe,"(N_{ttbar} - N^{input}_{ttbar})/sigma_obs); pull; number of PE", 10, -4., 4.);
  TH1F* h_Nobs = new TH1F(Nn,"Fitted number of ttbar events N_{ttbar}; N_{ttbar}; number of PE",100,200,800);
  TH1F* h_Nobs_err = new TH1F(Nne,"Error on N_{ttbar}; #delta N_{ttbar}; number of PE",50,15,30);
  TH1F* h_Cov_f_Nobs = new TH1F(Cov_f_Nobs,"Correlation between f_obs and N_{ttbar}; correlation; number of PE",120,-1.,0.);
  TH1F* h_Cov_f_W = new TH1F(Cov_f_W,"Correlation between f_obs and N_{bkg}; correlation; number of PE",120,0.,1.);
  TH1F* h_Cov_W_N = new TH1F(Cov_W_N,"Correlation between N_{ttbar} and N_{bkg}; correlation; number of PE",120,-1.,0.);

  TF1 *g_f = new TF1("g_f","gaus",-3,4);
  g_f->SetLineWidth(0.08);
  TF1 *g_ferr = new TF1("g_ferr","gaus",0.0,0.5);
  g_ferr->SetLineWidth(0.08);
  TF1 *g_N = new TF1("g_N","gaus",200.0,800.);
  g_N->SetLineWidth(0.08);
  TF1 *g_Nerr = new TF1("g_Nerr","gaus",15.,30.);
  g_Nerr->SetLineWidth(0.08);
  TF1 *g_pull = new TF1("g_pull","gaus",-6,6);
  g_pull->SetLineWidth(0.08);
  TF1 *g_pullN = new TF1("g_pullN","gaus",-4,4);
  g_pullN->SetLineWidth(0.08);
  TF1 *g_W = new TF1("g_W","gaus",50.0,200.);
  g_W->SetLineWidth(0.08);
  TF1 *g_Werr = new TF1("g_Werr","gaus",0.0,100);
  g_Werr->SetLineWidth(0.08);
  TF1 *g_Wpull = new TF1("g_Wpull","gaus",-4.0,4.0);
  g_Wpull->SetLineWidth(0.08);
  TF1 *g_Cov_f_N = new TF1("g_Cov_f_N","gaus",-1.0,0.0);
  g_Cov_f_N->SetLineWidth(0.08);
  TF1 *g_Cov_f_W = new TF1("g_Cov_f_W","gaus",0.0,1.0);
  g_Cov_f_W->SetLineWidth(0.08);
  TF1 *g_Cov_W_N = new TF1("g_Cov_W_N","gaus",-1.0,0.0);
  g_Cov_W_N->SetLineWidth(0.08);


  Pull->Sumw2();
  h_fobs->Sumw2();
  h_ferr->Sumw2();
  h_Wobs->Sumw2();
  h_Werr->Sumw2();
  h_WPull->Sumw2();
  h_NPull->Sumw2();
  h_Nobs->Sumw2();
  h_Nobs_err->Sumw2();
  h_Cov_f_Nobs->Sumw2();
  h_Cov_f_W->Sumw2();
  h_Cov_W_N->Sumw2();

  using namespace RooFit;

  RooRealVar* IndL_ = new RooRealVar("IndL","-2ln#lambda",low_range, high_range, "x");

  RooRealVar IndL = *IndL_;

  gROOT->cd();

  RooArgList X(IndL);
  ///////// convert Histograms into RooDataHists                                                                                                            
  RooDataHist* data = new RooDataHist("data","data",X, Template_Data);                                                                                     
  RooDataHist* MC_cor = new RooDataHist("MC_cor","MC_cor", X, Template_SigCor,2);
  RooDataHist* MC_uncor = new RooDataHist("MC_uncor","MC_uncor", X, Template_SigUnCor,2);
  RooDataHist* MC_back = new RooDataHist("MC_back","MC_back", X, Template_Background,2);
  RooHistPdf* data_orig = new RooHistPdf("data","data", X, *data);
  RooHistPdf* Cor_orig = new RooHistPdf("Cor_orig","Cor", X, *MC_cor);
  RooHistPdf* UnCor_orig = new RooHistPdf("UnCor_orig","UnCor", X, *MC_uncor);
  RooHistPdf* Back_orig = new RooHistPdf("Back_orig","Back", X, *MC_back);
  RooDataHist* H_Cor;
  RooDataHist* H_Uncor;
  RooDataHist* H_Back;
  RooDataHist *H_Data;
  TRandom2 *EvGen = new TRandom2();
  EvGen->SetSeed();
  
  double SigEv; 
  double BackEv;

  std::vector<RooDataHist> *hists = new std::vector<RooDataHist>();
  if(Use_data){
    H_Data = new RooDataHist("data","data",X, Template_Data);
    //    for(int i = 0; i < 250; i++){
    //SigEv = EvGen->Poisson(EvPE);
    //H_Data = data_orig->generateBinned(X, SigEv, Extended(kFALSE));
    hists->push_back(*H_Data);
    //    }
    std::cout<<"use the data"<<std::endl;
  }
  else{
    for(int i = 0; i < 250; i++){
      SigEv = EvGen->Poisson(EvPE);
      //      BackEv = EvGen->Poisson(Wfrac*EvPE);
      H_Cor = Cor_orig->generateBinned(X, mix_frac*(1-Wfrac)*SigEv, Extended(kFALSE));
      H_Uncor = UnCor_orig->generateBinned(X, (1-mix_frac)*(1-Wfrac)*SigEv, Extended(kFALSE));
      if(Use_back){H_Back = Back_orig->generateBinned(X, Wfrac*SigEv, Extended(kFALSE));}
      if(H_Cor){
	H_Data = (RooDataHist*) H_Cor->Clone();
	if(H_Uncor){
	  H_Data->add(*H_Uncor);
	}
      }
      else{
	H_Data = (RooDataHist*) H_Uncor->Clone();
      }
      if(Use_back && H_Back){
	  H_Data->add(*H_Back);
      }
      hists->push_back(*H_Data);
    }
  }

  //  FILE *event_f;
  //char name[2000];
  //sprintf(name,"event_f_%f",mix_frac);
  //event_f = fopen(name,"w");  

  for(unsigned int z = 0; z < hists->size(); z++){
    std::vector<double> res;
    if(Use_back){res = FitB((*hists)[z], z);}
    else{res = Fit((*hists)[z], z);}
    std::cout<<"Data has been fitted"<<std::endl;
    double f= (res)[0];
    double Err_f=(res)[1];
    double Nobs = (res)[2];
    double Nerr = (res)[3];
    double Converged = (res)[4];
    double status = (res)[5];
    double edm = (res)[6];
    double Cor_f_N = (res)[7];
    if(status == 0){
      if(!Use_data){Pull->Fill((f - mix_frac)/Err_f);}
      if(Use_data){
	std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
	std::cout<<"Data f = "<<f<<std::endl;
	std::cout<<"delta f = "<<Err_f<<std::endl;
	std::cout<<"N_obs = "<<Nobs<<std::endl;
	std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
      }
      h_fobs->Fill(f);
      h_ferr->Fill(Err_f);
      h_Nobs->Fill(Nobs);
      h_Nobs_err->Fill(Nerr);
      if(!Use_data){h_NPull->Fill((Nobs - (1-Wfrac)*EvPE)/Nerr);}
      h_Cov_f_Nobs->Fill(Cor_f_N);
      //      int NPE = (*EvInPE)[z];
      if(Use_back){
	double W = (res)[8];
	double W_err = (res)[9];
	double cor_f_w = (res)[10];
	double cor_w_N = (res)[11];
	h_Wobs->Fill(W);
	h_Werr->Fill(W_err);
	//	h_WPull->Fill((W - Wfrac)/W_err);
	if(!Use_data){h_WPull->Fill((W - Wfrac*EvPE)/W_err);}
	h_Cov_f_W->Fill(cor_f_w);
	h_Cov_W_N->Fill(cor_w_N);
      }
      //      int ev_cor = (int) ( NPE*mix_frac);
      //int ev_uncor = (int) ( NPE*(1-mix_frac));
      //for(int i = 0; i < ev_cor; i++){
      //fprintf(event_f,"%i\t%.3f\n",i+z*ev_cor,f); //correlated events                                                                                      
      // }
      //for(int i = 0; i < ev_uncor; i++){
      //fprintf(event_f,"%i\t%.3f\n",-1*(i+z*ev_uncor),f); //uncorrelated events                                                                            
      //}
    }
    gStyle->SetOptStat(1111111);
  }

  hists->clear();
  //  fclose(event_f);

  //for running with quality cuts on
  //  h_fobs->Rebin(4);
  //h_ferr->Rebin(4);

  TCanvas *c3 = new TCanvas();
  gStyle->SetOptStat(2200);
  c3->Divide(3,1);
  c3->cd(1);
  h_fobs->Draw();
  h_fobs->Fit("g_f");
  c3->cd(2);
  h_ferr->Draw();
  h_ferr->Fit("g_ferr");
  c3->cd(3);
  Pull->Draw();
  Pull->Fit("g_pull");
  stringstream namef;
  stringstream n;
  n<<"_mix_"<<mix_frac;
  namef<<"fhists_"<<Name<<n.str()<<".C";
  c3->SaveAs(namef.str().c_str());
  TCanvas *c5 = new TCanvas();
  c5->Divide(3,1);
  c5->cd(1);
  h_Nobs->Draw();
  h_Nobs->Fit("g_N");
  c5->cd(2);
  h_Nobs_err->Draw();
  h_Nobs_err->Fit("g_Nerr");
  c5->cd(3);
  h_NPull->Draw();
  h_NPull->Fit("g_pullN");
  stringstream nameP;
  nameP<<"Nobs_"<<Name<<n.str()<<".C";
  c5->SaveAs(nameP.str().c_str());

  if(Use_back){
  TCanvas *c4 = new TCanvas();
  c4->Divide(3,1);
  c4->cd(1);
  h_Wobs->Draw();
  h_Wobs->Fit("g_W");
  c4->cd(2);
  h_Werr->Draw();
  h_Werr->Fit("g_Werr");
  c4->cd(3);
  h_WPull->Draw();
  h_WPull->Fit("g_Wpull");
  stringstream nameW;
  nameW<<"Whists_"<<Name<<n.str()<<".C";
  c4->SaveAs(nameW.str().c_str());
  TCanvas *c11 = new TCanvas();
  c11->Divide(3,1);
  c11->cd(1);
  h_Cov_f_W->Draw();
  h_Cov_f_W->Fit("g_Cov_f_W");
  c11->cd(2);
  h_Cov_f_Nobs->Draw();
  h_Cov_f_Nobs->Fit("g_Cov_f_N");
  c11->cd(3);
  h_Cov_W_N->Draw();
  h_Cov_W_N->Fit("g_Cov_W_N");
  stringstream nameCovW;
  nameCovW<<"Correlation_"<<Name<<n.str()<<".C";
  c11->SaveAs(nameCovW.str().c_str());
  }
  //  else{
  // TCanvas *c10 = new TCanvas();
  //c10->cd();
  //h_Cov_f_Nobs->Draw();
  //h_Cov_f_Nobs->Fit("g_Cov_f_N");
  //stringstream nameCov;
  //nameCov<<"Correlation_"<<Name<<n.str()<<".C";
  //c10->SaveAs(nameCov.str().c_str());
  //}

  double f_av = g_f->GetParameter(1);
  double f_averr = g_f->GetParError(1);
  double p_av = g_pull->GetParameter(1);
  double p_averr = g_pull->GetParError(1);
  double p_RMS = g_pull->GetParameter(2);
  double p_RMSerr = g_pull->GetParError(2);

  if((f_av + 3*f_averr) < mix_frac || (f_av - 3*f_averr) > mix_frac){std::cout<<"%%%%%% WARNING! The mean f is biased for mixing fraction "<<mix_frac<<std::endl;}
  if((p_av + 3*p_averr) < 0 || (p_av - 3*p_averr) > 0){std::cout<<"%%%%%% WARNING! The pull mean is biased for mixing fraction "<<mix_frac<<std::endl;}
  if((p_RMS + 3*p_RMSerr) < 1 || (p_RMS - 3*p_RMSerr) > 1){std::cout<<"%%%%%% WARNING! The errors are not properly estimated for mixing fraction "<<mix_frac<<std::endl;}

  std::vector<double> Results;
  Results.push_back(f_av);
  Results.push_back(f_averr);
  Results.push_back(p_av);
  Results.push_back(p_averr);
  Results.push_back(p_RMS);
  Results.push_back(p_RMSerr);
  return Results;
}

std::vector<double> eire::PEBuilder::Fit(RooDataHist hist_data, int k)
{

  using namespace RooFit;
  RooMsgService::instance().setSilentMode(1);
  RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);  

  RooRealVar* IndL_ = new RooRealVar("IndL","-2ln#lambda",low_range, high_range, "x");

  RooRealVar IndL = *IndL_;

  gROOT->cd();
  
  RooArgList X(IndL);
  ///////// convert Histograms into RooDataHists                                                                                                           
  RooDataHist* data = &hist_data;
  RooDataHist* MC_cor = new RooDataHist("MC_cor","MC_cor", X, Template_SigCor,2);
  RooDataHist* MC_uncor = new RooDataHist("MC_uncor","MC_uncor", X, Template_SigUnCor,2);
  //  RooHistPdf* Cor_orig = new RooHistPdf("Cor_orig","Cor", X, *MC_cor);
  //RooHistPdf* UnCor_orig = new RooHistPdf("UnCor_orig","UnCor", X, *MC_uncor);
  
  // RooDataHist* H_Cor = Cor_orig->generateBinned(X, 350000, Extended(kTRUE)); 
  //RooDataHist* H_Uncor = UnCor_orig->generateBinned(X, 350000, Extended(kTRUE)); 

  RooHistPdf* Cor = new RooHistPdf("Cor","Cor", X, *MC_cor);
  RooHistPdf* UnCor = new RooHistPdf("UnCor","UnCor", X, *MC_uncor);

  RooArgList *Sum = new RooArgList();
  Sum->add(*Cor);
  Sum->add(*UnCor);

  RooRealVar* f = new RooRealVar("f","f",0.5);
  f->setConstant(kFALSE);

  RooRealVar* nEv = new RooRealVar("nEv","nEv",data->sumEntries());
  nEv->setConstant(kFALSE);
  
  RooFormulaVar* nCor = new RooFormulaVar("nCor","f*nEv",RooArgList(*f,*nEv));
  RooFormulaVar* nUnCor = new RooFormulaVar("nUnCor","(1-f)*nEv",RooArgList(*f,*nEv));

  RooArgList yields(*nCor,*nUnCor);
    //RooArgList yields(*f);
  RooAddPdf pdfPass("pdfPass","sum pdf", *Sum, yields);//f*cor+(1-f)*uncor                                                              
                   
  RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Save(),SumW2Error(kFALSE),Hesse(kTRUE),InitialHesse(kTRUE),Extended(kTRUE),Minimizer("Minuit2","migrad"));
  //  RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Save(),SumW2Error(kFALSE),Hesse(kTRUE),InitialHesse(kTRUE),Extended(kFALSE),Minimizer("Minuit2","migrad"));
  if(verbose){
  TString cname = TString("fit");
  TCanvas* c = new TCanvas(cname,cname,500,500);
  RooPlot* frame1 = IndL.frame();
  frame1->SetMinimum(-5);
  RooAbsData::ErrorType errorType = RooAbsData::SumW2;
  data->plotOn(frame1,RooFit::DataError(errorType));
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*Cor),RooFit::LineColor(kRed));
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*UnCor),RooFit::LineColor(kGreen));
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data));
  pdfPass.plotOn(frame1);
  pdfPass.paramOn(frame1,data);
  frame1->Draw("e0");
  char fname[1024];
  sprintf(fname,"fit_%i.pdf",k);
  c->SaveAs(fname);
  delete c;
  }

  std::vector<double> results;
  double x = f->getVal();
  results.push_back(x);
  double Err = f->getError();
  results.push_back(Err);
  double N = nEv->getVal();
  results.push_back(N);
  double N_err = nEv->getError();
  results.push_back(N_err);
  results.push_back(fitResult->covQual());
  results.push_back(fitResult->status());
  results.push_back(fitResult->edm());
  double cov = (fitResult->correlationMatrix())[0][1];
  results.push_back(fabs(cov));
  return results;
}

std::vector<double> eire::PEBuilder::FitB(RooDataHist hist_data, int k)
{

  using namespace RooFit;
  RooMsgService::instance().setSilentMode(1);
  RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);  
  
  //  RooRealVar* IndL_ = new RooRealVar("IndL","IndL",low_range, high_range, "x");
  RooRealVar* IndL_ = new RooRealVar("IndL","IndL",low_range, high_range, "x");
  RooRealVar IndL = *IndL_;
  
  gROOT->cd();
  
  RooArgList X(IndL);
  ///////// convert Histograms into RooDataHists                                                                                                           
  RooDataHist* data = &hist_data;
  RooDataHist* MC_cor = new RooDataHist("MC_cor","MC_cor", X, Template_SigCor,2);
  RooDataHist* MC_uncor = new RooDataHist("MC_uncor","MC_uncor", X, Template_SigUnCor,2);
  RooDataHist* MC_WJets = new RooDataHist("MC_WJets","WJets", X, Template_Background,2);

  RooHistPdf* Cor = new RooHistPdf("Cor","Cor", X, *MC_cor);
  RooHistPdf* UnCor = new RooHistPdf("UnCor","UnCor", X, *MC_uncor);
  RooHistPdf* WJets = new RooHistPdf("WJets","WJets", X, *MC_WJets);
  
  RooArgList *Sum = new RooArgList();
  Sum->add(*WJets);
  Sum->add(*Cor);
  Sum->add(*UnCor);
  
  RooRealVar* f = new RooRealVar("f","f",0.5);
  f->setConstant(kFALSE);

  //  RooRealVar* nEv = new RooRealVar("nEv","nEv",hist_data->GetEntries(),0.90*hist_data->GetEntries(),1.10*hist_data->GetEntries());
  //  std::cout<<"get entries = "<<hist_data->GetEntries()<<std::endl;
  //RooRealVar* nEv = new RooRealVar("nEv","nEv",hist_data->GetEntries(),0.5*hist_data->GetEntries(),1.5*hist_data->GetEntries());
  RooRealVar* nEv = new RooRealVar("nEv","nEv",data->sumEntries());
  nEv->setConstant(kFALSE);

  RooRealVar* nTt = new RooRealVar("nTt","nTt",0.9*data->sumEntries());
  nTt->setConstant(kFALSE);
  
  RooRealVar* nBkg = new RooRealVar("nBkg","nBkg",0.1*data->sumEntries());
  nBkg->setConstant(kFALSE);

  RooRealVar* W = new RooRealVar("W","W",0.10);//,0.09,0.11);
  W->setConstant(kFALSE);

  //RooFormulaVar* W = new RooFormulaVar("W","nBkg/nEv*nTot",RooArgList(*nBkg,*nEv,*nTot));//,0.09,0.11);
  //  W->setConstant(false);

  /*  
  RooFormulaVar* nCor = new RooFormulaVar("nCor","nEv*f*(1-W)",RooArgList(*nEv,*f,*W));
  RooFormulaVar* nUnCor = new RooFormulaVar("nUnCor","nEv*(1-f)*(1-W)",RooArgList(*nEv,*f,*W));
  */

  RooFormulaVar* nCor = new RooFormulaVar("nCor","(nTt+nBkg)*nTt*f/(nTt+nBkg)",RooArgList(*nTt,*f,*nBkg));
  RooFormulaVar* nUnCor = new RooFormulaVar("nUnCor","(nTt+nBkg)*nTt*(1-f)/(nTt+nBkg)",RooArgList(*nTt,*f,*nBkg));


  //RooRealVar* nCor = new RooRealVar("nCor",0.5*hist_data->GetEntries());
  //nCor->setConstant(false);
  //RooRealVar* nUnCor = new RooRealVar("nUnCor",0.5*hist_data->GetEntries());
  //nUnCor->setConstant(false);

  //  RooFormulaVar* nW = new RooFormulaVar("nW","W*nEv",RooArgList(*W,*nEv));

  RooFormulaVar* nW = new RooFormulaVar("nW","(nTt+nBkg)*nBkg/(nTt+nBkg)",RooArgList(*nTt,*nBkg));

  //  RooRealVar* nCor = new RooRealVar("nCor","nCor",0.5*hist_data->GetEntries());
  //nCor->setConstant(kFALSE);
  //RooRealVar* nUnCor = new RooRealVar("nUnCor","nUnCor",0.5*hist_data->GetEntries());
  //nUnCor->setConstant(kFALSE);
  //std::cout<<"nW = "<<0.10*hist_data->GetEntries()<<std::endl;
  //RooRealVar* nW  = new RooRealVar("nW","nW", 0.10*hist_data->GetEntries(),0.,hist_data->GetEntries());
  //RooRealVar* nW  = new RooRealVar("nW","nW", 0.10*hist_data->GetEntries());
  //nW->setConstant(false);

  //RooArgList yields(*d, *f);
  //RooArgList nSig(*nCor, *nUnCor);
  //RooAddPdf Sig("Sig","sum pdf", *Signal, nSig, kTRUE);
  //  RooArgList yields(*nW, *nCor, *nUnCor);
  RooArgList yields(*nW, *nCor, *nUnCor);//nW
  RooAddPdf pdfPass("pdfPass","sum pdf", *Sum, yields, kFALSE);//kFALSE = not recursive
  
  RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Save(),SumW2Error(kFALSE),Extended(kTRUE));
  if(verbose){
  TString cname = TString("fit");
  TCanvas* c = new TCanvas(cname,cname,500,500);                                                                                                       
  RooPlot* frame1 = IndL.frame();                                                                                                                      
  frame1->SetMinimum(-5);                                                                                                                               
  RooAbsData::ErrorType errorType = RooAbsData::SumW2;                                                                                                 
  data->plotOn(frame1,RooFit::DataError(errorType));                                                                                                   
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*Cor),RooFit::LineColor(kRed));                                                    
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*UnCor),RooFit::LineColor(kGreen));                                                
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*WJets),RooFit::LineColor(kOrange));                                               
  //  pdfPass.plotOn(frame1,RooFit::ProjWData(*data));                                                                                                     
  pdfPass.plotOn(frame1);                                                                                                                        
  pdfPass.paramOn(frame1,data);                                                                                                                        
  frame1->Draw("e0");                                                                                                                                  
  char fname[1024];                                                                                                                                    
  sprintf(fname,"fit_%i.pdf",k);                                                                                                                 
  c->SaveAs(fname);                                                                                                                                    
  delete c;                                                                                                                                            
  }
  std::vector<double> results;
  double x = f->getVal();
  results.push_back(x);
  double Err = f->getError();
  results.push_back(Err);
  double N = nTt->getVal();
  results.push_back(N);
  double N_err = nTt->getError();
  //double N_err = 1;
  results.push_back(N_err);
  results.push_back(fitResult->covQual());
  results.push_back(fitResult->status());
  results.push_back(fitResult->edm());
  //  std::cout<<"f= "<<f->getVal()<<" + "<<f->getError()<<" nEv= "<<nEv->getVal()<<" + "<<nEv->getError()<<" nW = "<<W->getVal()<<" + "<<W->getError()<<std::endl;
  //  std::cout<<"cor f,N= "<<(fitResult->correlationMatrix())[1][2]<<" cor f, W= "<<(fitResult->correlationMatrix())[0][1]<<" cor W,N = "<<(fitResult->correlationMatrix())[0][2]<<std::endl;
  double cov = (fitResult->correlationMatrix())[0][2];
  results.push_back(cov);
  results.push_back(nBkg->getVal());
  results.push_back(nBkg->getError());
  results.push_back((fitResult->correlationMatrix())[0][1]);
  results.push_back((fitResult->correlationMatrix())[1][2]);
  return results;
}
