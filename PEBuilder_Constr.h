#ifndef EIRE_PEBUILDER_H
#define EIRE_PEBUILDER_H
#include <math.h>
#include "TRandom2.h"
#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include "TROOT.h"
#include "TSystem.h"
#include "TVirtualFitter.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "RooMsgService.h"
#include "TH1.h"
#include "TH2.h"
#include "TStyle.h"
#include <RooFit.h>
#include <RooMinuit.h>
#include "RooMinimizer.h"
#include "RooFitResult.h"
#include <RooGlobalFunc.h>
#include <RooPlot.h>
#include "RooTFnBinding.h"
#include <TMultiGraph.h>
#include "TF2.h"
#include "RooCategory.h"
#include "TStyle.h"
#include <iomanip>
#include <string>
#include <cmath>
#include <cstdlib>
#include "TFile.h"
#include "TLegend.h"
#include "TTree.h"
#include "TPaveText.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TVector.h"
#include "TCanvas.h"
#include "TProfile.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooGenericPdf.h"
#include "RooPolynomial.h"
#include "RooChi2Var.h"
#include "RooMinuit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include <vector>
#include <TLeaf.h>
#include "RooHistPdf.h"
#include "RooAddPdf.h"
#include "RooAbsPdf.h"
 
using namespace std;

namespace eire{
  class PEBuilder{
  public:
    PEBuilder(std::vector< std::vector<float> > Cor, std::vector< std::vector<float> > Uncor, std::vector< std::vector<float> > WJets, float Back_frac, bool use_back, string name, int Bins, int EvPerPE, float lowRange, float highRange,bool v);
    ~PEBuilder();
    void SetEventWeights(std::vector<std::vector<float> > w_cor, std::vector<std::vector<float> > w_uncor, std::vector<std::vector<float> > w_back, float cor_errors, float uncor_errors, float background_errors);
    //    std::vector<double> Run(int Bins, int EvPerPE, float lowRange, float highRange, bool v, float mixing);
    void AddDataSet(std::vector< std::vector<float> > Likelihoods, float weight, float data_errors);
    void AddCorTemplate(std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float cor_errors);
    void AddUncorTemplate(std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float uncor_errors);
    void AddBackgroundTemplate(std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float background_errors);
    //    void SetProperties(int Bins, int EvPerPE, float lowRange, float highRange, bool v);
    void DrawTemplates();
    void SetUpWeights(std::vector< std::string > s);
    std::vector<double> Mix(float mixing);

  private:
    std::vector<double> Fit(RooDataHist hist_data, int k);
    std::vector<double> FitB(RooDataHist hist_data, int k, double LW, double sLW, double LT, double sLT);
    std::vector<double> FitL(RooDataHist hist_data, int k, RooHistPdf *L_s, RooHistPdf *L_b);
    std::vector< std::vector<float> > L_Cor;
    std::vector< std::vector<float> > L_Uncor;
    std::vector< std::vector<float> > L_Background;
    std::vector< std::vector<float> > L_Data;
    std::vector<float> Cor_weights;
    std::vector<float> Uncor_weights;
    std::vector<float> Background_weights;
    std::vector<float> Data_weights;
    int nBins;
    int EvPE;
    float low_range;
    float high_range;
    float mix_frac;
    bool verbose;
    string Name;

    TH1F* Template_SigCor;
    TH1F* Template_SigUnCor;
    TH1F* LikelihoodCutCor;
    TH1F* LikelihoodCutUnCor;
    TH1F* LikelihoodCutUnCorCor;
    TH1F* LikelihoodCutUnCorUnCor;
    TH2F* LikelihoodCorVsUnCor_corsample;
    TH2F* LikelihoodCorVsUnCor_uncorsample;
    TH1F* Template_Data;
    TH1F* LikelihoodCutData;
    TH1F* LikelihoodCutUnCorData;
    TH2F* LikelihoodCorVsUnCor_Data;

    TH1F* Template_Background;
    TH1F* LikelihoodCutBackground;
    TH1F* LikelihoodCutUnCorBackground;
    TH2F* LikelihoodCorVsUnCor_Background;
    bool Use_back;
    bool Use_data;
    float Wfrac;
    map<int, bool> w_setup;
  };
}

#endif
