#include "Linearity.h"

eire::Linearity::Linearity(string name)
{
  Name = name;
}

eire::Linearity::~Linearity()
{
}

void eire::Linearity::AddPointF(int index, double f_gen, double f_obs, double f_obs_err, double p_av, double p_averr, double p_rms, double p_rmserr)
{
  x[index] = f_gen;
  y_f[index] = f_obs;
  y_ferr[index] = f_obs_err;
  P_fav[index] = p_av;
  P_faverr[index] = p_averr;
  P_frms[index] = p_rms;
  P_frmserr[index] = p_rmserr;
}

void eire::Linearity::AddPointNtt(int index, double f_gen, double f_obs, double f_obs_err, double p_av, double p_averr, double p_rms, double p_rmserr)
{
  Ntt_in = f_gen;
  y_Ntt[index] = f_obs;
  y_Ntterr[index] = f_obs_err;
  P_Nttav[index] = p_av;
  P_Nttaverr[index] = p_averr;
  P_Nttrms[index] = p_rms;
  P_Nttrmserr[index] = p_rmserr;
}

void eire::Linearity::AddPointNbkg(int index, double f_gen, double f_obs, double f_obs_err, double p_av, double p_averr, double p_rms, double p_rmserr)
{
  Nbkg_in = f_gen;
  y_Nbkg[index] = f_obs;
  y_Nbkgerr[index] = f_obs_err;
  P_Nbkgav[index] = p_av;
  P_Nbkgaverr[index] = p_averr;
  P_Nbkgrms[index] = p_rms;
  P_Nbkgrmserr[index] = p_rmserr;
}

void eire::Linearity::Plot()
{
  double f_d[5];
  f_d[0] = y_f[0] - x[0];
  f_d[1] = y_f[1] - x[1];
  f_d[2] = y_f[2] - x[2];
  f_d[3] = y_f[3] - x[3];
  f_d[4] = y_f[4] - x[4];

  double Ntt_d[5];
  Ntt_d[0] = y_Ntt[0] - Ntt_in;
  Ntt_d[1] = y_Ntt[1] - Ntt_in;
  Ntt_d[2] = y_Ntt[2] - Ntt_in;
  Ntt_d[3] = y_Ntt[3] - Ntt_in;
  Ntt_d[4] = y_Ntt[4] - Ntt_in;

  double Nbkg_d[5];
  Nbkg_d[0] = y_Nbkg[0] - Nbkg_in;
  Nbkg_d[1] = y_Nbkg[1] - Nbkg_in;
  Nbkg_d[2] = y_Nbkg[2] - Nbkg_in;
  Nbkg_d[3] = y_Nbkg[3] - Nbkg_in;
  Nbkg_d[4] = y_Nbkg[4] - Nbkg_in;

  TCanvas *c1 = new TCanvas("c1", "c1",0,0,600,500);
  c1->Range(0,0,1,1);
  TPad *c1_1 = new TPad("c1_1", "newpad",0.01,0.01,0.99,0.32);
  c1_1->Draw();
  c1_1->cd();
  c1_1->SetTopMargin(0.01);
  c1_1->SetBottomMargin(0.3);
  c1_1->SetRightMargin(0.1);
  c1_1->SetFillStyle(0);
  TH1F *h2 = c1->DrawFrame(-0.15,-0.1,1.15,0.1);
  h2->GetXaxis()->SetTitle("f_{input}");
  h2->GetXaxis()->SetTitleSize(0.08);
  h2->GetXaxis()->SetLabelSize(0.08);
  h2->GetYaxis()->SetTitle("f_{measured} - f_{input}");
  h2->GetYaxis()->SetTitleSize(0.08);
  h2->GetYaxis()->SetLabelSize(0.08);
  h2->GetYaxis()->SetTitleOffset(0.55);
  TLine *l = new TLine(-0.15,0,1.15,0);
  TGraphErrors *gr_d = new TGraphErrors(5,x,f_d,0,y_ferr);
  gr_d->SetMarkerStyle(8);
  gr_d->SetMarkerSize(0.7);
  gr_d->Draw("p");
  l->Draw();
  c1->cd();
  TPad *c1_2 = new TPad("c1_2", "newpad",0.01,0.33,0.99,0.99);
  c1_2->Draw();
  c1_2->cd();
  c1_2->SetTopMargin(0.1);
  c1_2->SetBottomMargin(0.01);
  c1_2->SetRightMargin(0.1);
  c1_2->SetFillStyle(0);

  TH1F *h1 = c1->DrawFrame(-0.15,-0.15,1.15,1.15);
  h1->GetXaxis()->SetTitle("f_{input}");
  h1->GetYaxis()->SetTitle("f_{measured}");
  h1->GetYaxis()->SetTitleOffset(0.8);
  h1->GetYaxis()->SetTitleSize(0.05);


  TF1 *ff1 = new TF1("ff1","[0]+[1]*x",0,1);
  TGraphErrors *gr = new TGraphErrors(5,x,y_f,0,y_ferr);
  gr->SetMarkerStyle(8);
  gr->SetMarkerSize(0.7);
  gr->SetTitle();
  gr->Draw("p");
  gr->Fit(ff1,"R");
  //  TFitResult *result = gr->Fit(ff1,"R").Get();
  //double cor_p0_p1 = (result->GetCorrelationMatrix())[0][1];
  //std::cout<<"correlation between p0 and p1 = "<<cor_p0_p1<<std::endl;
  c1->Modified();
  c1->Update();
  c1->SaveAs(("Linearity_"+Name+".C").c_str());
  c1->SaveAs(("Linearity_"+Name+".pdf").c_str());

  double a = ff1->GetParameter(1);
  double a_err = ff1->GetParError(1);
  double b = ff1->GetParameter(0);
  double b_err = ff1->GetParError(0);

  if((a+3*a_err) < 1 || (a-3*a_err) > 1){std::cout<<"%%%%%%%% WARNING!  slope of Linearity curve is biased!"<<std::endl;}
  if((b+3*b_err) < 0 || (b-3*b_err) > 0){std::cout<<"%%%%%%%% WARNING!  offset of Linearity curve is biased!"<<std::endl;}

  TCanvas *c2 = new TCanvas();
  c2->Divide(2,1);
  c2->cd(1);
  TH1F *h3 = c1->DrawFrame(-0.15,-0.25,1.15,0.25);
  h3->GetXaxis()->SetTitle("f_{input}");
  h3->GetXaxis()->SetTitleSize(0.05);
  h3->GetYaxis()->SetTitle("Pull mean");
  h3->GetYaxis()->SetTitleOffset(1.5);
  
  TGraphErrors *PAv = new TGraphErrors(5,x,P_fav,0,P_faverr);
  PAv->SetMarkerStyle(8);
  PAv->SetMarkerSize(0.7);
  PAv->SetTitle();
  PAv->Draw("p");
  TLine *l2 = new TLine(0,0,1,0);
  l2->Draw("same");
  c2->Modified();
  c2->Update();
  c2->cd();
  c2->cd(2);
  TH1F *h4 = c1->DrawFrame(-0.15,0.8,1.15,1.2);
  h4->GetXaxis()->SetTitle("f_{input}");
  h4->GetXaxis()->SetTitleSize(0.05);
  h4->GetYaxis()->SetTitle("Pull RMS");
  h4->GetYaxis()->SetTitleOffset(1.5);
  TGraphErrors *PRMS = new TGraphErrors(5,x,P_frms,0,P_frmserr);
  //TF1 *P1 = new TF1("P1","[0]",0,1);
  PRMS->SetMarkerStyle(8);
  PRMS->SetMarkerSize(0.7);
  PRMS->SetTitle();
  PRMS->Draw("p");
  //  PRMS->Fit(P1,"R");
  //std::cout<<"error correction factor = "<<P1->GetParameter(0)<<" +- "<<P1->GetParError(0)<<std::endl;
  TLine *l3 = new TLine(0,1,1,1);
  l3->Draw("same");
  c2->Modified();
  c2->Update();
  c2->SaveAs(("PullLins_"+Name+".C").c_str());
  c2->SaveAs(("PullLins_"+Name+".pdf").c_str());

  TCanvas *cNtt = new TCanvas();
  cNtt->cd();
  TH1F *htt = cNtt->DrawFrame(-0.15,-20,1.15,20);
  htt->GetXaxis()->SetTitle("f_{input}");
  htt->GetYaxis()->SetTitle("N_{t#bart}^{meas} - N_{t#bart}^{input}");
  htt->GetYaxis()->SetTitleOffset(0.8);
  htt->GetYaxis()->SetTitleSize(0.05);
  TGraphErrors *GraphNtt = new TGraphErrors(5,x,Ntt_d,0,y_Ntterr);
  TF1 *FNtt = new TF1("FNtt","[0]+[1]*x",0,1);
  GraphNtt->SetMarkerStyle(8);
  GraphNtt->SetMarkerSize(0.7);
  GraphNtt->SetTitle();
  GraphNtt->Draw("p");
  GraphNtt->Fit(FNtt,"R");
  cNtt->Modified();
  cNtt->Update();
  cNtt->SaveAs(("Linearity_Ntt_"+Name+".C").c_str());
  cNtt->SaveAs(("Linearity_Ntt_"+Name+".pdf").c_str());

  TCanvas *cNtt_2 = new TCanvas();
  cNtt_2->Divide(2,1);
  cNtt_2->cd(1);
  TH1F *h5 = c1->DrawFrame(-0.15,-0.25,1.15,0.25);
  h5->GetXaxis()->SetTitle("f_{input}");
  h5->GetXaxis()->SetTitleSize(0.05);
  h5->GetYaxis()->SetTitle("Pull mean N_{t#bart}");
  h5->GetYaxis()->SetTitleOffset(1.5);
  TGraphErrors *PAv_Ntt = new TGraphErrors(5,x,P_Nttav,0,P_Nttaverr);
  PAv_Ntt->SetMarkerStyle(8);
  PAv_Ntt->SetMarkerSize(0.7);
  PAv_Ntt->SetTitle();
  PAv_Ntt->Draw("p");
  //  TLine *l2 = new TLine(0,0,1,0);
  l2->Draw("same");
  cNtt_2->Modified();
  cNtt_2->Update();
  cNtt_2->cd();
  cNtt_2->cd(2);
  TH1F *h6 = c1->DrawFrame(-0.15,0.8,1.15,1.2);
  h6->GetXaxis()->SetTitle("f_{input}");
  h6->GetXaxis()->SetTitleSize(0.05);
  h6->GetYaxis()->SetTitle("Pull RMS N_{t#bart}");
  h6->GetYaxis()->SetTitleOffset(1.5);
  TGraphErrors *PRMS_Ntt = new TGraphErrors(5,x,P_Nttrms,0,P_Nttrmserr);
  // TF1 *P1 = new TF1("P1","[0]",0,1);
  PRMS_Ntt->SetMarkerStyle(8);
  PRMS_Ntt->SetMarkerSize(0.7);
  PRMS_Ntt->SetTitle();
  PRMS_Ntt->Draw("p");
  //  TLine *l3 = new TLine(0,1,1,1);
  l3->Draw("same");
  cNtt_2->Modified();
  cNtt_2->Update();
  cNtt_2->SaveAs(("PullLins_Ntt_"+Name+".C").c_str());
  cNtt_2->SaveAs(("PullLins_Ntt_"+Name+".pdf").c_str());

  TCanvas *cNbkg = new TCanvas();
  cNbkg->cd();
  TH1F *hbkg = cNbkg->DrawFrame(-0.15,-20,1.15,20);
  hbkg->GetXaxis()->SetTitle("f_{input}");
  hbkg->GetYaxis()->SetTitle("N_{bkg}^{meas} - N_{bkg}^{input}");
  hbkg->GetYaxis()->SetTitleOffset(0.8);
  hbkg->GetYaxis()->SetTitleSize(0.05);
  TGraphErrors *GraphNbkg = new TGraphErrors(5,x,Nbkg_d,0,y_Nbkgerr);
  TF1 *FNbkg = new TF1("FNbkg","[0]+[1]*x",0,1);
  GraphNbkg->SetMarkerStyle(8);
  GraphNbkg->SetMarkerSize(0.7);
  GraphNbkg->SetTitle();
  GraphNbkg->Draw("p");
  GraphNbkg->Fit(FNbkg,"R");
  cNbkg->Modified();
  cNbkg->Update();
  cNbkg->SaveAs(("Linearity_Nbkg_"+Name+".C").c_str());
  cNbkg->SaveAs(("Linearity_Nbkg_"+Name+".pdf").c_str());

  TCanvas *cNbkg_2 = new TCanvas();
  cNbkg_2->Divide(2,1);
  cNbkg_2->cd(1);
  TH1F *h7 = c1->DrawFrame(-0.15,-0.25,1.15,0.25);
  h7->GetXaxis()->SetTitle("f_{input}");
  h7->GetXaxis()->SetTitleSize(0.05);
  h7->GetYaxis()->SetTitle("Pull Mean N_{bkg}");
  h7->GetYaxis()->SetTitleOffset(1.5);
  TGraphErrors *PAv_Nbkg = new TGraphErrors(5,x,P_Nbkgav,0,P_Nbkgaverr);
  PAv_Nbkg->SetMarkerStyle(8);
  PAv_Nbkg->SetMarkerSize(0.7);
  PAv_Nbkg->SetTitle();
  PAv_Nbkg->Draw("p");
  //  TLine *l2 = new TLine(0,0,1,0);
  l2->Draw("same");
  cNbkg_2->Modified();
  cNbkg_2->Update();
  cNbkg_2->cd();
  cNbkg_2->cd(2);
  TH1F *h8 = c1->DrawFrame(-0.15,0.8,1.15,1.2);
  h8->GetXaxis()->SetTitle("f_{input}");
  h8->GetXaxis()->SetTitleSize(0.05);
  h8->GetYaxis()->SetTitle("Pull RMS N_{bkg}");
  h8->GetYaxis()->SetTitleOffset(1.5);
  TGraphErrors *PRMS_Nbkg = new TGraphErrors(5,x,P_Nbkgrms,0,P_Nbkgrmserr);
  // TF1 *P1 = new TF1("P1","[0]",0,1);
  PRMS_Nbkg->SetMarkerStyle(8);
  PRMS_Nbkg->SetMarkerSize(0.7);
  PRMS_Nbkg->SetTitle();
  PRMS_Nbkg->Draw("p");
  //  TLine *l3 = new TLine(0,1,1,1);
  l3->Draw("same");
  cNbkg_2->Modified();
  cNbkg_2->Update();
  cNbkg_2->SaveAs(("PullLins_Nbkg_"+Name+".C").c_str());
  cNbkg_2->SaveAs(("PullLins_Nbkg_"+Name+".pdf").c_str());

}

