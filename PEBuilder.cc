#include "PEBuilder.h"

//Pseudo-Experiment Builder
//constructor
eire::PEBuilder::PEBuilder(float Back_frac, bool use_back, string name, int x_Bins, int EvPerPE, float x_lowRange, float x_highRange, int y_Bins, float y_lowRange, float y_highRange,bool v, float shift_weight, std::vector<string> X_binning, std::vector<string> Y_binning)
{
  //set the number of bins and range for the templates
  weight_shift1 = shift_weight;
  if(weight_shift1 == 0){weight_shift1 = 1;}
  x_nBins = x_Bins;
  y_nBins = y_Bins;
  EvPE = EvPerPE;
  x_low_range = x_lowRange;
  x_high_range = x_highRange;
  y_low_range = y_lowRange;
  y_high_range = y_highRange;
  verbose = v;

  vector_of_xbins = X_binning;
  vector_of_ybins = Y_binning;
  float x_binning[x_nBins];
  float y_binning[y_nBins];
  for(unsigned int i = 0; i < vector_of_xbins.size(); i++){
    x_binning[i] = atof((vector_of_xbins)[i].c_str());
  }

  for(unsigned int i = 0; i < vector_of_ybins.size(); i++){
    y_binning[i] = atof((vector_of_ybins)[i].c_str());
  }

  //initialise the template histograms
  //RatioVsLCor_SigCor = new TH2F("RatioVsLCor_SigCor","-2ln#lambda vs. -lnL_{H=C} TTbar correlated ;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning, y_nBins,y_binning);
  RatioVsLCor_SigCor = new TH2F("RatioVsLCor_SigCor","-2ln#lambda vs. -lnL_{H=C} TTbar correlated ;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range, y_nBins,y_low_range,y_high_range);
  //  RatioVsLCor_SigUnCor = new TH2F("RatioVsLCor_SigUnCor","-2ln#lambda vs. -lnL_{H=C} TTbar uncorrelated;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  RatioVsLCor_SigUnCor = new TH2F("RatioVsLCor_SigUnCor","-2ln#lambda vs. -lnL_{H=C} TTbar uncorrelated;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range,y_high_range);
  //  Template_SigCor = new TH1F("Correlated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  Template_SigCor = new TH1F("Correlated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  //  Template_SigUnCor = new TH1F("UnCorrelated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  Template_SigUnCor = new TH1F("UnCorrelated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  LikelihoodCutCor = new TH1F("LikelihoodCutCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCor = new TH1F("LikelihoodCutUnCor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorCor = new TH1F("LikelihoodCutUnCorCor","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCutUnCorUnCor = new TH1F("LikelihoodCutUnCorUnCor","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_corsample = new TH2F("LikelihoodCorVsUnCor_corsample","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);
  LikelihoodCorVsUnCor_uncorsample = new TH2F("LikelihoodCorVsUnCor_uncorsample","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);
  //  RatioVsLCor_Data = new TH2F("RatioVsLCor_Data","-2ln#lambda vs. -lnL_{H=C} Data ;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  //Template_Data = new TH1F("Data","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  RatioVsLCor_Data = new TH2F("RatioVsLCor_Data","-2ln#lambda vs. -lnL_{H=C} Data ;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range,y_high_range);
  Template_Data = new TH1F("Data","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  LikelihoodCutData = new TH1F("LikelihoodCutData","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorData = new TH1F("LikelihoodCutUnCorData","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_Data = new TH2F("LikelihoodCorVsUnCor_Data","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  //  RatioVsLCor_Background_Cor = new TH2F("RatioVsLCor_Background_Cor","-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning, y_nBins, y_binning);
  //RatioVsLCor_Background_Uncor = new TH2F("RatioVsLCor_Background_Uncor","-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning, y_nBins, y_binning);
  RatioVsLCor_Background_Cor = new TH2F("RatioVsLCor_Background_Cor","-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range, x_high_range,y_nBins, y_low_range,y_high_range);
  RatioVsLCor_Background_Uncor = new TH2F("RatioVsLCor_Background_Uncor","-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range, y_nBins, y_low_range,y_high_range);
  //  Template_Background_Cor = new TH1F("Background_Cor","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  Template_Background_Cor = new TH1F("Background_Cor","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  LikelihoodCutBackground_Cor = new TH1F("LikelihoodCutBackground_Cor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorBackground_Cor = new TH1F("LikelihoodCutUnCorBackground_Cor","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_Background_Cor = new TH2F("LikelihoodCorVsUnCor_Background_Cor","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);
  //  Template_Background_Uncor = new TH1F("Background_Uncor","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  Template_Background_Uncor = new TH1F("Background_Uncor","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  LikelihoodCutBackground_Uncor = new TH1F("LikelihoodCutBackground_Uncor","-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  LikelihoodCutUnCorBackground_Uncor = new TH1F("LikelihoodCutUnCorBackground_Uncor","-lnL_{H=U};-lnL_{H=U};entries", 50, 40, 100);
  LikelihoodCorVsUnCor_Background_Uncor = new TH2F("LikelihoodCorVsUnCor_Background_Uncor","-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);
  
  Wfrac = Back_frac;
  Use_back = use_back;
  Use_data = false;
  Name = name;

}

//destructor
eire::PEBuilder::~PEBuilder()
{
}

//read in which weights should be applied
void eire::PEBuilder::SetUpWeights(std::vector<std::string> s)
{
  if(s.size() < 14){std::cout<<"%%%%%%%%%ERROR%%%%%%%%: event weights are not configured properly! size is "<<s.size()<<std::endl;}
  //  std::cout<<"weights setup"<<std::endl;

  for(unsigned int i = 0; i < s.size(); i++)
    {
      std::cout<<atoi(((s)[i]).c_str())<<std::endl;
      w_setup[i] = atoi(((s)[i]).c_str());
    }
}

//if running on data, add the Dataset
std::vector<TH1F> eire::PEBuilder::AddDataSet(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, float data_errors, float shift_x, float shift_y)
{
  float x_binning[x_nBins];
  float y_binning[y_nBins];
  for(unsigned int i = 0; i < vector_of_xbins.size(); i++){
    x_binning[i] = atof((vector_of_xbins)[i].c_str());
  }

  for(unsigned int i = 0; i < vector_of_ybins.size(); i++){
    y_binning[i] = atof((vector_of_ybins)[i].c_str());
  }

  std::cout<<"Add Data"<<std::endl;
  Use_data = true;
  //  TH2F *temp_RatioVsLCor_Data = new TH2F("temp_RatioVsLCor_Data","-2ln#lambda vs. -lnL_{H=C} Data;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  TH2F *temp_RatioVsLCor_Data = new TH2F(("temp_RatioVsLCor_Data_"+name).c_str(),"-2ln#lambda vs. -lnL_{H=C} Data;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range, y_high_range);
  //  TH1F* temp_Template_Data = new TH1F("temp_Data","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  TH1F* temp_Template_Data = new TH1F(("temp_Data_"+name).c_str(),"-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  TH1F* temp_LikelihoodCutData = new TH1F(("temp_LikelihoodCutData_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorData = new TH1F(("temp_LikelihoodCutUnCorData_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_Data = new TH2F(("temp_LikelihoodCorVsUnCor_Data_"+name).c_str(),"-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];
  //  float w_for_shift = 1.;
  //if(shift_y != 0){w_for_shift = weight_shift1;}

  for(unsigned int i = 0; i < Cor.size(); i++){
    temp_Template_Data->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + data_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])));
    temp_RatioVsLCor_Data->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + data_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),-1*log((Cor)[i]) + data_errors*(-1)*(Cor_unc)[i]/(Cor)[i] + shift_y);
    temp_LikelihoodCutData->Fill(-1*log((Cor)[i]) + data_errors*(-1)*(Cor_unc)[i]/(Cor)[i]);
    temp_LikelihoodCutUnCorData->Fill(-1*log((Uncor)[i]) + data_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i]);
    temp_LikelihoodCorVsUnCor_Data->Fill(-1*log((Cor)[i])+ data_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i])+ data_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i]);
  }

  temp_RatioVsLCor_Data->Sumw2();
  temp_Template_Data->Sumw2();
  temp_LikelihoodCutData->Sumw2();
  temp_LikelihoodCutUnCorData->Sumw2();
  temp_LikelihoodCorVsUnCor_Data->Sumw2();

  temp_LikelihoodCutData->Scale(1./temp_LikelihoodCutData->Integral());
  temp_Template_Data->Scale(1./temp_Template_Data->Integral());
  temp_LikelihoodCutUnCorData->Scale(1./temp_LikelihoodCutUnCorData->Integral());

  RatioVsLCor_Data->Add(temp_RatioVsLCor_Data,weight); //w_for_shift);
  Template_Data->Add(temp_Template_Data,weight);
  LikelihoodCutData->Add(temp_LikelihoodCutData,weight);
  LikelihoodCutUnCorData->Add(temp_LikelihoodCutUnCorData,weight);
  LikelihoodCorVsUnCor_Data->Add(temp_LikelihoodCorVsUnCor_Data,weight);

  L_Data.push_back(Cor);
  L_Data.push_back(Uncor);
  Data_weights.push_back(weight*temp_Template_Data->Integral());

  std::vector<TH1F> vec;
  vec.push_back(*temp_Template_Data);
  vec.push_back(*temp_LikelihoodCutData);
  vec.push_back(*temp_LikelihoodCutUnCorData);

  delete temp_RatioVsLCor_Data;
  temp_RatioVsLCor_Data = NULL;
  delete temp_Template_Data;
  temp_Template_Data = NULL;
  delete temp_LikelihoodCutData;
  temp_LikelihoodCutData = NULL;
  delete temp_LikelihoodCutUnCorData;
  temp_LikelihoodCutUnCorData = NULL;
  delete temp_LikelihoodCorVsUnCor_Data;
  temp_LikelihoodCorVsUnCor_Data = NULL;

  return vec;
}

//add a correlated MC signal sample
//use the event weights to fill the templates and shift the likelihoods if necessary
std::vector<TH1F> eire::PEBuilder::AddCorTemplate(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector< std::vector<float> > event_weights, float cor_errors, float shift_x, float shift_y)
{
  float x_binning[x_nBins];
  float y_binning[y_nBins];
  for(unsigned int i = 0; i < vector_of_xbins.size(); i++){
    x_binning[i] = atof((vector_of_xbins)[i].c_str());
  }

  for(unsigned int i = 0; i < vector_of_ybins.size(); i++){
    y_binning[i] = atof((vector_of_ybins)[i].c_str());
  }

  //  std::cout<<"in addcortemplate"<<std::endl;
  TH2F *temp_RatioVsLCor_SigCor = new TH2F(("temp_RatioVsLCor_SigCor_"+name).c_str(),"-2ln#lambda vs. -lnL_{H=C} TTbar correlated;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range, y_high_range);
  TH1F* temp_Template_SigCor = new TH1F(("temp_Correlated_"+name).c_str(),"-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  //  TH2F *temp_RatioVsLCor_SigCor = new TH2F("temp_RatioVsLCor_SigCor","-2ln#lambda vs. -lnL_{H=C} TTbar correlated;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  //TH1F* temp_Template_SigCor = new TH1F("temp_Correlated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  TH1F* temp_LikelihoodCutCor = new TH1F(("temp_LikelihoodCutCor_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorCor = new TH1F(("temp_LikelihoodCutUnCorCor_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_corsample = new TH2F(("temp_LikelihoodCorVsUnCor_corsample_"+name).c_str(),"-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  //  float w_for_shift = 1.;
  //if(shift_y != 0){w_for_shift = weight_shift1;}

  std::vector<float> Cor = (Likelihoods)[0];
  //  std::cout<<"events = "<<Cor.size()<<std::endl;
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];

  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    // std::cout<<"weights applied"<<std::endl;
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j] >= 1){
	w *= pow((weights)[j],w_setup[j]);
	//std::cout<<"w_setup = "<<w_setup[j]<<" weight = "<<(weights)[j]<<std::endl;
      }//else{std::cout<<"w_setup = "<<w_setup[j]<<" weight = "<<(weights)[j]<<std::endl;}
    }
    temp_Template_SigCor->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + cor_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_RatioVsLCor_SigCor->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + cor_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),-1*log((Cor)[i]) + cor_errors*(-1)*(Cor_unc)[i]/(Cor)[i] + shift_y,w);
    temp_LikelihoodCutCor->Fill(-1*log((Cor)[i]) + cor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorCor->Fill(-1*log((Uncor)[i]) + cor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_corsample->Fill(-1*log((Cor)[i]) + cor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + cor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    //    std::cout<<"event weight = "<<w<<std::endl;
  }

  temp_RatioVsLCor_SigCor->Sumw2();
  temp_Template_SigCor->Sumw2();
  temp_LikelihoodCutCor->Sumw2();
  temp_LikelihoodCutUnCorCor->Sumw2();
  temp_LikelihoodCorVsUnCor_corsample->Sumw2();

  //normalise the temporary templates and add them to the fit template with a weight if necessary
  if(temp_RatioVsLCor_SigCor->Integral() != 0){
    temp_RatioVsLCor_SigCor->Scale(1./temp_RatioVsLCor_SigCor->Integral());
    temp_Template_SigCor->Scale(1./temp_Template_SigCor->Integral());
    temp_LikelihoodCutCor->Scale(1./temp_LikelihoodCutCor->Integral());
    temp_LikelihoodCutUnCorCor->Scale(1./temp_LikelihoodCutUnCorCor->Integral());
    temp_LikelihoodCorVsUnCor_corsample->Scale(1./temp_LikelihoodCorVsUnCor_corsample->Integral());
  }

  //  std::cout<<"weight = "<<weight<<std::endl;
  RatioVsLCor_SigCor->Add(temp_RatioVsLCor_SigCor,weight);//w_for_shift);
  Template_SigCor->Add(temp_Template_SigCor,weight);
  LikelihoodCutCor->Add(temp_LikelihoodCutCor,weight);
  LikelihoodCutUnCorCor->Add(temp_LikelihoodCutUnCorCor,weight);
  LikelihoodCorVsUnCor_corsample->Add(temp_LikelihoodCorVsUnCor_corsample,weight);

  L_Cor.push_back(Cor);
  L_Cor.push_back(Uncor);
  Cor_weights.push_back(weight*temp_Template_SigCor->Integral());

  std::vector<TH1F> vec;
  vec.push_back(*temp_Template_SigCor);
  vec.push_back(*temp_LikelihoodCutCor);
  vec.push_back(*temp_LikelihoodCutUnCorCor);

  delete temp_RatioVsLCor_SigCor;
  temp_RatioVsLCor_SigCor = NULL;
  delete temp_Template_SigCor;
  temp_Template_SigCor = NULL;
  delete temp_LikelihoodCutCor;
  temp_LikelihoodCutCor = NULL;
  delete temp_LikelihoodCutUnCorCor;
  temp_LikelihoodCutUnCorCor = NULL;
  delete temp_LikelihoodCorVsUnCor_corsample;
  temp_LikelihoodCorVsUnCor_corsample = NULL;

  //return the -2lnLambda template, so it can be collected and given to the Hypothesis Testing Sequence
  return vec;
}

//add an uncorrelated signal sample
//use the event weights to fill the templates and shift the likelihoods if necessary
std::vector<TH1F> eire::PEBuilder::AddUncorTemplate(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float uncor_errors, float shift_x, float shift_y)
{
  float x_binning[x_nBins];
  float y_binning[y_nBins];
  for(unsigned int i = 0; i < vector_of_xbins.size(); i++){
    x_binning[i] = atof((vector_of_xbins)[i].c_str());
  }

  for(unsigned int i = 0; i < vector_of_ybins.size(); i++){
    y_binning[i] = atof((vector_of_ybins)[i].c_str());
  }

  //  TH2F *temp_RatioVsLCor_SigUnCor = new TH2F("temp_RatioVsLCor_SigUnCor","-2ln#lambda vs. -lnL_{H=C} TTbar uncorrelated;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  //TH1F* temp_Template_SigUnCor = new TH1F("temp_UnCorrelated","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  TH2F *temp_RatioVsLCor_SigUnCor = new TH2F(("temp_RatioVsLCor_SigUnCor_"+name).c_str(),"-2ln#lambda vs. -lnL_{H=C} TTbar uncorrelated;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range,y_high_range);
  TH1F* temp_Template_SigUnCor = new TH1F(("temp_UnCorrelated_"+name).c_str(),"-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  TH1F* temp_LikelihoodCutUnCor = new TH1F(("temp_LikelihoodCutUnCor_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorUnCor = new TH1F(("temp_LikelihoodCutUnCorUnCor_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_uncorsample = new TH2F(("temp_LikelihoodCorVsUnCor_uncorsample_"+name).c_str(),"-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  //  float w_for_shift = 1.;
  //if(shift_y != 0){w_for_shift = weight_shift1;}

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];
  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j] >= 1){
        w *= pow((weights)[j],w_setup[j]);
      }
    }
    temp_Template_SigUnCor->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + uncor_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_RatioVsLCor_SigUnCor->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + uncor_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),-1*log((Cor)[i]) + uncor_errors*(-1)*(Cor_unc)[i]/(Cor)[i] + shift_y,w);
    temp_LikelihoodCutUnCor->Fill(-1*log((Cor)[i]) + uncor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorUnCor->Fill(-1*log((Uncor)[i]) + uncor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_uncorsample->Fill(-1*log((Cor)[i]) + uncor_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + uncor_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
  }

  temp_RatioVsLCor_SigUnCor->Sumw2();
  temp_Template_SigUnCor->Sumw2();
  temp_LikelihoodCutUnCor->Sumw2();
  temp_LikelihoodCutUnCorUnCor->Sumw2();
  temp_LikelihoodCorVsUnCor_uncorsample->Sumw2();

  if(temp_RatioVsLCor_SigUnCor->Integral() != 0){
    temp_RatioVsLCor_SigUnCor->Scale(1./temp_RatioVsLCor_SigUnCor->Integral());
    temp_Template_SigUnCor->Scale(1./temp_Template_SigUnCor->Integral());
    temp_LikelihoodCutUnCor->Scale(1./temp_LikelihoodCutUnCor->Integral());
    temp_LikelihoodCutUnCorUnCor->Scale(1./temp_LikelihoodCutUnCorUnCor->Integral());
    temp_LikelihoodCorVsUnCor_uncorsample->Scale(1./temp_LikelihoodCorVsUnCor_uncorsample->Integral());
  }

  RatioVsLCor_SigUnCor->Add(temp_RatioVsLCor_SigUnCor,weight);//w_for_shift);
  Template_SigUnCor->Add(temp_Template_SigUnCor,weight);
  LikelihoodCutUnCor->Add(temp_LikelihoodCutUnCor,weight);
  LikelihoodCutUnCorUnCor->Add(temp_LikelihoodCutUnCorUnCor,weight);
  LikelihoodCorVsUnCor_corsample->Add(temp_LikelihoodCorVsUnCor_uncorsample,weight);

  L_Uncor.push_back(Cor);
  L_Uncor.push_back(Uncor);
  Uncor_weights.push_back(weight*temp_Template_SigUnCor->Integral());

  std::vector<TH1F> vec;
  vec.push_back(*temp_Template_SigUnCor);
  vec.push_back(*temp_LikelihoodCutUnCor);
  vec.push_back(*temp_LikelihoodCutUnCorUnCor);

  delete temp_RatioVsLCor_SigUnCor;
  temp_RatioVsLCor_SigUnCor = NULL;
  delete temp_Template_SigUnCor;
  temp_Template_SigUnCor = NULL;
  delete temp_LikelihoodCutUnCor;
  temp_LikelihoodCutUnCor = NULL;
  delete temp_LikelihoodCutUnCorUnCor;
  temp_LikelihoodCutUnCorUnCor = NULL;
  delete temp_LikelihoodCorVsUnCor_uncorsample;
  temp_LikelihoodCorVsUnCor_uncorsample = NULL;

  //return the -2lnLambda template, so it can be collected and given to the Hypothesis Testing Sequence
  return vec;
}

//add a background to the background correlated template
//use the event weights to fill the templates and shift the likelihoods if necessary
//give the background the correct relative weight
std::vector<TH1F> eire::PEBuilder::AddBackgroundTemplateCor(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float background_errors, float shift_x, float shift_y)
{
//  std::cout<<"In PEBuilder"<<std::endl;

  float x_binning[x_nBins];
  float y_binning[y_nBins];
  for(unsigned int i = 0; i < vector_of_xbins.size(); i++){
    x_binning[i] = atof((vector_of_xbins)[i].c_str());
  }

  for(unsigned int i = 0; i < vector_of_ybins.size(); i++){
    y_binning[i] = atof((vector_of_ybins)[i].c_str());
  }

  //  TH2F *temp_RatioVsLCor_Background = new TH2F("temp_RatioVsLCor_Background","-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  //TH1F* temp_Template_Background = new TH1F("temp_Background","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  TH2F *temp_RatioVsLCor_Background = new TH2F(("temp_RatioVsLCor_Background_"+name).c_str(),"-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range,y_high_range);
  TH1F* temp_Template_Background = new TH1F(("temp_Background_"+name).c_str(),"-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  TH1F* temp_LikelihoodCutBackground = new TH1F(("temp_LikelihoodCutBackground_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorBackground = new TH1F(("temp_LikelihoodCutUnCorBackground_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_Background = new TH2F(("temp_LikelihoodCorVsUnCor_Background_"+name).c_str(),"-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  //  float w_for_shift = 1.;
  //if(shift_y != 0){w_for_shift = weight_shift1;}

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];

  double x_min[25] = {40,42.4, 44.8, 47.2, 49.6, 52, 54.4, 56.8, 59.2, 61.6, 64, 66.4, 68.8, 71.2, 73.6, 76, 78.4, 80.8, 83.2, 85.6, 88, 90.4, 92.8, 95.2, 97.6};
  double SF[25] = {1, 1, 1, 1, 1, 0.879954, 1.02069, 1.27325, 1.19104, 1.13992, 1.03755, 0.781648, 0.728188, 0.718793, 0.895555, 0.704009, 0.871801, 0.832712, 0.159288, 0.296922, 1, 1.17671, 1, 35.2459, 1};
  //SF Up
  //double SF[25] = {1, 1, 0.632449, 1.50073, 1, 0.998008, 1.10338, 1.34037, 1.25221, 1.20109, 1.10104, 0.844822, 0.796837, 0.810414, 1.02197, 0.8373, 1.07889, 1.14484, 0.475567, 0.840271, 1, 2.63693, 1, 97.8951, 1};
  //SF Down
  //double SF[25] = {1, 1, 1, 1, 1, 0.7619, 0.937997, 1.20613, 1.12987, 1.07874, 0.974047, 0.718473, 0.659539, 0.627171, 0.769136, 0.570718, 0.664707, 0.520589, -0.156992, 1, 1, 1, 1, 1, 1};


  //  std::cout<<"RAW events incoming = "<<Cor.size()<<std::endl;
  float totalweighted = 0.;
  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j] >= 1){
        w *= pow((weights)[j],w_setup[j]);
      }
    }
    double extra_scale = 1.;
    int t = 0;
    while(-1.*log((Cor)[i]) > x_min[t]){
      extra_scale = SF[t];
      t++;
    }
    //    w *= extra_scale;
    //    std::cout<<"weight for event = "<<w<<std::endl;
    totalweighted += w;
    temp_Template_Background->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + background_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_RatioVsLCor_Background->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + background_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i] + shift_y,w);
    temp_LikelihoodCutBackground->Fill(-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorBackground->Fill(-1*log((Uncor)[i]) + background_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_Background->Fill(-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + background_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
  }

  temp_RatioVsLCor_Background->Sumw2();
  temp_Template_Background->Sumw2();
  temp_LikelihoodCutBackground->Sumw2();
  temp_LikelihoodCutUnCorBackground->Sumw2();
  temp_LikelihoodCorVsUnCor_Background->Sumw2();

  //std::cout<<"total weighted = "<<totalweighted<<std::endl;
  //std::cout<<"weighted integral = "<<temp_RatioVsLCor_Background->Integral()<<std::endl;

  //normalise the templates to unity
  if(temp_RatioVsLCor_Background->Integral() != 0){
    temp_RatioVsLCor_Background->Scale(1./temp_RatioVsLCor_Background->Integral());
    temp_Template_Background->Scale(1./temp_Template_Background->Integral());
    temp_LikelihoodCutBackground->Scale(1./temp_LikelihoodCutBackground->Integral());
    temp_LikelihoodCutUnCorBackground->Scale(1./temp_LikelihoodCutUnCorBackground->Integral());
    temp_LikelihoodCorVsUnCor_Background->Scale(1./temp_LikelihoodCorVsUnCor_Background->Integral());
  }

  //add the template using the correct relative weight
  std::cout<<"weight for template cor = "<<weight<<std::endl;
  RatioVsLCor_Background_Cor->Add(temp_RatioVsLCor_Background,weight);//*w_for_shift);
  std::cout<<"integral of template cor = "<<RatioVsLCor_Background_Cor->Integral()<<std::endl;
  Template_Background_Cor->Add(temp_Template_Background,weight);
  LikelihoodCutBackground_Cor->Add(temp_LikelihoodCutBackground,weight);
  LikelihoodCutUnCorBackground_Cor->Add(temp_LikelihoodCutUnCorBackground,weight);
  LikelihoodCorVsUnCor_Background_Cor->Add(temp_LikelihoodCorVsUnCor_Background,weight);

  //  std::cout<<"template entries = "<<RatioVsLCor_Background_Cor->GetEntries()<<std::endl;

  L_Background_Cor.push_back(Cor);
  L_Background_Cor.push_back(Uncor);
  Background_weights.push_back(weight*temp_Template_Background->Integral());

  std::vector<TH1F> vec;
  vec.push_back(*temp_Template_Background);
  vec.push_back(*temp_LikelihoodCutBackground);
  vec.push_back(*temp_LikelihoodCutUnCorBackground);

  delete temp_RatioVsLCor_Background;
  temp_RatioVsLCor_Background = NULL;
  delete temp_Template_Background;
  temp_Template_Background = NULL;
  delete temp_LikelihoodCutBackground;
  temp_LikelihoodCutBackground = NULL;
  delete temp_LikelihoodCutUnCorBackground;
  temp_LikelihoodCutUnCorBackground = NULL;
  delete temp_LikelihoodCorVsUnCor_Background;
  temp_LikelihoodCorVsUnCor_Background = NULL;

  //return the -2lnLambda template, so it can be collected and given to the Hypothesis Testing Sequence
  return vec;
}

//add a background to the background correlated template
//use the event weights to fill the templates and shift the likelihoods if necessary
//give the background the correct relative weight
std::vector<TH1F> eire::PEBuilder::AddBackgroundTemplateUncor(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float background_errors, float shift_x, float shift_y)
{
  float x_binning[x_nBins];
  float y_binning[y_nBins];
  for(unsigned int i = 0; i < vector_of_xbins.size(); i++){
    x_binning[i] = atof((vector_of_xbins)[i].c_str());
  }

  for(unsigned int i = 0; i < vector_of_ybins.size(); i++){
    y_binning[i] = atof((vector_of_ybins)[i].c_str());
  }

  //  TH2F *temp_RatioVsLCor_Background = new TH2F("temp_RatioVsLCor_Background","-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_binning,y_nBins, y_binning);
  //TH1F* temp_Template_Background = new TH1F("temp_Background","-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_binning);
  TH2F *temp_RatioVsLCor_Background = new TH2F(("temp_RatioVsLCor_Background_"+name).c_str(),"-2ln#lambda vs. -lnL_{H=C} Background;-2ln#lambda;-lnL_{H=C}",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range,y_high_range);
  TH1F* temp_Template_Background = new TH1F(("temp_Background_"+name).c_str(),"-2ln#lambda = 2lnL_{H=C} - 2lnL_{H=U};;normalised entries", x_nBins, x_low_range,x_high_range);
  TH1F* temp_LikelihoodCutBackground = new TH1F(("temp_LikelihoodCutBackground_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH1F* temp_LikelihoodCutUnCorBackground = new TH1F(("temp_LikelihoodCutUnCorBackground_"+name).c_str(),"-lnL_{H=C};-lnL_{H=C};entries", 50, 40, 100);
  TH2F* temp_LikelihoodCorVsUnCor_Background = new TH2F(("temp_LikelihoodCorVsUnCor_Background_"+name).c_str(),"-lnL_{H=C} vs -lnL_{H=U};-lnL_{H=C};-lnL_{H=U}", 50, 40, 90,50,40,90);

  //  float w_for_shift = 1.;
  //if(shift_y != 0){w_for_shift = weight_shift1;}

  std::vector<float> Cor = (Likelihoods)[0];
  std::vector<float> Uncor = (Likelihoods)[1];
  std::vector<float> Cor_unc = (Likelihoods)[2];
  std::vector<float> Uncor_unc = (Likelihoods)[3];

  double x_min[25] = {40,42.4, 44.8, 47.2, 49.6, 52, 54.4, 56.8, 59.2, 61.6, 64, 66.4, 68.8, 71.2, 73.6, 76, 78.4, 80.8, 83.2, 85.6, 88, 90.4, 92.8, 95.2, 97.6};
  double SF[25] = {1, 1, 1, 1, 1, 0.879954, 1.02069, 1.27325, 1.19104, 1.13992, 1.03755, 0.781648, 0.728188, 0.718793, 0.895555, 0.704009, 0.871801, 0.832712, 0.159288, 0.296922, 1, 1.17671, 1, 35.2459, 1};
  //SF Up
  //double SF[25] = {1, 1, 0.632449, 1.50073, 1, 0.998008, 1.10338, 1.34037, 1.25221, 1.20109, 1.10104, 0.844822, 0.796837, 0.810414, 1.02197, 0.8373, 1.07889, 1.14484, 0.475567, 0.840271, 1, 2.63693, 1, 97.8951, 1};
  //SF Down
  //double SF[25] = {1, 1, 1, 1, 1, 0.7619, 0.937997, 1.20613, 1.12987, 1.07874, 0.974047, 0.718473, 0.659539, 0.627171, 0.769136, 0.570718, 0.664707, 0.520589, -0.156992, 1, 1, 1, 1, 1, 1};


  //  std::cout<<"RAW events incoming = "<<Cor.size()<<std::endl;
  float totalweighted = 0.;
  for(unsigned int i = 0; i < Cor.size(); i++){
    double w = 1;
    std::vector<float> weights =(event_weights)[i];
    for(unsigned int j = 0; j < weights.size(); j++){
      if(w_setup[j] >= 1){
        w *= pow((weights)[j],w_setup[j]);
      }
    }

    double extra_scale = 0.;
    int t = 0;
    while(-1.*log((Cor)[i]) > x_min[t]){
      extra_scale = SF[t];
      t++;
    }
    //    w *= extra_scale;

    //    std::cout<<"weight for event = "<<w<<std::endl;
    totalweighted += w;
    temp_Template_Background->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + background_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),w);
    temp_RatioVsLCor_Background->Fill(2*log((Cor)[i]) - 2*log((Uncor)[i]) + background_errors*2*sqrt(((Cor_unc)[i]/(Cor)[i])*((Cor_unc)[i]/(Cor)[i])+((Uncor_unc)[i]/(Uncor)[i])*((Uncor_unc)[i]/(Uncor)[i])),-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i] + shift_y,w);
    temp_LikelihoodCutBackground->Fill(-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i],w);
    temp_LikelihoodCutUnCorBackground->Fill(-1*log((Uncor)[i]) + background_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
    temp_LikelihoodCorVsUnCor_Background->Fill(-1*log((Cor)[i]) + background_errors*(-1)*(Cor_unc)[i]/(Cor)[i],-1*log((Uncor)[i]) + background_errors*(-1)*(Uncor_unc)[i]/(Uncor)[i],w);
  }

  temp_RatioVsLCor_Background->Sumw2();
  temp_Template_Background->Sumw2();
  temp_LikelihoodCutBackground->Sumw2();
  temp_LikelihoodCutUnCorBackground->Sumw2();
  temp_LikelihoodCorVsUnCor_Background->Sumw2();

  //  std::cout<<"total weighted = "<<totalweighted<<std::endl;
  //std::cout<<"weighted integral = "<<temp_RatioVsLCor_Background->Integral()<<std::endl;

  if(temp_RatioVsLCor_Background->Integral() != 0){
    temp_RatioVsLCor_Background->Scale(1./temp_RatioVsLCor_Background->Integral());
    temp_Template_Background->Scale(1./temp_Template_Background->Integral());
    temp_LikelihoodCutBackground->Scale(1./temp_LikelihoodCutBackground->Integral());
    temp_LikelihoodCutUnCorBackground->Scale(1./temp_LikelihoodCutUnCorBackground->Integral());
    temp_LikelihoodCorVsUnCor_Background->Scale(1./temp_LikelihoodCorVsUnCor_Background->Integral());
  }

  //add the template using the correct relative weight
  std::cout<<"weight for template uncor = "<<weight<<std::endl;
  RatioVsLCor_Background_Uncor->Add(temp_RatioVsLCor_Background,weight);//*w_for_shift);
  std::cout<<"integral of template uncor = "<<RatioVsLCor_Background_Uncor->Integral()<<std::endl;
  Template_Background_Uncor->Add(temp_Template_Background,weight);
  LikelihoodCutBackground_Uncor->Add(temp_LikelihoodCutBackground,weight);
  LikelihoodCutUnCorBackground_Uncor->Add(temp_LikelihoodCutUnCorBackground,weight);
  LikelihoodCorVsUnCor_Background_Uncor->Add(temp_LikelihoodCorVsUnCor_Background,weight);

  //  std::cout<<"template entries = "<<RatioVsLCor_Background_Uncor->GetEntries()<<std::endl;

  L_Background_Uncor.push_back(Cor);
  L_Background_Uncor.push_back(Uncor);
  Background_weights.push_back(weight*temp_Template_Background->Integral());

  std::vector<TH1F> vec;
  vec.push_back(*temp_Template_Background);
  vec.push_back(*temp_LikelihoodCutBackground);
  vec.push_back(*temp_LikelihoodCutUnCorBackground);

  delete temp_RatioVsLCor_Background;
  temp_RatioVsLCor_Background = NULL;
  delete temp_Template_Background;
  temp_Template_Background = NULL;
  delete temp_LikelihoodCutBackground;
  temp_LikelihoodCutBackground = NULL;
  delete temp_LikelihoodCutUnCorBackground;
  temp_LikelihoodCutUnCorBackground = NULL;
  delete temp_LikelihoodCorVsUnCor_Background;
  temp_LikelihoodCorVsUnCor_Background = NULL;
  //return the -2lnLambda template, so it can be collected and given to the Hypothesis Testing Sequence
  return vec;
}

//all the data has been collected, normalise the templates to unity and make plots
void eire::PEBuilder::DrawTemplates()
{
  std::cout<<"Draw templates"<<std::endl;
  RatioVsLCor_SigCor->Scale(1./RatioVsLCor_SigCor->Integral());
  RatioVsLCor_SigUnCor->Scale(1./RatioVsLCor_SigCor->Integral());
  Template_SigCor->Scale(1./Template_SigCor->Integral());
  Template_SigUnCor->Scale(1./Template_SigUnCor->Integral());
  LikelihoodCutCor->Scale(1./LikelihoodCutCor->Integral());
  LikelihoodCutUnCor->Scale(1./LikelihoodCutUnCor->Integral());
  LikelihoodCutUnCorCor->Scale(1./LikelihoodCutUnCorCor->Integral());
  LikelihoodCutUnCorUnCor->Scale(1./LikelihoodCutUnCorUnCor->Integral());
  TH1F* Draw_Template_Data = (TH1F*) Template_Data->Clone();
  TH2F* Draw_RatioVsLCor_Data = (TH2F*) RatioVsLCor_Data->Clone();

  //  int total_back = 0;
  //for(unsigned int i = 0; i < L_Background.size(); i++){
  //total_back += (L_Background)[i].size();
  //std::cout<<"background = "<<(L_Background)[i].size()<<std::endl;
  //}
  //std::cout<<"%%%%%%%%%%%%%%%%number of events in Background!! = "<<total_back<<std::endl;

  if(Use_back){
    RatioVsLCor_Background_Cor->Scale(1./RatioVsLCor_Background_Cor->Integral());
    RatioVsLCor_Background_Uncor->Scale(1./RatioVsLCor_Background_Uncor->Integral());
    Template_Background_Cor->Scale(1./Template_Background_Cor->Integral());
    LikelihoodCutBackground_Cor->Scale(1./LikelihoodCutBackground_Cor->Integral());
    LikelihoodCutUnCorBackground_Cor->Scale(1./LikelihoodCutUnCorBackground_Cor->Integral());
    Template_Background_Uncor->Scale(1./Template_Background_Uncor->Integral());
    LikelihoodCutBackground_Uncor->Scale(1./LikelihoodCutBackground_Uncor->Integral());
    LikelihoodCutUnCorBackground_Uncor->Scale(1./LikelihoodCutUnCorBackground_Uncor->Integral());
  }

  if(Use_data){
    Draw_RatioVsLCor_Data->Scale(1./Draw_RatioVsLCor_Data->Integral());
    Draw_Template_Data->Scale(1./Draw_Template_Data->Integral());
    LikelihoodCutData->Scale(1./LikelihoodCutData->Integral());
    LikelihoodCutUnCorData->Scale(1./LikelihoodCutUnCorData->Integral());
    std::cout<<"Data template integral = "<<LikelihoodCutData->Integral()<<std::endl;
    float backfrac = 0.2200;
    TH1F* ComparisonTemplate = (TH1F*) LikelihoodCutCor->Clone();
    ComparisonTemplate->Scale(1.-backfrac);
    std::cout<<"MC template integral = "<<ComparisonTemplate->Integral()<<std::endl;
    ComparisonTemplate->Add(LikelihoodCutBackground_Cor,0.2200);
    std::cout<<"MC template integral = "<<ComparisonTemplate->Integral()<<std::endl;
    TH1F* SF_DataMC = (TH1F*) LikelihoodCutData->Clone();
    SF_DataMC->Divide(ComparisonTemplate);
    TCanvas *cComp = new TCanvas();
    cComp->cd();
    TPad *cComp_SF = new TPad("cComp_SF","newpad",0.01,0.01,0.99,0.32);
    cComp_SF->Draw();
    cComp_SF->cd();
    cComp_SF->SetTopMargin(0.1496599);
    cComp_SF->SetBottomMargin(0.1836735);
    cComp_SF->SetFillColor(kWhite);
    SF_DataMC->Draw("p");
    cComp_SF->Modified();
    cComp_SF->Update();
    cComp->cd();
    TPad *cComp_Hist = new TPad("cComp_Hist","newpad",0.01,0.33,0.99,0.99);
    cComp_Hist->SetFillColor(kWhite);
    cComp_Hist->Draw();
    cComp_Hist->cd();
    LikelihoodCutData->Draw("p");
    ComparisonTemplate->Draw("histsame");
    double ks = ComparisonTemplate->KolmogorovTest(LikelihoodCutData);
    TPaveText *KS = new TPaveText(0.7389749,0.9092193,0.938365,0.9605804,"brNDC");
    KS->SetTextColor(kBlack);
    KS->SetFillColor(kWhite);
    KS->SetBorderSize(0);
    KS->SetTextAlign(12);
    KS->SetTextSize(0.03);
    char temp[100];
    sprintf(temp,"KS = %.5f", ks);
    KS->AddText(temp);
    KS->Draw();
    cComp_Hist->Modified();
    cComp_Hist->Update();
    cComp->SaveAs("DataMCComparison.C");
  }
  

  std::cout<<"Background cor integral = "<<RatioVsLCor_Background_Cor->Integral()<<std::endl;
  std::cout<<"Background uncor integral = "<<RatioVsLCor_Background_Uncor->Integral()<<std::endl;

  Template_SigCor->SetLineColor(kRed);
  Template_SigUnCor->SetLineColor(kBlack);
  Draw_Template_Data->SetLineColor(kBlack);
  Template_Background_Cor->SetLineColor(kBlue);
  Template_Background_Uncor->SetLineColor(kBlue);
  LikelihoodCutCor->SetLineColor(kRed);
  LikelihoodCutUnCor->SetLineColor(kBlack);
  LikelihoodCutData->SetLineColor(kBlack);
  LikelihoodCutBackground_Cor->SetLineColor(kBlue);
  LikelihoodCutBackground_Uncor->SetLineColor(kBlue);
  LikelihoodCutUnCorCor->SetLineColor(kRed);
  LikelihoodCutUnCorUnCor->SetLineColor(kBlack);
  LikelihoodCutUnCorData->SetLineColor(kBlack);
  LikelihoodCutUnCorBackground_Cor->SetLineColor(kBlue);
  LikelihoodCutUnCorBackground_Uncor->SetLineColor(kBlue);

  Template_SigUnCor->SetLineWidth(2);
  Template_SigCor->SetLineWidth(2);
  Draw_Template_Data->SetLineWidth(2);
  Template_Background_Cor->SetLineWidth(2);
  Template_Background_Uncor->SetLineWidth(2);
  LikelihoodCutCor->SetLineWidth(2);
  LikelihoodCutUnCor->SetLineWidth(2);
  LikelihoodCutData->SetLineWidth(2);
  LikelihoodCutBackground_Cor->SetLineWidth(2);
  LikelihoodCutBackground_Uncor->SetLineWidth(2);
  LikelihoodCutUnCorCor->SetLineWidth(2);
  LikelihoodCutUnCorUnCor->SetLineWidth(2);
  LikelihoodCutUnCorData->SetLineWidth(2);
  LikelihoodCutUnCorBackground_Cor->SetLineWidth(2);
  LikelihoodCutUnCorBackground_Uncor->SetLineWidth(2);
 
  Template_SigUnCor->SetLineStyle(2);
  LikelihoodCutUnCor->SetLineStyle(2);
  LikelihoodCutUnCorUnCor->SetLineStyle(2);
  Template_Background_Cor->SetLineStyle(3);
  Template_Background_Uncor->SetLineStyle(3);
  LikelihoodCutBackground_Cor->SetLineStyle(3);
  LikelihoodCutUnCorBackground_Cor->SetLineStyle(3);
  LikelihoodCutBackground_Uncor->SetLineStyle(3);
  LikelihoodCutUnCorBackground_Uncor->SetLineStyle(3);

  //  TH1D *BinCor;
  //TH1D *BinUnCor;
  /*
  for(int i = 0; i < RatioVsLCor_SigCor->GetNbinsY(); i++){
    BinCor = RatioVsLCor_SigCor->ProjectionX("BinCor",i+1,i+2,"e");
    BinUnCor = RatioVsLCor_SigUnCor->ProjectionX("BinUnCor",i+1,i+2,"e");
    TH1D *SF_bin = new TH1D("SF_bin","",x_nBins, x_low_range,x_high_range);
    SF_bin = (TH1D*) BinCor->Clone();
    SF_bin->Divide(BinUnCor);
    TCanvas *cBinned = new TCanvas();
    cBinned->Divide(3,1);
    cBinned->cd(1);
    BinCor->Draw();
    cBinned->cd(2);
    BinUnCor->Draw();
    cBinned->cd(3);
    SF_bin->Draw();
    char fname[1024];
    char cname[1024];
    sprintf(fname,"SFx_bin%i.pdf",i);
    sprintf(cname,"SFx_bin%i.C",i);
    cBinned->SaveAs(fname);
    cBinned->SaveAs(cname);
    //    delete &cBinned;
  }
  */
  /*
  for(int i = 0; i < RatioVsLCor_SigCor->GetNbinsX(); i++){
    BinCor = RatioVsLCor_SigCor->ProjectionY("BinCor",i+1,i+2,"e");
    BinUnCor = RatioVsLCor_Background_Cor->ProjectionY("BinBackground",i+1,i+2,"e");
    TH1D *SFy_bin = new TH1D("SFy_bin","",y_nBins, y_low_range,y_high_range);
    SFy_bin = (TH1D*) BinCor->Clone();
    SFy_bin->Divide(BinUnCor);
    TCanvas *cBinned = new TCanvas();
    cBinned->Divide(3,1);
    cBinned->cd(1);
    BinCor->Draw();
    cBinned->cd(2);
    BinUnCor->Draw();
    cBinned->cd(3);
    SFy_bin->Draw();
    char fname[1024];
    char cname[1024];
    sprintf(fname,"SFy_bin%i.pdf",i);
    sprintf(cname,"SFy_bin%i.C",i);
    cBinned->SaveAs(fname);
    cBinned->SaveAs(cname);
    //    delete &cBinned;
  }
  */

  TH2F *SF_2D = new TH2F("SF_2D","",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range, y_high_range);
  SF_2D = (TH2F*) RatioVsLCor_SigCor->Clone();
  SF_2D->Divide(RatioVsLCor_SigUnCor);

  TH2F *SFerr_2D = new TH2F("SF_2D","",x_nBins, x_low_range,x_high_range,y_nBins, y_low_range, y_high_range);
  for(int i = 0; i < SF_2D->GetNbinsX(); i++){
    for(int j = 0; j < SF_2D->GetNbinsY(); j++){
      SFerr_2D->SetBinContent(i,j,SF_2D->GetBinError(i,j)); 
    }
  }

  TH1F* SF = new TH1F("SF","", x_nBins, x_low_range, x_high_range);
  SF = (TH1F*) Template_SigCor->Clone();
  SF->Divide(Template_SigUnCor);
  SF->SetTitle("");
  TCanvas *c1 = new TCanvas("c1", "c1",0,0,600,500);
  gStyle->SetOptStat(2222);
  c1->Range(0,0,1,1);
  TPad *c1_1 = new TPad("c1_1", "newpad",0.01,0.01,0.99,0.32);
  c1_1->Draw();
  c1_1->cd();
  c1_1->SetTopMargin(0.01);
  c1_1->SetBottomMargin(0.3);
  c1_1->SetRightMargin(0.1);
  c1_1->SetFillStyle(0);
  SF->SetMinimum(0.800);
  SF->SetMaximum(1.200);
  SF->GetYaxis()->SetNdivisions(5);
  SF->GetXaxis()->SetTitle("-2ln#lambda");
  SF->GetYaxis()->SetTitle("cor/uncor");
  SF->GetXaxis()->SetTitleSize(0.08);
  SF->GetXaxis()->SetLabelSize(0.08);
  SF->GetYaxis()->SetLabelSize(0.08);
  SF->GetYaxis()->SetTitleSize(0.08);
  SF->GetYaxis()->SetTitleOffset(0.34);
  SF->Draw();
  TLine* l = new TLine(x_low_range,1,x_high_range,1);
  l->Draw("same");
  c1->cd();
  TPad *c1_2 = new TPad("c1_2", "newpad",0.01,0.33,0.99,0.99);
  c1_2->Draw();
  c1_2->cd();
  c1_2->SetTopMargin(0.1);
  c1_2->SetBottomMargin(0.01);
  c1_2->SetRightMargin(0.1);
  c1_2->SetFillStyle(0);
  TLegend *leg = new TLegend(0.6,0.7,0.89,0.89);
  leg->AddEntry(Template_SigCor,"correlated");
  leg->AddEntry(Template_SigUnCor,"uncorrelated");
  leg->SetFillColor(kWhite);
  Template_SigCor->GetYaxis()->SetLabelSize(0.05);
  Template_SigCor->GetYaxis()->SetTitleSize(0.05);
  Template_SigCor->GetYaxis()->SetTitleOffset(1.);
  Template_SigCor->Draw("EHIST");
  Template_SigUnCor->GetYaxis()->SetLabelSize(0.05);
  Template_SigUnCor->GetYaxis()->SetTitleSize(0.05);
  Template_SigUnCor->GetYaxis()->SetTitleOffset(1.);
  Template_SigUnCor->Draw("EHISTsames");
  c1->Modified();
  c1->Update();
  std::cout<<"use_back = "<<Use_back<<std::endl;
  std::cout<<"use_data = "<<Use_data<<std::endl;
  if(Use_back){
    leg->AddEntry(Template_Background_Cor,"Background_Cor");
    Template_Background_Cor->Draw("EHISTsames");
    leg->AddEntry(Template_Background_Uncor,"Background_Uncor");
    Template_Background_Uncor->Draw("EHISTsames");
  }
  if(Use_data){
    leg->AddEntry(Draw_Template_Data,"Data");
    Draw_Template_Data->Draw("pEHISTsames");
  }
  leg->Draw();
  c1->Modified();
  c1->Update();
  c1->SaveAs(("InputTemplates"+Name+".C").c_str());
  c1->SaveAs(("InputTemplates"+Name+".png").c_str());
  c1->SaveAs(("InputTemplates"+Name+".pdf").c_str());
  delete c1;

  TCanvas *c2 = new TCanvas();
  gStyle->SetOptStat(2222);
  c2->cd();
  TLegend *leg2 = new TLegend(0.6,0.7,0.89,0.89);
  leg2->AddEntry(LikelihoodCutCor,"correlated");
  leg2->AddEntry(LikelihoodCutUnCor,"uncorrelated");
  leg2->SetFillColor(kWhite);
  LikelihoodCutCor->Draw("EHIST");
  LikelihoodCutUnCor->Draw("EHISTsames");
  if(Use_back){
    LikelihoodCutBackground_Cor->Draw("EHISTsames");
    leg2->AddEntry(LikelihoodCutBackground_Cor,"Background_Cor");
    LikelihoodCutBackground_Uncor->Draw("EHISTsames");
    leg2->AddEntry(LikelihoodCutBackground_Uncor,"Background_Uncor");
  }
  if(Use_data){
    LikelihoodCutData->Draw("psames");
    leg2->AddEntry(LikelihoodCutData,"Data");
  }
  leg2->Draw();
  c2->Modified();
  c2->Update();
  c2->SaveAs(("LikelihoodCut"+Name+".C").c_str());
  c2->SaveAs(("LikelihoodCut"+Name+".png").c_str());
  c2->SaveAs(("LikelihoodCut"+Name+".pdf").c_str());
  delete c2;

  TCanvas *c25 = new TCanvas();
  gStyle->SetOptStat(2222);
  c25->cd();
  TLegend *leg25 = new TLegend(0.6,0.7,0.89,0.89);
  leg25->AddEntry(LikelihoodCorVsUnCor_corsample,"correlated");
  leg25->SetFillColor(kWhite);
  LikelihoodCorVsUnCor_corsample->Draw("colz");
  leg25->Draw();
  c25->Modified();
  c25->Update();
  c25->SaveAs(("LikelihoodCorVsUnCor_corsample"+Name+".C").c_str());
  c25->SaveAs(("LikelihoodCorVsUnCor_corsample"+Name+".png").c_str());
  c25->SaveAs(("LikelihoodCorVsUnCor_corsample"+Name+".pdf").c_str());
  delete c25;

  TCanvas *c26 = new TCanvas();
  gStyle->SetOptStat(2222);
  c26->cd();
  TLegend *leg26 = new TLegend(0.6,0.7,0.89,0.89);
  leg26->AddEntry(LikelihoodCorVsUnCor_uncorsample,"uncorrelated");
  leg26->SetFillColor(kWhite);
  LikelihoodCorVsUnCor_uncorsample->Draw("colz");
  leg26->Draw();
  c26->Modified();
  c26->Update();
  c26->SaveAs(("LikelihoodCorVsUnCor_uncorsample"+Name+".C").c_str());
  c26->SaveAs(("LikelihoodCorVsUnCor_uncorsample"+Name+".png").c_str());
  c26->SaveAs(("LikelihoodCorVsUnCor_uncorsample"+Name+".pdf").c_str());
  delete c26;

  if(Use_back){
    TCanvas *c27 = new TCanvas();
    gStyle->SetOptStat(2222);
    c27->cd();
    TLegend *leg27 = new TLegend(0.6,0.7,0.89,0.89);
    leg27->AddEntry(LikelihoodCorVsUnCor_Background_Cor,"Background Cor");
    leg27->SetFillColor(kWhite);
    LikelihoodCorVsUnCor_Background_Cor->Draw("colz");
    leg27->Draw();
    c27->Modified();
    c27->Update();
    c27->SaveAs(("LikelihoodCorVsUnCor_background"+Name+".C").c_str());
    c27->SaveAs(("LikelihoodCorVsUnCor_background"+Name+".png").c_str());
    c27->SaveAs(("LikelihoodCorVsUnCor_background"+Name+".pdf").c_str());
    delete c27;
  }

  TCanvas *c3 = new TCanvas();
  gStyle->SetOptStat(2222);
  c3->cd();
  TLegend *leg3 = new TLegend(0.6,0.7,0.89,0.89);
  leg3->AddEntry(LikelihoodCutUnCorCor,"correlated");
  leg3->AddEntry(LikelihoodCutUnCorUnCor,"uncorrelated");
  leg3->SetFillColor(kWhite);
  LikelihoodCutUnCorCor->Draw("EHIST");
  LikelihoodCutUnCorUnCor->Draw("EHISTsames");
  if(Use_back){
    LikelihoodCutUnCorBackground_Cor->Draw("EHISTsames");
    leg3->AddEntry(LikelihoodCutUnCorBackground_Cor,"Background Cor");
    LikelihoodCutUnCorBackground_Uncor->Draw("EHISTsames");
    leg3->AddEntry(LikelihoodCutUnCorBackground_Uncor,"Background Uncor");
  }
  if(Use_data){
    LikelihoodCutUnCorData->Draw("psames");
    leg3->AddEntry(LikelihoodCutUnCorData,"Data");
  }
  leg3->Draw();
  c3->Modified();
  c3->Update();
  c3->SaveAs(("LikelihoodCutUnCor"+Name+".C").c_str());
  c3->SaveAs(("LikelihoodCutUnCor"+Name+".png").c_str());
  c3->SaveAs(("LikelihoodCutUnCor"+Name+".pdf").c_str());
  delete c3;

  TCanvas *c41 = new TCanvas();
  gStyle->SetOptStat(210);
  c41->cd();
  RatioVsLCor_SigCor->Draw("colz");
  c41->Modified();
  c41->Update();
  c41->SaveAs(("RatioVsLCor_Cor"+Name+".C").c_str());
  c41->SaveAs(("RatioVsLCor_Cor"+Name+".png").c_str());
  c41->SaveAs(("RatioVsLCor_Cor"+Name+".pdf").c_str());
  delete c41;

  TCanvas *c42 = new TCanvas();
  gStyle->SetOptStat(210);
  c42->cd();
  RatioVsLCor_SigUnCor->Draw("colz");
  c42->Modified();
  c42->Update();
  c42->SaveAs(("RatioVsLCor_UnCor"+Name+".C").c_str());
  c42->SaveAs(("RatioVsLCor_UnCor"+Name+".png").c_str());
  c42->SaveAs(("RatioVsLCor_UnCor"+Name+".pdf").c_str());
  delete c42;

  if(Use_back){
    TCanvas *c43 = new TCanvas();
    gStyle->SetOptStat(210);
    c43->cd();
    RatioVsLCor_Background_Cor->Draw("colz");
    c43->Modified();
    c43->Update();
    c43->SaveAs(("RatioVsLCor_Background_Cor"+Name+".C").c_str());
    c43->SaveAs(("RatioVsLCor_Background_Cor"+Name+".png").c_str());
    c43->SaveAs(("RatioVsLCor_Background_Cor"+Name+".pdf").c_str());
    delete c43;

    TCanvas *c432 = new TCanvas();
    gStyle->SetOptStat(210);
    c432->cd();
    RatioVsLCor_Background_Uncor->Draw("colz");
    c432->Modified();
    c432->Update();
    c432->SaveAs(("RatioVsLCor_Background_Uncor"+Name+".C").c_str());
    c432->SaveAs(("RatioVsLCor_Background_Uncor"+Name+".png").c_str());
    c432->SaveAs(("RatioVsLCor_Background_Uncor"+Name+".pdf").c_str());
    delete c432;
  }

  if(Use_data){
    TCanvas *c44 = new TCanvas();
    gStyle->SetOptStat(210);
    c44->cd();
    Draw_RatioVsLCor_Data->Draw("colz");
    c44->Modified();
    c44->Update();
    c44->SaveAs(("RatioVsLCor_Data"+Name+".C").c_str());
    c44->SaveAs(("RatioVsLCor_Data"+Name+".png").c_str());
    c44->SaveAs(("RatioVsLCor_Data"+Name+".pdf").c_str());
    delete c44;
  }

  TCanvas *c45 = new TCanvas();
  gStyle->SetOptStat(2222);
  c45->cd();
  TLegend *leg45 = new TLegend(0.6,0.7,0.89,0.89);
  leg45->AddEntry(SF_2D,"Cor/Uncor");
  leg45->SetFillColor(kWhite);
  SF_2D->Draw("colz");
  leg45->Draw();
  c45->Modified();
  c45->Update();
  c45->SaveAs(("SF_2D"+Name+".C").c_str());
  c45->SaveAs(("SF_2D"+Name+".png").c_str());
  c45->SaveAs(("SF_2D"+Name+".pdf").c_str());
  delete c45;

  TCanvas *c46 = new TCanvas();
  gStyle->SetOptStat(2222);
  c46->cd();
  TLegend *leg46 = new TLegend(0.6,0.7,0.89,0.89);
  leg46->AddEntry(SFerr_2D,"error Cor/Uncor");
  leg46->SetFillColor(kWhite);
  SFerr_2D->Draw("colz");
  leg46->Draw();
  c46->Modified();
  c46->Update();
  c46->SaveAs(("SFerr_2D"+Name+".C").c_str());
  c46->SaveAs(("SFerr_2D"+Name+".png").c_str());
  c46->SaveAs(("SFerr_2D"+Name+".pdf").c_str());
  delete c46;
  
  std::cout<<"Templates drawn"<<std::endl;
}

//Draw the pseudo-experiments according to an f_input = mixing and fit them, this input parameter is ignored when we look at Data 
std::vector<double> eire::PEBuilder::Mix(float mixing)
{
  mix_frac = mixing;
  
  char Pn[2000], fn[2000], fen[2000], Wn[2000], Wen[2000], Wp[2000], Nn[2000], Nne[2000], Cov_f_Nobs[2000], Cov_f_W[2000];
  char PNe[2000], Cov_W_N[2000];
  sprintf(Pn,"Pull_mix_%i",(int) (mix_frac*10));
  sprintf(fn,"h_fobs_mix_%i", (int) (mix_frac*10));
  sprintf(fen,"h_ferr_mix_%i", (int) (mix_frac*10));
  sprintf(Wn,"h_Wobs_mix_%i",(int) (mix_frac*10));
  sprintf(Wen,"h_Werr_mix_%i",(int) (mix_frac*10));
  sprintf(Wp,"h_WPull_mix_%i",(int) (mix_frac*10));
  sprintf(Nn,"h_Nobs_mix_%i",(int) (mix_frac*10));
  sprintf(Nne,"h_Nobs_err_mix_%i",(int) (mix_frac*10));
  sprintf(PNe,"h_Pull_Ne_mix_%i",(int) (mix_frac*10));
  sprintf(Cov_f_Nobs,"h_Cov_f_Nobs_mix_%i",(int) (mix_frac*10));
  sprintf(Cov_f_W,"h_Cov_f_W_mix_%i",(int) (mix_frac*10));
  sprintf(Cov_W_N,"h_Cov_W_N_mix_%i",(int) (mix_frac*10));
  TH1F* Pull = new TH1F(Pn,"(f_obs - f_input)/sigma_obs; pull; number of PE", 15, -6, 6);
  TH1F* h_fobs = new TH1F(fn,"Fitted fraction of correlated events f; f_{obs}; number of PE", 60, -3., 4.);
  TH1F* h_ferr = new TH1F(fen,"Fit error; #delta f; number of PE", 150, 0.0, 1.0);
  TH1F* h_Wobs = new TH1F(Wn,"Fitted number of background events N_{bkg}; N_{bkg}; number of PE", 50, Wfrac*EvPE-150., Wfrac*EvPE+150.);//0.2
  TH1F* h_Werr = new TH1F(Wen,"Fitted error; #delta N_{bkg}; number of PE", 60, 0, 100);
  TH1F* h_WPull = new TH1F(Wp,"(N_{bkg} - N^{input}_{bkg})/sigma_obs); pull; number of PE", 10, -4., 4.);
  TH1F* h_NPull = new TH1F(PNe,"(N_{ttbar} - N^{input}_{ttbar})/sigma_obs); pull; number of PE", 10, -4., 4.);
  TH1F* h_Nobs = new TH1F(Nn,"Fitted number of ttbar events N_{ttbar}; N_{ttbar}; number of PE",100,(1-Wfrac)*EvPE-200,(1-Wfrac)*EvPE+200);
  TH1F* h_Nobs_err = new TH1F(Nne,"Error on N_{ttbar}; #delta N_{ttbar}; number of PE",60,0,100);
  TH1F* h_Cov_f_Nobs = new TH1F(Cov_f_Nobs,"Correlation between f_obs and N_{ttbar}; correlation; number of PE",200,-1.,1.);
  TH1F* h_Cov_f_W = new TH1F(Cov_f_W,"Correlation between f_obs and N_{bkg}; correlation; number of PE",80,-1.,1.);
  TH1F* h_Cov_W_N = new TH1F(Cov_W_N,"Correlation between N_{ttbar} and N_{bkg}; correlation; number of PE",120,-1.,1.);

  TF1 *g_f = new TF1("g_f","gaus",-3,4);
  g_f->SetLineWidth(0.08);
  TF1 *g_ferr = new TF1("g_ferr","gaus",0.0,0.5);
  g_ferr->SetLineWidth(0.08);
  TF1 *g_N = new TF1("g_N","gaus",(1-Wfrac)*EvPE-200,(1-Wfrac)*EvPE+200);
  g_N->SetLineWidth(0.08);
  TF1 *g_Nerr = new TF1("g_Nerr","gaus",0.0,100.);
  g_Nerr->SetLineWidth(0.08);
  TF1 *g_pull = new TF1("g_pull","gaus",-6,6);
  g_pull->SetLineWidth(0.08);
  TF1 *g_pullN = new TF1("g_pullN","gaus",-4,4);
  g_pullN->SetLineWidth(0.08);
  TF1 *g_W = new TF1("g_W","gaus",Wfrac*EvPE-150.,Wfrac*EvPE+150.);
  g_W->SetLineWidth(0.08);
  TF1 *g_Werr = new TF1("g_Werr","gaus",0.0,100);
  g_Werr->SetLineWidth(0.08);
  TF1 *g_Wpull = new TF1("g_Wpull","gaus",-4.0,4.0);
  g_Wpull->SetLineWidth(0.08);
  TF1 *g_Cov_f_N = new TF1("g_Cov_f_N","gaus",-1.0,1.0);
  g_Cov_f_N->SetLineWidth(0.08);
  TF1 *g_Cov_f_W = new TF1("g_Cov_f_W","gaus",1.0,1.0);
  g_Cov_f_W->SetLineWidth(0.08);
  TF1 *g_Cov_W_N = new TF1("g_Cov_W_N","gaus",-1.0,1.0);
  g_Cov_W_N->SetLineWidth(0.08);


  Pull->Sumw2();
  h_fobs->Sumw2();
  h_ferr->Sumw2();
  h_Wobs->Sumw2();
  h_Werr->Sumw2();
  h_WPull->Sumw2();
  h_NPull->Sumw2();
  h_Nobs->Sumw2();
  h_Nobs_err->Sumw2();
  h_Cov_f_Nobs->Sumw2();
  h_Cov_f_W->Sumw2();
  h_Cov_W_N->Sumw2();

  std::cout<<"Wfrac = "<<Wfrac<<std::endl;

  using namespace RooFit;

  //variables to fit in
  RooRealVar* Ratio_ = new RooRealVar("Ratio","-2ln#lambda",x_low_range, x_high_range, "x");
  RooRealVar* LCor_ = new RooRealVar("LCor","-lnL{H=C}",y_low_range, y_high_range, "y");

  RooRealVar RatioVar = *Ratio_;
  RooRealVar LCorVar = *LCor_;

  gROOT->cd();

  //make pdfs of the templates
  RooArgList X(RatioVar,LCorVar);
  ///////// convert Histograms into RooDataHists                                                                                                            
  //  RooDataHist* data = new RooDataHist("data","data",X, RatioVsLCor_Data);                                                                                     
  RooDataHist* MC_cor = new RooDataHist("MC_cor","MC_cor", X, RatioVsLCor_SigCor,2);
  RooDataHist* MC_uncor = new RooDataHist("MC_uncor","MC_uncor", X, RatioVsLCor_SigUnCor,2);
  RooDataHist* MC_backcor = new RooDataHist("MC_backcor","MC_back", X, RatioVsLCor_Background_Cor,2);
  RooDataHist* MC_backuncor = new RooDataHist("MC_backuncor","MC_back", X, RatioVsLCor_Background_Uncor,2);
  // RooHistPdf* data_orig = new RooHistPdf("data","data", X, *data);
  RooHistPdf* Cor_orig = new RooHistPdf("Cor_orig","Cor", X, *MC_cor);
  RooHistPdf* UnCor_orig = new RooHistPdf("UnCor_orig","UnCor", X, *MC_uncor);
  RooHistPdf* Back_origcor = new RooHistPdf("Back_origcor","Back", X, *MC_backcor);
  RooHistPdf* Back_origuncor = new RooHistPdf("Back_origuncor","Back", X, *MC_backuncor);
  RooDataHist* H_Cor;
  RooDataHist* H_Uncor;
  RooDataHist* H_Backcor;
  RooDataHist* H_Backuncor;
  RooDataHist *H_Data;
  TRandom2 *EvGen = new TRandom2();
  EvGen->SetSeed();
  
  double SigEv; 
  //  double BackEv;

  std::cout<<"integral data = "<<RatioVsLCor_Data->Integral()<<std::endl;

  //make the data or pseudo-date histograms
  std::vector<RooDataHist> *hists = new std::vector<RooDataHist>();
  if(Use_data){
    H_Data = new RooDataHist("data","data",X, RatioVsLCor_Data);
    //    for(int i = 0; i < 250; i++){
    //SigEv = EvGen->Poisson(EvPE);
    //H_Data = data_orig->generateBinned(X, SigEv, Extended(kFALSE));
    hists->push_back(*H_Data);
    //    }
    std::cout<<"use the data"<<std::endl;
  }
  else{
    //when not using data, run 300 pseudo-experiments
    for(int i = 0; i < 300; i++){
      SigEv = EvGen->Poisson(EvPE);
      //      BackEv = EvGen->Poisson(Wfrac*EvPE);
      //generate correlated and uncorrelated histograms
      H_Cor = Cor_orig->generateBinned(X, mix_frac*(1-Wfrac)*SigEv, Extended(kFALSE));
      H_Uncor = UnCor_orig->generateBinned(X, (1-mix_frac)*(1-Wfrac)*SigEv, Extended(kFALSE));
      //generate the background histogram
      if(Use_back){std::cout<<"adding background to pseudo-experiment"<<std::endl; H_Backcor = Back_origcor->generateBinned(X, mix_frac*Wfrac*SigEv, Extended(kFALSE)); H_Backuncor = Back_origuncor->generateBinned(X, (1-mix_frac)*Wfrac*SigEv, Extended(kFALSE));}
      //add the histograms together to one pseudo-date template
      if(H_Cor){
	H_Data = (RooDataHist*) H_Cor->Clone();
	if(H_Uncor){
	  H_Data->add(*H_Uncor);
	}
      }
      else{
	H_Data = (RooDataHist*) H_Uncor->Clone();
      }
      if(Use_back && H_Backcor){
	  H_Data->add(*H_Backcor);
      }
      if(Use_back && H_Backuncor){
	  H_Data->add(*H_Backuncor);
      }
      
      //store all the pseudo-data templates
      hists->push_back(*H_Data);
    }
  }

  //  FILE *event_f;
  //char name[2000];
  //sprintf(name,"event_f_%f",mix_frac);
  //event_f = fopen(name,"w");  

  //run over all the (pseudo-) experiments and fit
  for(unsigned int z = 0; z < hists->size(); z++){
    std::vector<double> res;
    //fit with or without the background templates and retrieve the fitted parameters
    if(Use_back){res = FitB((*hists)[z], z);}
    else{res = Fit((*hists)[z], z);}
    std::cout<<"Data has been fitted"<<std::endl;
    double f= (res)[0];
    double Err_f=(res)[1];
    double Nobs = (res)[2];
    double Nerr = (res)[3];
    //    double Converged = (res)[4];
    double status = (res)[5];
    //double edm = (res)[6];
    double Cor_f_N = (res)[7];
    if(status == 0){
      if(!Use_data){Pull->Fill((f - mix_frac)/Err_f);}
      if(Use_data){
	double Wd = (res)[8];
	double Wd_err = (res)[9];
	std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
	std::cout<<"Data f = "<<f<<std::endl;
	std::cout<<"delta f = "<<Err_f<<std::endl;
	std::cout<<"N_tt = "<<Nobs<<" +/- "<<Nerr<<std::endl;
	std::cout<<"N_bkg = "<<Wd<<" +/- "<<Wd_err<<std::endl;
	std::cout<<"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"<<std::endl;
      }
      h_fobs->Fill(f);
      h_ferr->Fill(Err_f);
      h_Nobs->Fill(Nobs);
      h_Nobs_err->Fill(Nerr);
      if(!Use_data){h_NPull->Fill((Nobs - (1-Wfrac)*EvPE)/Nerr);}
      h_Cov_f_Nobs->Fill(Cor_f_N);
      //      int NPE = (*EvInPE)[z];
      if(Use_back){
	double W = (res)[8];
	double W_err = (res)[9];
	double cor_f_w = (res)[10];
	double cor_w_N = (res)[11];
	h_Wobs->Fill(W);
	h_Werr->Fill(W_err);
	//	h_WPull->Fill((W - Wfrac)/W_err);
	if(!Use_data){h_WPull->Fill((W - Wfrac*EvPE)/W_err);}
	h_Cov_f_W->Fill(cor_f_w);
	h_Cov_W_N->Fill(cor_w_N);
      }
      //      int ev_cor = (int) ( NPE*mix_frac);
      //int ev_uncor = (int) ( NPE*(1-mix_frac));
      //for(int i = 0; i < ev_cor; i++){
      //fprintf(event_f,"%i\t%.3f\n",i+z*ev_cor,f); //correlated events                                                                                      
      // }
      //for(int i = 0; i < ev_uncor; i++){
      //fprintf(event_f,"%i\t%.3f\n",-1*(i+z*ev_uncor),f); //uncorrelated events                                                                            
      //}
    }
    gStyle->SetOptStat(1111111);
  }

  hists->clear();
  //  fclose(event_f);

  //for running with quality cuts on
  //  h_fobs->Rebin(4);
  //h_ferr->Rebin(4);

  TCanvas *c3 = new TCanvas();
  gStyle->SetOptStat(2200);
  c3->Divide(3,1);
  c3->cd(1);
  h_fobs->Draw();
  h_fobs->Fit("g_f");
  c3->cd(2);
  h_ferr->Draw();
  h_ferr->Fit("g_ferr");
  c3->cd(3);
  Pull->Draw();
  Pull->Fit("g_pull");
  stringstream namef;
  stringstream n;
  n<<"_mix_"<<mix_frac;
  namef<<"fhists_"<<Name<<n.str()<<".C";
  c3->SaveAs(namef.str().c_str());
  TCanvas *c5 = new TCanvas();
  c5->Divide(3,1);
  c5->cd(1);
  h_Nobs->Draw();
  h_Nobs->Fit("g_N");
  c5->cd(2);
  h_Nobs_err->Draw();
  h_Nobs_err->Fit("g_Nerr");
  c5->cd(3);
  h_NPull->Draw();
  h_NPull->Fit("g_pullN");
  stringstream nameP;
  nameP<<"Nobs_"<<Name<<n.str()<<".C";
  c5->SaveAs(nameP.str().c_str());

  if(Use_back){
  TCanvas *c4 = new TCanvas();
  c4->Divide(3,1);
  c4->cd(1);
  h_Wobs->Draw();
  h_Wobs->Fit("g_W");
  c4->cd(2);
  h_Werr->Draw();
  h_Werr->Fit("g_Werr");
  c4->cd(3);
  h_WPull->Draw();
  h_WPull->Fit("g_Wpull");
  stringstream nameW;
  nameW<<"Whists_"<<Name<<n.str()<<".C";
  c4->SaveAs(nameW.str().c_str());
  TCanvas *c11 = new TCanvas();
  c11->Divide(3,1);
  c11->cd(1);
  h_Cov_f_W->Draw();
  h_Cov_f_W->Fit("g_Cov_f_W");
  c11->cd(2);
  h_Cov_f_Nobs->Rebin(2);
  h_Cov_f_Nobs->Draw();
  h_Cov_f_Nobs->Fit("g_Cov_f_N");
  c11->cd(3);
  h_Cov_W_N->Draw();
  h_Cov_W_N->Fit("g_Cov_W_N");
  stringstream nameCovW;
  nameCovW<<"Correlation_"<<Name<<n.str()<<".C";
  c11->SaveAs(nameCovW.str().c_str());
  }else{
    TCanvas *c10 = new TCanvas();
    c10->cd();
    h_Cov_f_Nobs->Draw();
    h_Cov_f_Nobs->Fit("g_Cov_f_N");
    stringstream nameCov;
    nameCov<<"Correlation_"<<Name<<n.str()<<".C";
    c10->SaveAs(nameCov.str().c_str());
  }

  double f_av = g_f->GetParameter(1);
  double f_averr = g_f->GetParError(1);
  double p_av = g_pull->GetParameter(1);
  double p_averr = g_pull->GetParError(1);
  double p_RMS = g_pull->GetParameter(2);
  double p_RMSerr = g_pull->GetParError(2);

  double Ntt_av = g_N->GetParameter(1);
  double Ntt_averr = g_N->GetParError(1);
  double Nttp_av = g_pullN->GetParameter(1);
  double Nttp_averr = g_pullN->GetParError(1);
  double Nttp_RMS = g_pullN->GetParameter(2);
  double Nttp_RMSerr = g_pullN->GetParError(2);

  double Nbkg_av = g_W->GetParameter(1);
  double Nbkg_averr = g_W->GetParError(1);
  double Nbkgp_av = g_Wpull->GetParameter(1);
  double Nbkgp_averr = g_Wpull->GetParError(1);
  double Nbkgp_RMS = g_Wpull->GetParameter(2);
  double Nbkgp_RMSerr = g_Wpull->GetParError(2);


  if((f_av + 3*f_averr) < mix_frac || (f_av - 3*f_averr) > mix_frac){std::cout<<"%%%%%% WARNING! The mean f is biased for mixing fraction "<<mix_frac<<std::endl;}
  if((p_av + 3*p_averr) < 0 || (p_av - 3*p_averr) > 0){std::cout<<"%%%%%% WARNING! The pull mean is biased for mixing fraction "<<mix_frac<<std::endl;}
  if((p_RMS + 3*p_RMSerr) < 1 || (p_RMS - 3*p_RMSerr) > 1){std::cout<<"%%%%%% WARNING! The errors are not properly estimated for mixing fraction "<<mix_frac<<std::endl;}

  std::vector<double> Results;
  Results.push_back(f_av);
  Results.push_back(f_averr);
  Results.push_back(p_av);
  Results.push_back(p_averr);
  Results.push_back(p_RMS);
  Results.push_back(p_RMSerr);

  Results.push_back(Ntt_av);
  Results.push_back(Ntt_averr);
  Results.push_back(Nttp_av);
  Results.push_back(Nttp_averr);
  Results.push_back(Nttp_RMS);
  Results.push_back(Nttp_RMSerr);

  Results.push_back(Nbkg_av);
  Results.push_back(Nbkg_averr);
  Results.push_back(Nbkgp_av);
  Results.push_back(Nbkgp_averr);
  Results.push_back(Nbkgp_RMS);
  Results.push_back(Nbkgp_RMSerr);

  //return the average f, delta_f, ... to give to the Linearity Module
  return Results;
}

//fit the pseudo-experiment using signal only templates
std::vector<double> eire::PEBuilder::Fit(RooDataHist hist_data, int k)
{

  using namespace RooFit;
  RooMsgService::instance().setSilentMode(1);
  RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);  

  //RooRealVar* IndL_ = new RooRealVar("IndL","-2ln#lambda",low_range, high_range, "x");

  //RooRealVar IndL = *IndL_;

  gROOT->cd();
  
  //  RooArgList X(IndL);

  RooRealVar* Ratio_ = new RooRealVar("Ratio","-2ln#lambda",x_low_range, x_high_range, "x");
  RooRealVar* LCor_ = new RooRealVar("LCor","-lnL{H=C}",y_low_range, y_high_range, "y");

  RooRealVar RatioVar = *Ratio_;
  RooRealVar LCorVar = *LCor_;

  gROOT->cd();

  RooArgList X(RatioVar,LCorVar);


  ///////// convert Histograms into RooDataHists                                                                                                           
  RooDataHist* data = &hist_data;
  RooDataHist* MC_cor = new RooDataHist("MC_cor","MC_cor", X, RatioVsLCor_SigCor,2);
  RooDataHist* MC_uncor = new RooDataHist("MC_uncor","MC_uncor", X, RatioVsLCor_SigUnCor,2);
  //  RooHistPdf* Cor_orig = new RooHistPdf("Cor_orig","Cor", X, *MC_cor);
  //RooHistPdf* UnCor_orig = new RooHistPdf("UnCor_orig","UnCor", X, *MC_uncor);
  
  // RooDataHist* H_Cor = Cor_orig->generateBinned(X, 350000, Extended(kTRUE)); 
  //RooDataHist* H_Uncor = UnCor_orig->generateBinned(X, 350000, Extended(kTRUE)); 

  RooHistPdf* Cor = new RooHistPdf("Cor","Cor", X, *MC_cor);
  RooHistPdf* UnCor = new RooHistPdf("UnCor","UnCor", X, *MC_uncor);

  RooArgList *Sum = new RooArgList();
  Sum->add(*Cor);
  Sum->add(*UnCor);

  RooRealVar* f = new RooRealVar("f","f",0.5);
  f->setConstant(kFALSE);

  RooRealVar* nEv = new RooRealVar("nEv","nEv",data->sumEntries());
  nEv->setConstant(kFALSE);
  
  RooFormulaVar* nCor = new RooFormulaVar("nCor","f*nEv",RooArgList(*f,*nEv));
  RooFormulaVar* nUnCor = new RooFormulaVar("nUnCor","(1-f)*nEv",RooArgList(*f,*nEv));

  RooArgList yields(*nCor,*nUnCor);
    //RooArgList yields(*f);
  RooAddPdf pdfPass("pdfPass","sum pdf", *Sum, yields);//f*cor+(1-f)*uncor                                                              
                   
  RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Save(),SumW2Error(kFALSE),Hesse(kTRUE),InitialHesse(kTRUE),Extended(kTRUE),Minimizer("Minuit2","migrad"));
  //  RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Save(),SumW2Error(kFALSE),Hesse(kTRUE),InitialHesse(kTRUE),Extended(kFALSE),Minimizer("Minuit2","migrad"));
  if(verbose){
  TString cname = TString("fit");
  TCanvas* c = new TCanvas(cname,cname,500,500);                                                                                                       
  c->Divide(2,1);
  c->cd(1);
  RooPlot* frame1 = RatioVar.frame();                                                                                                                      
  frame1->SetMinimum(-5);                                                                                                                               
  RooAbsData::ErrorType errorType = RooAbsData::SumW2;                                                                                                 
  data->plotOn(frame1,RooFit::DataError(errorType));                                                                                                   
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*Cor),RooFit::LineColor(kRed));                                                    
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*UnCor),RooFit::LineColor(kGreen));                                                
  //  pdfPass.plotOn(frame1,RooFit::ProjWData(*data));                                                                                                     
  pdfPass.plotOn(frame1);                                                                                                                        
  pdfPass.paramOn(frame1,data);                                                                                                                        
  frame1->Draw("e0");
  c->cd(2);
  RooPlot* frame2 = LCorVar.frame();
  frame2->SetMinimum(-5);
  data->plotOn(frame2,RooFit::DataError(errorType));
  pdfPass.plotOn(frame2,RooFit::ProjWData(*data),RooFit::Components(*Cor),RooFit::LineColor(kRed));                                                    
  pdfPass.plotOn(frame2,RooFit::ProjWData(*data),RooFit::Components(*UnCor),RooFit::LineColor(kGreen));                                                
  //  pdfPass.plotOn(frame1,RooFit::ProjWData(*data));                                                                                                     
  pdfPass.plotOn(frame2);                                                                                                                        
  pdfPass.paramOn(frame2,data);                                                                                                                        
  frame2->Draw("e0");
  char fname[1024];                                                                                                                                    
  sprintf(fname,"fit_%i.pdf",k);                                                                                                                 
  c->SaveAs(fname);                                                                                                                                    
  delete c;                                                                                                                                            
  }

  std::vector<double> results;
  double x = f->getVal();
  results.push_back(x);
  double Err = f->getError();
  results.push_back(Err);
  double N = nEv->getVal();
  results.push_back(N);
  double N_err = nEv->getError();
  results.push_back(N_err);
  results.push_back(fitResult->covQual());
  results.push_back(fitResult->status());
  results.push_back(fitResult->edm());
  double cov = (fitResult->correlationMatrix())[0][1];
  results.push_back(fabs(cov));
  return results;
}

//fit the pseudo-experiments using signal and background templates
std::vector<double> eire::PEBuilder::FitB(RooDataHist hist_data, int k)
{
  std::cout<<"Starting fit"<<std::endl;

  using namespace RooFit;
  RooMsgService::instance().setSilentMode(1);
  RooMsgService::instance().setGlobalKillBelow(RooFit::WARNING);
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);  
  
  RooRealVar* Ratio_ = new RooRealVar("Ratio","-2ln#lambda",x_low_range, x_high_range, "x");
  RooRealVar* LCor_ = new RooRealVar("LCor","-lnL{H=C}",y_low_range, y_high_range, "y");

  RooRealVar RatioVar = *Ratio_;
  RooRealVar LCorVar = *LCor_;

  gROOT->cd();

  RooArgList X(RatioVar,LCorVar);


  gROOT->cd();
  
  ///////// convert Histograms into RooDataHists                                                                                                           
  RooDataHist* data = &hist_data;
  RooDataHist* MC_cor = new RooDataHist("MC_cor","MC_cor", X, RatioVsLCor_SigCor,2);
  RooDataHist* MC_uncor = new RooDataHist("MC_uncor","MC_uncor", X, RatioVsLCor_SigUnCor,2);
  RooDataHist* MC_WJetsCor = new RooDataHist("MC_WJetsCor","WJets", X, RatioVsLCor_Background_Cor,2);
  RooDataHist* MC_WJetsUncor = new RooDataHist("MC_WJetsUncor","WJets", X, RatioVsLCor_Background_Uncor,2);

  RooHistPdf* Cor = new RooHistPdf("Cor","Cor", X, *MC_cor);
  RooHistPdf* UnCor = new RooHistPdf("UnCor","UnCor", X, *MC_uncor);
  RooHistPdf* WJetsCor = new RooHistPdf("WJetsCor","WJets", X, *MC_WJetsCor);
  RooHistPdf* WJetsUncor = new RooHistPdf("WJetsUncor","WJets", X, *MC_WJetsUncor);
  
  RooArgList *Sum = new RooArgList();
  Sum->add(*WJetsCor);
  //  Sum->add(*WJetsUncor);
  Sum->add(*Cor);
  Sum->add(*UnCor);
  
  RooRealVar* f = new RooRealVar("f","f",0.5);
  f->setConstant(kFALSE);

  RooRealVar* nEv = new RooRealVar("nEv","nEv",data->sumEntries());
  nEv->setConstant(kFALSE);

  std::cout<<"initial values nTT = "<<0.78*data->sumEntries()<<" nBkg = "<<0.22*data->sumEntries()<<std::endl;

  RooRealVar* nTt = new RooRealVar("nTt","nTt",0.78*data->sumEntries());//,0,999999);
  nTt->setConstant(kFALSE);
  
  RooRealVar* nBkg = new RooRealVar("nBkg","nBkg",0.22*data->sumEntries());//,0,999999);
  nBkg->setConstant(kFALSE);
  
  RooFormulaVar* nCor = new RooFormulaVar("nCor","(nTt+nBkg)*nTt*f/(nTt+nBkg)",RooArgList(*nTt,*f,*nBkg));
  RooFormulaVar* nUnCor = new RooFormulaVar("nUnCor","(nTt+nBkg)*nTt*(1-f)/(nTt+nBkg)",RooArgList(*nTt,*f,*nBkg));

  RooFormulaVar* nW = new RooFormulaVar("nW","(nTt+nBkg)*nBkg/(nTt+nBkg)",RooArgList(*nTt,*nBkg));
  RooFormulaVar* nWC = new RooFormulaVar("nWC","(nTt+nBkg)*nBkg*f/(nTt+nBkg)",RooArgList(*nTt,*nBkg,*f));
  RooFormulaVar* nWU = new RooFormulaVar("nWU","(nTt+nBkg)*nBkg*(1-f)/(nTt+nBkg)",RooArgList(*nTt,*nBkg,*f));

  RooArgList yields(*nW, *nCor, *nUnCor);//nW
  //  RooArgList yields(*nWC,*nWU, *nCor, *nUnCor);//nW
  RooAddPdf pdfPass("pdfPass","sum pdf", *Sum, yields, kFALSE);//kFALSE = not recursive
  
  //  RooProdPdf modelc("modelc","model with constraint",RooArgSet(pdfPass,nBkgconstraint));

  RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Save(),SumW2Error(kFALSE),Extended(kTRUE));
  // RooFitResult *fitResult = pdfPass.fitTo(*data,Minos(kTRUE),Constrain(*nBkg),Save(),SumW2Error(kFALSE),Extended(kFALSE));
  if(verbose){
  TString cname = TString("fit");
  TCanvas* c = new TCanvas(cname,cname,500,500);                                                                                                       
  c->Divide(2,1);
  c->cd(1);
  RooPlot* frame1 = RatioVar.frame();                                                                                                                      
  frame1->SetMinimum(-5);                                                                                                                               
  RooAbsData::ErrorType errorType = RooAbsData::SumW2;                                                                                                 
  data->plotOn(frame1,RooFit::DataError(errorType));                                                                                                   
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*Cor),RooFit::LineColor(kRed));                                                    
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*UnCor),RooFit::LineColor(kGreen));                                                
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*WJetsCor),RooFit::LineColor(kOrange));                                               
  pdfPass.plotOn(frame1,RooFit::ProjWData(*data),RooFit::Components(*WJetsUncor),RooFit::LineColor(kYellow));                                               
  //pdfPass.plotOn(frame1,RooFit::ProjWData(*data));                                                                                                     
  pdfPass.plotOn(frame1);                                                                                                                        
  //pdfPass.paramOn(frame1,data);                                                                                                                        
  frame1->Draw("e0");
  c->cd(2);
  RooPlot* frame2 = LCorVar.frame();
  frame2->SetMinimum(-5);
  data->plotOn(frame2,RooFit::DataError(errorType));
  pdfPass.plotOn(frame2,RooFit::ProjWData(*data),RooFit::Components(*Cor),RooFit::LineColor(kRed));                                                    
  pdfPass.plotOn(frame2,RooFit::ProjWData(*data),RooFit::Components(*UnCor),RooFit::LineColor(kGreen));                                                
  pdfPass.plotOn(frame2,RooFit::ProjWData(*data),RooFit::Components(*WJetsCor),RooFit::LineColor(kOrange));                                               
  pdfPass.plotOn(frame2,RooFit::ProjWData(*data),RooFit::Components(*WJetsUncor),RooFit::LineColor(kYellow));                                               
  //  pdfPass.plotOn(frame2,RooFit::ProjWData(*data));                                                                                                     
  pdfPass.plotOn(frame2);                                                                                                                        
  //  pdfPass.paramOn(frame2,data);                                                                                                                        
  frame2->Draw("e0");
  char fname[1024];                                                                                                                                    
  sprintf(fname,"fit_%i.pdf",k);                                                                                                                 
  c->SaveAs(fname);                                                                                                                                    
  c->SaveAs("fit_data.C");
  c->SaveAs("fit_data.root");
  delete c;                                                                                                                                            
  }
  std::vector<double> results;
  double x = f->getVal();
  results.push_back(x);
  double Err = f->getError();
  results.push_back(Err);
  double N = nTt->getVal();
  results.push_back(N);
  double N_err = nTt->getError();
  //double N_err = 1;
  results.push_back(N_err);
  results.push_back(fitResult->covQual());
  results.push_back(fitResult->status());
  results.push_back(fitResult->edm());
  //  std::cout<<"f= "<<f->getVal()<<" + "<<f->getError()<<" nEv= "<<nEv->getVal()<<" + "<<nEv->getError()<<" nW = "<<W->getVal()<<" + "<<W->getError()<<std::endl;
  //  std::cout<<"cor f,N= "<<(fitResult->correlationMatrix())[1][2]<<" cor f, W= "<<(fitResult->correlationMatrix())[0][1]<<" cor W,N = "<<(fitResult->correlationMatrix())[0][2]<<std::endl;
  double cov = (fitResult->correlationMatrix())[0][2];
  results.push_back(cov);
  results.push_back(nBkg->getVal());
  results.push_back(nBkg->getError());
  results.push_back((fitResult->correlationMatrix())[0][1]);
  results.push_back((fitResult->correlationMatrix())[1][2]);
  return results;
}
