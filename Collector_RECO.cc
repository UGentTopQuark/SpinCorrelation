#include "Collector_RECO.h"

//constructor
eire::Collector_RECO::Collector_RECO(bool HF, float Chi, float M_low, float M_high, float Like_cor_cut_up, float Like_cor_cut_down, float Like_uncor_cut_up, float Like_uncor_cut_down, int PU)
{
  use_HitFit = HF;
  if(use_HitFit){
    chicut = Chi;
    m_low = M_low;
    m_high = M_high;
  }
  Like_cut_up = Like_cor_cut_up;
  Like_cut_down = Like_cor_cut_down;
  Like_cutuncor_up = Like_uncor_cut_up;
  Like_cutuncor_down = Like_uncor_cut_down;
  PU_var = PU;
}

//destructor
eire::Collector_RECO::~Collector_RECO()
{
}

//return a vector with the total event weight
std::vector<std::vector<float> > eire::Collector_RECO::CollectEventWeights()
{
  return weights_vector;
}

//read in which weights should be applied                                                                                                                   
void eire::Collector_RECO::SetUpWeights(std::vector<std::string> s)
{
  if(s.size() < 14){std::cout<<"%%%%%%%%%ERROR%%%%%%%%: event weights are not configured properly!"<<std::endl;}
  //  std::cout<<"weights setup"<<std::endl;                                                                                                               
  for(unsigned int i = 0; i < s.size(); i++)
    {
      w_setup[i] = atoi(((s)[i]).c_str());
    }
}


//collect the likelihoods for a particular sample
//the output will consist of events which have a converged likelihood for both hypotheses
//if there are cuts placed on the absolute likelihoods or HitFit output, these are applied here
//the output is a vector of vectors: first vector is correlated likelihoods, second is uncorrelated likelihoods
// third vector is uncertainties on correlated likelihoods, fourth vector is uncertainties on uncorrelated likelihoods
std::vector<std::vector<float> > eire::Collector_RECO::CollectLikelihoods(std::string name)
{
  std::cout<<"making Likelihoods for "<<name<<std::endl;
  std::cout<<"PU_var = "<<PU_var<<std::endl;
  weights_vector.clear();

  int number_of_files = 0.;
  int start_value = 0.;
  string Cor;
  string UnCor;
  string Map;
  string weights;
  string list;
  string barrel_cut;

  TTree *HitFitTree;
  TFile *infile;

  std::vector<float> cor;
  std::vector<float> uncor;
  std::vector<float> cor_unc;
  std::vector<float> uncor_unc;

  //look for the correct location of the MadWeight output files
  if(name == "RECO_Trial_Cor_e_pos"){number_of_files = 742;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_D/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/RECO_Trial_Cor_e_pos_uncor/weights-";
    weights = "./event_weight.txt";
  }
  else if(name == "8TeV_12k_Data_mu_pos"){number_of_files = 742;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_Data_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_Data_uncor/mu_plus/weights-";
    if(use_HitFit){
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_Data_muon_04_cutset.root","OPEN");
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_Data/event_weight_Data_muon_04_cutset.txt";
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_Data_muon_01_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_Data_muon_01_cutset.txt";
    //    barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_Data/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_Data_muon_04_cutset.txt";
  }
  else if(name == "8TeV_MW_mass_173_5_Data_mu_pos"){number_of_files = 704;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_173_5_Data_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_173_5_Data_uncor/mu_plus/weights-";
    if(use_HitFit){
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_Data_muon_04_cutset.root","OPEN");
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_MW_mass_172_5_Data_mu_pos"){number_of_files = 704;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_172_5_Data_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_172_5_Data_uncor/mu_plus/weights-";
    if(use_HitFit){
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_Data_muon_04_cutset.root","OPEN");
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Cor_mu_pos"){number_of_files = 4285;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_MCatNLO_Cor_muon_04_cutset.root","OPEN");
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Cor/event_weight_MCatNLO_Cor_muon_04_cutset.txt";
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Cor/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_MCatNLO_Cor_muon_04_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Cor_other_mu_pos"){number_of_files = 687;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_MCatNLO_Cor_mu_background_04_cutset.root","OPEN");
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Cor_other/event_weight_MCatNLO_Cor_mu_background_04_cutset.txt";
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Cor_other/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_MCatNLO_Cor_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Uncor_mu_pos"){number_of_files = 3231;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_MCatNLO_Uncor_muon_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Uncor/event_weight_MCatNLO_Uncor_muon_04_cutset.txt";
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Uncor/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_MCatNLO_Uncor_muon_04_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Uncor_other_mu_pos"){number_of_files = 520;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_MCatNLO_Uncor_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Uncor_other/BarrelCut_muon_pos.txt";
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Uncor_other/event_weight_MCatNLO_Uncor_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_MCatNLO_Uncor_other/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_MCatNLO_Uncor_mu_background_04_cutset.txt";
    }
  else if(name == "8TeV_12k_DY_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_DYJetsToLL_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_DY/event_weight_DYJetsToLL_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_DY/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_DYJetsToLL_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_W1JetsToLNu_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W1Jets/event_weight_W1JetsToLNu_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W1Jets/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_W1JetsToLNu_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_W2JetsToLNu_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W2Jets/event_weight_W2JetsToLNu_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W2Jets/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_W2JetsToLNu_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_W3Jets_mu_pos"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_W3JetsToLNu_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W3Jets/event_weight_W3JetsToLNu_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W3Jets/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_W3JetsToLNu_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_W4Jets_mu_pos"){number_of_files = 60;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_W4JetsToLNu_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W4Jets/event_weight_W4JetsToLNu_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    //High jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_W4Jets/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_W4JetsToLNu_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_TsChan_mu_pos"){number_of_files = 7;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_TsChan_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TsChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TsChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TsChan_mu_background_01_cutset.txt";
    }
      //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TsChan/event_weight_TsChan_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_TsChan_mu_background_01_cutset.txt";
    //High jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TsChan_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TsChan/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_TsChan_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_TbarsChan_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TbarsChan/event_weight_TbarsChan_mu_background_04_cutset.txt";
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_TbarsChan_mu_background_01_cutset.txt";
    //High jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TbarsChan_mu_background_01_cutset.txt";
    //    barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TbarsChan/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_TbarsChan_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_TtChan_mu_pos"){number_of_files = 53;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_TtChan_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TtChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TtChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TtChan_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TtChan/event_weight_TtChan_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_TtChan_mu_background_01_cutset.txt";
    //Hight jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TtChan_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TtChan/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_TtChan_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_TbartChan_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbartChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbartChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbartChan_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TbartChan/event_weight_TbartChan_mu_background_04_cutset.txt";
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_TbartChan_mu_background_01_cutset.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_TbartChan_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_TtWChan_mu_pos"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_TtWChan_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TtWChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TtWChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TtWChan_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TtWChan/event_weight_TtWChan_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_TtWChan_mu_background_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TtWChan_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TtWChan/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_TtWChan_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_12k_TbartWChan_mu_pos"){number_of_files = 26;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_TbartWChan_mu_background_04_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TbartWChan/event_weight_TbartWChan_mu_background_04_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_TbartWChan_mu_background_01_cutset.txt";
    //High jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TbartWChan_mu_background_01_cutset.txt";
    //barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_TbartWChan/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_TbartWChan_mu_background_04_cutset.txt";
  }
  else if(name == "8TeV_JESUp_Data_mu_pos"){number_of_files = 697;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_new_Data_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_new_Data_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_Data_mu_neg"){number_of_files = 684;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_new_Data_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_new_Data_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_Data_mu_pos"){number_of_files = 672;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_new_Data_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_new_Data_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal                                                                                                                                                
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_Data_mu_neg"){number_of_files = 656;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_new_Data_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_new_Data_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal                                                                                                                                                
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_12k_Data_mu_neg"){number_of_files = 685;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_Data_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_Data_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_Data_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_Data_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
    }
    //hight jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_MW_mass_172_5_Data_mu_neg"){number_of_files = 685;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_172_5_Data_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_172_5_Data_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_Data_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_Data_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
    }
    //hight jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_MW_mass_173_5_Data_mu_neg"){number_of_files = 685;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_173_5_Data_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_MW_mass_173_5_Data_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_Data_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_Data_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
    }
    //hight jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_Data_muon_01_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Cor_mu_neg"){number_of_files = 4231;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Cor_other_mu_neg"){number_of_files = 675;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Uncor_mu_neg"){number_of_files = 3171;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_12k_MCatNLO_Uncor_other_mu_neg"){number_of_files = 501;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_12k_DY_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_DY_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_DY_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_W1Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W1Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W1Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_W2Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W2Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W2Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_W3Jets_mu_neg"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W3Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W3Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_W4Jets_mu_neg"){number_of_files = 43;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W4Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_W4Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }
    //high jet pt 
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_TsChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TsChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TsChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TsChan_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_TbarsChan_mu_neg"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbarsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbarsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_TtChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TtChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TtChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TtChan_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_TbartChan_mu_neg"){number_of_files = 31;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbartChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbartChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbartChan_mu_background_01_cutset.txt";
    }
    //high jet pt 
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_TtWChan_mu_neg"){number_of_files = 28;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TtWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TtWChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TtWChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TtWChan_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_12k_TbartWChan_mu_neg"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_12k_TbartWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Cor_mu_neg"){number_of_files = 4174;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Cor_other_mu_neg"){number_of_files = 678;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Uncor_mu_neg"){number_of_files = 3126;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Uncor_other_mu_neg"){number_of_files = 504;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JERUp_DY_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_DY_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_DY_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W1Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W1Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W1Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W2Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W2Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W2Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W3Jets_mu_neg"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W3Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W3Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W4Jets_mu_neg"){number_of_files = 43;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W4Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W4Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TsChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TbarsChan_mu_neg"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbarsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbarsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TtChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TbartChan_mu_neg"){number_of_files = 31;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TtWChan_mu_neg"){number_of_files = 28;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TbartWChan_mu_neg"){number_of_files = 21;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Cor_mu_pos"){number_of_files = 4165;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Cor_other_mu_pos"){number_of_files = 682;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Uncor_mu_pos"){number_of_files = 3143;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_MCatNLO_Uncor_other_mu_pos"){number_of_files = 515;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JERUp_DY_mu_pos"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W3Jets_mu_pos"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_W4Jets_mu_pos"){number_of_files = 60;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TsChan_mu_pos"){number_of_files = 7;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TtChan_mu_pos"){number_of_files = 55;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TtWChan_mu_pos"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERUp_TbartWChan_mu_pos"){number_of_files = 26;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERUp_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERUp/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }  else if(name == "8TeV_JERDown_MCatNLO_Cor_mu_neg"){number_of_files = 4282;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Cor_other_mu_neg"){number_of_files = 674;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Uncor_mu_neg"){number_of_files = 3208;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Uncor_other_mu_neg"){number_of_files = 498;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JERDown_DY_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_DY_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_DY_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W1Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W1Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W1Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W2Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W2Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W2Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W3Jets_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W3Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W3Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W4Jets_mu_neg"){number_of_files = 43;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W4Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W4Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TsChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TbarsChan_mu_neg"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbarsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbarsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TtChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TbartChan_mu_neg"){number_of_files = 30;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TtWChan_mu_neg"){number_of_files = 28;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TbartWChan_mu_neg"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Cor_mu_pos"){number_of_files = 4279;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Cor_other_mu_pos"){number_of_files = 674;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Uncor_mu_pos"){number_of_files = 3229;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_MCatNLO_Uncor_other_mu_pos"){number_of_files = 510;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JERDown_DY_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W3Jets_mu_pos"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_W4Jets_mu_pos"){number_of_files = 60;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TsChan_mu_pos"){number_of_files = 7;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TtChan_mu_pos"){number_of_files = 52;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TtWChan_mu_pos"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JERDown_TbartWChan_mu_pos"){number_of_files = 26;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JERDown_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JERDown/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Cor_mu_pos"){number_of_files = 1795;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Cor_other_mu_pos"){number_of_files = 113;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Uncor_mu_pos"){number_of_files = 980;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Uncor_other_mu_pos"){number_of_files = 60;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Cor_mu_neg"){number_of_files = 1786;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Cor_other_mu_neg"){number_of_files = 111;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Uncor_mu_neg"){number_of_files = 975;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_175_5_MCatNLO_Uncor_other_mu_neg"){number_of_files = 62;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_175_5_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_175_5/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Cor_mu_pos"){number_of_files = 1632;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Cor_other_mu_pos"){number_of_files = 101;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Uncor_mu_pos"){number_of_files = 1747;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Uncor_other_mu_pos"){number_of_files = 104;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Cor_mu_neg"){number_of_files = 1620;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Cor_other_mu_neg"){number_of_files = 100;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Uncor_mu_neg"){number_of_files = 1745;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_mass_169_5_MCatNLO_Uncor_other_mu_neg"){number_of_files = 106;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_mass_169_5_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_mass_169_5/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Cor_mu_pos"){number_of_files = 1376;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Cor_other_mu_pos"){number_of_files = 84;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Uncor_mu_pos"){number_of_files = 1371;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Uncor_other_mu_pos"){number_of_files = 82;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Cor_mu_neg"){number_of_files = 1379;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Cor_other_mu_neg"){number_of_files = 84;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Uncor_mu_neg"){number_of_files = 1370;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Up_MCatNLO_Uncor_other_mu_neg"){number_of_files = 82;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Up_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Up/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Cor_mu_pos"){number_of_files = 1857;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Cor_other_mu_pos"){number_of_files = 115;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Uncor_mu_pos"){number_of_files = 1406;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Uncor_other_mu_pos"){number_of_files = 88;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Cor_mu_neg"){number_of_files = 1848;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Cor_other_mu_neg"){number_of_files = 116;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Uncor_mu_neg"){number_of_files = 1387;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_Q2Down_MCatNLO_Uncor_other_mu_neg"){number_of_files = 85;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_Q2Down_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/TT_Q2Down/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
  }else if(name == "8TeV_JESUp_MCatNLO_Cor_mu_neg"){number_of_files = 4170;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Cor_other_mu_neg"){number_of_files = 674;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Uncor_mu_neg"){number_of_files = 2930;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Uncor_other_mu_neg"){number_of_files = 470;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JESUp_DY_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_DY_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_DY_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W1Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W1Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W1Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W2Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W2Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W2Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W3Jets_mu_neg"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W3Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W3Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W4Jets_mu_neg"){number_of_files = 44;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W4Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W4Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TsChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TbarsChan_mu_neg"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbarsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbarsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TtChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TbartChan_mu_neg"){number_of_files = 32;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TtWChan_mu_neg"){number_of_files = 29;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TbartWChan_mu_neg"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Cor_mu_pos"){number_of_files = 4160;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Cor_other_mu_pos"){number_of_files = 678;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Uncor_mu_pos"){number_of_files = 2947;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_MCatNLO_Uncor_other_mu_pos"){number_of_files = 482;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JESUp_DY_mu_pos"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W3Jets_mu_pos"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_W4Jets_mu_pos"){number_of_files = 62;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TsChan_mu_pos"){number_of_files = 7;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TtChan_mu_pos"){number_of_files = 54;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TtWChan_mu_pos"){number_of_files = 23;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESUp_TbartWChan_mu_pos"){number_of_files = 27;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESUp_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }  else if(name == "8TeV_JESDown_MCatNLO_Cor_mu_neg"){number_of_files = 4075;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Cor_other_mu_neg"){number_of_files = 624;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_Cor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_Cor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Uncor_mu_neg"){number_of_files = 2991;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Uncor_other_mu_neg"){number_of_files = 453;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_UnCor_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_UnCor_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JESDown_DY_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_DY_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_DY_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W1Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W1Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W1Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W2Jets_mu_neg"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W2Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W2Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W3Jets_mu_neg"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W3Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W3Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W4Jets_mu_neg"){number_of_files = 39;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W4Jets_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W4Jets_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TsChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TbarsChan_mu_neg"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbarsChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbarsChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TtChan_mu_neg"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TbartChan_mu_neg"){number_of_files = 27;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TtWChan_mu_neg"){number_of_files = 26;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TbartWChan_mu_neg"){number_of_files = 21;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartWChan_cor/mu_minus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartWChan_uncor/mu_minus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Cor_mu_pos"){number_of_files = 4068;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Cor_other_mu_pos"){number_of_files = 624;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Uncor_mu_pos"){number_of_files = 3008;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_MCatNLO_Uncor_other_mu_pos"){number_of_files = 463;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_JESDown_DY_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W3Jets_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_W4Jets_mu_pos"){number_of_files = 55;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TsChan_mu_pos"){number_of_files = 6;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TtChan_mu_pos"){number_of_files = 44;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TtWChan_mu_pos"){number_of_files = 21;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_JESDown_TbartWChan_mu_pos"){number_of_files = 25;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_JESDown_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }else if(name == "8TeV_noTF_MCatNLO_Cor_mu_pos"){number_of_files = 4285;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_MCatNLO_Cor_other_mu_pos"){number_of_files = 687;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_MCatNLO_Uncor_mu_pos"){number_of_files = 3231;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_MCatNLO_Uncor_other_mu_pos"){number_of_files = 520;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
    }
  else if(name == "8TeV_noTF_DY_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_W3Jets_mu_pos"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_W4Jets_mu_pos"){number_of_files = 60;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_TsChan_mu_pos"){number_of_files = 7;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TsChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TsChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TsChan_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbarsChan_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_TtChan_mu_pos"){number_of_files = 53;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TtChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TtChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TtChan_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbartChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbartChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbartChan_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_TtWChan_mu_pos"){number_of_files = 22;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TtWChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TtWChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TtWChan_mu_background_01_cutset.txt";
    }
  }
  else if(name == "8TeV_noTF_TbartWChan_mu_pos"){number_of_files = 26;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    if(PU_var == 1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_up_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }else if(PU_var == -1){
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/PU_down_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }else{
      weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_TbartWChan_mu_background_01_cutset.txt";
    }
  }  else if(name == "8TeV_noTF_JESUp_MCatNLO_Cor_mu_pos"){number_of_files = 4160;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_MCatNLO_Cor_other_mu_pos"){number_of_files = 678;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_MCatNLO_Uncor_mu_pos"){number_of_files = 2947;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_MCatNLO_Uncor_other_mu_pos"){number_of_files = 482;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_noTF_JESUp_DY_mu_pos"){number_of_files = 3;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_W3Jets_mu_pos"){number_of_files = 4;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_W4Jets_mu_pos"){number_of_files = 62;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_TsChan_mu_pos"){number_of_files = 7;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_TtChan_mu_pos"){number_of_files = 54;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_TtWChan_mu_pos"){number_of_files = 23;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESUp_TbartWChan_mu_pos"){number_of_files = 27;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESUp_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESUp_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }  else if(name == "8TeV_noTF_JESDown_MCatNLO_Cor_mu_pos"){number_of_files = 4068;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Cor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Cor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_MCatNLO_Cor_other_mu_pos"){number_of_files = 624;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_other_Cor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_other_Cor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Cor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Cor_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_MCatNLO_Uncor_mu_pos"){number_of_files = 3008;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Uncor_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Uncor_muon_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_MCatNLO_Uncor_other_mu_pos"){number_of_files = 463;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_other_UnCor_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_MCatNLO_other_UnCor_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_MCatNLO_Uncor_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_MCatNLO_Uncor_mu_background_01_cutset.txt";
    }
  else if(name == "8TeV_noTF_JESDown_DY_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_DY_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_DY_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_DYJetsToLL_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_DYJetsToLL_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_W1Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W1Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W1Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W1JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W1JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_W2Jets_mu_pos"){number_of_files = 1;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W2Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W2Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W2JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W2JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_W3Jets_mu_pos"){number_of_files = 2;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W3Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W3Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W3JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W3JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_W4Jets_mu_pos"){number_of_files = 55;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W4Jets_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_W4Jets_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_W4JetsToLNu_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_W4JetsToLNu_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_TsChan_mu_pos"){number_of_files = 6;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_TbarsChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TbarsChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TbarsChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbarsChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbarsChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_TtChan_mu_pos"){number_of_files = 44;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TtChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TtChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TtChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TtChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_TbartChan_mu_pos"){number_of_files = 0;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TbartChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TbartChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbartChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbartChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_TtWChan_mu_pos"){number_of_files = 21;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TtWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TtWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TtWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TtWChan_mu_background_01_cutset.txt";
  }
  else if(name == "8TeV_noTF_JESDown_TbartWChan_mu_pos"){number_of_files = 25;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TbartWChan_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_JESDown_TbartWChan_uncor/mu_plus/weights-";
    if(use_HitFit){
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/HitFit_TbartWChan_mu_background_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/JESDown_new/event_weight_TbartWChan_mu_background_01_cutset.txt";
  }else if(name == "8TeV_noTF_Data_mu_pos"){number_of_files = 742;
    Cor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_Data_cor/mu_plus/weights-";
    UnCor = "/afs/cern.ch/work/k/ksbeerna/public/weights/8TeV_noTF_Data_uncor/mu_plus/weights-";
    if(use_HitFit){
      //infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/HitFit_Data_muon_04_cutset.root","OPEN");
      infile = new TFile("/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/HitFit_Data_muon_01_cutset.root","OPEN");
      HitFitTree = (TTree*) infile->GetDirectory("HitFit")->Get("HitFitResults");
    }
    //nominal
    //weights = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_Data/event_weight_Data_muon_04_cutset.txt";
    weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto/event_weight_Data_muon_01_cutset.txt";
    //high jet pt
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter_Veto_eventighter/event_weight_Data_muon_01_cutset.txt";
    //CSVT
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/CSVT/event_weight_Data_muon_01_cutset.txt";
    //    barrel_cut = "/afs/cern.ch/work/k/ksbeerna/public/input_8TeV_12k_Data/BarrelCut_muon_pos.txt";
    //excluded extended eta-range
    //weights = "/data/input/LHCO_Muon_full2012_InclB/OrigKin_CombinOnly/Tighter/ExtendedEtaRange/5jets/event_weight_Data_muon_04_cutset.txt";
  }




  //if an additional kinematic selection needs to be done on the fly, do this here
  map<int, bool> Selection_map;
  if(barrel_cut != ""){
    std::cout<<"selecting events!"<<std::endl;
    ifstream listofevents(barrel_cut.c_str());
    string ev;
    string selected;
    if(listofevents.is_open()){
      while(listofevents>>ev){
	listofevents>>selected;
	if(atoi(selected.c_str()) == 1){
	  Selection_map[atoi(ev.c_str())] = 1;
	}else{Selection_map[atoi(ev.c_str())] = 0;} //std::cout<<"deleted event = "<<ev<<std::endl;}
      }
    }
  }
  
  /*
  //historic event selection
  bool select = false;
  int n_sel = 0.;
  if(name == "TTbar_cor" || name == "TTbar_uncor" || name == "WJets"){
    select=true;
    ifstream mapfile(Map.c_str());
    string ev_nr;
    string selected;
    if(mapfile.is_open()){
      while(mapfile>>ev_nr){
	mapfile>>selected;
	Selection_map[atoi(ev_nr.c_str())] = atoi(selected.c_str());
	if(atoi(selected.c_str()) > 0){n_sel++;}
      }
    }
  }
  */

  //if using cuts on HitFit output, do this here
  map<int, bool> ChiCut_map;

  if(use_HitFit){
    int nevents = HitFitTree->GetEntries();
    for(int x=0; x < nevents; x++){
      HitFitTree->GetEntry(x);
      TObjArray *leaves = (TObjArray*)HitFitTree->GetListOfLeaves();
      TLeaf *sol = (TLeaf*)leaves->UncheckedAt(1);
      sol = HitFitTree->GetLeaf("nSol");
      TLeaf *evnr = (TLeaf*)leaves->UncheckedAt(56);
      evnr = HitFitTree->GetLeaf("event_number");
      TLeaf *chi2 = (TLeaf*)leaves->UncheckedAt(30);
      chi2 = HitFitTree->GetLeaf("Chi2");
      TLeaf *mtop = (TLeaf*)leaves->UncheckedAt(32);
      mtop = HitFitTree->GetLeaf("MT");
      TLeaf *extrajet_pt = (TLeaf*)leaves->UncheckedAt(60);
      extrajet_pt = HitFitTree->GetLeaf("ExtraJet_pt");
      if(sol->GetValue(0) == 0){continue;}
      //std::cout<<"event nr = "<<evnr->GetValue(0)<<" extra jet pt = "<<extrajet_pt->GetValue(0)<<std::endl;
      
      /*
      if(extrajet_pt->GetValue(0) == -1){//4 jet events
	//	ChiCut_map[evnr->GetValue(0)] = true;
	if(chi2->GetValue(0) < chicut ){
	  //if(mtop->GetValue(0) >= m_low && mtop->GetValue(0) <= m_high){
	  ChiCut_map[evnr->GetValue(0)] = true;
	  //}
	  //else{ChiCut_map[evnr->GetValue(0)] = false;}
	  //}
	  //else{ChiCut_map[evnr->GetValue(0)] = false;}
	}
	else{
	  ChiCut_map[evnr->GetValue(0)] = false;
	}
      }else{ChiCut_map[evnr->GetValue(0)] = false;}
      */

      if( chi2->GetValue(0) < chicut ){
	//if(mtop->GetValue(0) >= m_low && mtop->GetValue(0) <= m_high){
	ChiCut_map[evnr->GetValue(0)] = true;
	//}
	//else{ChiCut_map[evnr->GetValue(0)] = false;}
	//}
	//else{ChiCut_map[evnr->GetValue(0)] = false;}
      }
      else{
	ChiCut_map[evnr->GetValue(0)] = false;
	}

    }
      
    infile->Close();
  }
  

  map<int, float> map_cor_lik;
  map<int, float> map_uncor_lik;
  map<int, float> map_cor_uncer;
  map<int, float> map_uncor_uncer;
  map<int, float> map_relcut_cor;
  map<int, float> map_relcut_uncor;
  std::vector<int> list_of_events;
  TH1F *DRel_cor = new TH1F("DRel_cor","ln(#deltaL)/ln(L)",50,0.9,1.2);
  TH1F *DRel_uncor = new TH1F("DRel_uncor","ln(#deltaL)/ln(L)",50,0.9,1.2);

  //open all the output files and store the likelihoods
  for(int i= start_value; i < number_of_files + start_value; i++){
    double Ev = i*50;
    stringstream nameCor;
    nameCor<<Cor<<Ev<<".out";
    stringstream nameUnCor;
    nameUnCor<<UnCor<<Ev<<".out";

    //Open input file                                                                                                                                                                                                                                                                                                  
    ifstream myInfile(nameCor.str().c_str());
    ifstream myInfile2(nameUnCor.str().c_str());
    string event_number;
    string likelihood;
    string likelihood_unc;
    double relcut = 0.;
    //correlated likelihood
    if ( myInfile.is_open() ) {
      while ( myInfile>>event_number){
	myInfile>>likelihood; myInfile>>likelihood_unc;
	relcut = log(atof(likelihood_unc.c_str()))/log(atof(likelihood.c_str()));
	//DRel_cor->Fill(relcut);
	if(atof(likelihood.c_str()) != 0 && relcut > 1.00){//&& atof(likelihood_unc.c_str()) < 0.85*atof(likelihood.c_str()) ){ //&& Selection_map[atoi(event_number.c_str())]){
	  map_cor_lik[atoi(event_number.c_str())] = atof(likelihood.c_str());
	  map_cor_uncer[atoi(event_number.c_str())] = atof(likelihood_unc.c_str());
	  map_relcut_cor[atoi(event_number.c_str())] = relcut;
	  //store the event numbers
	  list_of_events.push_back(atoi(event_number.c_str()));
	}
      }
      myInfile.close();
    } 
    //uncorrelated likelihood
    if ( myInfile2.is_open() ) {
      while ( myInfile2>>event_number){
	myInfile2>>likelihood; myInfile2>>likelihood_unc;
	//	DRel_uncor->Fill(relcut);
	relcut = log(atof(likelihood_unc.c_str()))/log(atof(likelihood.c_str()));
	if(atof(likelihood.c_str()) != 0 && relcut > 1.00){//&& atof(likelihood_unc.c_str()) < 0.85*atof(likelihood.c_str())){
	  map_uncor_lik[atoi(event_number.c_str())] = atof(likelihood.c_str());
	  map_uncor_uncer[atoi(event_number.c_str())] = atof(likelihood_unc.c_str());
	  map_relcut_uncor[atoi(event_number.c_str())] = relcut;
	}
      }
      myInfile2.close();      
    }
  }
  
  //  std::cout<<"Collector!"<<std::endl;
  //read in the event weights and store all the values separately in a vector
  map<int, std::vector<float> > Event_weights;
  map<int, float> total_event_weight;
  std::vector<float> event_list_weight;
  ifstream weightsfile(weights.c_str());
  string eventnr;
  char GenWeight[256], PU[256], TopPt[256], leptonId[256], leptonIdUp[256], leptonIdDown[256], trigger[256], triggerUp[256], triggerDown[256], btag[256], btagUp[256], btagDown[256], mistagUp[256], mistagDown[256];
  char temp[256];
  int teller = 0;
  if( weightsfile.is_open() ){
    while(!weightsfile.eof()){
      weightsfile >> eventnr;
      if(eventnr == "event"){weightsfile >>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp>>temp;
      }
      else{
	weightsfile >> GenWeight >> PU >> TopPt >> leptonId >> leptonIdUp >> leptonIdDown >> trigger >> triggerUp >> triggerDown >> btag >> btagUp >> btagDown >> mistagUp >> mistagDown;
	std::vector<float> w;
	w.push_back(atof(GenWeight));
	w.push_back(atof(PU)); w.push_back(atof(TopPt)); w.push_back(atof(leptonId)); w.push_back(atof(leptonIdUp)); w.push_back(atof(leptonIdDown));
	w.push_back(atof(trigger)); w.push_back(atof(triggerUp)); w.push_back(atof(triggerDown)); w.push_back(atof(btag)); w.push_back(atof(btagUp)); w.push_back(atof(btagDown)); w.push_back(atof(mistagUp)); w.push_back(atof(mistagDown));
	Event_weights[atoi(eventnr.c_str())] = w;
	//	std::cout<<"TopPt weight = "<<pow(atof(TopPt),w_setup[2])<< " setup = "<<w_setup[2]<<std::endl;;
	total_event_weight[atoi(eventnr.c_str())] = pow(atof(PU),w_setup[1])*pow(atof(TopPt),w_setup[2])*pow(atof(leptonId),w_setup[3])*pow(atof(leptonIdUp),w_setup[4])*pow(atof(leptonIdDown),w_setup[5])*pow(atof(trigger),w_setup[6])*pow(atof(triggerUp),w_setup[7])*pow(atof(triggerDown),w_setup[8])*pow(atof(btag),w_setup[9])*pow(atof(btagUp),w_setup[10])*pow(atof(btagDown),w_setup[11])*pow(atof(mistagUp),w_setup[12])*pow(atof(mistagDown),w_setup[13]);
	//std::cout<<"event nr = "<<eventnr<<" weight = "<<atof(PU)*atof(TopPt)*atof(leptonId)*atof(trigger)*atof(btag)*atof(GenWeight)<<std::endl;
	teller++;
      }
    }
   
  }

  
  //run over all event numbers, keep only the events where both correlated and uncorrelated likelihoods converged
  //check the absolute likelihoods and cut away the event if necessary
  float weighted_nr_of_events = 0.;
  for(unsigned int j = 0; j < list_of_events.size(); j++){
    //    std::cout<<"event: "<<list_of_events[j]<<" cor: "<<map_cor_lik[list_of_events[j]]<<" uncor: "<<map_uncor_lik[list_of_events[j]]<<std::endl;
    //    if(Selection_map.size() != 0 && Selection_map[list_of_events[j]] == 0){continue;}
    if(Event_weights.size() != 0 && Event_weights[list_of_events[j]].size() == 0){continue;}
    if(map_cor_lik[list_of_events[j]] == 0 || map_uncor_lik[list_of_events[j]] == 0){continue;}
    if((-1)*log(map_cor_lik[list_of_events[j]]) > Like_cut_up || (-1)*log(map_cor_lik[list_of_events[j]]) < Like_cut_down){continue;}
    if((-1)*log(map_uncor_lik[list_of_events[j]]) > Like_cutuncor_up || (-1)*log(map_uncor_lik[list_of_events[j]]) < Like_cutuncor_down){continue;}
    if(use_HitFit && !ChiCut_map[list_of_events[j]]){continue;}
    DRel_cor->Fill(map_relcut_cor[list_of_events[j]]);
    DRel_uncor->Fill(map_relcut_uncor[list_of_events[j]]);
    cor.push_back(map_cor_lik[list_of_events[j]]);
    uncor.push_back(map_uncor_lik[list_of_events[j]]);
    cor_unc.push_back(map_cor_uncer[list_of_events[j]]);
    uncor_unc.push_back(map_uncor_uncer[list_of_events[j]]);
    weighted_nr_of_events += total_event_weight[list_of_events[j]];
    //    std::cout<<"event weight = "<<total_event_weight[list_of_events[j]]<<std::endl;
    if(Event_weights.size() == 0){
      std::vector<float> dummy; for(int i = 0; i < 14; i++){dummy.push_back(1.);}
      weights_vector.push_back(dummy);
    }else{
      weights_vector.push_back(Event_weights[list_of_events[j]]);
    }
  }

  /*
  TCanvas *c = new TCanvas();
  c->cd();
  DRel_cor->Draw();
  DRel_uncor->SetMarkerColor(kRed);
  DRel_uncor->SetLineColor(kRed);
  DRel_uncor->Draw("sames");
  c->SaveAs(("DRel_"+name+".C").c_str());
  delete c;
  */
  delete DRel_cor;
  delete DRel_uncor;

  //return the output
  std::cout<<"Collected: RAW nr of events in template = "<<cor.size()<<std::endl;
  std::cout<<"Collected: weighted nr of events in template = "<<weighted_nr_of_events<<std::endl;
  std::vector<std::vector<float> > result;
  result.push_back(cor);
  result.push_back(uncor);
  result.push_back(cor_unc);
  result.push_back(uncor_unc);

  return result;
}
