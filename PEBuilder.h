#ifndef EIRE_PEBUILDER_H
#define EIRE_PEBUILDER_H
#include <math.h>
#include "TRandom2.h"
#include <iostream>
#include <algorithm>
#include <string.h>
#include <sstream>
#include <stdio.h>
#include <fstream>
#include "TROOT.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TVirtualFitter.h"
#include "TTree.h"
#include "TGraphErrors.h"
#include "RooMsgService.h"
#include "TH1.h"
#include "TH2.h"
#include "TStyle.h"
#include <RooFit.h>
#include <RooMinuit.h>
#include "RooMinimizer.h"
#include "RooFitResult.h"
#include <RooGlobalFunc.h>
#include <RooPlot.h>
#include "RooTFnBinding.h"
#include <TMultiGraph.h>
#include "TF2.h"
#include "RooCategory.h"
#include "TStyle.h"
#include <iomanip>
#include <string>
#include <cmath>
#include <cstdlib>
#include "TFile.h"
#include "TLegend.h"
#include "TTree.h"
#include "TPaveText.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TVector.h"
#include "TCanvas.h"
#include "TProfile.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooProdPdf.h"
#include "RooConstVar.h"
#include "RooFormulaVar.h"
#include "RooGenericPdf.h"
#include "RooPolynomial.h"
#include "RooChi2Var.h"
#include "RooMinuit.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include <vector>
#include <TLeaf.h>
#include "RooHistPdf.h"
#include "RooAddPdf.h"
#include "RooAbsPdf.h"
 
using namespace std;

namespace eire{
  class PEBuilder{
  public:
    PEBuilder(float Back_frac, bool use_back, string name, int x_Bins, int EvPerPE, float x_lowRange, float x_highRange, int y_Bins, float y_lowRange, float y_highRange, bool v, float shift_weight, std::vector<string> X_binning, std::vector<string> Y_binning);
    ~PEBuilder();
    std::vector<TH1F> AddDataSet(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, float data_errors, float shift_x, float shift_y);
    std::vector<TH1F> AddCorTemplate(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float cor_errors, float shift_x, float shift_y);
    std::vector<TH1F> AddUncorTemplate(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float uncor_errors, float shift_x, float shift_y);
    std::vector<TH1F> AddBackgroundTemplateCor(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float background_errors, float shift_x, float shift_y);
    std::vector<TH1F> AddBackgroundTemplateUncor(std::string name, std::vector< std::vector<float> > Likelihoods, float weight, std::vector<std::vector<float> > event_weights, float background_errors, float shift_x, float shift_y);
    void DrawTemplates();
    void SetUpWeights(std::vector< std::string > s);
    std::vector<double> Mix(float mixing);

  private:
    std::vector<double> Fit(RooDataHist hist_data, int k);
    std::vector<double> FitB(RooDataHist hist_data, int k);
    std::vector< std::vector<float> > L_Cor;
    std::vector< std::vector<float> > L_Uncor;
    std::vector< std::vector<float> > L_Background;
    std::vector< std::vector<float> > L_Background_Cor;
    std::vector< std::vector<float> > L_Background_Uncor;

    std::vector< std::vector<float> > L_Data;
    std::vector<float> Cor_weights;
    std::vector<float> Uncor_weights;
    std::vector<float> Background_weights;
    std::vector<float> Data_weights;
    int x_nBins;
    int EvPE;
    float x_low_range;
    float x_high_range;
    int y_nBins;
    std::vector<string> vector_of_xbins;
    std::vector<string> vector_of_ybins;
    float y_low_range;
    float y_high_range;
    float mix_frac;
    bool verbose;
    string Name;
    float weight_shift1;

    TH2F* RatioVsLCor_SigCor;
    TH2F* RatioVsLCor_SigUnCor;
    TH1F* Template_SigCor;
    TH1F* Template_SigUnCor;
    TH1F* LikelihoodCutCor;
    TH1F* LikelihoodCutUnCor;
    TH1F* LikelihoodCutUnCorCor;
    TH1F* LikelihoodCutUnCorUnCor;
    TH2F* LikelihoodCorVsUnCor_corsample;
    TH2F* LikelihoodCorVsUnCor_uncorsample;
    TH2F* RatioVsLCor_Data;
    TH1F* Template_Data;
    TH1F* LikelihoodCutData;
    TH1F* LikelihoodCutUnCorData;
    TH2F* LikelihoodCorVsUnCor_Data;

    TH2F* RatioVsLCor_Background_Cor;
    TH2F* RatioVsLCor_Background_Uncor;
    TH1F* Template_Background_Cor;
    TH1F* LikelihoodCutBackground_Cor;
    TH1F* LikelihoodCutUnCorBackground_Cor;
    TH2F* LikelihoodCorVsUnCor_Background_Cor;
    TH1F* Template_Background_Uncor;
    TH1F* LikelihoodCutBackground_Uncor;
    TH1F* LikelihoodCutUnCorBackground_Uncor;
    TH2F* LikelihoodCorVsUnCor_Background_Uncor;

    bool Use_back;
    bool Use_data;
    float Wfrac;
    map<int, int> w_setup;
  };
}

#endif
