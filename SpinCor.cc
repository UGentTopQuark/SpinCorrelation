//
//Steering Code for the spin correlations measurement
//author: Kelly Beernaert
//
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
#include "ConfigReader/ConfigReader.h"
#include "Collector.h"
#include "Collector_RECO.h"
#include "PEBuilder.h"
#include "Linearity.h"
#include "TFile.h"
#include "HypTesting.h"
#include "Systematics.h"

int main(int argc, char **argv)
{
  //give a configuration file as input
  if(argc < 2){
    std::cout<<"running systematics"<<std::endl;
    eire::Systematics *syst_eval = new eire::Systematics();
    syst_eval->RunSystematics();
    if(syst_eval){
      delete syst_eval;
      syst_eval = NULL;
    }
    //    std::cerr << "usage: ./SpinCor <config file>" << std::endl;
    //exit(1);
    return 0;
  }else{
  
  // read config file name as parameter from command line
  std::string filename(argv[1]);
  
  eire::ConfigReader *config_reader = new eire::ConfigReader();
  config_reader->read_config_from_file(filename);

  //read config file and get all parameters necessary
  //get the samples that will make your correlated and uncorrelated template, if more than one sample makes a template, then provide the relative weights
  //this assumes that the same number of correlated and uncorrelated samples will be given
  std::vector< std::string > Template_cor = config_reader->get_vec_var("Template_cor","global",false);
  std::vector< std::string > Template_uncor = config_reader->get_vec_var("Template_uncor","global",false);
  //get the signal templates for the shifted distribution (we choose e_pos to be shifted)
  std::vector< std::string > Template_cor_combine = config_reader->get_vec_var("Template_cor_combine","global",false);
  std::vector< std::string > Template_uncor_combine = config_reader->get_vec_var("Template_uncor_combine","global",false);
  //if more than one signal template is given, give relative weights (should mostlikely be one)
  std::vector< std::string > Sig_weights = config_reader->get_vec_var("Sig_weights","global",true);

  //when running in systematics mode
  std::vector< std::string > Template_cor_syst_up = config_reader->get_vec_var("Template_cor_syst_up","global",false);
  std::vector< std::string > Template_uncor_syst_up = config_reader->get_vec_var("Template_uncor_syst_up","global",false);
  //get the signal templates for the shifted distribution (we choose e_pos to be shifted)
  std::vector< std::string > Template_cor_combine_syst_up = config_reader->get_vec_var("Template_cor_combine_syst_up","global",false);
  std::vector< std::string > Template_uncor_combine_syst_up = config_reader->get_vec_var("Template_uncor_combine_syst_up","global",false);
  std::vector< std::string > event_weights_syst_up = config_reader->get_vec_var("event_weights_syst_up", "global",false);
  std::cout<<"size of weights vec = "<<event_weights_syst_up.size()<<std::endl;
  std::vector< std::string > Template_cor_syst_down = config_reader->get_vec_var("Template_cor_syst_down","global",false);
  std::vector< std::string > Template_uncor_syst_down = config_reader->get_vec_var("Template_uncor_syst_down","global",false);
  //get the signal templates for the shifted distribution (we choose e_pos to be shifted)
  std::vector< std::string > Template_cor_combine_syst_down = config_reader->get_vec_var("Template_cor_combine_syst_down","global",false);
  std::vector< std::string > Template_uncor_combine_syst_down = config_reader->get_vec_var("Template_uncor_combine_syst_down","global",false);
  std::vector< std::string > event_weights_syst_down = config_reader->get_vec_var("event_weights_syst_down", "global",false);

  int PU_var_up = atoi(config_reader->get_var("PU_var_up","global",false).c_str());
  int PU_var_down = atoi(config_reader->get_var("PU_var_down","global",false).c_str());

  //if needed a scaling factor to correct fraction of e_pos/e_minus
  float shift1_over_def = atof(config_reader->get_var("shift1_over_def","global",false).c_str());
  shift1_over_def = 1.;
  //turn likelihood errors on or off
  float cor_errors = atof(config_reader->get_var("cor_errors","global",false).c_str());
  float uncor_errors = atof(config_reader->get_var("uncor_errors","global",false).c_str());
  float cor_errors_systup = atof(config_reader->get_var("cor_errors_systup","global",false).c_str());
  float uncor_errors_systup = atof(config_reader->get_var("uncor_errors_systup","global",false).c_str());
  float cor_errors_systdown = atof(config_reader->get_var("cor_errors_systdown","global",false).c_str());
  float uncor_errors_systdown = atof(config_reader->get_var("uncor_errors_systdown","global",false).c_str());
  //only fill if you want a background template or background mixture
  //if more than one background sample is added, give the relative contribution
  //TTbar other correlated
  std::vector< std::string > Template_background_cor = config_reader->get_vec_var("Template_background_cor","global",false);
  std::vector< std::string > Template_background_cor_combine = config_reader->get_vec_var("Template_background_cor_combine","global",false);
  //TTbar other uncorrelated
  std::vector< std::string > Template_background_uncor = config_reader->get_vec_var("Template_background_uncor","global",false);
  std::vector< std::string > Template_background_uncor_combine = config_reader->get_vec_var("Template_background_uncor_combine","global",false);
  //other backgrounds
  std::vector< std::string > Template_background = config_reader->get_vec_var("Template_background","global",false);
  std::vector< std::string > Template_background_combine = config_reader->get_vec_var("Template_background_combine","global",false);

  //when running in systematics mode
  //TTbar other correlated
  std::vector< std::string > Template_background_cor_syst_up = config_reader->get_vec_var("Template_background_cor_syst_up","global",false);
  std::vector< std::string > Template_background_cor_combine_syst_up = config_reader->get_vec_var("Template_background_cor_combine_syst_up","global",false);
  //TTbar other uncorrelated
  std::vector< std::string > Template_background_uncor_syst_up = config_reader->get_vec_var("Template_background_uncor_syst_up","global",false);
  std::vector< std::string > Template_background_uncor_combine_syst_up = config_reader->get_vec_var("Template_background_uncor_combine_syst_up","global",false);
  //other backgrounds
  std::vector< std::string > Template_background_syst_up = config_reader->get_vec_var("Template_background_syst_up","global",false);
  std::vector< std::string > Template_background_combine_syst_up = config_reader->get_vec_var("Template_background_combine_syst_up","global",false);

  //TTbar other correlated
  std::vector< std::string > Template_background_cor_syst_down = config_reader->get_vec_var("Template_background_cor_syst_down","global",false);
  std::vector< std::string > Template_background_cor_combine_syst_down = config_reader->get_vec_var("Template_background_cor_combine_syst_down","global",false);
  //TTbar other uncorrelated
  std::vector< std::string > Template_background_uncor_syst_down = config_reader->get_vec_var("Template_background_uncor_syst_down","global",false);
  std::vector< std::string > Template_background_uncor_combine_syst_down = config_reader->get_vec_var("Template_background_uncor_combine_syst_down","global",false);
  //other backgrounds
  std::vector< std::string > Template_background_syst_down = config_reader->get_vec_var("Template_background_syst_down","global",false);
  std::vector< std::string > Template_background_combine_syst_down = config_reader->get_vec_var("Template_background_combine_syst_down","global",false);

  //add relative weights for the background, sum should be one, first weight is for TTbar other
  std::vector< std::string > Background_weights = config_reader->get_vec_var("Background_weights","global",false);
  float background_frac = atof(config_reader->get_var("Background_frac","global",false).c_str());
  //when running in systematics mode
  std::vector< std::string > Background_weights_systup = config_reader->get_vec_var("Background_weights_syst_up","global",false);
  float background_frac_systup = atof(config_reader->get_var("Background_frac_syst_up","global",false).c_str());
  std::vector< std::string > Background_weights_systdown = config_reader->get_vec_var("Background_weights_syst_down","global",false);
  float background_frac_systdown = atof(config_reader->get_var("Background_frac_syst_down","global",false).c_str());
  if(Background_weights_systup.size() == 0){Background_weights_systup = Background_weights; Background_weights_systdown = Background_weights; background_frac_systup = background_frac; background_frac_systdown = background_frac;}
  //turn likelihood errors on/off
  float background_errors = atof(config_reader->get_var("Background_errors","global",false).c_str());
  float background_errors_systup = atof(config_reader->get_var("Background_errors_systup","global",false).c_str());
  float background_errors_systdown = atof(config_reader->get_var("Background_errors_systdown","global",false).c_str());
  //only fill if you want your "Data" to be independent of your MC_template or for real data
  std::vector< std::string > Dataset = config_reader->get_vec_var("Dataset","global",false);
  std::vector< std::string > Dataset_combine = config_reader->get_vec_var("Dataset_combine","global",false);
  //if you add more than one dataset, you may for some reason add a correction
  std::vector< std::string > Data_weights = config_reader->get_vec_var("Data_weights","global",false);
  float data_errors = atof(config_reader->get_var("Data_errors","global",false).c_str());
  //Which event weights (lepton ID, trigger, ...) do you want applied to the Templates?
  std::vector< std::string > event_weights = config_reader->get_vec_var("event_weights", "global",false);

  //Name for the output files
  std::string Name = config_reader->get_var("output_name","global",true);

  //verbose will turn on output for RooFit and store the fit.pdfs
  bool verbose = false;
  verbose = config_reader->get_bool_var("verbose","global",false);

  //when not running on Data, which fraction of background do you want to mix in? This is the total background fraction
  float Wfrac = atof(config_reader->get_var("Wfrac","global",false).c_str());

  //only fill for MC, which f_input do you want?
  float mixing = atof(config_reader->get_var("mixing","global",false).c_str());

  //if -1 take full data in one pseudo-experiment, else set the number of events per pseudo-experiment
  int nEvPE = atoi(config_reader->get_var("Ev_PE","global",true).c_str());

  //number of bins and range used in the InputTemplate Distribution, x = -2lnLambda, y = -ln(L=c)
  int x_nbins = atoi(config_reader->get_var("x_nbins","global",true).c_str());
  float x_lowRange = atof(config_reader->get_var("x_lowRange","global",true).c_str());
  float x_highRange = atof(config_reader->get_var("x_highRange","global",true).c_str());
  int y_nbins = atoi(config_reader->get_var("y_nbins","global",true).c_str());
  float y_lowRange = atof(config_reader->get_var("y_lowRange","global",true).c_str());
  float y_highRange = atof(config_reader->get_var("y_highRange","global",true).c_str());
  std::vector< std::string> xbinning = config_reader->get_vec_var("x_binning","global",true);
  std::vector< std::string> ybinning = config_reader->get_vec_var("y_binning","global",true);
  

  //do you want to make cuts on the HitFit output tree? If so, then switch on HitFit
  bool with_HitFit = false;
  with_HitFit = config_reader->get_bool_var("with_HitFit","global",false);
  float Chicut = atof(config_reader->get_var("Chicut","global",false).c_str());
  if(Chicut == 0){Chicut = 99999;}
  float MassLow = atof(config_reader->get_var("MassLow","global",false).c_str());
  if(MassLow == 0){MassLow = -10;}
  float MassHigh = atof(config_reader->get_var("MassHigh","global",false).c_str());
  if(MassHigh == 0){MassHigh = 99999;}

  //add extra cuts to the absolute likelihood distributions
  float Like_cut_up = atof(config_reader->get_var("Like_cor_cut_up","global",false).c_str());
    if(Like_cut_up == 0){Like_cut_up = 999999;}
  float Like_cut_down = atof(config_reader->get_var("Like_cor_cut_down","global",false).c_str());
  float Like_cutuncor_up = atof(config_reader->get_var("Like_uncor_cut_up","global",false).c_str());
  if(Like_cutuncor_up == 0){Like_cutuncor_up = 999999;}
  float Like_cutuncor_down = atof(config_reader->get_var("Like_uncor_cut_down","global",false).c_str());

  //if you want to run the complete pseudo-experiments at once! Only makes sense in MC
  bool RunFullExp = config_reader->get_bool_var("RunFullExp","global",false);

  //naming
  stringstream name;
  name<<Name<<"_xb"<<x_nbins<<x_lowRange<<"-"<<x_highRange<<"_yb"<<y_nbins<<"_"<<y_lowRange<<"-"<<y_highRange<<nEvPE<<"evPE";
  
  //all parameters have been read in, proceed to processing

  //check if you want to fit using a background template: note you can add a background fraction but keep UseBack false, meaning you will mix in background but fit with a signal template only
  bool UseBack = false;
  UseBack = config_reader->get_bool_var("use_back","global",true);

  //collect the -2lnLambda templates to give to the Hypothesis Testing module
  std::vector<std::vector<TH1F> > collect_templates;
  std::vector<std::vector<TH1F> > collect_data_templates;

  //collect the -2lnLambda templates to give to the Hypothesis Testing module
  //when running in systematics mode
  std::vector<std::vector<TH1F> > collect_templates_syst_up;

  //collect the -2lnLambda templates to give to the Hypothesis Testing module
  //when running in systematics mode
  std::vector<std::vector<TH1F> > collect_templates_syst_down;

  //split into the Pseudo-Experiments with proper mixing
  eire::PEBuilder *builder;
  builder = new eire::PEBuilder(Wfrac, UseBack, name.str(), x_nbins,nEvPE,x_lowRange,x_highRange,y_nbins,y_lowRange,y_highRange,verbose,shift1_over_def, xbinning, ybinning);
  builder->SetUpWeights(event_weights);

  //split into the Pseudo-Experiments with proper mixing
  //when running in systematics mode
  eire::PEBuilder *builder_syst_up;
  builder_syst_up = new eire::PEBuilder(Wfrac, UseBack, name.str(), x_nbins,nEvPE,x_lowRange,x_highRange,y_nbins,y_lowRange,y_highRange,verbose,shift1_over_def, xbinning, ybinning);
  std::cout<<"event weights size = "<<event_weights_syst_up.size()<<std::endl;
  builder_syst_up->SetUpWeights(event_weights_syst_up);

  //split into the Pseudo-Experiments with proper mixing
  //when running in systematics mode
  eire::PEBuilder *builder_syst_down;
  builder_syst_down = new eire::PEBuilder(Wfrac, UseBack, name.str(), x_nbins,nEvPE,x_lowRange,x_highRange,y_nbins,y_lowRange,y_highRange,verbose,shift1_over_def, xbinning, ybinning);
  builder_syst_down->SetUpWeights(event_weights_syst_down);

  //Collect the signal correlated samples and add them to the Builder
  for(unsigned int j = 0; j < Template_cor.size(); j++){
    std::vector<std::vector<float> > L_extra_cor;
    eire::Collector_RECO *Collector_extra_Cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_Cor->SetUpWeights(event_weights); 
    L_extra_cor = Collector_extra_Cor->CollectLikelihoods((Template_cor)[j]);
    collect_templates.push_back(builder->AddCorTemplate((Template_cor)[j], L_extra_cor,atof((Sig_weights)[j].c_str()), Collector_extra_Cor->CollectEventWeights(),cor_errors,0,0));
    L_extra_cor.clear();
    delete Collector_extra_Cor;
  }

  //Collect the signal uncorrelated samples and add them to the Builder
  for(unsigned int j = 0; j < Template_uncor.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor;
    eire::Collector_RECO *Collector_extra_UnCor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_UnCor->SetUpWeights(event_weights); 
    L_extra_uncor = Collector_extra_UnCor->CollectLikelihoods((Template_uncor)[j]);
    collect_templates.push_back(builder->AddUncorTemplate((Template_uncor)[j],L_extra_uncor,atof((Sig_weights)[j].c_str()), Collector_extra_UnCor->CollectEventWeights(),uncor_errors,0,0));
    L_extra_uncor.clear();
    delete Collector_extra_UnCor;
  }

  //Get TTbar other correlated and add to builder
  for(unsigned int j = 0; j < Template_background_cor.size(); j++){
    std::vector<std::vector<float> > L_extra_back_cor;
    eire::Collector_RECO *Collector_extra_back_cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_back_cor->SetUpWeights(event_weights); 
    L_extra_back_cor = Collector_extra_back_cor->CollectLikelihoods((Template_background_cor)[j]);
    collect_templates.push_back(builder->AddBackgroundTemplateCor((Template_background_cor)[j],L_extra_back_cor,atof((Background_weights)[j].c_str()), Collector_extra_back_cor->CollectEventWeights(),background_errors,0,0));
    L_extra_back_cor.clear();
    delete Collector_extra_back_cor;
  }

  //get TTbar other uncorrelated and add to builder
  for(unsigned int j = 0; j < Template_background_uncor.size(); j++){
    std::vector<std::vector<float> > L_extra_back_uncor;
    eire::Collector_RECO *Collector_extra_back_uncor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_back_uncor->SetUpWeights(event_weights); 
    L_extra_back_uncor = Collector_extra_back_uncor->CollectLikelihoods((Template_background_uncor)[j]);
    collect_templates.push_back(builder->AddBackgroundTemplateUncor((Template_background_uncor)[j],L_extra_back_uncor,atof((Background_weights)[j].c_str()), Collector_extra_back_uncor->CollectEventWeights(),background_errors,0,0));
    L_extra_back_uncor.clear();
    delete Collector_extra_back_uncor;
  }

  //get Background samples and add to builder
  //add these backgrounds to both the correlated background template and uncorrelated background template
  for(unsigned int j = 0; j < Template_background.size(); j++){
    std::vector<std::vector<float> > L_extra_back;
    eire::Collector_RECO *Collector_extra_back = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_back->SetUpWeights(event_weights); 
    L_extra_back = Collector_extra_back->CollectLikelihoods((Template_background)[j]);
    collect_templates.push_back(builder->AddBackgroundTemplateCor((Template_background)[j],L_extra_back,atof((Background_weights)[j+1].c_str()), Collector_extra_back->CollectEventWeights(),background_errors,0,0));
    std::vector<TH1F> test = builder->AddBackgroundTemplateUncor((Template_background)[j],L_extra_back,atof((Background_weights)[j+1].c_str()), Collector_extra_back->CollectEventWeights(),background_errors,0,0);
    L_extra_back.clear();
    delete Collector_extra_back;
  }

  //If there is a real Data sample specified, collect it and add to the builder as a Dataset
  std::vector<std::vector<float> > L_extra_Data;
  if(Dataset.size() != 0){
    for(unsigned int i = 0; i < Dataset.size(); i++){
      eire::Collector_RECO *Collector_extra_Data = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
      Collector_extra_Data->SetUpWeights(event_weights); 
      L_extra_Data = Collector_extra_Data->CollectLikelihoods((Dataset)[i]);
      std::cout<<"Data has been collected"<<std::endl;
      collect_data_templates.push_back(builder->AddDataSet((Dataset)[i],L_extra_Data,atof((Data_weights)[i].c_str()),data_errors,0,0));
      //      L_extra_Data.clear();
      delete Collector_extra_Data;
    }
  }

  //If there is a real Data sample specified, collect it and add to the builder as a Dataset
  //for the data that needs to be shifted in the fit
  std::vector<std::vector<float> > L_extra_Data_shift1;
  for(unsigned int i = 0; i < Dataset_combine.size(); i++){  
    eire::Collector_RECO *Collector_extra_DataS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_DataS1->SetUpWeights(event_weights); 
    L_extra_Data_shift1 = Collector_extra_DataS1->CollectLikelihoods((Dataset_combine)[i]);
    std::cout<<"Data has been collected"<<std::endl;
    collect_data_templates.push_back(builder->AddDataSet((Dataset_combine)[i],L_extra_Data_shift1,atof((Data_weights)[1].c_str()),data_errors,0,0));
    //L_extra_Data_shift1.clear();
    delete Collector_extra_DataS1;
  }


  //Collect the signal correlated samples that you want shifted in the fit (usually e_pos)
  for(unsigned int j = 0; j < Template_cor_combine.size(); j++){
    std::vector<std::vector<float> > L_extra_cor_shift1;
    eire::Collector_RECO *Collector_extra_CorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_CorS1->SetUpWeights(event_weights); 
    L_extra_cor_shift1 = Collector_extra_CorS1->CollectLikelihoods((Template_cor_combine)[j]);
    collect_templates.push_back(builder->AddCorTemplate((Template_cor_combine)[j],L_extra_cor_shift1,atof((Sig_weights)[1].c_str()), Collector_extra_CorS1->CollectEventWeights(),cor_errors,0,0));
    L_extra_cor_shift1.clear();
    delete Collector_extra_CorS1;
  }

  //Collect the signal uncorrelated samples that you want shifted in the fit (usually e_pos)
  for(unsigned int j = 0; j < Template_uncor_combine.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor_shift1;
    eire::Collector_RECO *Collector_extra_UnCorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_UnCorS1->SetUpWeights(event_weights); 
    L_extra_uncor_shift1 = Collector_extra_UnCorS1->CollectLikelihoods((Template_uncor_combine)[j]);
    collect_templates.push_back(builder->AddUncorTemplate((Template_uncor_combine)[j],L_extra_uncor_shift1,atof((Sig_weights)[1].c_str()), Collector_extra_UnCorS1->CollectEventWeights(),uncor_errors,0,0));
    L_extra_uncor_shift1.clear();
    delete Collector_extra_UnCorS1;
  }

  //Get TTbar other correlated and add to builder
  for(unsigned int j = 0; j < Template_background_cor_combine.size(); j++){
    std::vector<std::vector<float> > L_extra_back_cor_shift;
    eire::Collector_RECO *Collector_extra_back_corS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_back_corS1->SetUpWeights(event_weights); 
    L_extra_back_cor_shift = Collector_extra_back_corS1->CollectLikelihoods((Template_background_cor_combine)[j]);
    collect_templates.push_back(builder->AddBackgroundTemplateCor((Template_background_cor_combine)[j],L_extra_back_cor_shift,atof((Background_weights)[Template_background.size()+j+1].c_str()), Collector_extra_back_corS1->CollectEventWeights(),background_errors,0,0));
    L_extra_back_cor_shift.clear();
    delete Collector_extra_back_corS1;
  }

  //get TTbar other uncorrelated and add to builder
  for(unsigned int j = 0; j < Template_background_uncor_combine.size(); j++){
    std::vector<std::vector<float> > L_extra_back_uncor_shift;
    eire::Collector_RECO *Collector_extra_back_uncorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_back_uncorS1->SetUpWeights(event_weights); 
    L_extra_back_uncor_shift = Collector_extra_back_uncorS1->CollectLikelihoods((Template_background_uncor_combine)[j]);
    collect_templates.push_back(builder->AddBackgroundTemplateUncor((Template_background_uncor_combine)[j],L_extra_back_uncor_shift,atof((Background_weights)[Template_background.size()+j+1].c_str()), Collector_extra_back_uncorS1->CollectEventWeights(),background_errors,0,0));
    L_extra_back_uncor_shift.clear();
    delete Collector_extra_back_uncorS1;
  }

  //get Background samples that should be shifted in the fit
  for(unsigned int j = 0; j < Template_background_combine.size(); j++){
    std::vector<std::vector<float> > L_extra_back_shift1;
    eire::Collector_RECO *Collector_extra_backS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down);
    Collector_extra_backS1->SetUpWeights(event_weights); 
    L_extra_back_shift1 = Collector_extra_backS1->CollectLikelihoods((Template_background_combine)[j]);
    collect_templates.push_back(builder->AddBackgroundTemplateCor((Template_background_combine)[j],L_extra_back_shift1,atof((Background_weights)[Template_background.size()+j+2].c_str()), Collector_extra_backS1->CollectEventWeights(),background_errors,0,0));
    std::vector<TH1F> test = builder->AddBackgroundTemplateUncor((Template_background_combine)[j],L_extra_back_shift1,atof((Background_weights)[Template_background.size()+j+2].c_str()), Collector_extra_backS1->CollectEventWeights(),background_errors,0,0);
    L_extra_back_shift1.clear();
    delete Collector_extra_backS1;
  }


  //when running in systematics mode
  //Collect the signal correlated samples and add them to the Builder
  for(unsigned int j = 0; j < Template_cor_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_cor;
    eire::Collector_RECO *Collector_extra_Cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_Cor->SetUpWeights(event_weights_syst_up); 
    L_extra_cor = Collector_extra_Cor->CollectLikelihoods((Template_cor_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddCorTemplate((Template_cor_syst_up)[j], L_extra_cor,atof((Sig_weights)[j].c_str()), Collector_extra_Cor->CollectEventWeights(),cor_errors_systup,0,0));
    L_extra_cor.clear();
    delete Collector_extra_Cor;
  }

  //Collect the signal uncorrelated samples and add them to the Builder
  for(unsigned int j = 0; j < Template_uncor_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor;
    eire::Collector_RECO *Collector_extra_UnCor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_UnCor->SetUpWeights(event_weights_syst_up); 
    L_extra_uncor = Collector_extra_UnCor->CollectLikelihoods((Template_uncor_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddUncorTemplate((Template_uncor_syst_up)[j],L_extra_uncor,atof((Sig_weights)[j].c_str()), Collector_extra_UnCor->CollectEventWeights(),uncor_errors_systup,0,0));
    L_extra_uncor.clear();
    delete Collector_extra_UnCor;
  }

  //Get TTbar other correlated and add to builder
  for(unsigned int j = 0; j < Template_background_cor_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_back_cor;
    eire::Collector_RECO *Collector_extra_back_cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_back_cor->SetUpWeights(event_weights_syst_up); 
    L_extra_back_cor = Collector_extra_back_cor->CollectLikelihoods((Template_background_cor_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddBackgroundTemplateCor((Template_background_cor_syst_up)[j],L_extra_back_cor,atof((Background_weights_systup)[j].c_str()), Collector_extra_back_cor->CollectEventWeights(),background_errors_systup,0,0));
    L_extra_back_cor.clear();
    delete Collector_extra_back_cor;
  }

  //get TTbar other uncorrelated and add to builder
  for(unsigned int j = 0; j < Template_background_uncor_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_back_uncor;
    eire::Collector_RECO *Collector_extra_back_uncor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_back_uncor->SetUpWeights(event_weights_syst_up); 
    L_extra_back_uncor = Collector_extra_back_uncor->CollectLikelihoods((Template_background_uncor_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddBackgroundTemplateUncor((Template_background_uncor_syst_up)[j],L_extra_back_uncor,atof((Background_weights_systup)[j].c_str()), Collector_extra_back_uncor->CollectEventWeights(),background_errors_systup,0,0));
    L_extra_back_uncor.clear();
    delete Collector_extra_back_uncor;
  }

  //get Background samples and add to builder
  //add these backgrounds to both the correlated background template and uncorrelated background template
  for(unsigned int j = 0; j < Template_background_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_back;
    eire::Collector_RECO *Collector_extra_back = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_back->SetUpWeights(event_weights_syst_up); 
    L_extra_back = Collector_extra_back->CollectLikelihoods((Template_background_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddBackgroundTemplateCor((Template_background_syst_up)[j],L_extra_back,atof((Background_weights_systup)[j+1].c_str()), Collector_extra_back->CollectEventWeights(),background_errors,0,0));
    std::vector<TH1F> test = builder_syst_up->AddBackgroundTemplateUncor((Template_background_syst_up)[j],L_extra_back,atof((Background_weights_systup)[j+1].c_str()), Collector_extra_back->CollectEventWeights(),background_errors_systup,0,0);
    L_extra_back.clear();
    delete Collector_extra_back;
  }

  //Collect the signal correlated samples that you want shifted in the fit (usually e_pos)
  for(unsigned int j = 0; j < Template_cor_combine_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_cor_shift1;
    eire::Collector_RECO *Collector_extra_CorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_CorS1->SetUpWeights(event_weights_syst_up); 
    L_extra_cor_shift1 = Collector_extra_CorS1->CollectLikelihoods((Template_cor_combine_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddCorTemplate((Template_cor_combine_syst_up)[j],L_extra_cor_shift1,atof((Sig_weights)[1].c_str()), Collector_extra_CorS1->CollectEventWeights(),cor_errors_systup,0,0));
    L_extra_cor_shift1.clear();
    delete Collector_extra_CorS1;
  }

  //Collect the signal uncorrelated samples that you want shifted in the fit (usually e_pos)
  for(unsigned int j = 0; j < Template_uncor_combine_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor_shift1;
    eire::Collector_RECO *Collector_extra_UnCorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_UnCorS1->SetUpWeights(event_weights_syst_up); 
    L_extra_uncor_shift1 = Collector_extra_UnCorS1->CollectLikelihoods((Template_uncor_combine_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddUncorTemplate((Template_uncor_combine_syst_up)[j],L_extra_uncor_shift1,atof((Sig_weights)[1].c_str()), Collector_extra_UnCorS1->CollectEventWeights(),uncor_errors_systup,0,0));
    L_extra_uncor_shift1.clear();
    delete Collector_extra_UnCorS1;
  }

  //Get TTbar other correlated and add to builder
  for(unsigned int j = 0; j < Template_background_cor_combine_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_back_cor_shift;
    eire::Collector_RECO *Collector_extra_back_corS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_back_corS1->SetUpWeights(event_weights_syst_up); 
    L_extra_back_cor_shift = Collector_extra_back_corS1->CollectLikelihoods((Template_background_cor_combine_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddBackgroundTemplateCor((Template_background_cor_combine_syst_up)[j],L_extra_back_cor_shift,atof((Background_weights_systup)[Template_background.size()+j+1].c_str()), Collector_extra_back_corS1->CollectEventWeights(),background_errors_systup,0,0));
    L_extra_back_cor_shift.clear();
    delete Collector_extra_back_corS1;
  }

  //get TTbar other uncorrelated and add to builder
  for(unsigned int j = 0; j < Template_background_uncor_combine_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_back_uncor_shift;
    eire::Collector_RECO *Collector_extra_back_uncorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_back_uncorS1->SetUpWeights(event_weights_syst_up); 
    L_extra_back_uncor_shift = Collector_extra_back_uncorS1->CollectLikelihoods((Template_background_uncor_combine_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddBackgroundTemplateUncor((Template_background_uncor_combine_syst_up)[j],L_extra_back_uncor_shift,atof((Background_weights_systup)[Template_background.size()+j+1].c_str()), Collector_extra_back_uncorS1->CollectEventWeights(),background_errors_systup,0,0));
    L_extra_back_uncor_shift.clear();
    delete Collector_extra_back_uncorS1;
  }

  //get Background samples that should be shifted in the fit
  for(unsigned int j = 0; j < Template_background_combine_syst_up.size(); j++){
    std::vector<std::vector<float> > L_extra_back_shift1;
    eire::Collector_RECO *Collector_extra_backS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_up);
    Collector_extra_backS1->SetUpWeights(event_weights_syst_up); 
    L_extra_back_shift1 = Collector_extra_backS1->CollectLikelihoods((Template_background_combine_syst_up)[j]);
    collect_templates_syst_up.push_back(builder_syst_up->AddBackgroundTemplateCor((Template_background_combine_syst_up)[j],L_extra_back_shift1,atof((Background_weights_systup)[Template_background.size()+j+2].c_str()), Collector_extra_backS1->CollectEventWeights(),background_errors,0,0));
    std::vector<TH1F> test = builder_syst_up->AddBackgroundTemplateUncor((Template_background_combine_syst_up)[j],L_extra_back_shift1,atof((Background_weights_systup)[Template_background.size()+j+2].c_str()), Collector_extra_backS1->CollectEventWeights(),background_errors_systup,0,0);
    L_extra_back_shift1.clear();
    delete Collector_extra_backS1;
  }


  //when running in systematics mode
  //Collect the signal correlated samples and add them to the Builder
  for(unsigned int j = 0; j < Template_cor_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_cor;
    eire::Collector_RECO *Collector_extra_Cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_Cor->SetUpWeights(event_weights_syst_down); 
    L_extra_cor = Collector_extra_Cor->CollectLikelihoods((Template_cor_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddCorTemplate((Template_cor_syst_down)[j], L_extra_cor,atof((Sig_weights)[j].c_str()), Collector_extra_Cor->CollectEventWeights(),cor_errors_systdown,0,0));
    L_extra_cor.clear();
    delete Collector_extra_Cor;
  }

  //Collect the signal uncorrelated samples and add them to the Builder
  for(unsigned int j = 0; j < Template_uncor_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor;
    eire::Collector_RECO *Collector_extra_UnCor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_UnCor->SetUpWeights(event_weights_syst_down); 
    L_extra_uncor = Collector_extra_UnCor->CollectLikelihoods((Template_uncor_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddUncorTemplate((Template_uncor_syst_down)[j],L_extra_uncor,atof((Sig_weights)[j].c_str()), Collector_extra_UnCor->CollectEventWeights(),uncor_errors_systdown,0,0));
    L_extra_uncor.clear();
    delete Collector_extra_UnCor;
  }

  //Get TTbar other correlated and add to builder
  for(unsigned int j = 0; j < Template_background_cor_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_back_cor;
    eire::Collector_RECO *Collector_extra_back_cor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_back_cor->SetUpWeights(event_weights_syst_down); 
    L_extra_back_cor = Collector_extra_back_cor->CollectLikelihoods((Template_background_cor_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddBackgroundTemplateCor((Template_background_cor_syst_down)[j],L_extra_back_cor,atof((Background_weights_systdown)[j].c_str()), Collector_extra_back_cor->CollectEventWeights(),background_errors_systdown,0,0));
    L_extra_back_cor.clear();
    delete Collector_extra_back_cor;
  }

  //get TTbar other uncorrelated and add to builder
  for(unsigned int j = 0; j < Template_background_uncor_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_back_uncor;
    eire::Collector_RECO *Collector_extra_back_uncor = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_back_uncor->SetUpWeights(event_weights_syst_down); 
    L_extra_back_uncor = Collector_extra_back_uncor->CollectLikelihoods((Template_background_uncor_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddBackgroundTemplateUncor((Template_background_uncor_syst_down)[j],L_extra_back_uncor,atof((Background_weights_systdown)[j].c_str()), Collector_extra_back_uncor->CollectEventWeights(),background_errors_systdown,0,0));
    L_extra_back_uncor.clear();
    delete Collector_extra_back_uncor;
  }

  //get Background samples and add to builder
  //add these backgrounds to both the correlated background template and uncorrelated background template
  for(unsigned int j = 0; j < Template_background_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_back;
    eire::Collector_RECO *Collector_extra_back = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_back->SetUpWeights(event_weights_syst_down); 
    L_extra_back = Collector_extra_back->CollectLikelihoods((Template_background_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddBackgroundTemplateCor((Template_background_syst_down)[j],L_extra_back,atof((Background_weights_systdown)[j+1].c_str()), Collector_extra_back->CollectEventWeights(),background_errors,0,0));
    std::vector<TH1F> test = builder_syst_down->AddBackgroundTemplateUncor((Template_background_syst_down)[j],L_extra_back,atof((Background_weights_systdown)[j+1].c_str()), Collector_extra_back->CollectEventWeights(),background_errors_systdown,0,0);
    L_extra_back.clear();
    delete Collector_extra_back;
  }

  //Collect the signal correlated samples that you want shifted in the fit (usually e_pos)
  for(unsigned int j = 0; j < Template_cor_combine_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_cor_shift1;
    eire::Collector_RECO *Collector_extra_CorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_CorS1->SetUpWeights(event_weights_syst_down); 
    L_extra_cor_shift1 = Collector_extra_CorS1->CollectLikelihoods((Template_cor_combine_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddCorTemplate((Template_cor_combine_syst_down)[j],L_extra_cor_shift1,atof((Sig_weights)[1].c_str()), Collector_extra_CorS1->CollectEventWeights(),cor_errors_systdown,0,0));
    L_extra_cor_shift1.clear();
    delete Collector_extra_CorS1;
  }

  //Collect the signal uncorrelated samples that you want shifted in the fit (usually e_pos)
  for(unsigned int j = 0; j < Template_uncor_combine_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_uncor_shift1;
    eire::Collector_RECO *Collector_extra_UnCorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_UnCorS1->SetUpWeights(event_weights_syst_down); 
    L_extra_uncor_shift1 = Collector_extra_UnCorS1->CollectLikelihoods((Template_uncor_combine_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddUncorTemplate((Template_uncor_combine_syst_down)[j],L_extra_uncor_shift1,atof((Sig_weights)[1].c_str()), Collector_extra_UnCorS1->CollectEventWeights(),uncor_errors_systdown,0,0));
    L_extra_uncor_shift1.clear();
    delete Collector_extra_UnCorS1;
  }

  //Get TTbar other correlated and add to builder
  for(unsigned int j = 0; j < Template_background_cor_combine_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_back_cor_shift;
    eire::Collector_RECO *Collector_extra_back_corS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_back_corS1->SetUpWeights(event_weights_syst_down); 
    L_extra_back_cor_shift = Collector_extra_back_corS1->CollectLikelihoods((Template_background_cor_combine_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddBackgroundTemplateCor((Template_background_cor_combine_syst_down)[j],L_extra_back_cor_shift,atof((Background_weights_systdown)[Template_background.size()+j+1].c_str()), Collector_extra_back_corS1->CollectEventWeights(),background_errors_systdown,0,0));
    L_extra_back_cor_shift.clear();
    delete Collector_extra_back_corS1;
  }

  //get TTbar other uncorrelated and add to builder
  for(unsigned int j = 0; j < Template_background_uncor_combine_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_back_uncor_shift;
    eire::Collector_RECO *Collector_extra_back_uncorS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_back_uncorS1->SetUpWeights(event_weights_syst_down); 
    L_extra_back_uncor_shift = Collector_extra_back_uncorS1->CollectLikelihoods((Template_background_uncor_combine_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddBackgroundTemplateUncor((Template_background_uncor_combine_syst_down)[j],L_extra_back_uncor_shift,atof((Background_weights_systdown)[Template_background.size()+j+1].c_str()), Collector_extra_back_uncorS1->CollectEventWeights(),background_errors_systdown,0,0));
    L_extra_back_uncor_shift.clear();
    delete Collector_extra_back_uncorS1;
  }

  //get Background samples that should be shifted in the fit
  for(unsigned int j = 0; j < Template_background_combine_syst_down.size(); j++){
    std::vector<std::vector<float> > L_extra_back_shift1;
    eire::Collector_RECO *Collector_extra_backS1 = new eire::Collector_RECO(with_HitFit, Chicut, MassLow, MassHigh,Like_cut_up,Like_cut_down,Like_cutuncor_up,Like_cutuncor_down,PU_var_down);
    Collector_extra_backS1->SetUpWeights(event_weights_syst_down); 
    L_extra_back_shift1 = Collector_extra_backS1->CollectLikelihoods((Template_background_combine_syst_down)[j]);
    collect_templates_syst_down.push_back(builder_syst_down->AddBackgroundTemplateCor((Template_background_combine_syst_down)[j],L_extra_back_shift1,atof((Background_weights_systdown)[Template_background.size()+j+2].c_str()), Collector_extra_backS1->CollectEventWeights(),background_errors_systdown,0,0));
    std::vector<TH1F> test = builder_syst_down->AddBackgroundTemplateUncor((Template_background_combine_syst_down)[j],L_extra_back_shift1,atof((Background_weights_systdown)[Template_background.size()+j+2].c_str()), Collector_extra_backS1->CollectEventWeights(),background_errors_systdown,0,0);
    L_extra_back_shift1.clear();
    delete Collector_extra_backS1;
  }

  //Only do the Hypothesis testing when running on Data
  string channel = "mu_pos";
  eire::HypTesting *hypothesis_tester;  
  eire::HypTesting *hypothesis_tester_syst;

  //check if you are running on the positive channel (to choose the binning for the sample likelihood plots and choose the total background fraction)
  if(Template_cor.size() > 0){
    if((Template_cor)[0].find("e_pos") != string::npos){channel = "e_pos";}
    if((Template_cor)[0].find("mu_pos") != string::npos){channel = "mu_pos"; if(Template_cor_combine.size() > 0){channel = "mu_combined";}}
    if((Template_cor)[0].find("e_neg") != string::npos){channel = "e_neg";}
    if((Template_cor)[0].find("mu_neg") != string::npos){channel = "mu_neg";}
    //initialise the hypothesis tester
    if(Background_weights_systup.size() != 0){   hypothesis_tester = new eire::HypTesting(Name, channel, background_frac_systup, background_frac_systdown);}
    else{   hypothesis_tester = new eire::HypTesting(Name, channel, background_frac);}
    if(Dataset.size() != 0){
      //HYPOTHESIS TESTING SEQUENCE
      //give it the background weights
      if(Background_weights_systup.size() != 0){hypothesis_tester->SetUp(Background_weights_systup,Background_weights_systdown,Sig_weights);}
      else{hypothesis_tester->SetUp(Background_weights,Sig_weights);}
      //add the data
      hypothesis_tester->SetData(L_extra_Data, L_extra_Data_shift1);
      //give it the templates you collected from the builder (these are normalised and have correct efficiency factors applied)
      hypothesis_tester->SetTemplates(collect_templates,collect_data_templates);
      hypothesis_tester->DrawStack();
      //do the pseudo-experiments    
      hypothesis_tester->DrawSeparation();
      //do the measurement
      hypothesis_tester->DoMeasurement();
    }
  }else{
    //running in systematics mode
    if((Template_cor_syst_up)[0].find("e_pos") != string::npos){channel = "e_pos";}
    if((Template_cor_syst_up)[0].find("mu_pos") != string::npos){channel = "mu_pos"; if(Template_cor_combine_syst_up.size() > 0){channel = "mu_combined";}}
    if((Template_cor_syst_up)[0].find("e_neg") != string::npos){channel = "e_neg";}
    if((Template_cor_syst_up)[0].find("mu_neg") != string::npos){channel = "mu_neg";}
    //initialise the hypothesis tester
    if(Background_weights_systup.size() != 0){hypothesis_tester_syst = new eire::HypTesting(Name, channel, background_frac_systup, background_frac_systdown);}
    else{    hypothesis_tester_syst = new eire::HypTesting(Name, channel, background_frac);}
    if(Dataset.size() != 0){
      //HYPOTHESIS TESTING SEQUENCE
      //give it the background weights
      if(Background_weights_systup.size() != 0){hypothesis_tester_syst->SetUp(Background_weights_systup,Background_weights_systdown,Sig_weights);}
      else{      hypothesis_tester_syst->SetUp(Background_weights,Sig_weights);}
      //add the data
      hypothesis_tester_syst->SetData(L_extra_Data, L_extra_Data_shift1);
      //give it the templates you collected from the builder (these are normalised and have correct efficiency factors applied)
      hypothesis_tester_syst->SetTemplates(collect_templates_syst_up,collect_data_templates,+1);
      hypothesis_tester_syst->SetTemplates(collect_templates_syst_down,collect_data_templates,-1);
      hypothesis_tester_syst->DrawStack(true);
      //do the pseudo-experiments    
      hypothesis_tester_syst->DrawSeparation(true);
      //do the measurement
      hypothesis_tester_syst->DoMeasurement();
    }
  }

  //FITTING SEQUENCE
  //if running various mixing fractions, store the output to make the linearity plots
  eire::Linearity *LinPlotter;
  LinPlotter = new eire::Linearity(name.str());
  
  std::vector<TH1F> templates;
  std::vector<double> Results; 

  //make the plots of the input templates
  builder->DrawTemplates();
  //run the pseudo-experiments at a particular mixing fraction
  if(!RunFullExp){
    Results = builder->Mix(mixing);
  }
  else{
    Results = builder->Mix(0);
    LinPlotter->AddPointF(0, 0.,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    LinPlotter->AddPointNtt(0, (1-Wfrac)*nEvPE,(Results)[6],(Results)[7],(Results)[8],(Results)[9],(Results)[10],(Results)[11]);
    LinPlotter->AddPointNbkg(0, Wfrac*nEvPE,(Results)[12],(Results)[13],(Results)[14],(Results)[15],(Results)[16],(Results)[17]);
    Results = builder->Mix(0.25);
    LinPlotter->AddPointF(1, 0.25,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    LinPlotter->AddPointNtt(1, (1-Wfrac)*nEvPE,(Results)[6],(Results)[7],(Results)[8],(Results)[9],(Results)[10],(Results)[11]);
    LinPlotter->AddPointNbkg(1, Wfrac*nEvPE,(Results)[12],(Results)[13],(Results)[14],(Results)[15],(Results)[16],(Results)[17]);
    Results = builder->Mix(0.50);
    LinPlotter->AddPointF(2, 0.50,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    LinPlotter->AddPointNtt(2, (1-Wfrac)*nEvPE,(Results)[6],(Results)[7],(Results)[8],(Results)[9],(Results)[10],(Results)[11]);
    LinPlotter->AddPointNbkg(2, Wfrac*nEvPE,(Results)[12],(Results)[13],(Results)[14],(Results)[15],(Results)[16],(Results)[17]);
    Results = builder->Mix(0.75);
    LinPlotter->AddPointF(3, 0.75,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    LinPlotter->AddPointNtt(3, (1-Wfrac)*nEvPE,(Results)[6],(Results)[7],(Results)[8],(Results)[9],(Results)[10],(Results)[11]);
    LinPlotter->AddPointNbkg(3, Wfrac*nEvPE,(Results)[12],(Results)[13],(Results)[14],(Results)[15],(Results)[16],(Results)[17]);
    Results = builder->Mix(1.00);
    LinPlotter->AddPointF(4, 1.,(Results)[0],(Results)[1],(Results)[2],(Results)[3],(Results)[4],(Results)[5]);
    LinPlotter->AddPointNtt(4, (1-Wfrac)*nEvPE,(Results)[6],(Results)[7],(Results)[8],(Results)[9],(Results)[10],(Results)[11]);
    LinPlotter->AddPointNbkg(4, Wfrac*nEvPE,(Results)[12],(Results)[13],(Results)[14],(Results)[15],(Results)[16],(Results)[17]);
    LinPlotter->Plot();
  }

  Results.clear();
  templates.clear();

  if(config_reader){
    delete config_reader;
    config_reader = NULL;
  }
  if(builder){
    delete builder;
    builder = NULL;
  }
  if(builder_syst_up){
    delete builder_syst_up;
    builder_syst_up = NULL;
  }
  if(builder_syst_down){
    delete builder_syst_down;
    builder_syst_down = NULL;
  }
  if(LinPlotter){
    delete LinPlotter;
    LinPlotter = NULL;
  }
  if(hypothesis_tester){
    delete hypothesis_tester;
    hypothesis_tester = NULL;
  }
  if(hypothesis_tester_syst){
    delete hypothesis_tester_syst;
    hypothesis_tester_syst = NULL;
  }
  return 0;
  }
}
